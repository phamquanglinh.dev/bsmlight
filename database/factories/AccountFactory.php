<?php

namespace Database\Factories;

use App\Models\Account;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AccountFactory extends Factory
{
    public static function modifyAccount(Account $account, mixed $input): Account
    {
        $account->populate($input);

        return $account;
    }

    public function definition()
    {
        // TODO: Implement definition() method.
    }

    public static function makeAccount($attributes): Account
    {
        $account = new Account();

        $account->populate($attributes);

        $account->setIfEmpty('token', Str::random());

        $account->setIfEmpty('publisher_code', Str::random(10));

        if (Auth::check()) {
            $account->setIfEmpty('account_manager', Auth::id());

            $account->setIfEmpty('branch', Auth::user()->{'branch'});

            $account->setIfEmpty('uuid', Account::makeUUID(Auth::user()->{'branch'}));
        }

        return $account;
    }
}
