<?php

namespace Database\Factories;

use App\Models\CustomFieldStruct;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Auth;

class CustomFieldStructFactory extends Factory
{


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }

    public static function makeCustomFieldStruct(array $data = [], string $module = ""): CustomFieldStruct
    {
        $customField = new CustomFieldStruct();

        $customField->populate($data);

        $customField->setIfEmpty('field_html_type', CustomFieldStruct::HTML_TEXT_TYPE);
        $customField->setIfEmpty('is_required', 0);
        $customField->setIfEmpty('is_duplicate', 0);
        $customField->setIfEmpty('is_multiple', 0);
        $customField->setIfEmpty('default', 0);
        $customField->setAttribute('module', $module);

        if (Auth::check()) {
            $customField->setIfEmpty('branch', Auth::user()->{'branch'});
        }
        return $customField;
    }

    public static function modifyCustomFieldStruct(CustomFieldStruct $customField, array $data = []): CustomFieldStruct
    {
        $customField->populate($data);

        return $customField;
    }
}
