<?php

namespace Database\Factories;

use App\Models\AccountRelation;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class AccountRelationFactory extends Factory
{
    public static function makeAccountRelation(array $input): AccountRelation
    {
        foreach ($input as $key => $value) {
            if (!in_array($key, array_keys(AccountRelation::defineFillableField()))) {
                throw new BadRequestException("Trường $key không được phép nhập");
            }
        }

        $accountRelation = new AccountRelation();

        $accountRelation->fill($input);

        $accountRelation->setIfEmpty('color', '#007bff');

        $accountRelation->setAttribute('branch', Auth::user()->{'branch'});

        return $accountRelation;
    }

    public static function modifyAccountRelation(AccountRelation $accountRelation, array $input): AccountRelation
    {
        foreach ($input as $key => $value) {
            if (!in_array($key, array_keys(AccountRelation::defineFillableField()))) {
                throw new BadRequestException("Trường $key không được phép nhập");
            }
        }

        if (! $accountRelation->{'branch'}) {
            throw new BadRequestException('Không được phép đổi mối quan hệ mặc định');
        }

        $accountRelation->fill($input);

        return $accountRelation;
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
