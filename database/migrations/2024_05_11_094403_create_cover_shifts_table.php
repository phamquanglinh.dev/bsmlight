<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoverShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cover_shifts', function (Blueprint $table) {
            $table->id();
            $table->integer("classroom_id");
            $table->integer("teacher_id");
            $table->integer("supporter_id");
            $table->date("start_cover_date");
            $table->date("end_cover_date");
            $table->time("start");
            $table->time("end");
            $table->string('week_days');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cover_shifts');
    }
}
