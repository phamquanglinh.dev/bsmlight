<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InitTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table("user_configs")->insert([
            'user_id' => 0,
            'type' => 'payment_template',
            'branch' => '',
            'settings' => '<table style="width: 100.174%;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="vertical-align: text-top; width: 55.0577%;">
<div style="float: left; padding-right: 10px;">{{company_logo:120xauto}}:<strong>{{company_name}}</strong></div>
<p>&nbsp;</p>
<p><strong>Địa chỉ:</strong> {{company_address1}}</p>
<p><strong>Chi nh&aacute;nh:</strong> {{company_address2}}</p>
<p><strong>Email:</strong> {{company_email}} - <strong>Website:</strong> {{company_website}}</p>
<p><strong>Đt:</strong> {{company_phone1}} - {{company_phone2}}</p>
<p><strong>Mst:</strong> {{company_tax_code}} - <strong>Fax:</strong> {{company_fax}}</p>
</td>
<td style="width: 44.9437%; vertical-align: text-top;">
<p style="font-weight: bold;">Mẫu số: {{id}}</p>
<p>Số phiếu: <strong>{{code}}</strong></p>
</td>
</tr>
</tbody>
</table>
<p style="font-size: 20px; font-weight: bold; text-align: center; padding-top: 20px; text-transform: uppercase;">Phiếu thu</p>
<p style="font-weight: bold; text-align: center;">Ng&agrave;y {{day}} Th&aacute;ng {{month}} Năm {{year}}</p>
<div style="line-height: 22px;">
<p>Người nộp tiền: {{paying_customer}}</p>
<p>Người nhận tiền: {{money_receiver}}</p>
<p>Về khoản: {{content}}</p>
<p>M&ocirc; tả: {{desc}}</p>
<p>Số tiền: <strong>{{amount}}</strong></p>
<p>Bằng chữ: <strong>{{w_amount}}</strong></p>
</div>
<table style="text-align: center; height: 46px; width: 98.1691%;">
<tbody>
<tr style="height: 45px;">
<td style="font-weight: bold; width: 14.1083%;">Gi&aacute;m đốc</td>
<td style="font-weight: bold; width: 21.9943%;">Kế to&aacute;n trưởng</td>
<td style="font-weight: bold; width: 12.3238%;">Thủ quỹ</td>
<td style="font-weight: bold; width: 22.0485%;">Người nộp tiền</td>
<td style="font-weight: bold; width: 23.3168%;">Người lập phiếu</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Đ&atilde; nhận đủ số tiền (Viết bằng chữ): {{w_amount}}</p>'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
