<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogToStudylogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('studylogs', function (Blueprint $table) {
            $table->dateTime('submit_time')->nullable();
            $table->dateTime('cancel_time')->nullable();
            $table->dateTime('accept_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('studylogs', function (Blueprint $table) {
            $table->dropColumn([
                'submit_time',
                'cancel_time',
                'accept_time'
            ]);
        });
    }
}
