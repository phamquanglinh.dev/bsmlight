<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_criteria', function (Blueprint $table) {
            $table->id();
            $table->string('criteria_name');
            $table->string('criteria_code');
            $table->integer('criteria_type');
            $table->integer('is_bsm_criteria');
            $table->longText('criteria_description');
            $table->integer('unit_type');
            $table->decimal('criteria_rate');
            $table->string('branch');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_criteria');
    }
}
