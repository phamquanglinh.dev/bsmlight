<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonPlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_plants', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment("Tên giáo án");
            $table->string("title");
            $table->string("audio")->nullable();
            $table->string("video")->nullable();
            $table->string("link")->nullable();
            $table->longText("content")->nullable();
            $table->integer("created_by");
            $table->integer("status")->default(0);
            $table->integer('confirm_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_plants');
    }
}
