<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClassroomToSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('salary_sheet', 'classroom_id')) {
            Schema::table('salary_sheet', function (Blueprint $table) {
                $table->integer('classroom_id')->nullable();
            });
        }
        if (!Schema::hasColumn('salary_sheet_detail', 'classroom_id')) {
            Schema::table('salary_sheet_detail', function (Blueprint $table) {
                $table->integer('classroom_id')->nullable();
            });
        }
        if (!Schema::hasColumn('salary_snap', 'classroom_id')) {
            Schema::table('salary_snap', function (Blueprint $table) {
                $table->integer('classroom_id')->nullable();
            });
        }
        if (!Schema::hasColumn('salary_snap_detail', 'classroom_id')) {
            Schema::table('salary_snap_detail', function (Blueprint $table) {
                $table->integer('classroom_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('salary_sheet', 'classroom_id')) {
            Schema::table('salary_sheet', function (Blueprint $table) {
                $table->dropColumn('classroom_id')->nullable();
            });
        }
        if (Schema::hasColumn('salary_sheet_detail', 'classroom_id')) {
            Schema::table('salary_sheet_detail', function (Blueprint $table) {
                $table->dropColumn('classroom_id')->nullable();
            });
        }
        if (Schema::hasColumn('salary_snap', 'classroom_id')) {
            Schema::table('salary_snap', function (Blueprint $table) {
                $table->dropColumn('classroom_id')->nullable();
            });
        }
        if (Schema::hasColumn('salary_snap_detail', 'classroom_id')) {
            Schema::table('salary_snap_detail', function (Blueprint $table) {
                $table->dropColumn('classroom_id')->nullable();
            });
        }
    }
}
