<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarySheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_sheet', function (Blueprint $table) {
            $table->id();
            $table->string('sheet_name');
            $table->integer('loop_type');
            $table->integer('period_year');
            $table->string('description');
            $table->integer('salary_rate');
            $table->string('fund_percent');
            $table->longText('attributes');
            $table->string('branch');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_sheet');
    }
}
