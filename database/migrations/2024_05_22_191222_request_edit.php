<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RequestEdit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_edit', function (Blueprint $table) {
            $table->id();
            $table->integer('status');
            $table->integer('salary_id');
            $table->longText('snap_salary_rate');
            $table->longText('snap_salary_fund_percent');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_edit');
    }
}
