<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarySheetDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_sheet_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('salary_sheet_id');
            $table->string('criteria_name');
            $table->string('criteria_code');
            $table->integer('criteria_type');
            $table->integer('criteria_rate');
            $table->integer('unit_type');
            $table->longText('criteria_description');
            $table->string('branch');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_sheet_detail');
    }
}
