<?php

use App\Models\Card;
use App\Models\CardLog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCanDegToCardLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('card_logs', 'can_deg')) {
            Schema::table('card_logs', function (Blueprint $table) {
                $table->integer('can_deg')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('card_logs', function (Blueprint $table) {
            $table->dropColumn('can_deg');
        });
    }
}
