<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary', function (Blueprint $table) {
            $table->id();
            $table->string('snap_salary_uuid');
            $table->integer('user_id');
            $table->string('user_full_name');
            $table->string('user_uuid');
            $table->string('user_role');
            $table->date('period');
            $table->decimal('salary_rate');
            $table->decimal('salary_fund');
            $table->decimal('salary_actually_total');
            $table->longText('salary_comments')->nullable();
            $table->longText('salary_detail')->nullable();
            $table->longText('salary_attribute')->nullable();
            $table->integer('salary_status');
            $table->string('branch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary');
    }
}
