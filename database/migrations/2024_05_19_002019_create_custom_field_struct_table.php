<?php

use App\Helper\Struct;
use App\Models\CustomFieldStruct;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldStructTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_struct', function (Blueprint $table) {
            $table->id();
            $table->string('field_name')->nullable();
            $table->string('field_html_type')->nullable();
            $table->integer('is_required')->nullable();
            $table->integer('is_duplicate')->nullable();
            $table->integer('is_multiple')->nullable();
            $table->string('field_label')->nullable();
            $table->string('module')->nullable();
            $table->integer('default')->nullable();
            $table->string('branch')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('custom_field_struct')->insert(Struct::$accountCustomFields);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_struct');
    }
}
