<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarySnapDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_snap_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('salary_sheet_detail_id');
            $table->string('snap_criteria_name');
            $table->string('snap_criteria_code');
            $table->integer('snap_criteria_type');
            $table->string('snap_criteria_rate');
            $table->integer('user_id');
            $table->integer('salary_sheet_id');
            $table->string('snap_value');
            $table->integer('snap_unit_type');
            $table->date('period');
            $table->string('branch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_snap_detail');
    }
}
