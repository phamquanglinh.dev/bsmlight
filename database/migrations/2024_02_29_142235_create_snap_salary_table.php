<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSnapSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_snap', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('snap_salary_name');
            $table->string('snap_salary_rate');
            $table->string('snap_salary_value');
            $table->decimal('snap_salary_fund_percent');
            $table->date('period');
            $table->integer('salary_sheet_id');
            $table->integer('user_id');
            $table->string('branch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_snap');
    }
}
