<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddBranchTokenToBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches', function (Blueprint $table) {
            if (! Schema::hasColumn('branches','api-x-token')){
                $table->string('api-x-token')->default(Str::random());
            }
        });

        $branches = DB::table('branches')->get();

        foreach ($branches as $branch) {
            DB::table('branches')->where('id', $branch->{'id'})->update([
                'api-x-token' => Str::random()
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branches', function (Blueprint $table) {
            $table->dropColumn('api-x-token');
        });
    }
}
