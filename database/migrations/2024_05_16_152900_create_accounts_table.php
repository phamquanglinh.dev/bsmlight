<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('account_name')->nullable();
            $table->string('account_phone')->nullable();
            $table->string('account_email')->nullable();
            $table->string('uuid')->unique();
            $table->string('description')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('account_type')->nullable();
            $table->integer('account_source')->nullable();
            $table->integer('account_relation')->nullable();
            $table->integer('account_manager')->nullable();
            $table->string('token')->nullable()->unique();
            $table->dateTime('last_active')->nullable();
            $table->integer('ref_id')->nullable();
            $table->integer('ref_code')->nullable();
            $table->string('publisher_code')->nullable();
            $table->string('branch');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
