<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_relations', function (Blueprint $table) {
            $table->id();
            $table->string('relation_name');
            $table->string('color');
            $table->string('branch')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('account_sources', function (Blueprint $table) {
            $table->id();
            $table->string('source_name');
            $table->string('branch')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('account_types', function (Blueprint $table) {
            $table->id();
            $table->string('type_name');
            $table->string('branch')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_relations');
        Schema::dropIfExists('account_sources');
        Schema::dropIfExists('account_types');
    }
}
