<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /**
     * uuid: $salary['snap_salary_uuid'],
     * snap_salary_rate: $salary['salary_rate'],
     * snap_salary_fund_percent: $salary['salary_fund'],
     * period:  $salary['period'],
     * salary_sheet_id:  0,
     * user_id:  $salary['user_id'],
     * user: User::query()->where('id',$salary['user_id'])->firstOrFail(),
     * classroom_id:  0,
     * snap_salary_kpi:  $salary['snap_salary_kpi'],
     * fund_value:  $salary['salary_fund'],
     * total_value:  $salary['salary_actually_total'],
     * salarySnapDetails:  Collection::make($salarySnapDetail),
     * branch:  $salary['branch'],
     * snap_salary_value:  $salary['snap_salary_value']
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v2_salaries', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->decimal('snap_salary_rate');
            $table->decimal('snap_salary_fund_percent');
            $table->string('period');
            $table->integer('salary_sheet_id');
            $table->integer('user_id');
            $table->integer('classroom_id');
            $table->string('snap_salary_kpi');
            $table->string('fund_value');
            $table->string('total_value');
            $table->longText('salary_snap_details');
            $table->string('branch');
            $table->string('snap_salary_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v2_salaries');
    }
}
