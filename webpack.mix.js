const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/js');
mix.js('resources/js/notification.js', 'public/js');
mix.js('resources/js/interaction.js', 'public/js');
mix.js('resources/js/crm.js', 'public/js');
mix.js('resources/js/integration.js', 'public/js');
mix.js('resources/js/finance.js', 'public/js');
mix.js('resources/js/studylog.js', 'public/js');
mix.js('resources/js/card.js', 'public/js');
mix.js('resources/js/bcoin.js', 'public/js');
mix.js('resources/js/import.js', 'public/js');
mix.js('resources/js/report.js', 'public/js');
mix.js('resources/js/beli_chat.js', 'public/js');
