# Sử dụng PHP 8.1 làm base image
FROM php:8.1-apache

# Cài đặt các extension PHP cần thiết cho Laravel
RUN docker-php-ext-install pdo pdo_mysql

# Cài đặt Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Thiết lập thư mục làm việc
WORKDIR /var/www/html

# Copy mã nguồn Laravel vào container
COPY . /var/www/html/

# Cài đặt các dependency của Composer
RUN composer install

# Chạy các lệnh của Laravel (nếu cần)
# RUN php artisan migrate
# RUN php artisan key:generate

# Expose cổng mặc định của Apache
EXPOSE 80
