@php
    use App\Helper\CalendarViewModel;
    use App\Helper\Object\ShiftEvent;

    /**
    * @var CalendarViewModel $calendarViewModel
    * @var ShiftEvent $event
    */
    $month = $calendarViewModel->getMonthEvent();

    $weekDayOfTable = 2; // Bắt đầu từ chủ nhật
    $dayOfMonth = 1;
@endphp

@extends('layouts.app')

@section('content')
    <style>
        td {
            min-width: 12rem;
            height: 5rem;
        }
    </style>
    <div class="p-5">
        <table class="table-bordered">
            <tr>
                @while($dayOfMonth <= count($month))
                    @if($weekDayOfTable != $month["$dayOfMonth"]['week_day'])
                        <td></td>
                    @else
                        <td>
                            @include("calendar-event",['events' =>  $month["$dayOfMonth"]['events'] ?? [] ])
                        </td>
                        @php
                            $dayOfMonth++;
                        @endphp
                    @endif

                    @php
                        $weekDayOfTable++;
                        if ($weekDayOfTable > 8) {
                            $weekDayOfTable = 2;
                            echo '</tr><tr>';
                        }
                    @endphp
                @endwhile
                @while($weekDayOfTable <= 8)
                    <td></td>
                    @php
                        $weekDayOfTable++;
                    @endphp
                @endwhile
            </tr>
        </table>
    </div>
@endsection
