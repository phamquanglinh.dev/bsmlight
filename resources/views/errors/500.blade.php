@php
    if(\Illuminate\Support\Facades\Auth::check()){
        $layout = "layouts.app";

    }else{
        $layout = "layouts.auth";
    }
 @endphp
@extends($layout)

@section('content')
    <div class="misc-wrapper">
        <div class="d-flex justify-content-center mt-5">
            <div class="d-flex flex-column align-items-center">
                <h1 class="mb-2 mx-2" style="font-size: 6rem;">500</h1>
                <h4 class="mb-2">Đã xảy ra lỗi</h4>
                <p class="mb-2 mx-2">Thao tác không thành công, thử lại hoặc báo lỗi với admin</p>
                <img src="{{asset("demo/assets/img/illustrations/404.png")}}" alt="misc-error"
                     class="misc-model img-fluid z-index-1" width="780">
                <div>
                    <div>
                        <small>Lỗi đã được gửi về bộ phận kỹ thuật, chúng tôi sẽ sớm phản hồi</small>
                    </div>
                    <a href="{{url('/')}}"
                       class="w-100 btn btn-primary text-center my-4">Về trang chủ</a>
                </div>
            </div>
        </div>
    </div>
@endsection
