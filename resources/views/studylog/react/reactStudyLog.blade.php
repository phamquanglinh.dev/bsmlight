@extends("layouts.app")

@section('content')
    <div id="studylog-react" class="h-100 w-100"></div>
@endsection
@push('after_scripts')
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}"/>
    <script src="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/select2/select2.css')}}"/>
    <script src="{{asset('js/studylog.js')}}"></script>
    <script>
        // $(".selectpicker").selectpicker();
        // $(".select3").select2({
        //
        // });
    </script>
    <style>
        .select2-selection__clear{
            display: none!important;
        }
    </style>
@endpush
