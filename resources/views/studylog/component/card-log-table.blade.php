@php
    use App\Helper\Object\CardLogObject;
    use Illuminate\Support\Facades\Auth;
    use App\Models\User;
        /**
         * @var CardLogObject[] $cardLogs
         */
@endphp
<div class="small">
    <div class="row">
        <div class="col-12 h3">Thẻ học  được điểm danh</div>
        @foreach($cardLogs as $cardLog)
            @if($cardLog->getCanDeg())
                @if(Auth::user()->role != User::STUDENT_ROLE || $cardLog->getStudentUuid() == Auth::user()->{'uuid'})
                    <div class="border p-3">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="border rounded p-2">
                                    <div class="mb-2">
                                        <div class="mb-1">Thẻ học : <span
                                                class="text-white badge bg-primary h3">{{$cardLog->getUuid()}}</span>
                                        </div>
                                        <div class="mb-2 small">
                                            <span class="text-primary">Đã dùng: {{$cardLog->getAttendedDays()}}</span> |
                                            <span class="text-success">Còn lại: {{$cardLog->getCanUseDay()}}</span>
                                        </div>
                                        <div class="d-flex">
                                            <div>
                                                <img style="width: 2.5rem;height: 2.5rem" class="rounded-circle me-2"
                                                     src="{{$cardLog->getStudentAvatar()}}">
                                            </div>
                                            <div>
                                                    <?php
                                                    $englishName = '';
                                                    $englishNamePrefix = '';
                                                    ?>
                                                <div
                                                    class="fw-bold">{{$cardLog->getStudentName()}}</div>
                                                <div class="small">{{$cardLog->getStudentUuid()}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label class="switch">
                                            <input name="" disabled type="checkbox"
                                                   class="switch-input is-valid" {{$cardLog->getDay() ?'checked':null}}/>
                                            <span class="switch-toggle-slider">
                                                        <span class="switch-on"></span>
                                                        <span class="switch-off"></span>
                                                    </span>
                                            <span class="switch-label">Trừ buổi học</span>
                                        </label>
                                    </div>
                                    <div class="form-floating form-floating-outline ">
                                        <div class="p-2 h6">
                                            {{$cardLog->getStatusText()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-floating form-floating-outline">
                                                <textarea disabled id="" name="" class="h-px-200 form-control">{{$cardLog->getTeacherComment()}}
                                                </textarea>
                                    <label>Lời nhắn của giáo viên và trợ giảng cho
                                        HS/PHHS</label>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach

    </div>
    <div class="row mt-5">
        <div class="col-12 h3">Thẻ học không được điểm danh</div>
        @foreach($cardLogs as $cardLog)
            @if(! $cardLog->getCanDeg())
                @if(Auth::user()->role != User::STUDENT_ROLE || $cardLog->getStudentUuid() == Auth::user()->{'uuid'})
                    <div class="card_template col-4">
                        <div class="p-3">
                            <div class="row">
                                <div class="border rounded p-2" style="opacity: 0.6">
                                    <div class="mb-2">
                                        <div class="mb-1">Thẻ học : <span
                                                class="text-white badge bg-danger">{{$cardLog->getUuid()}}</span>
                                        </div>
                                        <div class="mb-2 small">
                                            <span
                                                class="text-primary">Đã dùng: {{$cardLog->getAttendedDays()}}</span>
                                            |
                                            <span class="text-danger">Còn lại: {{$cardLog->getCanUseDay()}}</span>
                                        </div>
                                        <div class="d-flex">
                                            <div>
                                                <img style="width: 2.5rem;height: 2.5rem" class="rounded-circle me-2"
                                                     src="{{$cardLog->getStudentAvatar()}}">
                                            </div>
                                            <div>
                                                <div
                                                    class="fw-bold">{{$cardLog->getStudentName()}}</div>
                                                <div class="small">{{$cardLog->getStudentUuid()}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    </div>

</div>
