@php
    /**
     * @var array $cardTemplate
     * @var int $cardKey
     * @var array $listCardLogStatus
     */
@endphp
<div class="card_template col-4">
    <div class="p-3">
        <div class="row">
                <div class="border rounded p-2" style="opacity: 0.6">
                    <div class="mb-2">
                        <div class="mb-1">Thẻ học : <span
                                class="text-white badge bg-danger">{{$cardTemplate['card_uuid']}}</span></div>
                        <div class="mb-2 small">
                            <span class="text-primary">Đã dùng: {{$cardTemplate['attended_days']}}</span> |
                            <span class="text-danger">Còn lại: {{$cardTemplate['can_use_day']}}</span>
                        </div>
                        <div class="d-flex">
                            <div>
                                <img style="width: 2.5rem;height: 2.5rem" class="rounded-circle me-2"
                                     src="{{$cardTemplate['student_avatar']}}">
                            </div>
                            <div>
                                <?php
                                    $englishName = $cardTemplate['english_name'] ?? '';
                                    $englishNamePrefix = $englishName ? ' - ' : '';
                                ?>
                                <div class="fw-bold">{{$cardTemplate['student_name']}} {{$englishNamePrefix . $englishName}}</div>
                                <div class="small">{{$cardTemplate['student_uuid']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
