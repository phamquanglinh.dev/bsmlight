<style>
    .chat-area {
        position: fixed;
        bottom: 3rem;
        right: 3rem;
        width: 18rem;
        z-index: 1000000;
        background: #28243d;
    }
</style>
<div class="chat-area border-primary border rounded">
{{--    <div class="open-chat-btn">--}}
{{--        <img src="https://cdn-icons-png.flaticon.com/512/9171/9171503.png" style="width: 4rem;height: 4rem" alt="">--}}
{{--    </div>--}}
    <div class="chat-screen shadow-lg rounded border-primary" style="height: 25rem" id="chat-container">
{{--        @include("google-chat.chat-list-item")--}}
{{--        @include("google-chat.chat-list-item")--}}
{{--        @include("google-chat.chat-list-item")--}}
{{--        @include("google-chat.chat-list-item")--}}
{{--        @include("google-chat.chat-list-item")--}}
        @include("google-chat.chat-screen")
    </div>
</div>

