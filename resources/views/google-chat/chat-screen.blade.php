<div class="position-relative" style="height:100%">
    <div class="d-flex justify-content-between p-2 border-bottom">
        <div class="room_label fw-bold">
            <div class="d-flex justify-content-between align-items-center">
                <img class="rounded-circle me-2"
                     src=""
                     style="width: 2rem">
                <div>Phạm Quang Linh</div>
            </div>
        </div>
        <div class="room_action d-flex justify-content-center align-items-center">
            <div><span class="mdi mdi-window-close cursor-pointer" id="chat-close"></span>
            </div>
        </div>
    </div>
    <div class="chat-content py-2 d-flex h-100" id="#19120201200000" style="flex-direction: column-reverse">
        <div class="d-flex justify-content-start align-items-start my-2">
            <div class="px-1">
                <img class="rounded" src="https://cdn.sforum.vn/sforum/wp-content/uploads/2024/01/avartar-anime-51.jpg" style="width: 2rem;height: 2rem">
            </div>
            <div class="p-1 px-2 rounded text-white w-75 small mx-1 border-primary border" title="19:00 19/5/2024">
                Thế đầu đuôi thế nào kể khúc giữa nghe coi chơi dc không
            </div>
        </div>

        <div class="d-flex justify-content-end align-items-start my-2">
            <div class="p-1 px-2 rounded bg-primary text-white w-75 small mx-1 border" title="19:00 19/5/2024">
                Chuyện dài lắm, kể đến mai mới hết được
            </div>
        </div>

        <div class="d-flex justify-content-start align-items-start my-2">
            <div class="px-1">
                <img class="rounded" src="https://cdn.sforum.vn/sforum/wp-content/uploads/2024/01/avartar-anime-51.jpg" style="width: 2rem;height: 2rem">
            </div>
            <div class="p-1 px-2 rounded text-white w-75 small mx-1 border-primary border" title="19:00 19/5/2024">
                Tôi không , vì sao ?
            </div>
        </div>

        <div class="d-flex justify-content-end align-items-start my-2">
            <div class="p-1 px-2 rounded bg-primary text-white w-75 small mx-1 border" title="19:00 19/5/2024">
                Xin chào mình tên là nai sừng tấm, tôi có cái sừng tấm to lắm, bạn có biết sừng này do đâu mà có không ?
            </div>
        </div>

        <div class="d-flex justify-content-start align-items-start my-2">
            <div class="px-1">
                <img class="rounded" src="https://cdn.sforum.vn/sforum/wp-content/uploads/2024/01/avartar-anime-51.jpg" style="width: 2rem;height: 2rem">
            </div>
            <div class="p-1 px-2 rounded text-white w-75 small mx-1 border-primary border" title="19:00 19/5/2024">
                Xin chào tôi là chú nai ngơ ngác quật chết bác thợ săn
            </div>
        </div>

    </div>
    <div class="chat-writing-box p-1 d-flex justify-content-between align-items-center">
        <textarea aria-label="none" class="shadow-lg chat-writing-input flex-grow-1 p-1 rounded border-none" rows="1"></textarea>
        <a class="chat-send-btn px-2">
            <span class="mdi mdi-send"></span>
        </a>
    </div>
</div>
<style>
    .chat-writing-box {
        position: absolute;
        bottom: 0;
        width: 100%;
    }

    .chat-writing-input:focus-visible {
        outline: none;
    }

    .chat-send-btn span:hover {
        color: #9055fd;
        transition: 0.6s;
        cursor: pointer;
    }
    .chat-content {
        height: 20rem!important;
        overflow-y: scroll;
    }
</style>

@push('after_scripts')
    <script>
        $(".chat-writing-input").each(function () {
            this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
        }).on("input", function () {
            this.style.height = 0;
            this.style.height = (this.scrollHeight) + "px";
        });
    </script>
@endpush
