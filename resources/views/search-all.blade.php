@php
    /**
     * @var array $searchResultCollection
     */
@endphp

@foreach($searchResultCollection as $searchResult)
    <div class="p-2 border mb-3">
        <a href="{{url($searchResult['url'])}}" class="linkedin text-light">
            <div class="d-flex">
                <div class="me-2 text-center">
                    @if(isset($searchResult['attributes']['avatar']['value']))
                        <img class="avatar-sm rounded-circle"
                             src="{{url($searchResult['attributes']['avatar']['value'])}}">
                    @endif
                    <div class="my-1 small bg-label-danger p-1 rounded">{{$searchResult['module']}}</div>
                </div>
                <div class="small">
                    @foreach($searchResult['attributes'] as $key => $attribute)
                        @if($key != 'avatar')
                            <div><span class="fw-bold">{{$attribute['label']}}: </span><span>{{$attribute['value']}}</span>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </a>
    </div>
@endforeach
