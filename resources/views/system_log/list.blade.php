@php use App\Models\SystemLog;use Illuminate\Support\Carbon; @endphp
@php
    /**
     * @var SystemLog[] $systemLogs
     */
@endphp
@extends("layouts.app")
@section('content')
    <div class="container-fluid">
        <div class="h3 px-5 pt-3">Log hệ thống</div>
        <div class="table-responsive text-nowrap px-5">
            <table class="table table-bordered border">
                <thead>
                <tr>
                    <th>Hành động</th>
                    <th>Người thực hiện</th>
                    <th>Đối tượng tác động</th>
                    <th>Địa chỉ IP</th>
                    <th>Thời gian</th>
                </tr>
                </thead>
                <tbody>
                @foreach($systemLogs as $systemLog)
                    <tr>
                        <td>
                            {{$systemLog->title}}
                        </td>
                        <td>
                            <a href="{{$systemLog->getUserIdUrl()}}">
                                <img style="width: 3rem" src="{{$systemLog->user?->avatar}}" alt="Avatar"
                                     class="rounded-circle"/>
                                <span>{{$systemLog->user?->name}}</span>
                            </a>
                        </td>
                        <td>
                            <a href="{{$systemLog->getUrlAttribute()}}">
                                <div>{{$systemLog->getObjectTypeLabelAttribute()}}</div>
                            </a>
                        </td>
                        <td>
                            <a href="https://whatismyipaddress.com/ip/{{$systemLog->ip_address}}">
                                <div>{{$systemLog->ip_address}}</div>
                            </a>
                        </td>
                        <td>
                            <a>
                                <div>{{Carbon::parse($systemLog->created_at)->isoFormat('DD/MM/YYYY hh:mm:ss')}}</div>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
