@php use App\Models\Salary;use Illuminate\Support\Facades\Auth; @endphp
@php
    /**
     * @var Salary $salary
    * @var \App\Models\User $user
    */
@endphp
@extends('layouts.app')
@section('content')
    <div class="content-wrapper">

        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <div class="row invoice-preview">
                <!-- Invoice -->
                <div class="col-xl-9 col-md-8 col-12 mb-md-0 mb-4">
                    <div class="card invoice-preview-card">
                        <div class="card-body">
                            <div
                                    class="d-flex justify-content-between flex-xl-row flex-md-column flex-sm-row flex-column">
                                <div class="mb-xl-0 pb-3">
                                    <div class="d-flex svg-illustration align-items-center gap-2 mb-4">
                                        <span class="h4 mb-0 app-brand-text fw-semibold">BSM</span>
                                    </div>
                                    <p class="mb-1">BSM Chi nhánh {{Auth::user()->{'branch'} }}</p>
                                    <p class="mb-1">{{\Illuminate\Support\Carbon::parse($salary['period'])->isoFormat('YYYY - MM')}}</p>
                                    {{--                                    <p class="mb-0">+1 (123) 456 7891, +44 (876) 543 2198</p>--}}
                                </div>
                                <div>
                                    <h4 class="fw-medium text-capitalize pb-1 text-nowrap">{{$salary['uuid']}}</h4>
                                    <div class="mb-1">
                                        <span>Thời gian tạo:</span>
                                        <span>{{$salary['created_at']->toDateTimeString()}}</span>
                                    </div>
                                    <div>
                                        <span>Cập nhật lần cuối:</span>
                                        <span>{{$salary['updated_at']->toDateTimeString()}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-0"/>
                        <div class="card-body">
                            <div class="d-flex justify-content-between flex-wrap">
                                <div class="my-3 me-3">
                                    <h6>Thông tin nhân sự:</h6>
                                    <p class="mb-1 fw-bold">{{$user['name']}}</p>
                                    <p class="mb-1">{{$user->getRoleLabel()}}</p>
                                    <p class="mb-1">{{$user->uuid}}</p>
                                    {{--                                    <p class="mb-1">718-986-6062</p>--}}
                                    {{--                                    <p class="mb-0">peakyFBlinders@gmail.com</p>--}}
                                </div>
                                <div class="my-3">
                                    <h6>Thông tin lớp học:</h6>
                                    <p class="mb-1 fw-bold">{{$salary->Classroom()?->first()['name']}}</p>
                                    <p class="mb-1 text-end">{{$salary->countStudyLog()}} buổi học</p>
                                    <p class="mb-1 text-end">{{$salary->countWorkingShift()}} ca học</p>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless m-0">
                                <thead class="border-top">
                                <tr>
                                    <th>Chỉ tiêu</th>
                                    <th>Chỉ số</th>
                                    <th class="text-end">Tổng</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($salary->getCriteria() as $criteria)
                                    <tr>
                                        <td class="text-nowrap text-heading">{{$criteria['label']}}</td>
                                        <td class="text-nowrap">{{number_format($criteria['rate'] == 0 ? 1 : $criteria['rate'])}}
                                            đ
                                        </td>
                                        <td class="text-end">{{number_format($criteria['total'])}} đ</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <hr class="my-0"/>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 mb-md-0 mb-3">
                                    <div>
                                        <p class="mb-2">
                                            <span class="me-1 text-heading">Ghi chú:</span>
                                            <span></span>
                                        </p>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2 justify-content-end d-flex">
                                    <div class="col-10 ">
                                        <div class="d-flex justify-content-between mb-2">
                                            <span class="">Lương theo chỉ tiêu :</span>
                                            <h6 class="mb-0 pt-1">{{number_format($salary->countCriteriaTotal())}}
                                                đ</h6>
                                        </div>
                                        <div class="d-flex justify-content-between mb-2">
                                            <span class="">Tổng lương theo giờ:</span>
                                            <h6 class="mb-0 pt-1">{{number_format($salary['total_salary'])}} đ</h6>
                                        </div>
                                        <div class="d-flex justify-content-between mb-2">
                                            <span class="">Quỹ gắn bó</span>
                                            <h6 class="mb-0 pt-1">{{number_format($salary['actually_salary'] - $salary['total_salary'])}}
                                                đ ({{$salary['fund']}} %)</h6>
                                        </div>
                                        <hr/>
                                        <div class="d-flex justify-content-between">
                                            <span class="">Thực lĩnh:</span>
                                            <h6 class="mb-0 pt-1">{{number_format($salary->getFinalSalary())}} đ</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-0"/>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
{{--                                    <span class="fw-medium">Note:</span>--}}
{{--                                    <span>It was a pleasure working with you and your team. We hope you will keep us in mind for future freelance--}}
{{--              projects. Thank You!</span>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Invoice -->

                <!-- Invoice Actions -->
                <div class="col-xl-3 col-md-4 col-12 invoice-actions">
                    <div class="card">
                        <div class="card-body">
                            <button class="btn btn-primary d-grid w-100 mb-3" data-bs-toggle="offcanvas"
                                    data-bs-target="#sendInvoiceOffcanvas">
                                <span class="d-flex align-items-center justify-content-center text-nowrap"><i
                                        class="mdi mdi-send-outline scaleX-n1-rtl me-2"></i>Gửi thông báo</span>
                            </button>
                            <button class="btn btn-outline-secondary d-grid w-100 mb-3">
                                In phiếu thẻ lương
                            </button>
                            <a class="btn btn-outline-secondary d-grid w-100 mb-3" target="_blank" href="print.html">
                                Print
                            </a>
                            <a href="edit.html" class="btn btn-outline-secondary d-grid w-100 mb-3">
                                Chỉnh sửa thẻ lương
                            </a>
                            <button class="btn btn-success d-grid w-100" data-bs-toggle="offcanvas"
                                    data-bs-target="#addPaymentOffcanvas">
                                <span class="d-flex align-items-center justify-content-center text-nowrap"><i
                                        class="mdi mdi-currency-usd me-1"></i>Chốt thẻ lương</span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /Invoice Actions -->
            </div>

            <!-- Offcanvas -->
            <!-- Send Invoice Sidebar -->
            <div class="offcanvas offcanvas-end" id="sendInvoiceOffcanvas" aria-hidden="true">
                <div class="offcanvas-header mb-3">
                    <h5 class="offcanvas-title">Send Invoice</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                </div>
                <div class="offcanvas-body flex-grow-1">
                    <form>
                        <div class="form-floating form-floating-outline mb-4">
                            <input type="text" class="form-control" id="invoice-from" value="shelbyComapny@email.com"
                                   placeholder="company@email.com"/>
                            <label for="invoice-from">From</label>
                        </div>
                        <div class="form-floating form-floating-outline mb-4">
                            <input type="text" class="form-control" id="invoice-to" value="qConsolidated@email.com"
                                   placeholder="company@email.com"/>
                            <label for="invoice-to">To</label>
                        </div>
                        <div class="form-floating form-floating-outline mb-4">
                            <input type="text" class="form-control" id="invoice-subject"
                                   value="Invoice of purchased Admin Templates" placeholder="Invoice regarding goods"/>
                            <label for="invoice-subject">Subject</label>
                        </div>
                        <div class="form-floating form-floating-outline mb-4">
        <textarea class="form-control" name="invoice-message" id="invoice-message" style="height: 190px;">Dear Queen Consolidated,
          Thank you for your business, always a pleasure to work with you!
          We have generated a new invoice in the amount of $95.59
          We would appreciate payment of this invoice by 05/11/2021</textarea>
                            <label for="invoice-message">Message</label>
                        </div>
                        <div class="mb-4">
        <span class="badge bg-label-primary rounded-pill">
          <i class="mdi mdi-link-variant mdi-14px me-1"></i>
          <span class="align-middle">Invoice Attached</span>
        </span>
                        </div>
                        <div class="mb-3 d-flex flex-wrap">
                            <button type="button" class="btn btn-primary me-3" data-bs-dismiss="offcanvas">Send</button>
                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="offcanvas">Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /Send Invoice Sidebar -->
            <!-- Add Payment Sidebar -->
            <div class="offcanvas offcanvas-end" id="addPaymentOffcanvas" aria-hidden="true">
                <div class="offcanvas-header mb-3">
                    <h5 class="offcanvas-title">Add Payment</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                </div>
                <div class="offcanvas-body flex-grow-1">
                    <div class="d-flex justify-content-between bg-lighter p-2 mb-3">
                        <p class="mb-0">Invoice Balance:</p>
                        <p class="fw-medium mb-0">$5000.00</p>
                    </div>
                    <form>
                        <div class="input-group input-group-merge mb-4">
                            <span class="input-group-text">$</span>
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="invoiceAmount" name="invoiceAmount"
                                       class="form-control invoice-amount" placeholder="100"/>
                                <label for="invoiceAmount">Payment Amount</label>
                            </div>
                        </div>
                        <div class="form-floating form-floating-outline mb-4">
                            <input id="payment-date" class="form-control invoice-date" type="text"/>
                            <label for="payment-date">Payment Date</label>
                        </div>
                        <div class="form-floating form-floating-outline mb-4">
                            <select class="form-select" id="payment-method">
                                <option value="" selected disabled>Select payment method</option>
                                <option value="Cash">Cash</option>
                                <option value="Bank Transfer">Bank Transfer</option>
                                <option value="Debit Card">Debit Card</option>
                                <option value="Credit Card">Credit Card</option>
                                <option value="Paypal">Paypal</option>
                            </select>
                            <label for="payment-method">Payment Method</label>
                        </div>
                        <div class="form-floating form-floating-outline mb-4">
                            <textarea class="form-control" id="payment-note" style="height: 62px;"></textarea>
                            <label for="payment-note">Internal Payment Note</label>
                        </div>
                        <div class="mb-3 d-flex flex-wrap">
                            <button type="button" class="btn btn-primary me-3" data-bs-dismiss="offcanvas">Send</button>
                            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="offcanvas">Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /Add Payment Sidebar -->
            <!-- /Offcanvas -->

        </div>
        <!-- / Content -->

        <!-- Footer -->
        <!-- Footer-->
        <footer class="content-footer footer bg-footer-theme">
            <div class="container-xxl">
                <div
                    class="footer-container d-flex align-items-center justify-content-between py-3 flex-md-row flex-column">
                    <div class="text-body mb-2 mb-md-0">
                        ©
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        , made with <span class="text-danger"><i class="tf-icons mdi mdi-heart"></i></span> by <a
                            href="https://themeselection.com/" target="_blank" class="footer-link fw-medium">ThemeSelection</a>
                    </div>
                    <div class="d-none d-lg-inline-block">
                        <a href="https://themeselection.com/license/" class="footer-link me-3"
                           target="_blank">License</a>
                        <a href="https://themeselection.com/" target="_blank" class="footer-link me-3">More Themes</a>
                        <a href="https://demos.themeselection.com/materio-bootstrap-html-admin-template/documentation/laravel-introduction.html"
                           target="_blank" class="footer-link me-3">Documentation</a>
                        <a href="https://themeselection.com/support/" target="_blank"
                           class="footer-link d-none d-sm-inline-block">Support</a>
                    </div>
                </div>
            </div>
        </footer>
        <!--/ Footer-->
        <!-- / Footer -->
        <div class="content-backdrop fade"></div>
    </div>
@endsection
