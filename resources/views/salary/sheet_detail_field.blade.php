@php use App\Models\SalaryCriteria; @endphp
@php
    /**
    * $criteria_name
    * $criteria_code
    * criteria_type
    * criteria_rate
    * unit_type
    * criteria_description
     * @var array $sheetDetail
     * @var int $sheetDetailKey
     */
@endphp

<tr id="{{$sheetDetailKey}}">
    <td>{{$sheetDetail['criteria_name']}}</td>
    <td>
        @if($sheetDetail['criteria_type'] == SalaryCriteria::TYPE_REWARD)
            <span class="badge rounded-pill bg-label-success me-1">Thưởng</span>
        @else
            <span class="badge rounded-pill bg-label-danger me-1">Phạt</span>
        @endif
    </td>
    <td style="max-width: 20rem" class="text-wrap">
        <span class="small">
            {{$sheetDetail['criteria_description']}}
        </span>
    </td>
    <td>
        <label>
            <input class="form-control" value="{{$sheetDetail['criteria_rate']}}" name="sheet_detail[{{$sheetDetailKey}}][criteria_rate]"/>
            <input hidden name="sheet_detail[{{$sheetDetailKey}}][id]"
                   value="{{$sheetDetail['id']}}"/>
            <input hidden name="sheet_detail[{{$sheetDetailKey}}][criteria_name]"
                   value="{{$sheetDetail['criteria_name']}}"/>
            <input hidden name="sheet_detail[{{$sheetDetailKey}}][criteria_code]"
                   value="{{$sheetDetail['criteria_code']}}"/>
            <input hidden name="sheet_detail[{{$sheetDetailKey}}][criteria_type]"
                   value="{{$sheetDetail['criteria_type']}}"/>
            <input hidden name="sheet_detail[{{$sheetDetailKey}}][unit_type]"
                   value="{{$sheetDetail['unit_type']}}"/>
            <input hidden name="sheet_detail[{{$sheetDetailKey}}][criteria_description]"
                   value="{{$sheetDetail['criteria_description']}}"/>
            @error("sheet_detail.".$sheetDetailKey.".criteria_rate")
            <div style="max-width: 25rem" class="text-wrap small text-danger mt-1">{{$message}}</div>
            @enderror
        </label>
    </td>
    <td>{{$sheetDetail['unit_type'] == SalaryCriteria::UNIT_PERCENT? '%': "Số lượng"}}</td>
    <td>
        <a onclick="removeDetail('{{$sheetDetailKey}}')" class="remove cursor-pointer">
            <span class="mdi mdi-trash-can"></span>
        </a>
    </td>
</tr>

