@php use App\Models\SalaryCriteria;use App\Models\SalarySheet;use App\Models\SalarySheetDetail;use App\Models\SalarySnap; @endphp
@php
    /**
    * @var SalarySnap[] $salarySnaps
    * @var SalarySheet $salarySheet
    * @var SalarySheetDetail $salarySheetDetail
    * @var int $periodMonth
    */
@endphp

@extends("layouts.app")
@section("content")
    <div class="container-fluid p-5">
        <div class="mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <div class="h5">
                    Phiếu Cài đặt mức lương "{{$salarySheet->sheet_name}}"
                </div>
                <div>
                    <div class="form-floating-outline form-floating">
                        <select id="period_month" class="form-control" name="period_month">
                            @for($month = 1 ;$month<=12;$month++)
                                <option value="{{$month}}" @if($periodMonth == $month) selected @endif >Tháng {{$month}} </option>
                            @endfor
                        </select>
                        <label for="period_month">Tháng</label>
                    </div>
                </div>
            </div>
            <div>Lương theo giờ : {{number_format($salarySheet->salary_rate)}} đ</div>

            <div>% Quỹ gắn bó : {{number_format($salarySheet->fund_percent)}} %</div>
            <div>
                <table class="table table-bordered my-3">
                    <thead>
                    <tr>
                        <th>Tên chỉ tiêu</th>
                        <th>Loại</th>
                        <th>Mô tả</th>
                        <th>Giá trị</th>
                        <th>Tính theo</th>
                    </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                    @foreach($salarySheet->getSalarySheetDetails()->get() as $salarySheetDetail)
                        <tr>
                            <td>
                                <div class="small">{{$salarySheetDetail->criteria_name}}</div>
                            </td>
                            <td>
                                @if($salarySheetDetail->criteria_type == SalaryCriteria::TYPE_REWARD)
                                    <div class="badge bg-label-success">Thưởng</div>
                                @else
                                    <div class="badge bg-label-danger">Phạt</div>
                                @endif
                            </td>
                            <td>
                                <div class="small">{{$salarySheetDetail->criteria_description}}</div>
                            </td>
                            <td>
                                <div class="small">{{$salarySheetDetail->criteria_rate}}</div>
                            </td>
                            <td>
                                <div
                                    class="small">{{$salarySheetDetail->unit_type == SalaryCriteria::UNIT_INTEGER ? 'Số lượng' : '%'}}</div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="mb-3 h5">Danh sách thẻ lương</div>
        <div class="table-responsive text-nowrap">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Thẻ lương</th>
                    <th>Lương theo giờ</th>
                    <th>Thưởng/Phạt</th>
                    <th>Thực tế</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                @foreach($salarySnaps as $salarySnap)
                    <tr>
                        <td>
                            <div class="">
                                <div class="text-primary"># {{$salarySnap->uuid}}</div>
                                <div class="small d-flex flex-row py-2">
                                    <div class="me-2">
                                        <img style="width: 2rem" class="rounded-circle"
                                             src="{{$salarySnap->user?->avatar}}">
                                    </div>
                                    <div>
                                        <div>{{$salarySnap->user?->name}}</div>
                                        <div class="">{{$salarySnap->user?->uuid}}</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="">{{number_format($salarySnap->snap_salary_rate)}} đ</td>
                        <td class="">{{number_format($salarySnap->snap_salary_kpi)}} đ</td>
                        <td class="">{{number_format($salarySnap->snap_salary_value)}} đ</td>
                        <td class="">
                           @if($salarySnap->is_close)
                                <div class="badge bg-label-success">Đã chốt</div>
                            @else
                                <div class="badge bg-label-danger">Chưa chốt</div>
                           @endif
                        </td>
                        <td>
                            <a href="{{url("salary-snap/show/$salarySnap->id")}}">Xem chi tiết</a>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('after_scripts')
    <script>
        $(document).ready(function () {
            console.clear()
            $("select[name=period_month]").change(function () {
                const periodMonth = $(this).val()
                console.log(periodMonth)
                const url = '{{url('/salary-snap/list')}}?salary_sheet_id={{$salarySheet->id}}&period_month=' + periodMonth

                window.location.replace(url)
            })
        })
    </script>
@endpush
