@php use App\Models\SalaryCriteria;use App\Models\SalarySnap;use App\Models\SalarySnapDetail; @endphp
@php
    /**
    * @var SalarySnap $salarySnap
    * @var SalarySnapDetail $salarySnapDetail
 * @var \App\Helper\Object\SalarySnapObject $salarySnapObject
    */
@endphp
@extends("layouts.app")
@section("content")
    <div class="container-fluid p-5">
        <div class="mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <div class="h5">
                    Phiếu lương của {{$salarySnapObject->getUser()?->name}}
                </div>
                <div>
                    <div class="form-floating-outline form-floating">
                        <select id="period_month" class="form-control" name="period_month">
                            @for($month = 1 ;$month<=12;$month++)
                                <option value="{{$month}}" @if($periodMonth == $month) selected @endif >
                                    Tháng {{$month}} </option>
                            @endfor
                        </select>
                        <label for="period_month">Tháng</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="my-3">
                        <div>Thẻ lương #{{$salarySnap->uuid}}</div>
                        <div>Lương theo giờ : {{number_format($salarySnapObject->getSnapSalaryRate())}} đ</div>
                        <div>Lương thực tế : {{number_format($salarySnapObject->getSnapSalaryValue())}} đ</div>
                        <div>Lương thưởng/phạt (KPI) : <span id="salary_kpi">{{number_format($salarySnapObject->getSnapSalaryKpi())}} đ</span>
                        </div>
                        <div>% Quỹ gắn bó (KPI) : {{number_format($salarySnapObject->getSnapSalaryFundPercent())}} %
                            = {{number_format($salarySnapObject->getFundValue())}} đ
                        </div>
                        <hr>
                        <div>Thực nhận : <span
                                    id="total" class="fw-bold text-success">{{number_format($salarySnapObject->getTotalValue())}} đ </span></div>
                    </div>
                    @if(! $close)
                        <div>
                            <a href="{{url('salary-snap/close/'.$salarySnap['id'])}}" class="btn btn-success">Chốt phiếu
                                lương</a>
                            @if($salarySnap->is_close)
                                <a href="{{url("salary-snap/show/".$salarySnap['id']."?period_month=$periodMonth&close=1")}}"
                                   class="btn btn-primary">Xem dữ liệu đã chốt</a>
                            @endif
                        </div>
                    @else
                        <div>Dữ liệu chốt vào {{$closeTime}}</div>
                        <a href="{{url("salary-snap/show/".$salarySnap['id']."?period_month=$periodMonth")}}"
                           class="btn btn-primary my-3">Xem dữ liệu thời gian thực</a>
                    @endif
                </div>

                <div class="col-md-8">
                    <div>
                        <table class="table table-bordered my-3">
                            <thead>
                            <tr>
                                <th>Tên chỉ tiêu</th>
                                <th>Loại</th>
                                <th>Tỷ lệ</th>
                                <th>Tính theo</th>
                                <th>Thực tế</th>
                                <th>Thành tiền</th>

                            </tr>
                            </thead>
                            <tbody class="table-border-bottom-0">
                            @foreach($salarySnapObject->getSalarySnapDetails() as $salarySnapDetail)
                                <tr class="small">
                                    <td>{{$salarySnapDetail->snap_criteria_name}}</td>
                                    <td>
                                        @if($salarySnapDetail->snap_criteria_type == SalaryCriteria::TYPE_REWARD)
                                            <div class="badge bg-label-success">Thưởng</div>
                                        @else
                                            <div class="badge bg-label-danger">Phạt</div>
                                        @endif
                                    </td>
                                    <td>
                                        {{number_format($salarySnapDetail->snap_criteria_rate)}}
                                    </td>
                                    <td>
                                        <div
                                                class="small">{{$salarySnapDetail->snap_unit_type == SalaryCriteria::UNIT_INTEGER ? 'Số lượng' : '%'}}</div>
                                    </td>
                                    <td>
                                        @if($salarySnapDetail->snap_criteria_code == SalaryCriteria::CRITERIA_MANUAL && !$close)
                                            <div class="manual_result">
                                                    <span
                                                            class="manual_value"> {{number_format($salarySnapDetail->snap_value)}} </span>
                                                <span class="mdi mdi-square-edit-outline"></span>
                                            </div>
                                            <div class="manual_update d-none">
                                                <div class="d-flex flex-row align-items-center">
                                                    <input type="number" style="width: 6rem" class="form-control"
                                                           id="{{$salarySnapDetail->id}}">
                                                    <div class="ms-2">
                                                        <a class="manual_update_confirm cursor-pointer">
                                                            <span class="mdi mdi-check-circle text-success"></span>
                                                        </a>
                                                        <a class="manual_update_cancel cursor-pointer">
                                                            <span class="mdi mdi-close-circle text-danger"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div> {{number_format($salarySnapDetail->snap_value)}}</div>
                                        @endif
                                    </td>
                                    <td>
                                        {{number_format($salarySnapDetail->amount)}} đ
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div>
                        @include("salary-snap.comments",['comments' => $comments,''])
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .show {
            display: block !important;
        }
    </style>
@endsection
@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        $(document).ready(function () {
            console.clear()
            $("select[name=period_month]").change(function () {
                const periodMonth = $(this).val()
                console.log(periodMonth)
                const url = '{{url('/salary-snap/show')}}?user_id={{$salarySnap->user_id}}&period_month=' + periodMonth

                window.location.replace(url)
            })

            $(".manual_result").on('click', function () {
                $(this).next().removeClass('d-none')
                $(this).addClass('d-none')
            })

            $(".manual_update_confirm").on('click', function (e) {

                const inputValue = $(this).parent().prev().val()
                const inputId = $(this).parent().prev().attr('id')
                const currentNode = $(this)
                axios.post('{{url('/salary-snap/manual/update')}}', {
                    snap_value: inputValue,
                    id: inputId,
                    _token: '{{csrf_token()}}'
                }, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).then(function (response) {
                    currentNode.parent().parent().parent().prev().find('.manual_value').text(response.data.value)
                    currentNode.parent().parent().parent().addClass('d-none')
                    currentNode.parent().parent().parent().prev().removeClass('d-none')
                    currentNode.parent().parent().parent().parent().next().text(response.data.amount);
                    $("#salary_kpi").text(response.data.salary_kpi)
                    $("#total").text(response.data.total)
                    toastr.success('', 'Cập nhật thành công');
                }).catch(function (error) {
                    toastr.error(error, 'Cập nhật thất bại vui lòng thử lại');
                });

            })

            $(".manual_update_cancel").on('click', function () {
                $(this).parent().parent().parent().addClass('d-none')
                $(this).parent().parent().parent().prev().removeClass('d-none')
            })
        })
    </script>
@endpush
