@php use App\Models\User;use Illuminate\Support\Facades\Route; @endphp
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">

    <!-- ! Hide app brand if navbar-full -->
    <div class="app-brand demo">
        <a href="{{url('/')}}" class="app-brand-link">
            <span class="app-brand-text demo menu-text fw-semibold ms-2">BSM</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="mdi menu-toggle-icon d-xl-block align-middle mdi-20px"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <li class="menu-item {{Route::current()->getName() == "index"?'active':''}}">
            <a href="{{url("/")}}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-home-outline"></i>
                <div>Bảng thống kê</div>
            </a>
        </li>
        @if(check_permission('list crm'))
            <li class="menu-item {{is_module('branch') ? ' active' :''}}">
                <a href="{{url("/accounts/list")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-account-group"></i>
                    <div>CRM</div>
                </a>
            </li>
        @endif
        @if(check_permission('list student'))
            <li class="menu-item {{is_module('student') ? ' active' :''}}">
                <a href="{{url("/student/list")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-account-group"></i>
                    <div>Học sinh</div>
                </a>
            </li>
        @endif

        @if(check_permission('list card'))
            <li class="menu-item {{is_module('card') ? ' active' :''}}">
                <a href="{{url("/card/list")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-card-bulleted"></i>
                    <div>Thẻ học</div>
                </a>
            </li>
        @endif
        <li class="menu-item {{(is_module('staff') || is_module('teacher') || is_module('supporter')) ? ' open' :''}}">
            <a data-dir="https://demos.themeselection.com/materio-bootstrap-html-laravel-admin-template/demo-1/aewsome"
               href="javascript:void(0);" class="menu-link menu-toggle waves-effect">
                <i class="menu-icon tf-icons mdi mdi-account"></i>
                <div>Nhân sự</div>
            </a>
            <ul class="menu-sub">
                @if(check_permission('list staff'))
                    <li class="menu-item">
                        <a href="{{url('staff/list')}}" class="menu-link">
                            <div>Nhân viên</div>
                        </a>
                    </li>
                @endif
                @if(check_permission('list teacher'))
                    <li class="menu-item ">
                        <a href="{{url('teacher/list')}}" class="menu-link">
                            <div>Giáo viên</div>
                        </a>
                    </li>
                @endif
                @if(check_permission('list supporter'))
                    <li class="menu-item ">
                        <a href="{{url('supporter/list')}}" class="menu-link">
                            <div>Trợ giảng</div>
                        </a>
                    </li>
                @endif
            </ul>
        </li>
{{--        #TODO Group may thang cuoi--}}
        @if(check_permission('list classroom'))
            <li class="menu-item {{is_module('classroom') ? ' active' :''}}">
                <a href="{{url("/classroom/list")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-google-classroom"></i>
                    <div>Lớp học</div>
                </a>
            </li>
        @endif
        <li class="menu-item {{Route::current()->getName() == "calendar"?'active':''}}">
            <a href="{{url("/calendar")}}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-calendar"></i>
                <div>Lịch</div>
            </a>
        </li>
        <li class="menu-item {{Route::current()->getName() == "lesson_plan"?'active':''}}">
            <a href="{{url("/lesson_plant/list")}}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-book"></i>
                <div>Giáo án</div>
            </a>
        </li>
        @if(check_permission('list studylog'))
            <li class="menu-item {{is_module('studylog') ? ' active' :''}}">
                <a href="{{url("/studylog/list")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-ballot-outline"></i>
                    <div>Điểm danh</div>
                </a>
            </li>
        @endif
        @if(check_permission('list salary'))
            <li class="menu-item {{is_module('salary') ? ' open' :''}}">
                <a data-dir="https://demos.themeselection.com/materio-bootstrap-html-laravel-admin-template/demo-1/aewsome"
                   href="javascript:void(0);" class="menu-link menu-toggle waves-effect">
                    <i class="menu-icon tf-icons mdi mdi-hand-coin"></i>
                    <div>Lương</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item ">
                        <a href="{{url('salary-criteria/list')}}" class="menu-link">
                            <div>Cài đặt thưởng phạt</div>
                        </a>
                    </li>

                    <li class="menu-item ">
                        <a href="{{url('salary-sheet/list')}}" class="menu-link">
                            <div>Cài đặt mức lương</div>
                        </a>
                    </li>
                    <li class="menu-item ">
                        <a href="{{url('salary-snap/all')}}" class="menu-link">
                            <div>Tính thẻ lương</div>
                        </a>
                    </li>
                    <li class="menu-item ">
                        <a href="{{url('salary-group/list')}}" class="menu-link">
                            <div>Chi phiếu lương</div>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        @if(\Illuminate\Support\Facades\Auth::user()->{'role'} == User::HOST_ROLE)
            <li class="menu-item {{is_module('finance') ? ' open' :''}}">
                <a data-dir="https://demos.themeselection.com/materio-bootstrap-html-laravel-admin-template/demo-1/aewsome"
                   href="javascript:void(0);" class="menu-link menu-toggle waves-effect">
                    <i class="menu-icon tf-icons mdi mdi-finance"></i>
                    <div>Tài chính</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item ">
                        <a href="{{url("finance/cashiers/list")}}" class="menu-link">
                            <div>Sổ quỹ</div>
                        </a>
                        <a href="{{url("report/cash_flow")}}" class="menu-link">
                            <div>Báo cáo tài chính</div>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        @if(Auth::user()->{'role'} ==User::HOST_ROLE)
            <li class="menu-item {{is_module('branch') ? ' active' :''}}">
                <a href="{{url("/branch/list")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-office-building-plus"></i>
                    <div>Chi nhánh</div>
                </a>
            </li>
        @endif
        @if(force_permission('list permission'))
            <li class="menu-item {{is_module('branch') ? ' active' :''}}">
                <a href="{{url("/permission/setting")}}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-account-key    "></i>
                    <div>Phân quyền</div>
                </a>
            </li>
        @endif
        @if(Auth::user()->{'role'} ==User::HOST_ROLE)
        <li class="menu-item {{is_module('salary') ? ' open' :''}}">
            <a data-dir="https://demos.themeselection.com/materio-bootstrap-html-laravel-admin-template/demo-1/aewsome"
               href="javascript:void(0);" class="menu-link menu-toggle waves-effect">
                <i class="menu-icon tf-icons mdi mdi-cogs"></i>
                <div>Cài đặt</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item ">
                    <a href="{{url("/custom_field/list")}}" class="menu-link">
                        <div>Trường tự định nghĩa</div>
                    </a>
                </li>

                <li class="menu-item ">
                    <a href="{{url("/integration/custom_field#account")}}" class="menu-link">
                        <div>Cấu hình dữ liệu</div>
                    </a>
                </li>
                <li class="menu-item ">
                    <a href="{{url("/system_log/list")}}" class="menu-link">
                        <div>Log hệ thống</div>
                    </a>
                </li>
            </ul>
        </li>
        @endif
    </ul>
</aside>
