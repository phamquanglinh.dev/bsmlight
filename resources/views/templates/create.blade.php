@php
    /**
     * @var string $init
     */
@endphp
    <!DOCTYPE html>
<html>
<head>
    <script src="https://cdn.tiny.cloud/1/v9a3hkc5xsw542n1csexyl6eaveglb8el5zminkjlklbn3v4/tinymce/7/tinymce.min.js"
            referrerpolicy="origin"></script>
</head>
<body>
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
        toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
    });
</script>
<div style="display: flex">
    <div class="wrapper">
        <form id="form" action="{{url("/templates/store")}}" method="post">
            @csrf
            <div style="margin-bottom: 10px">
                <input value="Bản in phiếu thu học phí" name="name" placeholder="Tên bản in"
                       style="padding: 10px;width: 80%">
            </div>
            <input name="content" id="content" hidden="">
        </form>
        <textarea id="mytextarea">
            {!! $init !!}
        </textarea>
        <button onclick="getContent()" class="btn">Tạo bản in</button>
    </div>
    <div style="width: 25%;padding: 10px">
        <div>
            <p>Huớng dẫn : </p>
            <p>Chỉnh sửa thông tin công ty, chi nhánh, sdt trực tiếp trên bản in</p>
            <p>Các giá trị nằm trong dấu @{{}} sẽ tự động thay thế khi tiến hành in</p>
        </div>
        <div>Các biến hỗ trợ (có thể copy và dán vào trong bản in)</div>
        <div>
            <p><span>{{id}}</span> id của mẫu</p>
            <p><span>@{{code}}</span> mã giao dịch</p>
            <p><span>@{{day}}</span> ngày</p>
            <p><span>@{{month}}</span> tháng</p>
            <p><span>@{{year}}</span> name</p>
            <p><span>@{{customer_name}}</span> tên người nộp tiền</p>
            <p><span>@{{staff_name}}</span> tên người thu tiền</p>
            <p><span>@{{desc}}</span> ghi chú</p>
            <p><span>@{{amount}}</span> số tiền</p>
            <p><span>@{{w_amount}}</span> số tiền bằng chữ</p>
        </div>
    </div>
</div>
</body>
<style>
    .wrapper {
        width: 75%;
        min-height: 1000px;
    }

    .tox-tinymce {
        min-height: 600px;
    }

    .btn {
        border: none;
        background: #0d6efd;
        color: white;
        padding: 10px;
        margin-top: 10px;
        cursor: pointer;
    }

    span {
        color: red;
        font-weight: bold;
        cursor: pointer;
    }
</style>

<script>
    function getContent() {
        document.getElementById('content').value = tinymce.get('mytextarea').getContent()
        const form = document.getElementById('form')
        form.submit()
    }

    document.addEventListener('DOMContentLoaded', (event) => {
        // Tìm tất cả các phần tử span
        var spanElements = document.getElementsByTagName('span');

        // Lặp qua từng phần tử span và gắn sự kiện click
        for (var i = 0; i < spanElements.length; i++) {
            spanElements[i].addEventListener('click', function () {
                // Lấy nội dung văn bản từ phần tử span được nhấp vào
                var textToCopy = this.innerText;

                // Tạo một thẻ textarea tạm thời để sao chép văn bản
                var tempInput = document.createElement('textarea');
                tempInput.value = textToCopy;
                document.body.appendChild(tempInput);

                // Chọn và sao chép văn bản
                tempInput.select();
                tempInput.setSelectionRange(0, 99999); // Dành cho các thiết bị di động

                // Sao chép văn bản vào clipboard
                document.execCommand('copy');

                // Xóa thẻ textarea tạm thời
                document.body.removeChild(tempInput);

                // Thông báo cho người dùng rằng văn bản đã được sao chép
                alert('Đã copy biến ' + textToCopy + ', dán vào nơi thích hợp');
            });
        }
    });

</script>
</html>
