@php
    use App\Models\User;
    use Illuminate\Support\Facades\Auth;
@endphp
@php
    /**
     * @var User $user
     */
@endphp
@php
    use  App\Models\UserConfig;
    $userConfig = UserConfig::query()->where("user_id",$user->id)->first();
    $checkGoogle = UserConfig::query()->where("user_id",Auth::id())->where("type","google")->exists();
    $checkFaceBook = UserConfig::query()->where("type","facebook")->exists();

@endphp
<div class="card mb-4 position-sticky">
    <div class="card-body">
        <div class="user-avatar-section">
            <div class=" d-flex align-items-center flex-column">
                <a href="{{route("user.edit",$user->id)}}" class="" data-toggle="tooltip"
                   title="Chỉnh sửa"><span
                            class="btn  mdi mdi-square-edit-outline"></span></a>
                <img class="img-fluid rounded mb-3 mt-4" src="{{$user->avatar}}" height="120" width="120"
                     alt="User avatar">
                <div class="user-info text-center">
                    <h4>{{$user->name}}</h4>
                    <div class="row">
                        @if($checkFaceBook)
                            <div class="mb-3 col">
                                <div class="btn btn-icon btn-lg rounded-pill btn-text-facebook" data-toggle="tooltip"
                                     title="Đã kết nối với Facebook">
                                    <i class="tf-icons mdi mdi-24px mdi-facebook"></i>
                                </div>
                            </div>
                        @endif
                        @if($checkGoogle)
                            <div class="col">
                                <div class="btn btn-icon btn-lg rounded-pill btn-text-google-plus" data-toggle="tooltip"
                                     title="Đã kết nối với Google">
                                    <i class="tf-icons mdi mdi-24px mdi-google"></i>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <h5 class="pb-3 border-bottom mb-3"></h5>
        <div class="info-container">
            <ul class="list-unstyled mb-4">
                <li class="mb-3">
                    <span class="fw-medium text-heading me-2">Username:</span>
                    <span>{{$user->uuid}}</span>
                </li>
                <li class="mb-3">
                    <span class="fw-medium text-heading me-2">Email:</span>
                    <span>{{$user->email}}</span>
                </li>
            </ul>
            <div class="d-flex justify-content-center mb-3">
                <a href="{{route('logout')}}" class="btn btn-primary me-3 waves-effect waves-light">Đăng xuất</a>
            </div>

        </div>
    </div>
</div>
@push("after_scripts")
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endpush
