<!DOCTYPE html>

<html lang="en" class="dark-style layout-compact layout-navbar-fixed layout-menu-fixed"
      dir="ltr" data-theme="theme-default" data-assets-path="{{url('/demo/assets/')}}"
      data-base-url="https://demos.themeselection.com/materio-bootstrap-html-laravel-admin-template/demo-1"
      data-framework="laravel" data-template="vertical-menu-theme-default-dark">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta name="csrf_token" content="{{csrf_token()}}"/>
@include("layouts.inc.header")
<body>
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        @include("components.menu")
        <!-- Layout page -->
        <div class="layout-page">
            @include("components.navbar")
            <!-- Content wrapper -->
            <div class="content-wrapper">
                @yield('content')
                @include("beli-chat")
                <div class="content-backdrop fade"></div>
            </div>
        </div>
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
    <!-- Drag Target Area To SlideIn Menu On Small Screens -->
    <div class="drag-target"></div>
</div>
<!-- / Layout wrapper -->
<!--/ Layout Content -->
@include("notification-list")
{{--@include('layouts.inc.fb-chat')--}}
{{--@include("google-chat.chat-container")--}}
@include("layouts.inc.footer")
@stack("after_scripts")
@if(session('success'))
    <script>
        toastr.options = {
            closeButton: true,
            progressBar: true,
            positionClass: 'toast-top-right',
            timeOut: 2000
        };
        toastr.success('{{session('success')}}', 'Thành công');
    </script>
@endif

@if(session('error'))
    <script>
        toastr.options = {
            closeButton: true,
            progressBar: true,
            positionClass: 'toast-top-center',
            timeOut: 2000
        };
        toastr.error('{{session('error')}}', 'Thất bại');
    </script>
@endif

@include("layouts.inc.notification")
@include("beli-chat")

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
@if(env('debug') !== false)
    <script>
        $(document).ready(function () {
            accessSystemLog()
        })

        function accessSystemLog() {
            axios.post("{{url('system_log/access')}}", {
                    _token: '{{csrf_token()}}'
                }, {
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                },
            ).then((response) => {
                console.log('Truy cập BSM thành công !')
                localStorage.setItem('has_connected_bsm', 'bsm')
            })
        }
    </script>
@endif
{{--<script>!function (s, u, b, i, z) {--}}
{{--        var o, t, r, y;--}}
{{--        s[i] || (s._sbzaccid = z, s[i] = function () {--}}
{{--            s[i].q.push(arguments)--}}
{{--        }, s[i].q = [], s[i]("setAccount", z), r = ["widget.subiz.net", "storage.googleapis" + (t = ".com"), "app.sbz.workers.dev", i + "a" + (o = function (k, t) {--}}
{{--            var n = t <= 6 ? 5 : o(k, t - 1) + o(k, t - 3);--}}
{{--            return k !== t ? n : n.toString(32)--}}
{{--        })(20, 20) + t, i + "b" + o(30, 30) + t, i + "c" + o(40, 40) + t], (y = function (k) {--}}
{{--            var t, n;--}}
{{--            s._subiz_init_2094850928430 || r[k] && (t = u.createElement(b), n = u.getElementsByTagName(b)[0], t.async = 1, t.src = "https://" + r[k] + "/sbz/app.js?accid=" + z, n.parentNode.insertBefore(t, n), setTimeout(y, 2e3, k + 1))--}}
{{--        })(0))--}}
{{--    }(window, document, "script", "subiz", "acsamwwtygchxcimpcmy")</script>--}}

</body>
</html>
