@php
    /**
     * @var array $importAbilities
     */
@endphp
<div class="fw-bold mb-3">Đồng bộ thông tin chi nhánh của bạn: </div>
<div class="mb-3">
    @if($importAbilities['staff'] == 1)
        <a href="{{url("/staff/import")}}">
            <div class="cursor-pointer p-2 rounded shadow bg-label-primary">
                <div class="d-flex justify-content-between">
                    <div>
                        <div class="fw-bold">Import Nhân viên</div>
                        <div class="mt-1 small">Để trống MÃ NHÂN VIÊN nếu muốn tạo mới, điền mã trợ giảng nếu muốn cập nhật thông tin nhân viên</div>
                    </div>
                    <div class="">
                        <span class="mdi mdi-step-forward"></span>
                    </div>
                </div>
            </div>
        </a>
    @else
        <div class="p-2 rounded shadow bg-label-success">
            <div class="d-flex justify-content-between">
                <div class="fw-bold">Import Nhân viên</div>
                <div class="">
                    <span class="mdi mdi-check-circle"></span>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="mb-3">
    @if($importAbilities['teacher'] == 1)
        <a href="{{url("/teacher/import")}}">
            <div class="cursor-pointer p-2 rounded shadow bg-label-primary">
                <div class="d-flex justify-content-between">
                    <div>
                        <div class="fw-bold">Import Giáo viên</div>
                        <div class="mt-1 small">Để trống MÃ GIÁO VIÊN nếu muốn tạo mới, điền mã trợ giảng nếu muốn cập nhật thông tin giáo viên</div>
                    </div>
                    <div class="">
                        <span class="mdi mdi-step-forward"></span>
                    </div>
                </div>
            </div>
        </a>
    @else
        <div class="p-2 rounded shadow bg-label-success">
            <div class="d-flex justify-content-between">
                <div class="fw-bold">Import Giáo viên</div>
                <div class="">
                    <span class="mdi mdi-check-circle"></span>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="mb-3">
    @if($importAbilities['supporter'] == 1)
        <a href="{{url("/supporter/import")}}">
            <div class="cursor-pointer p-2 rounded shadow bg-label-primary">
                <div class="d-flex justify-content-between">
                    <div>
                        <div class="fw-bold">Import Trợ giảng</div>
                        <div class="mt-1 small">Để trống MÃ TRỢ GIẢNG nếu muốn tạo mới, điền mã trợ giảng nếu muốn cập nhật thông tin trợ giảng</div>
                    </div>
                    <div class="">
                        <span class="mdi mdi-step-forward"></span>
                    </div>
                </div>
            </div>
        </a>
    @else
        <div class="p-2 rounded shadow bg-label-success">
            <div class="d-flex justify-content-between">
                <div class="fw-bold">Import Trợ giảng</div>
                <div class="">
                    <span class="mdi mdi-check-circle"></span>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="mb-3">
    @if($importAbilities['classroom'] == 1)
        <a href="{{url("/classroom/import")}}">
            <div class="cursor-pointer p-2 rounded shadow bg-label-primary">
                <div class="d-flex justify-content-between">
                    <div>
                        <div class="fw-bold">Import Lớp học</div>
                        <div class="mt-1 small">Để trống MÃ LỚP HỌC nếu muốn tạo mới, điền mã lớp học nếu muốn cập nhật thông tin lớp học</div>
                        <div class="mt-1 small">Nếu muốn lớp có nhân viên phụ trách, hãy điền thêm mã nhân viên</div>
                        <div class="mt-1 small">Bắt buộc điền mã giáo viên, có thể điền mã trợ giảng trong ca học</div>
                        <div class="mt-1 small">Thứ trong tuần là từ 2 đến 8 (CN)</div>
                    </div>
                    <div class="">
                        <span class="mdi mdi-step-forward"></span>
                    </div>
                </div>
            </div>
        </a>
    @elseif($importAbilities['classroom'] == 2)
        <div class="p-2 rounded shadow bg-label-secondary ">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <div class="fw-bold mb-1">Import Lớp học</div>
                    <div class="small">Yêu cầu : Import trợ giảng, giáo viên, nhân viên</div>
                </div>
                <div class="">
                    <span class="mdi mdi-block-helper"></span>
                </div>
            </div>
        </div>
    @else
        <div class="p-2 rounded shadow bg-label-success">
            <div class="d-flex justify-content-between">
                <div class="fw-bold">Import lớp học</div>
                <div class="">
                    <span class="mdi mdi-check-circle"></span>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="mb-3">
    @if($importAbilities['student'] == 1)
        <a href="{{url("/student/import")}}">
            <div class="cursor-pointer p-2 rounded shadow bg-label-primary">
                <div class="d-flex justify-content-between">
                    <div>
                        <div class="fw-bold">Import học sinh</div>
                        <div class="mt-1 small">Để trống MÃ HỌC SINH nếu muốn tạo mới, điền mã học sinh nếu muốn cập nhật thông tin học sinh</div>
                        <div class="mt-1 small">Để trống MÃ THẺ HỌC nếu muốn tạo mới, điền mã thẻ học nếu muốn cập nhật thông tin thẻ học</div>
                        <div class="mt-1 small">Nếu muốn gán lớp cho thẻ học, hãy điền MÃ LỚP HỌC</div>
                        <div class="mt-1 small">LƯU Ý : Nếu cột SỐ TIỀN ĐÃ ĐÓNG có giá trị, sẽ ghi nhận giá trị vào thẻ học mà không tạo giao dịch, khi cập nhật sẽ cộng dồn</div>
                        <div class="mt-1 small">Tất cả các giá trị ngày đều là ngày-tháng-năm (VD 01-01-2024)</div>
                    </div>
                    <div class="">
                        <span class="mdi mdi-step-forward"></span>
                    </div>
                </div>
            </div>
        </a>
    @elseif($importAbilities['student'] == 2)
        <div class="p-2 rounded shadow bg-label-secondary ">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <div class="fw-bold mb-1">Import học sinh</div>
                    <div class="small">Yêu cầu : Import lớp học</div>
                </div>
                <div class="">
                    <span class="mdi mdi-block-helper"></span>
                </div>
            </div>
        </div>
    @else
        <div class="p-2 rounded shadow bg-label-success">
            <div class="d-flex justify-content-between">
                <div class="fw-bold">Import học sinh</div>
                <div class="">
                    <span class="mdi mdi-check-circle"></span>
                </div>
            </div>
        </div>
    @endif
</div>

