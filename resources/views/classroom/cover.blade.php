@php use App\Helper\Fields;use App\Models\Classroom; @endphp
@php
    /**
     * @var Classroom $classroom
 * @var array $covers
 * @var array $listUsers
     */
    $key = -1;
@endphp
@extends("layouts.app")
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <form action="{{url('classroom/cover/'.$classroom['id'])}}" method="POST">
            @csrf
            <div class="h3">Cover "{{$classroom['name']}}"</div>
            @foreach($covers as $key => $cover)
                <div class="row">
                    <div class="col-5">
                        @include("fields.select",[
                'field' => new Fields([
                    'name' => 'user_id['.$key.']',
                    'type' => 'select',
                    'options' => $listUsers,
                    'label' => 'Nhân sự cover',
                    'value' => $cover['user_id']
               ])
            ])
                    </div>
                    <div class="col-5">
                        @include("fields.datetime",[
                   'field' => new Fields([
                    'name' => 'due['.$key.']',
                    'type' => 'datetime',
                    'options' => $listUsers,
                    'label' => 'Kết thúc',
                    'value' => $cover['due']
               ])
            ])
                    </div>
                    <a class="col-1 text-danger h2 cursor-pointer remove">
                        <span class="mdi mdi-trash-can"></span>
                    </a>
                </div>
            @endforeach
            <div class="row">
                <div class="col-5">
                    @include("fields.select",[
            'field' => new Fields([
                'name' => 'user_id['.($key+1).']',
                'type' => 'select',
                'options' => $listUsers,
                'label' => 'Nhân sự',
                'nullable' => true
           ])
        ])
                </div>
                <div class="col-5">
                    @include("fields.datetime",[
               'field' => new Fields([
                'name' => 'due['.($key+1).']',
                'type' => 'datetime',
                'options' => $listUsers,
                'label' => 'Ngày kết thúc',
           ])
        ])
                </div>
            </div>
            <button class="btn btn-primary">Cập nhật</button>
        </form>
    </div>
@endsection

@push('after_scripts')
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/select2/select2.css')}}"/>
    <script src="{{asset('/demo/assets/vendor/libs/select2/select2.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}"/>
    <script src="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
    <script>
        $(".select2").select2();
        $(".selectpicker").selectpicker();
    </script>
    <script>
        $(window).ready(function (){
            $(".remove").click(function (){
                $(this).parent().remove()
            })
        })
    </script>
@endpush
