@php use App\Helper\CrudBag;use App\Models\Home; @endphp
@php
    /**
    * @var CrudBag $crudBag
    * @var Home $home
    */
    $home = $crudBag->getParam('Home');
@endphp
<div class="d-flex">
    @foreach($home->Students()->get() as $student)
        <div class="me-2 bg-label-success p-2 rounded">
            <span>
                <img style="width: 1.2rem;height: 1.2rem" src="{{$student->{'avatar'} }}">
            </span>
            <span>{{$student->{'name'} }}</span>
            <span onclick="unlink({{$student['id']}})" class="mdi mdi-link-off text-danger cursor-pointer"></span>
        </div>
    @endforeach
</div>
<script>
    function unlink(id) {
        const result = confirm('Sẽ không thể chuyển nhanh tài khoản của học sinh này với các tài khoản trong nhóm này nữa \n Xác nhận gỡ?')

        if (result) {
            window.location.href = window.location.origin + "/student/unlink/" + id
        }
    }
</script>

