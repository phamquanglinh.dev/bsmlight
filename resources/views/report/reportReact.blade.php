@extends("layouts.app")

@section('content')
    <div id="report-react" class="h-100 w-100"></div>
@endsection
@push('after_scripts')
    <script src="{{asset('js/report.js')}}"></script>
@endpush
