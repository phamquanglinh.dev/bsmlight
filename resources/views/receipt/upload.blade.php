@extends("layouts.app")
@section("content")
    <div class="container-fluid p-5">
        <div class="col-10">
            <form enctype="multipart/form-data" action="{{url("document/receipt/upload")}}" method="post">
                <div class="row">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-floating-outline">
                            <label for="name">Tên mẫu</label>
                            <input class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating-outline">
                            <label for="name">Upload File ZIP</label>
                            <input class="form-control" type="file" name="file" placeholder="Upload">
                        </div>
                    </div>
                </div>
                <button class="my-2 btn btn-primary">Chuyển đổi</button>
            </form>
        </div>
    </div>
@endsection
