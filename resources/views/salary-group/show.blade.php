@php use App\Models\Salary;use App\Models\SalaryGroup; @endphp
@php
    /**
     * @var SalaryGroup $salaryGroup
     */
    /**
* @var Salary $salary
 */
@endphp
@extends('layouts.app')

@section('content')
    <div class="container-fluid p-5">
        <div class="mb-3">
            <div class="d-flex justify-content-between align-items-center">
                <div class="h5">
                    Phiếu lương của {{$salaryGroup->getLabel()}}
                    <span
                            class="text-white p-2 rounded small {{SalaryGroup::bgStatusLabel()[$salaryGroup->getAttribute('status')]}}">
                        {{SalaryGroup::statusLabel()[$salaryGroup->getAttribute('status')]}}
                    </span>
                </div>
            </div>
        </div>

        <div class="my-3">
            <div>
                Tổng số tiền <b class="text-primary">cần</b> thanh toán: {{ number_format($salaryGroup->{'total_amount'})  }} đ
            </div>
            <div>
                Tổng số tiền <b class="text-success">đã</b> thanh toán: {{ number_format($salaryGroup->{'total_paid'})  }} đ
            </div>
            <div>
                Tổng số tiền <b class="text-warning">chưa</b> thanh toán: {{ number_format($salaryGroup->{'total_unpaid'})  }} đ
            </div>

        </div>

        <div class="table-responsive text-nowrap small">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Thẻ lương</th>
                    <th>Lớp</th>
                    <th>Lương theo giờ</th>
                    <th>Lương theo giờ dạy</th>
                    <th>Thưởng KPI</th>
                    <th>Quỹ gắn bỏ</th>
                    <th>Tổng</th>
                    <th>Đã chi</th>
                    <th>Còn lại</th>
                </tr>
                </thead>
                <tbody>
                @foreach($salaryGroup->Salaries()->get() as $salary)
                    <tr>
                        <td>
                            <div class="fw-bold ">{{$salary->uuid}}</div>
                            <div
                                    class="{{$salary->backgroundStatus()}} rounded-pill mt-1 text-center">{{$salary->status()}}</div>
                            @if($salary->getUnpaidAttribute() > 0 && $salary->getAttribute('status') != -1)
                                <div class="row">
                                    <div class="mt-2 col-md-6 col-12">
                                        <a href="{{url("salary-group/paid/{$salary->salary_group_id}?salary_id={$salary->id}")}}">Chi
                                            lương</a>
                                    </div>
                                    <div class="mt-2 col-md-6 col-12">
                                        <a class="text-danger" href="{{url("salary-group/cancel/{$salary->id}")}}">Hủy
                                            chi lương</a>
                                    </div>
                                </div>
                            @endif
                            @if($salary->getAttribute('status') == -1)
                                <div class="mt-2">
                                    "{{$salary->getAttribute('refund_reason')}}"
                                </div>
                            @endif
                        </td>
                        <td>
                            <div class="d-flex">
                                <div>
                                    <img class="avatar rounded-circle"
                                         src="{{$salary->getClassroomEntityAttribute()['avatar']}}">
                                </div>
                                <div class="ms-2">
                                    <div>{{$salary->getClassroomEntityAttribute()['name']}}</div>
                                    <div>{{$salary->getClassroomEntityAttribute()['uuid']}}</div>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">{{number_format($salary->snap_salary_rate)}} đ</td>
                        <td class="text-center">{{number_format($salary->snap_salary_value)}} đ</td>
                        <td class="text-center">{{number_format($salary->snap_salary_kpi)}} đ</td>
                        <td class="text-center">{{number_format($salary->fund_value)}} đ</td>
                        <td class="text-center">{{number_format($salary->total_value)}} đ</td>
                        <td class="text-center">{{number_format($salary->getPaidAttribute())}} đ</td>
                        <td class="text-center">{{$salary->isCancelled()?0:number_format($salary->getUnpaidAttribute())}}
                            đ
                        </td>
                    </tr>
                @endforeach
                {{--                <tr>--}}
                {{--                    <td>--}}
                {{--                        <i class="ri-suitcase-2-line ri-20px text-danger me-4"></i><span class="fw-medium">Tours Project</span>--}}
                {{--                    </td>--}}
                {{--                    <td>Albert Cook</td>--}}
                {{--                    <td>--}}
                {{--                        <ul class="list-unstyled users-list m-0 avatar-group d-flex align-items-center">--}}
                {{--                            <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"--}}
                {{--                                class="avatar avatar-xs pull-up" title="Lilian Fuller">--}}
                {{--                                <img src="../../assets/img/avatars/5.png" alt="Avatar" class="rounded-circle" />--}}
                {{--                            </li>--}}
                {{--                            <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"--}}
                {{--                                class="avatar avatar-xs pull-up" title="Sophia Wilkerson">--}}
                {{--                                <img src="../../assets/img/avatars/6.png" alt="Avatar" class="rounded-circle" />--}}
                {{--                            </li>--}}
                {{--                            <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"--}}
                {{--                                class="avatar avatar-xs pull-up" title="Christina Parker">--}}
                {{--                                <img src="../../assets/img/avatars/7.png" alt="Avatar" class="rounded-circle" />--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </td>--}}
                {{--                    <td><span class="badge rounded-pill bg-label-primary me-1">Active</span></td>--}}
                {{--                    <td>--}}
                {{--                        <div class="dropdown">--}}
                {{--                            <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">--}}
                {{--                                <i class="ri-more-2-line"></i>--}}
                {{--                            </button>--}}
                {{--                            <div class="dropdown-menu">--}}
                {{--                                <a class="dropdown-item" href="javascript:void(0);"><i class="ri-pencil-line me-1"></i> Edit</a>--}}
                {{--                                <a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-7-line me-1"></i>--}}
                {{--                                    Delete</a>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </td>--}}
                {{--                </tr>--}}
                </tbody>
            </table>
        </div>
    </div>
@endsection
