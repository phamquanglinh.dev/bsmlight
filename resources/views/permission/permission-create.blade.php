@php use App\Helper\Fields; @endphp
@php
    /**
   * @var array $modules,
    * @var Fields $field
    * @var Fields $userSelectField
   */
@endphp

@extends('layouts.app')
@section('content')
    <div class="p-3 col-10">
        <div class="mb-5 text-danger small">
            <div>Thêm vai trò cho các nhân sự</div>
            <div>Khi gắn vai trò này cho nhân sự, nhân sự sẽ có những quyền mặc định + những quyền tự định nghĩa</div>
        </div>

        @include("fields.select", ['field' => $field])

        <form action="{{url('role/store')}}" method="POST">
            @csrf
            <div class="form-floating form-floating-outline mb-3">
                <input type="text" name="name" class="form-control">
                <label>Tên vai trò</label>
            </div>
            <div>
                @include("fields.select-multiple", ['field' => $userSelectField])
            </div>
            @foreach($modules as $module)
                <div class="text-uppercase fw-bold">{{$module['label']}}</div>
                <div class="row m-0 py-3">
                    @foreach($module['permissions'] as $permission)
                        <div class="col-md-6 col-12">
                            <div class="form-check form-check-success">
                                <input id="{{$permission['key']}}" class="form-check-input permission-input"
                                       name="permissions[{{$permission['key']}}]"
                                       type="checkbox"/>
                                <label class="form-check-label" for="{{$permission['key']}}">{{$permission['name']}}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
            <button class="btn btn-primary mb-2 d-grid my-2">Thêm mới</button>
        </form>
    </div>
@endsection
@push('after_scripts')
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/select2/select2.css')}}"/>
    <script src="{{asset('/demo/assets/vendor/libs/select2/select2.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}"/>
    <script src="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
    <script>
        $(".select2").select2();
        $(".selectpicker").selectpicker();
    </script>

    <script>
        $("select[name=roles]").change(function (e) {
            console.clear()
            const select_role = $(e.currentTarget).val()
            $.ajax({
                url: "{{ url('static/role') }}",
                method: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    role: select_role,
                },
                success: function (response) {
                    // Xử lý phản hồi thành công ở đây
                    const permissionsData = response.permissions
                    $(".permission-input").prop('checked', false);
                    console.log(permissionsData)
                    permissionsData.forEach(function (permission) {
                        console.log(permission)
                        $("input[name='permissions[" + permission + "]']").prop('checked', true);
                    })
                },
                error: function (xhr, status, error) {
                    // Xử lý lỗi ở đây
                    console.error(xhr.responseText);
                }
            });
        })

    </script>

@endpush
