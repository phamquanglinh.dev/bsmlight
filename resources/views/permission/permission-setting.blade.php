@php use App\Helper\Object\RoleSettingObject;use App\Models\Role; @endphp
@php
    /**
   * @var array $modules
    * @var RoleSettingObject $roleSetting
   */
    $roles = $roleSetting->getRoles();
@endphp
@extends("layouts.app")
@section('content')
    <div class="container-fluid">
        <div class="p-3">
            <a href="{{url('role/create')}}" class="btn btn-primary" >Thêm vai trò
            </a>
        </div>

        <div class="p-3">
            <div class="row g-4">
                @foreach($roles as $role)
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between mb-2">
                                    <p>Có {{count($role->getUsers())}} người dùng</p>
                                    <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                        @foreach($role->getUsers() as $index => $user)
                                            @if($index < 5)
                                                <li data-bs-toggle="tooltip" data-popup="tooltip-custom"
                                                    data-bs-placement="top"
                                                    class="avatar pull-up" aria-label="Vinnie Mostowy"
                                                    data-bs-original-title="{{$user->getName()}}">
                                                    <img class="rounded-circle" src="{{$user->getAvatar()}}"
                                                         alt="Avatar">
                                                </li>
                                            @endif
                                        @endforeach
                                        @if(count($role->getUsers()) > 5)
                                            <li class="avatar">
                                                <span class="avatar-initial rounded-circle pull-up bg-lighter text-body"
                                                      data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                      data-bs-original-title="{{count($role->getUsers())-5}}">+{{count($role->getUsers())-5}}</span>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                <div class="d-flex justify-content-between align-items-end">
                                    <div class="role-heading">
                                        <h4 class="mb-1 text-body">{{$role->getName()}}</h4>
                                    </div>
                                    <a class="text-muted edit-role"><i class="mdi mdi-pencil-box mdi-20px"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
