@php
    use Illuminate\Support\Facades\Auth;
    use App\Models\User;
    use  App\Models\UserConfig;
    $user = Auth::user();
    $images = User::avatarStudentSelect();

    $checkGoogle = UserConfig::query()->where("user_id",Auth::id())->where("type","google")->exists();
    $checkFaceBook = UserConfig::query()->where("user_id",Auth::id())->where("type","facebook")->exists();
@endphp
@extends('layouts.app')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h3>Chỉnh sửa thông tin</h3>
        <div class="card mb-4 position-sticky ">
            <div class="container m-3">
                <form action="{{ route("user.update",$user->id) }}" method="POST" enctype="multipart/form-data"
                      class="mb-3">
                    @csrf
                    @method('PUT')
                    <div class="mb-3 row">
                        <div class="row gy-3 mt-0 mb-3">
                            @foreach($images as $index => $image)
                                <div class="col-md-2 fv-plugins-icon-container fv-plugins-bootstrap5-row-valid">
                                    <div class="form-check custom-option custom-option-icon">
                                        <label for="avatarUpdate{{ $index }}"
                                               class="form-check-label custom-option-content">
                                                <span class="custom-option-body">
                                                        <img src="{{ $image }}" alt="" class="w-100 rounded-circle p-3">
                                                 </span>
                                            <input id="avatarUpdate{{ $index }}" class="form-check-input" type="radio"
                                                   name="avatar" value="{{ $image }}">
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col">
                                <label class="form-label" for="name">Tên</label>
                                <input type="text" name="name" id="name" class="form-control"
                                       value="{{old('name',$user->{"name"} )}}"
                                       placeholder="Tên của bạn">
                                @error('name')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col">
                                <label class="form-label">Email</label>
                                <input type="email" name="email" class="form-control"
                                       value="{{old('email',$user->{"email"} )}}"
                                       placeholder="Email của bạn">
                                @error('email')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col">
                                <label class="form-label" for="phone">Số điện thoại</label>
                                <input type="text" id="phone" name="phone" class="form-control"
                                       value="{{old('phone',$user->{"phone"} )}}"
                                       placeholder="số điện thoại">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="old_password">Mật khẩu cũ</label>
                            <input id="old_password" type="password" name="old_password" class="form-control">
                            @error('old_password')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="newPassword">Mật khẩu mới</label>
                            <input type="password" name="password" class="form-control" id="newPassword">
                            @error('password')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group mb-3 ">
                            <label for="confirmNewPassword">Xác nhận mật khẩu mới</label>
                            <input id="confirmNewPassword" type="password" name="password_confirmation"
                                   class="form-control">
                            @error('password_confirmation')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Xác nhận</button>
                </form>
                <div class="mb-3 row">
{{--                    <div class="mb-3 col">--}}
{{--                        @if(!$checkFaceBook)--}}
{{--                            <a href="{{ url('/auth/facebook') }}" class="btn btn-primary form-control">--}}
{{--                                <i class="fab fa-facebook-f"></i> Facebook--}}
{{--                            </a>--}}
{{--                        @else--}}
{{--                            <a href="{{url("/facebook/delete")}}" class="btn btn-primary form-control">--}}
{{--                                <i class="fab fa-facebook-f"></i> Hủy liên kết Facebook--}}
{{--                            </a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                    <div class="mb-3 col">
                        @if(!$checkGoogle)
                            <a href="{{ url('/auth/google') }}" class="btn btn-primary form-control">
                                <i class="fab fa-facebook-f"></i> Google
                            </a>
                        @else
                            <a href="{{url("/auth/google/delete")}}" class="btn btn-primary form-control">
                                <i class="fab fa-facebook-f"></i> Hủy liên kết Google
                            </a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

