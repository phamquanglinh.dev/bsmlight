@php use App\Helper\Fields; @endphp
@php

    /** @var Fields $field */
//    $schedules = old('schedules') ?? session('schedules') ?? $field->getAttributes()['value']['schedules'];
//dd(old('sheet_detail'));
 $sheetDetails = old('sheet_detail') ?? session(' sheet_detail') ?? $field->getAttributes()['value']['sheetDetails'];
 $criterias = old('criterias')  ?? session('criterias') ?? $field->getAttributes()['value']['criterias'];
//    $teacherList = $field->getAttributes()['value']['teacher_list'];
//    $supporterList = $field->getAttributes()['value']['supporter_list'];
@endphp
<div class="my-2" id="render">
    <div class="table-responsive text-nowrap">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Chỉ tiêu</th>
                <th>Loại</th>
                <th>Mô tả</th>
                <th>Giá trị</th>
                <th>Tính theo</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="sheet_detail_result">
            @foreach($sheetDetails as $sheetDetailKey => $sheetDetail)
                @include("salary.sheet_detail_field",[
                        'sheetDetail' => $sheetDetail,
                        'sheetDetailKey' => $sheetDetailKey
                ])
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="my-2 mt-4">
    <div class="text-primary d-flex align-items-center cursor-pointer" type="button" data-bs-toggle="offcanvas"
         data-bs-target="#offcanvasEnd" aria-controls="offcanvasEnd">
        <span class="mdi mdi-plus bg-primary text-white p-1 rounded-circle me-2"></span>
        <span>Thêm chỉ tiêu</span>
    </div>
</div>

<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasEnd" aria-labelledby="offcanvasEndLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasEndLabel" class="offcanvas-title">Thư viện chỉ tiêu</h5>
        <button type="button" class="btn-close text-reset" id="close-criteria-list-btn" data-bs-dismiss="offcanvas"
                aria-label="Close"></button>
    </div>
    <div class="offcanvas-body my-auto mx-0 ">
        @foreach($criterias as $criteria)
            <div class="cursor-pointer border p-2 mb-3 criteria-list-items" data-criteria-id="{{$criteria['id']}}">
                <div class="fw-bold">{{$criteria['criteria_name']}}
                    @if($criteria['is_bsm_criteria'])
                        <span style="font-size: 12px" class="rounded bg-label-primary p-1">BSM</span>
                    @endif
                </div>
                <div class="small">"{{$criteria['criteria_description']}}"</div>
            </div>
        @endforeach
    </div>
</div>

@push("after_scripts")
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        $(".criteria-list-items").click((e) => {
            const criteriaId = $(e.currentTarget).attr('data-criteria-id')
            appendNewCriteria(criteriaId)
            $(e.currentTarget).remove()
            $('#close-criteria-list-btn').click()
        })

        const removeDetail = (row_id) => {
            if (confirm('Bạn có chắc chắn muốn xóa ?')) {
                $('#' + row_id).remove()
            }
        }

        const appendNewCriteria = (criteriaId) => {

            axios.post('{{url('/salary-criteria/one')}}', {
                id: criteriaId,
                _token: '{{csrf_token()}}'
            }, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
                .then(function (response) {
                    console.log(response.data)
                    $('#sheet_detail_result').append(response.data)
                })
                .catch(function (error) {
                    console.error(error);
                });
        }
    </script>

    <script>
        $(document).ready(function () {
            console.clear()
            console.log('i')
            $("select[name='classroom_id']").change(function (e) {
                let selectedClassroomId = $(this).val()
                if (!selectedClassroomId) {
                    selectedClassroomId = 0;
                }

                $("select[name='sheet_user[]']").select2({
                    ajax: {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{url('/salary-criteria/sheet-user-classroom/')}}/' + selectedClassroomId,
                        dataType: 'json',
                        method: 'GET',
                    }
                });
            })
        })
    </script>
@endpush
