<div class="form-floating form-floating-outline">
    <input id="TagifyCustomListSuggestion" name="TagifyCustomListSuggestion" class="form-control h-auto" placeholder="select technologies" value="css, html, php">
    <label for="TagifyCustomListSuggestion">Custom List Suggestions</label>
</div>

@push('after_scrips')
    <link rel="stylesheet" href="{{asset('demo/assets/vendor/libs/tagify/tagify.css')}}" />
    <script src="{{asset('demo/assets/vendor/libs/tagify/tagify.js')}}"></script>
    <script>
        const TagifyCustomListSuggestionEl = document.querySelector("#TagifyCustomListSuggestion");

        const whitelist = [
            "A# .NET",
            "A# (Axiom)",
            "A-0 System",
            "A+",
            "A++",
            "ABAP",
            "ABC",
            "ABC ALGOL",
            "ABSET",
            "ABSYS",
            "ACC",
            "Accent",
            "Ace DASL",
            "ACL2",
            "Avicsoft",
            "ACT-III",
            "Action!",
            "ActionScript",
            "Ada",
            "Adenine",
            "Agda",
            "Agilent VEE",
            "Agora",
            "AIMMS",
            "Alef",
            "ALF",
            "ALGOL 58",
            "ALGOL 60",
            "ALGOL 68",
            "ALGOL W",
            "Alice",
            "Alma-0",
            "AmbientTalk",
            "Amiga E",
            "AMOS",
            "AMPL",
            "Apex (Salesforce.com)",
            "APL",
            "AppleScript",
            "Arc",
            "ARexx",
            "Argus",
            "AspectJ",
            "Assembly language",
            "ATS",
            "Ateji PX",
            "AutoHotkey",
            "Autocoder",
            "AutoIt",
            "AutoLISP / Visual LISP",
            "Averest",
            "AWK",
            "Axum",
            "Active Server Pages",
            "ASP.NET"
        ];
        // List
        let TagifyCustomListSuggestion = new Tagify(TagifyCustomListSuggestionEl, {
            whitelist: whitelist,
            maxTags: 10, // allows to select max items
            dropdown: {
                maxItems: 20, // display max items
                classname: "", // Custom inline class
                enabled: 0,
                closeOnSelect: false
            }
        });
    </script>
@endpush
