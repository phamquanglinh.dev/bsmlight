@php use App\Helper\Fields;use App\Models\ReceiptTemplate; @endphp
@php
    /**
     * @var Fields $field
     */
    $hasPrint = $field->getAttributes()['print_btn'] ?? false;
    $templates = ReceiptTemplate::query()->get();
@endphp
@if($hasPrint)
    <div class="row">
        <div class="col-5">
            <select class="form-select my-2" name="template_id">
                @foreach($templates as $template)
                    <option value="{{$template->id}}">{{$template->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-5">
            <button type="button" class="print-btn btn btn-label-dribbble my-2">Tạo bản in</button>
        </div>
    </div>
@endif
<div class="mb-3 form-floating-outline">
    <label for="{{$field->getName()}}" style="padding: 0.8rem" id="label_{{$field->getName()}}"
           class="form-control">{{$field->getLabel()}}</label>
    <input class="form-control d-none" type="file" name="{{$field->getName()}}"
           id="{{$field->getName()}}">
</div>

@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        $("#{{$field->getName()}}").on('change', function (e) {
            const files = e.target.files[0];

            $("#label_{{$field->getName()}}").text(files.name.substring(0, 10) + "...")
        })
    </script>
    <script>
        $(document).ready(() => {
            $(".print-btn").click(async (e) => {
                const form = $(e.currentTarget).closest('form');

                let formData = {};
                form.find('input,textarea,select').each(function () {
                    formData[$(this).attr('name')] = $(this).val();
                });

                formData['_token'] = '{{csrf_token()}}'

                const urlParams = new URLSearchParams(window.location.search);
                const params = {};
                for (const [key, value] of urlParams) {
                    params[key] = value;
                }

                const finalData = {
                    ...formData,
                    ...params
                };

                try {
                    const response = await axios.post('{{url("templates/print/card")}}', finalData);

                    window.open(response.data.data.url, '_blank');
                } catch (error) {
                    console.error('Error generating or showing PDF:', error);
                }
            })
        })
    </script>
@endpush
