@php use App\Helper\Fields; @endphp
@php
    /**
     * @var Fields $field
     */
@endphp
<div class="form-floating form-floating-outline mb-4">
    <textarea
        class="form-control"
        name="{{$field->getName()}}"
        id="{{$field->getName()}}"
        {{--        {{$field->isRequired()?"required":""}}--}}
        placeholder="Nhập {{$field->getLabel()}}"
    >{{old($field->getName()) ?? $field->getValue()}}</textarea>
    <label for="{{$field->getName()}}">{{$field->getLabel()}}</label>
    @error($field->getName())
    <p style="color: red;">{{ $message }}</p>
    @enderror
</div>

<!-- Include jQuery -->

@push("after_scripts")
    <script>
        $(document).ready(function() {
            var $textarea = $('#{{ $field->getName() }}');

            // Function to adjust the height of the textarea
            function adjustTextareaHeight($textarea) {
                // Reset the height so that it can shrink if needed
                $textarea.css('height', 'auto');
                // Set the height to scrollHeight to fit the content
                $textarea.css('height', ($textarea.prop('scrollHeight') + 10) + 'px');
            }

            // Adjust the height initially
            adjustTextareaHeight($textarea);

            // Add event listener to adjust the height on input
            $textarea.on('input', function() {
                adjustTextareaHeight($(this));
            });
        });
    </script>
@endpush
