@php
    use App\Models\TransactionType;

    $show = "";

    if ($errors->has('group') || $errors->has('name')) {
        $show = "show";
    }
@endphp

<div class="my-3">
    <a type="button" class="text-primary" data-bs-toggle="offcanvas" data-bs-target="#quick_create_transaction"
       aria-controls="offcanvasEnd">Thêm loại giao dịch</a>
</div>
<div class="offcanvas offcanvas-end {{$show}} " tabindex="-1" id="quick_create_transaction"
     aria-labelledby="offcanvasEndLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasEndLabel" class="offcanvas-title">Thêm loại giao dịch</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body my-auto mx-0 flex-grow-0 h-100">
        <form action="{{url("/finance/transaction-type/store")}}" method="post">
            @csrf
            <div class="form-floating form-floating-outline mb-3">
                <select required name="group" class="form-select" id="exampleFormControlSelect1"
                        aria-label="Default select example">
                    @foreach(TransactionType::groupOptions() as $key => $label)
                        <option value="{{$key}}">{{$label}}</option>
                    @endforeach
                </select>
                <label for="exampleFormControlSelect1">Loại nhóm giao dịch</label>
                @error('group')
                <div class="small text-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-floating form-floating-outline mb-3">
                <input required name="name" type="text" class="form-control" id="floatingInput"
                       aria-describedby="floatingInputHelp"/>
                <label for="floatingInput">Loại giao dịch cụ thể</label>
                @error('name')
                <div class="small text-danger">{{$message}}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mb-2 d-grid w-100">Lưu</button>
        </form>
        <button type="button" class="btn btn-outline-secondary d-grid w-100" data-bs-dismiss="offcanvas">Huỷ</button>
        <hr/>
        <div>

        </div>
    </div>
</div>
