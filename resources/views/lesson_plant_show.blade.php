@php use App\Models\LessonPlant;use App\Models\User;use Illuminate\Support\Facades\Auth; @endphp
@php
    /**
     * @var LessonPlant $lessonPlant
     */

@endphp
@extends("layouts.app")
@section("content")
    <div class="container-fluid p-5">

        <div class="h3">
            {{$lessonPlant['name']}}
            @if($lessonPlant['status'] == 1)
                <span class="bg-label-success small p-2 rounded">Đã duyệt</span>
            @endif
        </div>
        <a href="{{route('lesson_plant.list')}}" class="my-2">Quay về danh sách</a>
        <hr>
        <div>Tiêu đề: <span class="text-success">{{$lessonPlant['title']}}</span></div>
        @if($lessonPlant['video'])
            <div>Link video: <span class="text-success"><a href="{{$lessonPlant['video']}}">Link</a></span></div>
        @endif
        @if($lessonPlant['audio'])
            <div>Link audio: <span class="text-success"><a href="{{$lessonPlant['audio']}}">Link</a></span></div>
        @endif
        @if($lessonPlant['link'])
            <div>Link tài liệu: <span class="text-success"><a href="{{$lessonPlant['link']}}">Link</a></span></div>
        @endif
        <div class="my-2">Nội dung</div>
        <div class="border p-2 rounded ">
            {!! nl2br($lessonPlant['content']) !!}
        </div>
        @if($lessonPlant['status'] == 0 && in_array(Auth::user()->{'role'},[User::HOST_ROLE,User::STAFF_ROLE]))
            <div class="my-3">
                <a href="{{url("/lesson_plant/accept/".$lessonPlant->{'id'})}}" class="btn btn-success text-white">Duyệt
                    giáo án</a>
            </div>
        @endif
    </div>
@endsection
