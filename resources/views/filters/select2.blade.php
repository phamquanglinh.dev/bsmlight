@php use App\Helper\Fields;use App\Helper\Filter; @endphp
@php
    /**
     * @var Filter $filter
     */
@endphp


<div class="form-floating form-floating-outline mb-4">
    <select name="{{$filter->getName()}}" id="{{$filter->getName()}}" class="selectpicker w-100 small"
            data-style="btn-default"
            data-live-search="true"
    >
        <option value="">Chọn</option>
        @foreach($filter->getAttributes()['options'] as $key => $value)
            <option
                value="{{$key}}"
                class="small"
                @if((old($filter->getName()) == $filter->getValue() && $filter->getValue()!=null) || $key == $filter->getValue()) selected @endif>{{$value}}</option>
        @endforeach
    </select>
    <label for="{{$filter->getName()}}">{{$filter->getLabel()}}</label>
</div>
<style>
    .filter-option-inner-inner {
        text-transform: none !important;
    }
</style>

@push('after_scripts')
{{--    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/select2/select2.css')}}"/>--}}
{{--    <script src="{{asset('/demo/assets/vendor/libs/select2/select2.js')}}"></script>--}}
    <link rel="stylesheet" href="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.css')}}"/>
    <script src="{{asset('/demo/assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
    <script>
        // $(".select2").select2();
        $(".selectpicker").selectpicker();
    </script>
@endpush

