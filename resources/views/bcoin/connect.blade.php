@php
    /**
* @var array $bcoinUserInfo
 */
@endphp
@extends("layouts.app")
@section("content")
    <div class="container-xxl p-5">
        @if(! $bcoinUserInfo['is_connected'])
            <div class="border p-4 rounded shadow-lg">
                <form action="{{url("bcoin/connect")}}" method="POST">
                    @csrf
                    <div class="form-floating form-floating-outline mb-4">
                        <input type="text" name="b_username" value="" aria-autocomplete="off" class="form-control"/>
                        <label>Tên tài khoản Bcoin</label>
                        @error("b_username")
                        <div class="small text-danger">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-floating form-floating-outline mb-3">
                        <input autocomplete="off" type="password" name="b_password" class="form-control"/>
                        <label>Mật khẩu</label>
                        @error("b_password")
                        <div class="small text-danger">{{$message}}</div>
                        @enderror
                    </div>

                    <button class="btn btn-success">Kết nối</button>
                </form>
            </div>
        @else
            <div class="border p-4 rounded shadow-lg">
                <div class="mb-2">Tên tài khoản: {{$bcoinUserInfo['username']}}</div>
                <div class="mb-2">Họ và tên: {{$bcoinUserInfo['name']}}</div>
                <div>Số dư: <span class="fw-bold text-success">{{$bcoinUserInfo['balance']}}</span> Bcoin</div>
                <a href="{{url( "bcoin/disconnect" )}}" class="btn btn-danger mt-2">Đăng xuất</a>
            </div>
            @include("bcoin.inc.bcoin")
        @endif
    </div>
@endsection
