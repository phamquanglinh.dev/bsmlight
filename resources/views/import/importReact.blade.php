@extends("layouts.app")
@section("content")
    <link rel="stylesheet" href="{{asset("/demo/assets/vendor/libs/bs-stepper/bs-stepper.css")}}" />
    <div id="import-react"></div>
@endsection
@push("after_scripts")
   <script src="{{asset('js/import.js')}}"></script>
@endpush
