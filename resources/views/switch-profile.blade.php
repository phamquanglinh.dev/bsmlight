@php use App\Models\Student; @endphp
@php
    /**
    * @var Student[] $profiles
    */
    $defaultAvatar = "https://img.freepik.com/premium-vector/round-avatar-portrait-icon-elementary-student-boy-with-backpack-flat-style_768258-3401.jpg";
@endphp
@extends("layouts.app")

@section("content")
    <div class="h-100 d-flex align-items-center justify-content-center">
        @if(! empty($profiles))
            <div class="col-md-6 col-12">
                <div class="row">
                    @foreach($profiles as $profile)
                        <div class="col-3">
                            <a href="{{url("/switch/profile/{$profile['id']}")}}" class="text-none">
                                <div class="text-center">
                                    <img
                                        alt=""
                                        class="w-75  rounded-circle"
                                        src="{{$profile['avatar']??$defaultAvatar}}">
                                </div>
                                <div class="my-2 fw-bold text-center">
                                    {{$profile['name']}}
                                </div>
                                <div class="small text-center">{{$profile['uuid']}}</div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        @if(empty($profiles))
            <div>
                <div class="text-center">
                    <img src="https://media.tenor.com/s3W8dwoQThAAAAAj/sad-depressed.gif">
                </div>
                <div class="text-center">Bạn không có tài khoản liên kết</div>
            </div>
        @endif
    </div>
@endsection
