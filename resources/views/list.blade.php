@php
    use App\Helper\CrudBag;use App\Helper\ListViewModel;use App\Models\User;use Illuminate\Support\Facades\Auth;
@endphp
@php
    /**
     * @var ListViewModel $listViewModel
     * @var CrudBag $crudBag
     */
@endphp
@extends("layouts.app")
@section("content")
    <style>
        .table th {
            min-width: 10rem !important;
            text-wrap: wrap;
            vertical-align: middle;
            max-width: 26rem;
        }
    </style>
    <style>
        .small-zoom {
            zoom: 0.9; /* Thu nhỏ trang web 80% */
            -moz-transform: scale(0.9); /* Firefox */
            -moz-transform-origin: 0 0;
            -o-transform: scale(0.9); /* Opera */
            -o-transform-origin: 0 0;
            -webkit-transform: scale(0.9); /* Safari và Chrome */
            -webkit-transform-origin: 0 0;
            transform: scale(1); /* Phần còn lại */
            transform-origin: 0 0;
        }
    </style>
    <div class="small-zoom container-fluid pe-5 py-5 flex-grow-1 container-p-y">
        <h4 class="">
            <span class="text-muted fw-light">{{$crudBag->getLabel()}} /</span> Danh sách
        </h4>

        @include("components.statistics",['statistics'=>$crudBag->getStatistics()])
        <div class="d-flex justify-content-between mt-4">
            @if(! $crudBag->getParam('disable_all_top_btn'))
                <div>
                    @if(check_permission("create ".$crudBag->getEntity()))
                        <a href="{{url($crudBag->getEntity()."/create")}}"
                           class="btn btn-primary waves-effect waves-light mb-2">
                            <span class="mdi mdi-plus me-2"></span>
                            Thêm mới {{$crudBag->getLabel()}}</a>
                    @endif
                    @if(Auth::user()->role==User::HOST_ROLE)
                        @if(has_import($crudBag->getEntity()))
                                <a
                                    href="{{asset("{$crudBag->getEntity()}/import")}}"
                                    class="text-white btn btn-primary waves-effect waves-light mb-2">
                                    <span class="mdi mdi-cloud-upload me-2"></span>
                                    Import {{$crudBag->getLabel()}}
                                </a>
                                <a
                                    href="{{asset("export/{$crudBag->getEntity()}")}}"
                                    class="text-white btn btn-primary waves-effect waves-light mb-2">
                                    <span class="mdi mdi-cloud-download me-2"></span>
                                    Export {{$crudBag->getLabel()}}
                                </a>
                        @endif
                    @endif
                    @if($crudBag->getParam(CrudBag::BULK_CARD))
                        <a href="{{url($crudBag->getEntity()."/bulk/clone_card")}}"
                           class="btn btn-primary waves-effect waves-light mb-2">
                            <span class="mdi mdi-plus me-2"></span>
                            Thêm hàng loạt</a>
                    @endif
                </div>
            @endif
            <div class="col-md-3">
                <form class="form replace_form" id="search_form">
                    <input name="search" value="{{old('search') ?? $crudBag->getSearchValue()}}" type="search"
                           class="form-control"
                           placeholder="{{$crudBag->getParam(CrudBag::PLACEHOLDER_SEARCH) ?? "Tìm kiếm"}}">
                </form>
            </div>
        </div>
        @if($crudBag->getParam(CrudBag::ORDER_MODE))
            <div class="small mt-3">
                <button onclick="setMode(1)" class="{{$mode !== '1' ? "btn btn-label-secondary ":"btn btn-label-success"}}">Xem theo học sinh</button>
                <button onclick="setMode(2)" class="{{$mode !== '1' ? "btn btn-label-success ":"btn btn-label-secondary"}}">Xem theo thẻ học</button>
            </div>
        @endif
        @include("components.filter",['filters' => $crudBag->getFilters(),'crudBag' => $crudBag])
        <div class="table-responsive text-nowrap shadow-lg" style="height: 95vh;scroll-behavior: smooth;">
            <table class="table shadow" id="myTable">
                <thead>
                <tr class="text-nowrap" style="position: sticky;top: 0; z-index: 50">
                    @foreach($crudBag->getColumns() as $column)
                        @if($column->getFixed() == 'first')
                            <th class="border fw-bold bg-primary fixed-left"
                                style="position: sticky;top: 0!important; z-index: 1">{{$column->getLabel()}}</th>
                        @elseif($column->getFixed() == 'second')
                            <th class="border fw-bold bg-primary fixed-left-second"
                                style="position: sticky;top: 0!important; z-index: 1">{{$column->getLabel()}}</th>
                        @else
                            <th class="border fw-bold bg-primary">{{$column->getLabel()}}</th>
                        @endif
                    @endforeach
                    @if(! $crudBag->getParam('disable_action'))
                        <th class="border fw-bold bg-primary">Hành động</th>
                    @endif
                </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                @foreach($listViewModel->getCollectionItem() as $item)
                    <tr>
                        @foreach($crudBag->getColumns() as $column)
                            @include("columns.".$column->getType(),['item' => $item,'column' => $column])
                        @endforeach
                        @if(! $crudBag->getParam('disable_action'))
                            <th class="border text-center">
                                @if(! $crudBag->getParam('disable_delete'))
                                    @if(check_permission("delete ".$crudBag->getEntity()))
                                        <a class="delete-action"
                                           href="{{url($crudBag->getEntity()."/delete/".$item['id'])}}">
                                            <span class="mdi mdi-delete"></span>
                                        </a>
                                    @endif
                                @endif
                            </th>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div>
            @include("components.pagination",['paginator' => $listViewModel->getOriginCollection()])
        </div>
    </div>
    @stack('off_canvas')
@endsection
@push('after_scripts')
    <style>
        .fixed-left {
            position: sticky;
            left: 0;
        }

        td.fixed-left .d-flex {
            background: #9055fd26 !important;
            padding: 0.3rem;
        }

        .fixed-left-second {
            position: sticky;
            left: 21rem;
        }
    </style>
    <script>
        $(".delete-action").click((e) => {
            e.preventDefault();
            const result = confirm("Bạn có chắc chắn muốn xóa {{$crudBag->getLabel()}}");

            if (result) {
                window.location.href = e.currentTarget.href;
            }
        })
    </script>

    <script>
        // Tìm tất cả các thẻ <tr> có thẻ <button> bên trong có class là "card-danger" và thêm class "has-danger" vào chúng
        document.querySelectorAll('tr td button.card-danger').forEach(function (button) {
            button.closest('tr').classList.add('has-danger');
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        $(".change-salary-group-status").change((e) => {
            const id = e.currentTarget.id
            const value = e.currentTarget.value
            axios.post('{{url('/salary-group/update')}}/' + id + "/json", {
                status: value,
                _token: '{{csrf_token()}}'
            }, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).then(function (response) {
                const background = response.data.background
                const thisNode = $("#" + id)
                thisNode.removeClass(thisNode.attr('class').split(' ').pop())
                thisNode.addClass(background)
            }).catch(function (error) {

            });
        })
    </script>
@endpush
