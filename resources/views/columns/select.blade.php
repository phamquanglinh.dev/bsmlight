@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $options = $column->getAttributes()['options'];
    $tooltip = silence(fn() => $column->getAttributes()['tooltips'], []);
    $title= silence(fn()=>$tooltip[$item[$column->getName()]],"");
    $bg = silence(fn()=> $column->getAttributes()['bg'][$item[$column->getName()]],"")
@endphp
<td class="border text-center dark-style {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    <button type="button" @if(! empty($tooltip)) data-bs-toggle="tooltip" data-bs-placement="left" title="{{$title}}"
            @endif class="cursor-pointer btn {{$bg}} text-white p-1 px-3 rounded-pill">
        {{$options[$item[$column->getName()]] ?? $item[$column->getName()]}}
   </button>
</td>
