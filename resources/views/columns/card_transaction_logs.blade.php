@php use App\Helper\Column;

@endphp
@php
    /**\
     * @var Column $column
     */
    $logs = $item['transaction_logs'] ?? [];
@endphp

<td class="border {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    <span
        type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas_{{$item['id']}}"
        aria-controls="offcanvasEnd"
        class="text-center cursor-pointer p-1 px-2 rounded-pill {{$column->getAttributes()['bg'][$item[$column->getName()]]??""}}">
             {{number_format($item[$column->getName()])}}
    </span>
    <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvas_{{$item['id']}}"
         aria-labelledby="offcanvasTopLabel">
        <div class="offcanvas-header">
            <h5 id="offcanvasTopLabel" class="offcanvas-title">Lịch sử thanh toán</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <div class="table-responsive text-nowrap">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Mã giao dịch</th>
                        <th>Ngày giao dịch</th>
                        <th>Số tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td>#{{$log['uuid']}}</td>
                            <td>{{\Illuminate\Support\Carbon::parse($log['transaction_day'])->isoFormat('DD/MM/YYYY hh:mm:ss')}}</td>
                            <td>{{number_format($log['amount'])}} đ</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</td>

