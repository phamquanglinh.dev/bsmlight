@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $attributes = $column->getAttributes();
    $options = $column->getAttributes()['options']??[];
@endphp

<td class="border {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    @if(!empty($column->getAttributes()['bg']))
        <span
            class="text-white p-1 px-2 rounded-pill {{$column->getAttributes()['bg'][$item[$column->getName()]]??""}}">
             {{$item[$column->getName()]}}
        </span>
    @else
        {{$item[$column->getName()]}}
    @endif
    @if(isset($attributes['edit']))
        <a href="{{url($attributes['entity']."/edit/".$item['id'])}}">
            <span class="mdi mdi-square-edit-outline"></span>
        </a>
    @endif
    @if(isset($attributes['show']))
        <a href="{{url($attributes['entity']."/show/".$item['id'])}}">
            <span class="mdi mdi-list-box"></span>
        </a>
    @endif
    @if(isset($attributes['transaction']))
        <a href="{{url('transaction/create/'.$attributes['entity']."?".$attributes["entity"]."_id=".$item['id'])}}">
            <span class="mdi mdi-database-plus"></span>
        </a>
    @endif
    <div class="mt-3">
        <span class="{{$column->getAttributes()['bg'][$item['status']]}} text-white p-1 px-3 rounded-pill">
       {{$options[$item['status']] ?? $item['status']}}
   </span>
    </div>
</td>
