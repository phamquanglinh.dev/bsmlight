@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $users = $item[$column->getName()];
@endphp

<td class="border text-center {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    <ul class="list-unstyled users-list m-0 avatar-group d-flex align-items-center">
        @foreach($users as $user)
            <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                class="avatar avatar-xs pull-up" title="{{$user['name']}}">
                <img src="{{$user['avatar']}}" alt="Avatar" class="rounded-circle">
            </li>
        @endforeach
    </ul>
</td>
