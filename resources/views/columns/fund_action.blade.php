@php
    /**
     * @var array $item
     */
    $transactions = getFundTransaction($item['id'])
@endphp
<td class="border">
    <div class="">
        <div class="mb-2">
            <a href="{{url("fund/paid/{$item['id']}")}}">Chi quỹ gắn bó</a>
        </div>
        <div class="mb-2">
            <a href="{{url("fund/edit/{$item['id']}")}}">Cập nhật số dư</a>
        </div>
        <div>
            <a type="button" data-bs-toggle="offcanvas"
               data-bs-target="#fund_{{$item['id']}}" aria-controls="offcanvasStart"
               href="{{url("fund/edit/{$item['id']}")}}">Xem lịch sử thanh toán</a>
        </div>
    </div>
</td>


@push('off_canvas')
    <div class="offcanvas offcanvas-start" tabindex="-1" id="fund_{{$item['id']}}"
         aria-labelledby="offcanvasStartLabel">
        <div class="offcanvas-header">
            <h5 id="offcanvasStartLabel" class="offcanvas-title">Danh sách giao dịch</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body my-auto mx-0 flex-grow-0 h-100">
            <div>
                @foreach($transactions as $transaction)
                    <div class="border p-2 mb-3 rounded position-relative">
                        <div>Mã giao dịch : #{{ $transaction->{'uuid'} }}</div>
                        <div>Số tiền: {{ number_format($transaction->{'amount'}) }} đ</div>
                        <div>Ghi chú: {!! $transaction->{'notes'} !!}</div>
                        <div>Ngày giao dịch: {!! $transaction->{'transaction_day'} !!}</div>
                        <div class="mt-1">
                            <a href="{{url('/finance/cashiers/edit/'.$transaction->{'id'})}}" class="text-primary me-2">Sửa</a>
                            <a href="{{url('/transaction/delete/'.$transaction->{'id'})}}" class="text-danger">Xoá</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endpush
