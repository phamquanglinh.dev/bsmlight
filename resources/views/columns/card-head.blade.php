@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $avatar = $column->getAttributes()['avatar'];
    $name = $column->getAttributes()['name'];
    $name = $column->getAttributes()['name'];
    $uuid = $column->getAttributes()['uuid'];
    $id = $column->getAttributes()['id'];
    $entity = $column->getAttributes()['entity'];
    $model = $column->getAttributes()['model'];

    $data = $item[$entity];

    $cardUuid = $item['uuid'];
    $cardId = $item['id'];
@endphp


<td class="border {{$column->getFixed() == 'first' ? 'fixed-left':''}} {{$column->getFixed() == 'second' ? 'fixed-left-second':''}}">

    <div class="d-flex align-items-center">
        @if($data)
            <img style="width: 2rem;height: 2rem" class="rounded-circle d-block me-2"
                 src="{{$data[$avatar]??"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQS0Oi53Y0SYUnNZ6FDFALWjbzr2siFFZqRAI_ygcnbVunsa0Ywsn1u1xGx7FisdgzGdcQ&usqp=CAU"}}">
            <div class="small">
                <div class="mb-1">
                    {{$cardUuid}}
                    @if(check_permission('edit card'))
                        <a href="{{url("card/edit/".$cardId)}}">
                            <span class="mdi mdi-square-edit-outline"></span>
                        </a>
                    @endif
                    <a href="{{url("card/show/".$cardId)}}">
                        <span class="mdi mdi-list-box"></span>
                    </a>
                </div>
                <div class="mb-1">
                    {{$data[$name]}}
                    @if(check_permission('edit '.$model))
                        <a href="{{url($model."/edit/".$data[$id])}}">
                            <span class="mdi mdi-square-edit-outline"></span>
                        </a>
                    @endif
                </div>
                <div class="small">{{$data[$uuid]}}</div>
            </div>
        @else
            <div style="display:flex; align-items: center">
                <div class="me-2">
                    <img src="{{avatar('1.webp')}}" class="avatar-xs rounded-circle">
                </div>
                <div>
                    <div>
                        <span class="small">{{$item['uuid']}}</span>
                        <a href="{{url("card/show/".$cardId)}}">
                            <span class="mdi mdi-list-box"></span>
                        </a>
                        @if(check_permission('edit card'))
                            <a href="{{url("card/edit/".$cardId)}}">
                                <span class="mdi mdi-square-edit-outline"></span>
                            </a>
                        @endif
                    </div>
                    <div class="small">-Chưa gắn học sinh-</div>
                </div>
            </div>
        @endif
    </div>

</td>
