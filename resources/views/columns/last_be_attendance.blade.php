@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $attributes = $column->getAttributes();
@endphp

<td class="border text-center {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
     <span class="@if($item['be_attendance_warning']) text-white bg-danger @endif p-1 px-2 rounded-pill">
             {{$item[$column->getName()]}}
     </span>
</td>
