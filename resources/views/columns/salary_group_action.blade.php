@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $attributes = $column->getAttributes();
    $id = $item[$attributes['id']];
@endphp

<td class="border text-start {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    <div class="mb-2">
        <a class="text-primary" href="{{url('salary-group/show/'.$id)}}">Xem chi tiết phiếu</a>
    </div>
    <div class="mb-2">
        <a class="text-success" href="{{url('salary-group/paid/'.$id)}}">Chi lương</a>
    </div>
    <div class="mb-2">
        <a class="text-warning" href="{{url('salary-group/print_and_send?id='.$id)}}">Gửi phiếu lương</a>
    </div>
</td>
