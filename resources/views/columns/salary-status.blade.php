@php use App\Helper\Column;use App\Models\SalaryGroup; @endphp
@php
    /**
    * @var Column $column
    * @var array $item
    */
    $transactions = getSalaryGroupTransaction($item['id']);

@endphp
<td class="border text-center dark-style {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    <div class="d-flex">
        <select id="{{$item['id']}}"
                class="rounded-0 change-salary-group-status text-white form-control form-floating-outline {{SalaryGroup::bgStatusLabel()[$item[$column->getName()]]}}">
            @foreach(SalaryGroup::statusLabel() as $key => $label)
                <option {{$key == $item[$column->getName()] ? 'selected' :''}} class="bg-body text-center"
                        value="{{$key}}">
                    {{$label}}
                </option>
            @endforeach
        </select>
        <button class="rounded-0 btn btn-primary" type="button" data-bs-toggle="offcanvas"
                data-bs-target="#transaction_{{$item['id']}}" aria-controls="offcanvasStart">#{{$item['id']}}
        </button>
    </div>
</td>

@push('off_canvas')
    <div class="offcanvas offcanvas-start" tabindex="-1" id="transaction_{{$item['id']}}"
         aria-labelledby="offcanvasStartLabel">
        <div class="offcanvas-header">
            <h5 id="offcanvasStartLabel" class="offcanvas-title">Danh sách giao dịch</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body my-auto mx-0 flex-grow-0 h-100">
            <div>
                @foreach($transactions as $transaction)
                    <div class="border p-2 mb-3 rounded position-relative">
                        <div>Mã giao dịch : #{{ $transaction->{'uuid'} }}</div>
                        <div>Số tiền: {{ number_format($transaction->{'amount'}) }} đ</div>
                        <div>Ghi chú: {!! $transaction->{'notes'} !!}</div>
                        <div>Ngày giao dịch: {!! $transaction->{'transaction_day'} !!}</div>
                        <div class="mt-1">
                            <a href="{{url('/finance/cashiers/edit/'.$transaction->{'id'})}}" class="text-primary me-2">Sửa</a>
                            <a href="{{url('/transaction/delete/'.$transaction->{'id'})}}" class="text-danger">Xoá</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endpush
