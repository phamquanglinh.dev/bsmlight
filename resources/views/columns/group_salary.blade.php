@php use App\Helper\Column; @endphp
@php
    /**
    * @var array $item
    * @var Column $column
    */
    $attributes = $column->getAttributes();
    $user = \App\Models\User::query()->where('id',$item['user_id'])->first();
@endphp

<td class="border text-center {{$column->getFixed() == 'first' ? 'fixed-left':''}}">
    <div>
        @if(!empty($column->getAttributes()['bg']))
            <span
                class="text-white p-1 px-2 rounded-pill {{$column->getAttributes()['bg'][$item[$column->getName()]]??""}}">
             {{$item[$column->getName()]}}
        </span>
        @else
            {{$item[$column->getName()]}}
        @endif
        @if(isset($attributes['edit']))
            <a href="{{url($attributes['entity']."/edit/".$item['id'])}}">
                <span class="mdi mdi-square-edit-outline"></span>
            </a>
        @endif
        @if(isset($attributes['show']))
            <a href="{{url($attributes['entity']."/show/".$item['id'])}}">
                <span class="mdi mdi-list-box"></span>
            </a>
        @endif
        @if(isset($attributes['transaction']))
            <a href="{{url('transaction/create/'.$attributes['entity']."?".$attributes["entity"]."_id=".$item['id'])}}">
                <span class="mdi mdi-database-plus"></span>
            </a>
        @endif
    </div>
    <div class="small px-2 my-2">
        <div class="d-flex flex-row">
            <div class="me-2">
                <img style="width: 2rem" class="rounded-circle" src="{{$user->getAttribute('avatar')}}">
            </div>
            <div class="text-start">
                <div>{{$user->getAttribute('name')}}</div>
                <div>{{$user->getAttribute('uuid')}}</div>
            </div>
        </div>
    </div>
</td>
