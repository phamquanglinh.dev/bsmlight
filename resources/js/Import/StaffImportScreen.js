import React, {useState} from "react";
import TeacherSteps from "./components/teacher/TeacherSteps";
import TeacherUploadStep from "./components/teacher/TeacherUploadStep";
import TeacherConfigStep from "./components/teacher/TeacherConfigStep";
import TeacherValidateStep from "./components/teacher/TeacherValidateStep";
import TeacherResultStep from "./components/teacher/TeacherResultStep";
import SupporterSteps from "./components/supporter/SupporterSteps";
import SupporterUploadStep from "./components/supporter/SupporterUploadStep";
import SupporterConfigStep from "./components/supporter/SupporterConfigStep";
import SupporterValidateStep from "./components/supporter/SupporterValidateStep";
import SupporterResultStep from "./components/supporter/SupporterResultStep";
import StaffSteps from "./components/staff/StaffSteps";
import StaffUploadStep from "./components/staff/StaffUploadStep";
import StaffConfigStep from "./components/staff/StaffConfigStep";
import StaffValidateStep from "./components/staff/StaffValidateStep";
import StaffResultStep from "./components/staff/StaffResultStep";

const SupporterImportScreen = () => {
    const [step, setStep] = useState(1)
    const [importData, setImportData] = useState([])
    return (
        <div className={"container-xxl p-5"}>
            <div className={"my-2 h5"}>Upload nhân viên</div>
            <StaffSteps step={step} setStep={setStep} importData={importData}/>
            <div className={"my-3"}>
                {
                    step === 1 &&
                    <StaffUploadStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 2 &&
                    <StaffConfigStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 3 &&
                    <StaffValidateStep step={step} setStep={setStep} data={importData} setData={setImportData}/>
                }
                {
                    step === 4 &&
                    <StaffResultStep step={step} setStep={setStep} data={importData} setData={setImportData}/>
                }
            </div>
        </div>
    )
}

export default SupporterImportScreen
