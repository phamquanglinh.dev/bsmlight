import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/s"
const baseAxios = axios
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};

export const getAbility = ({setAbility}) => {
    const url = baseUrl + "/import/ability"

    baseAxios.get(url, {}).then((response) => {
        setAbility(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getAllStudentTitles = ({setTitles}) => {
    const url = baseUrl + "/student/import/titles"

    baseAxios.get(url, {}).then((response) => {
        setTitles(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}
export const getAllClassroomTitles = ({setTitles}) => {
    const url = baseUrl + "/classroom/import/titles"

    baseAxios.get(url, {}).then((response) => {
        setTitles(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getAllTeacherTitles = ({setTitles}) => {
    const url = baseUrl + "/teacher/import/titles"

    baseAxios.get(url, {}).then((response) => {
        setTitles(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getAllSupporterTitles = ({setTitles}) => {
    const url = baseUrl + "/supporter/import/titles"

    baseAxios.get(url, {}).then((response) => {
        setTitles(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getAllStaffTitles = ({setTitles}) => {
    const url = baseUrl + "/staff/import/titles"

    baseAxios.get(url, {}).then((response) => {
        setTitles(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const validateStudentImport = ({data, setData}) => {
    const url = baseUrl + "/student/import/validate"
    baseAxios.post(url, data).then((response) => {
        setData({
            ...data,
            validate_collection: (response.data.data)
        })
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const validateClassroomImport = ({data, setData}) => {
    const url = baseUrl + "/classroom/import/validate"
    baseAxios.post(url, data).then((response) => {
        setData({
            ...data,
            validate_collection: (response.data.data)
        })
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const validateTeacherImport = ({data, setData}) => {
    const url = baseUrl + "/teacher/import/validate"
    baseAxios.post(url, data).then((response) => {
        setData({
            ...data,
            validate_collection: (response.data.data)
        })
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const validateSupporterImport = ({data, setData}) => {
    const url = baseUrl + "/supporter/import/validate"
    baseAxios.post(url, data).then((response) => {
        setData({
            ...data,
            validate_collection: (response.data.data)
        })
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const validateStaffImport = ({data, setData}) => {
    const url = baseUrl + "/staff/import/validate"
    baseAxios.post(url, data).then((response) => {
        setData({
            ...data,
            validate_collection: (response.data.data)
        })
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const studentImport = ({rows,setStep}) => {
    const url = baseUrl + "/student/import"
    baseAxios.post(url, rows).then((response) => {
        setStep(4)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const classroomImport = ({rows,setStep}) => {
    const url = baseUrl + "/classroom/import"
    baseAxios.post(url, rows).then((response) => {
        setStep(4)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const teacherImport = ({rows,setStep}) => {
    const url = baseUrl + "/teacher/import"
    baseAxios.post(url, rows).then((response) => {
        setStep(4)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}
export const supporterImport = ({rows,setStep}) => {
    const url = baseUrl + "/supporter/import"
    baseAxios.post(url, rows).then((response) => {
        setStep(4)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const staffImport = ({rows,setStep}) => {
    const url = baseUrl + "/staff/import"
    baseAxios.post(url, rows).then((response) => {
        setStep(4)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}


