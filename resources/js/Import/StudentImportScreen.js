import React, {useState} from "react";
import StudentSteps from "./components/students/StudentSteps";
import UploadStep from "./components/students/UploadStep";
import StudentConfigStep from "./components/students/StudentConfigStep";
import ValidateStep from "./components/students/ValidateStep";
import StudentResultStep from "./components/students/StudentResultStep";
import ClassroomResultStep from "./components/classrooms/ClassroomResultStep";
import ClassroomValidateStep from "./components/classrooms/ClassroomValidateStep";
import StudentValidateStep from "./components/students/ValidateStep";

const StudentImportScreen = () => {
    const [step, setStep] = useState(1)
    const [importData, setImportData] = useState([])
    return (
        <div className={"container-xxl p-5"}>
            <div className={"my-2 h5"}>Upload Học sinh + thẻ học</div>
            <StudentSteps step={step} setStep={setStep} importData={importData}/>
            <div className={"my-3"}>
                {
                    step === 1 &&
                    <UploadStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 2 &&
                    <StudentConfigStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 3 &&
                    <StudentValidateStep step={step} setStep={setStep} data={importData} setData={setImportData} />
                }
                {
                    step === 4 &&
                    <StudentResultStep step={step} setStep={setStep} data={importData} setData={setImportData}/>
                }
            </div>
        </div>
    )
}

export default StudentImportScreen
