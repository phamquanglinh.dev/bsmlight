import React, {useRef, useState} from "react";
import {toast} from "react-toastify";

const SupporterUploadStep = ({data, setData, step, setStep}) => {
    const excelFileRef = useRef(null)
    const [fileName, setFileName] = useState('')
    const isExcelFile = (file) => {
        const validExtensions = ['xlsx', 'xls'];
        const fileExtension = file.name.split('.').pop().toLowerCase();
        const mimeType = file.type;

        return validExtensions.includes(fileExtension) &&
            (mimeType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
                mimeType === 'application/vnd.ms-excel');
    };

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        if (file && isExcelFile(file)) {
            console.log("File is a valid Excel file:", file);
            setFileName(file.name)
            setData({
                ...data,
                raw_file: file
            })

        } else {
            toast.error("File bị sai định dạng excel")
        }
    };

    const handleChangeToConfig = () => {
        if (data?.raw_file !== undefined) {
            setStep(2)
        } else {
            toast.error("File không được để trống")
        }
    }

    return (
        <div className={"my-2"}>
            <div className={"d-flex p-2 justify-content-center align-items-center w-100 border round shadow-lg"}
                 style={{height: "200px"}}>
                <div onClick={() => excelFileRef.current.click()}
                     className={"cursor-pointer d-flex align-items-center"}>
                    <span className="mdi mdi-file-excel me-2 mdi-24px"></span>
                    <span className={"h5 mb-0"}>{fileName || 'Tải file của bạn lên'}</span>
                </div>
            </div>
            <input ref={excelFileRef} onChange={handleFileChange} className={"d-none"} type={"file"}/>
            <div className={"my-2 d-flex justify-content-between"}>
                <a target={"_blank"} href={window.location.origin + "/s/supporter/import/template"} className={"my-2"}>Tải teplate mẫu</a>
                <button onClick={handleChangeToConfig} className={"btn btn-primary"}>Tiếp theo</button>
            </div>
        </div>
    )
}
export default SupporterUploadStep
