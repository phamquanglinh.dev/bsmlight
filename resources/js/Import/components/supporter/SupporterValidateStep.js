import React, {useEffect, useState} from "react";
import {classroomImport, studentImport, supporterImport, teacherImport} from "../../api/importApi";

const SupporterValidateStep = ({data, setData, step, setStep}) => {
    const handleRestart = () => {
        setData({})
        setStep(1)
    }

    const handleImport = () => {
        const validImportData = data?.validate_collection?.rows;
        supporterImport({
            rows: validImportData,
            setStep: setStep
        })
    }

    return (
        <div className={"my-2"}>
            <div className={"d-flex p-2 w-100 border round shadow-lg"}>
                <div className={"cursor-pointer d-flex"}>
                    <div>
                        <div className={"mb-3"}>Số lớp hợp lệ: <span
                            className={"text-success fw-bold"}> {data?.validate_collection?.valid_classroom}</span></div>
                        {data?.validate_collection?.invalid_student > 0 &&
                            <div className={"mb-3"}>Số lớp không hợp lệ: <span
                                className={"text-danger fw-bold"}> {data?.validate_collection?.invalid_classroom}</span>
                            </div>
                        }
                        <div className="small">
                            {data?.validate_collection?.errors.map((error, key) => (
                                <div key={key} className="mb-3">
                                    <div>Dòng số {error?.line}: {error?.message}</div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>

            <div className={"my-2"}>
                {data?.validate_collection?.can_import === true ?
                    <div>
                        <button onClick={handleImport} className={"btn btn-primary"}>Tải lên</button>
                    </div>
                    :
                    <div>
                        <button onClick={handleRestart} className={"btn btn-secondary"}>Tải lại file</button>
                    </div>
                }
            </div>
        </div>
    );
}

export default SupporterValidateStep
