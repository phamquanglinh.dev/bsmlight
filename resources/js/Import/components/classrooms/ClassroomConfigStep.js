import React, {useEffect, useState} from "react";
import * as XLSX from "xlsx";
import {
    getAllClassroomTitles,
    getAllStudentTitles,
    validateClassroomImport,
    validateStudentImport
} from "../../api/importApi";
import Select from "react-select";
import {customStyles} from "../../../StudyLog/components/WorkingShifts";
import {toast} from "react-toastify";

const ClassroomConfigStep = ({data, setData, step, setStep}) => {
    const [headers, setHeaders] = useState([])
    const [rows, setRows] = useState([])
    const [titles, setTitles] = useState([])
    const [validationCollection, setValidateCollection] = useState({})
    useEffect(() => {
        if (data?.raw_file) {
            const reader = new FileReader();

            reader.readAsArrayBuffer(data?.raw_file);

            reader.onloadend = (event) => {
                const arrayBuffer = event.target.result;
                const workbook = XLSX.read(arrayBuffer, {type: 'array'});
                const firstSheetName = workbook.SheetNames[0];
                const worksheet = workbook.Sheets[firstSheetName];
                const jsonData = XLSX.utils.sheet_to_json(worksheet, {header: 1});
                setHeaders(jsonData[0])
                setRows(jsonData)
            };

        }
    }, [data.raw_file]);

    useEffect(() => {
        setData({
            ...data,
            rows: rows
        })
    }, [rows])

    useEffect(() => {
        getAllClassroomTitles({setTitles: setTitles})
    }, [])

    useEffect(() => {
        const initMappedValue = headers.map((header, key) => {
            const mapping = titles.find(m => m.label === header)
            return {
                name: mapping ? mapping.name : "",
                label: header
            }
        })

        setData({
            ...data, mappedTitles: initMappedValue
        })

    }, [titles, headers]);

    const handleValidate = () => {
        validateClassroomImport({data: data, setData: setData})
    }

    useEffect(() => {
        if (data?.validate_collection) {
            setStep(3)
        }
    }, [data?.validate_collection])

    return (
        <div>
            <div className={"my-3 text-danger"}>
                Vui lòng kiểm tra tên tiêu đề của excel (bên trái) đã khớp với BSM (bên phải) hay chưa, nếu tiêu đề nào chưa có trường bên BSM, hãy chọn trường tương ứng
            </div>
            {data?.mappedTitles?.map((mappedTitle, key) =>
                <div key={key}>
                    <div className={"row mb-2 align-items-center"}>
                        <div className={"col-6"}>{mappedTitle?.label}</div>
                        <div className={"col-6"}>
                            <Select
                                className="basic-single"
                                classNamePrefix="select"
                                onChange={(r) => {
                                    console.log(r)
                                    const newMappedTitles = data?.mappedTitles.map((m, k) => {
                                        if (m.name === mappedTitle.name) {
                                            if (m.label !== r?.label) {
                                                if (data.mappedTitles.find(f => f.label === r?.label)) {
                                                    toast.error("Không thể chọn trùng lặp")
                                                    return m
                                                }
                                            }

                                            return r;
                                        }


                                        return m
                                    })

                                    setData({
                                        ...data,
                                        mappedTitles: newMappedTitles
                                    })
                                }}
                                value={titles.find((v) => v.name === mappedTitle?.name)}
                                isClearable={true}
                                isSearchable={true}
                                name="supporter"
                                options={titles}
                                styles={customStyles}
                            />
                        </div>
                    </div>
                </div>
            )}

            <div className={"my-2 d-flex justify-content-between"} onClick={handleValidate}>
                <button onClick={() => {
                    setData({})
                    setStep(1)
                }} className={"btn btn-secondary"}>Tải lên file khác
                </button>
                <button className={"btn btn-primary"}>Tiếp tục</button>
            </div>
        </div>
    )
}

export default ClassroomConfigStep
