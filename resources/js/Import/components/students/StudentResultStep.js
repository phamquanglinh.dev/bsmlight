import React from "react";

const StudentResultStep = ({data, setData, step, setStep}) => {
    return (
        <div className={"my-2"}>
            <div className={"d-flex p-2 justify-content-center align-items-center w-100 border round shadow-lg"}
                 style={{height: "200px"}}>
                <div className={"text-success"}>Tải lên thành công</div>
            </div>
            <div className={"my-2"}>
                <button onClick={() => {
                    window.location.href = window.location.origin + "/"
                }} className={"btn btn-primary"}>Quay về trang chủ
                </button>
            </div>
        </div>
    )
}

export default StudentResultStep
