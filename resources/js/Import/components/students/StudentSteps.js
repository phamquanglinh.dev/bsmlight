import React, {useEffect, useState} from "react";

const StudentSteps = ({step, setStep, importData}) => {
    const [stepStatus, setStepStatus] = useState({
        uploadStep: "active",
        settingStep: "",
        validationStep: "",
        resultStep: ""
    })

    useEffect(() => {
        switch (step) {
            case 1:
                setStepStatus({
                    uploadStep: "active",
                    settingStep: "",
                    validationStep: "",
                    resultStep: ""
                })
                break;
            case 2:
                setStepStatus({
                    uploadStep: "crossed",
                    settingStep: "active",
                    validationStep: "",
                    resultStep: ""
                })
                break;
            case 3:
                setStepStatus({
                    uploadStep: "crossed",
                    settingStep: "crossed",
                    validationStep: "active",
                    resultStep: ""
                })
                break;
            case 4:
                setStepStatus({
                    uploadStep: "crossed",
                    settingStep: "crossed",
                    validationStep: "crossed",
                    resultStep: "active"
                })
                break;
            default:
                break;
        }
    },[step])

    const handleChangeStep = (stepChange) => {
        // switch (stepChange) {
        //     case 1:
        //         setStep(1)
        //         setStepStatus({
        //             uploadStep: "active",
        //             settingStep: "",
        //             validationStep: "",
        //             resultStep: ""
        //         })
        //         break;
        //     case 2:
        //         setStep(2)
        //         setStepStatus({
        //             uploadStep: "crossed",
        //             settingStep: "active",
        //             validationStep: "",
        //             resultStep: ""
        //         })
        //         break;
        //     case 3:
        //         setStep(3)
        //         setStepStatus({
        //             uploadStep: "crossed",
        //             settingStep: "crossed",
        //             validationStep: "active",
        //             resultStep: ""
        //         })
        //         break;
        //     case 4:
        //         setStep(4)
        //         setStepStatus({
        //             uploadStep: "crossed",
        //             settingStep: "crossed",
        //             validationStep: "crossed",
        //             resultStep: "active"
        //         })
        //         break;
        //     default:
        //         break;
        // }
    }

    return (
        <div>
            <div className="bs-stepper wizard-numbered">
                <div className="bs-stepper-header">
                    <div onClick={() => handleChangeStep(1)} className={"step " + (stepStatus.uploadStep)}>
                        <button type="button" className="step-trigger">
                            <span className="bs-stepper-circle"><i className="ri-check-line"></i></span>
                            <span className="bs-stepper-label">
                                <span className="bs-stepper-number">01</span>
                                <span className="d-flex flex-column gap-1 ms-2">
                                <span className="bs-stepper-title">Upload</span>
                                    <span className="bs-stepper-subtitle">Upload Excel File</span>
                                </span>
                            </span>
                        </button>
                    </div>
                    <div className="line"></div>
                    <div onClick={() => handleChangeStep(2)} className={"step " + (stepStatus.settingStep)}>
                        <button type="button" className="step-trigger">
                            <span className="bs-stepper-circle"><i className="ri-check-line"></i></span>
                            <span className="bs-stepper-label">
                                <span className="bs-stepper-number">02</span>
                                <span className="d-flex flex-column gap-1 ms-2">
                                <span className="bs-stepper-title">Setting</span>
                                    <span className="bs-stepper-subtitle">Config Your Data</span>
                                </span>
                            </span>
                        </button>
                    </div>
                    <div className="line"></div>
                    <div onClick={() => handleChangeStep(3)} className={"step " + (stepStatus.validationStep)}>
                        <button type="button" className="step-trigger">
                            <span className="bs-stepper-circle"><i className="ri-check-line"></i></span>
                            <span className="bs-stepper-label">
                                <span className="bs-stepper-number">03</span>
                                <span className="d-flex flex-column gap-1 ms-2">
                                <span className="bs-stepper-title">Validation</span>
                                    <span className="bs-stepper-subtitle">Validate Your Data</span>
                                </span>
                            </span>
                        </button>
                    </div>
                    <div className="line"></div>
                    <div className={"step " + (stepStatus.resultStep)}>
                        <button type="button" className="step-trigger">
                            <span className="bs-stepper-circle"><i className="ri-check-line"></i></span>
                            <span className="bs-stepper-label">
                                <span className="bs-stepper-number">04</span>
                                <span className="d-flex flex-column gap-1 ms-2">
                                <span className="bs-stepper-title">Result</span>
                                    <span className="bs-stepper-subtitle">Upload Result</span>
                                </span>
                            </span>
                        </button>
                    </div>
                    <div className="line"></div>
                </div>
            </div>
        </div>
    );
}
export default StudentSteps
