import React, {useState} from "react";
import TeacherSteps from "./components/teacher/TeacherSteps";
import TeacherUploadStep from "./components/teacher/TeacherUploadStep";
import TeacherConfigStep from "./components/teacher/TeacherConfigStep";
import TeacherValidateStep from "./components/teacher/TeacherValidateStep";
import TeacherResultStep from "./components/teacher/TeacherResultStep";

const TeacherImportScreen = () => {
    const [step, setStep] = useState(1)
    const [importData, setImportData] = useState([])
    return (
        <div className={"container-xxl p-5"}>
            <div className={"my-2 h5"}>Upload giáo viên</div>
            <TeacherSteps step={step} setStep={setStep} importData={importData}/>
            <div className={"my-3"}>
                {
                    step === 1 &&
                    <TeacherUploadStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 2 &&
                    <TeacherConfigStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 3 &&
                    <TeacherValidateStep step={step} setStep={setStep} data={importData} setData={setImportData} />
                }
                {
                    step === 4 &&
                    <TeacherResultStep step={step} setStep={setStep} data={importData} setData={setImportData}/>
                }
            </div>
        </div>
    )
}

export default TeacherImportScreen
