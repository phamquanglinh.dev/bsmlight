import React, {Component, useEffect, useState} from "react";

import ReactDOM from "react-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {BrowserRouter as Router, useRoutes} from 'react-router-dom';
import StudentImportScreen from "./StudentImportScreen";
import ClassroomImportScreen from "./ClassroomImportScreen";
import TeacherImportScreen from "./TeacherImportScreen";
import {getAbility} from "./api/importApi";
import LimitImportScreen from "./LimitImportScreen";
import SupporterImportScreen from "./SupporterImportScreen";
import StaffImportScreen from "./StaffImportScreen";

const App = () => {
    const [ability, setAbility] = useState({
        student: 1,
        teacher: 1,
        staff: 1,
        classroom: 1,
        supporter: 1
    })

    useEffect(() => {
        getAbility({setAbility: setAbility})
    }, []);

    return useRoutes([
        {
            path: '/student/import/',
            element: ability?.student === 1 ? <StudentImportScreen/> : <LimitImportScreen/>
        },
        {
            path: '/classroom/import/',
            element: ability?.classroom === 1 ? <ClassroomImportScreen/> : <LimitImportScreen/>
        },
        {
            path: '/teacher/import/',
            element: ability?.teacher ? <TeacherImportScreen/> : <LimitImportScreen/>
        },
        {
            path: '/supporter/import/',
            element: ability?.supporter ? <SupporterImportScreen/> : <LimitImportScreen/>
        },
        {
            path: '/staff/import/',
            element: ability?.staff ? <StaffImportScreen/> : <LimitImportScreen/>
        },
    ]);
}

class StudyLogProvider extends Component {
    render() {
        return (
            <React.StrictMode>
                <Router>
                    <App/>
                    <ToastContainer/>
                </Router>
            </React.StrictMode>
        )
    }
}

ReactDOM.render(<StudyLogProvider/>, document.getElementById('import-react'))
