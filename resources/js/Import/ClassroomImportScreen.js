import React, {useState} from "react";
import StudentStepsHeading from "./components/students/StudentSteps";
import StudentUploadStep from "./components/students/UploadStep";
import StudentConfigStep from "./components/students/StudentConfigStep";
import StudentValidateStep from "./components/students/ValidateStep";
import StudentResultStep from "./components/students/StudentResultStep";
import ClassroomSteps from "./components/classrooms/ClassroomSteps";
import ClassroomUploadStep from "./components/classrooms/ClassroomUploadStep";
import ClassroomConfigStep from "./components/classrooms/ClassroomConfigStep";
import ClassroomValidateStep from "./components/classrooms/ClassroomValidateStep";
import ClassroomResultStep from "./components/classrooms/ClassroomResultStep";

const ClassroomImportScreen = () => {
    const [step, setStep] = useState(1)
    const [importData, setImportData] = useState([])
    return (
        <div className={"container-xxl p-5"}>
            <div className={"my-2 h5"}>Upload lớp học</div>
            <ClassroomSteps step={step} setStep={setStep} importData={importData}/>
            <div className={"my-3"}>
                {
                    step === 1 &&
                    <ClassroomUploadStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 2 &&
                    <ClassroomConfigStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 3 &&
                    <ClassroomValidateStep step={step} setStep={setStep} data={importData} setData={setImportData} />
                }
                {
                    step === 4 &&
                    <ClassroomResultStep step={step} setStep={setStep} data={importData} setData={setImportData}/>
                }
            </div>
        </div>
    )
}

export default ClassroomImportScreen
