import React, {useState} from "react";
import TeacherSteps from "./components/teacher/TeacherSteps";
import TeacherUploadStep from "./components/teacher/TeacherUploadStep";
import TeacherConfigStep from "./components/teacher/TeacherConfigStep";
import TeacherValidateStep from "./components/teacher/TeacherValidateStep";
import TeacherResultStep from "./components/teacher/TeacherResultStep";
import SupporterSteps from "./components/supporter/SupporterSteps";
import SupporterUploadStep from "./components/supporter/SupporterUploadStep";
import SupporterConfigStep from "./components/supporter/SupporterConfigStep";
import SupporterValidateStep from "./components/supporter/SupporterValidateStep";
import SupporterResultStep from "./components/supporter/SupporterResultStep";

const SupporterImportScreen = () => {
    const [step, setStep] = useState(1)
    const [importData, setImportData] = useState([])
    return (
        <div className={"container-xxl p-5"}>
            <div className={"my-2 h5"}>Upload trợ giảng</div>
            <SupporterSteps step={step} setStep={setStep} importData={importData}/>
            <div className={"my-3"}>
                {
                    step === 1 &&
                    <SupporterUploadStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 2 &&
                    <SupporterConfigStep setData={setImportData} data={importData} step={step} setStep={setStep}/>
                }
                {
                    step === 3 &&
                    <SupporterValidateStep step={step} setStep={setStep} data={importData} setData={setImportData} />
                }
                {
                    step === 4 &&
                    <SupporterResultStep step={step} setStep={setStep} data={importData} setData={setImportData}/>
                }
            </div>
        </div>
    )
}

export default SupporterImportScreen
