import CashFlowIncomePieChart from "./components/CashflowIncomePieChart";
import CashFlowOuterPieChart from "./components/CashflowOuterPieChart";
import CashFlowStatistic from "./components/CashFlowStatistic";
import React, {useState} from "react";
import EducationGrossBarChart from "./components/EducationGrossBarChart";
import DatePicker from "react-datepicker";

const EducationGrossScreen = () => {
    const [month, setMonth] = useState((new Date()))
    return (
        <div className={"container-xxl"}>
            <div className={"h5 my-5"}>
                <span>Báo cáo tài chính</span>
                <span className={"ms-3 text-primary input-text-primary"}>
                    <DatePicker
                        selected={month}
                        onChange={(date) => setMonth(date)}
                        showMonthYearPicker
                        dateFormat="MM/YYYY"
                    />
                </span>
            </div>
            <div className={"my-2"}>
                <button onClick={() => {
                    window.location.href = window.location.origin + "/report/cash_flow"
                }} className={"btn btn-secondary me-2"}>Cashflow
                </button>
                <button className={"btn btn-primary "}>Gross Đào tạo</button>

            </div>
            <div className={"row"}>
                <div className={"col-12 p-3"}>
                    <EducationGrossBarChart time={month}/>
                </div>
            </div>
        </div>
    )
}
export default EducationGrossScreen
