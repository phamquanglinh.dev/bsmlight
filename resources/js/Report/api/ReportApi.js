import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/s/report"
const baseAxios = axios
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};


export const getCashFlowPie = ({setPieData, type, time = null}) => {
    const url = baseUrl + "/cash_flow_pie?type=" + type + (time ? "&time=" + time.getTime() / 1000 : '')

    baseAxios.get(url).then((response) => {
        setPieData(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getCashFlowStatistic = ({setPieStatisticData, time}) => {
    const url = baseUrl + "/cash_flow_statistic" + (time ? "?time=" + time.getTime() / 1000 : "")

    baseAxios.get(url).then((response) => {
        setPieStatisticData(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getEducationBarChart = ({setData, time}) => {
    const url = baseUrl + "/education_gross_data" + (time ? "?time=" + time.getTime() / 1000 : '')

    baseAxios.get(url).then((response) => {
        setData(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}


