import React, {useState} from "react";
import CashFlowIncomePieChart from "./components/CashflowIncomePieChart";
import CashFlowOuterPieChart from "./components/CashflowOuterPieChart";
import CashFlowStatistic from "./components/CashFlowStatistic";
import DatePicker from "react-datepicker";

const CashFlowScreen = () => {
    const [status, setStatus] = useState(true)
    const [month, setMonth] = useState((new Date()))
    return (
        <div className={"container-xxl"}>
            <div className={"h5 my-5"}>
                <span>Báo cáo tài chính</span>
                <span className={"ms-3 text-primary input-text-primary"}>
                    <DatePicker
                        selected={month}
                        onChange={(date) => setMonth(date)}
                        showMonthYearPicker
                        dateFormat="MM/YYYY"
                    />
                </span>
            </div>
            <div className={"my-2"}>
                <button className={"btn btn-primary me-2"}>Cashflow</button>
                <button onClick={() => {
                    window.location.href = window.location.origin + "/report/education_gross"
                }} className={"btn btn-secondary"}>Gross Đào tạo
                </button>
            </div>
            <div className={"row"}>
                <div className={"col-md-4 col-6 p-3"}>
                    <CashFlowIncomePieChart time={month}/>
                </div>
                <div className={"col-md-4 col-6 p-3"}>
                    <CashFlowOuterPieChart time={month}/>
                </div>
                <div className={"col-md-4 col-6 p-3"}>
                    <CashFlowStatistic time={month}/>
                </div>
            </div>
        </div>
    )
}

export default CashFlowScreen
