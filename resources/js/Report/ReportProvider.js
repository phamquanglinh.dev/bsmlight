import React, {Component, useEffect, useState} from "react";

import ReactDOM from "react-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {BrowserRouter as Router, useRoutes} from 'react-router-dom';
import CashFlowScreen from "./CashFlowScreen";
import EducationGrossScreen from "./EducationGrossScreen";
import "react-datepicker/dist/react-datepicker.css";

const App = () => {
    return useRoutes([
        {
            path: 'report/cash_flow',
            element: <CashFlowScreen/>
        },
        {
            path: 'report/education_gross',
            element: <EducationGrossScreen/>
        },
    ]);
}

class ReportProvider extends Component {
    render() {
        return (
            <React.StrictMode>
                <Router>
                    <App/>
                    <ToastContainer/>
                </Router>
            </React.StrictMode>
        )
    }
}

ReactDOM.render(<ReportProvider/>, document.getElementById('report-react'))
