import {BarChart} from "@mui/x-charts/BarChart";
import React, {useEffect, useState} from "react";
import {getEducationBarChart} from "../api/ReportApi";

const EducationGrossBarChart = ({time}) => {
    const [data, setData] = useState({})
    useEffect(() => {
        getEducationBarChart({setData: setData, time: time})
    }, [time]);
    return (
        <div>
            <BarChart
                dataset={data?.dataset || []}
                xAxis={[{scaleType: 'band', data: data?.labels || []}]}
                series={data.series || []}
                height={300}
            />

            <div>
                <div className="table-responsive text-nowrap">
                    <table className="table table-bordered">
                        <thead>
                        <tr>
                            {data?.tableTitles?.map((title, key) =>
                                <th className={"bg-primary text-white fw-bold"} style={key === 0 ? fixed : null}
                                    key={key}>{title.label}</th>
                            )}
                        </tr>
                        </thead>
                        <tbody>
                        {data?.dataTable?.map((item, dkey) =>
                            <tr key={dkey} className={dkey === 0 ? "text-primary fw-bold" : null}>
                                {data?.tableTitles?.map((title, key) =>
                                    <td style={key === 0 ? fixed : null}
                                        key={key}>{typeof item[title?.key] == 'number' ? item[title?.key].toLocaleString() : item[title?.key]}</td>
                                )}
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
export default EducationGrossBarChart

const fixed = {
    position: "sticky",
    left: 0,
    background: "#28243d"
}
