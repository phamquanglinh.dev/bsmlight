import * as React from 'react';
import {BarChart} from '@mui/x-charts/BarChart';
import {pieArcLabelClasses, PieChart} from "@mui/x-charts";
import {useEffect, useState} from "react";
import {getCashFlowPie} from "../api/ReportApi";

const CashFlowIncomePieChart = ({time = null}) => {
    const [pieData, setPieData] = useState({})
    useEffect(() => {
        getCashFlowPie({setPieData: setPieData, type: "in", time: time})
    }, [time]);
    return (
        <div className={"pie-chart-wrapper p-3 shadow rounded"}>
            <div style={{width: "400px"}}>
                <PieChart
                    series={[{
                        data: pieData?.data || [],
                        innerRadius: 30,
                        outerRadius: 150,
                        paddingAngle: 5,
                        cornerRadius: 5,
                    }]}
                    width={500}
                    height={500}
                />
            </div>
            <div className={"my-2 row justify-content-between fw-bold"}>
                <div className={"text-white col-6"}>Tổng thu</div>
                <div className={"text-white col-6 text-end"}>{pieData?.total?.toLocaleString()} đ</div>
            </div>
            {pieData?.data?.map((chart, key) =>
                <div key={key} className={"my-2 row d-flex justify-content-between"}>
                    <div className={"text-white col-6 d-flex"}>
                        <div style={{color: chart?.color}}>{chart?.label}</div>
                    </div>
                    <div className={"text-white col-6 text-end"}>{chart?.value?.toLocaleString()} đ ({chart?.percent})
                        %
                    </div>
                </div>
            )}
        </div>
    );
}

export default CashFlowIncomePieChart
