import * as React from 'react';
import {BarChart} from '@mui/x-charts/BarChart';
import {pieArcLabelClasses, PieChart} from "@mui/x-charts";
import {useEffect, useState} from "react";
import {getCashFlowPie, getCashFlowStatistic} from "../api/ReportApi";

const CashFlowStatistic = ({time}) => {
    const [pieStatistic, setPieStatistic] = useState({})
    useEffect(() => {
        getCashFlowStatistic({setPieStatisticData: setPieStatistic, type: "out", time:time})
    }, [time]);
    return (
        <div className={"pie-chart-wrapper pie-chart-wrapper p-3 shadow rounded"}>
            <div className={"my-2 row justify-content-between fw-bold"}>
                <div className={"text-white col-6"}>Tổng Thu</div>
                <div className={"text-white col-6 text-end"}>{pieStatistic?.revenue?.toLocaleString()} đ</div>
            </div>
            <div className={"my-2 row justify-content-between fw-bold"}>
                <div className={"text-white col-6"}>Tổng Chi</div>
                <div className={"text-white col-6 text-end"}>{pieStatistic?.payment?.toLocaleString()} đ</div>
            </div>
            <div className={"my-2 row justify-content-between fw-bold"}>
                <div className={"text-white col-6"}>Tổng</div>
                <div
                    className={"col-6 text-end text-" + (pieStatistic?.color)}>{pieStatistic?.total?.toLocaleString()} đ
                </div>
            </div>
        </div>
    );
}

export default CashFlowStatistic
