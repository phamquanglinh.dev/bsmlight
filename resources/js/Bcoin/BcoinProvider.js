import React, {Component} from "react";

import ReactDOM from "react-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {BrowserRouter as Router, useRoutes} from 'react-router-dom';
import BcoinScreen from "./BcoinScreen";

const App = () => {
    return useRoutes([
        {path: '/bcoin/connect', element: <BcoinScreen/>},
    ]);
}

class BcoinProvider extends Component {
    render() {
        return (
            <React.StrictMode>
                <Router>
                    <App/>
                    <ToastContainer/>
                </Router>
            </React.StrictMode>
        )
    }
}

ReactDOM.render(<BcoinProvider/>, document.getElementById('bcoin-react'))
