import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/s/bcoin"
const baseAxios = axios.create({
    retries: 3,
    timeout: 100000,
    retryDelay: 100
})
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};


export const createRechargeAction = ({amount, setRechargeData}) => {
    const url = baseUrl + "/recharge"

    baseAxios.post(url, {
        amount: amount
    }).then((r) => {
        setRechargeData(r.data.data)
        toast("Thành công")
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
    })
}
