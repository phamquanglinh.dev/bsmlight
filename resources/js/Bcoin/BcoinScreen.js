import React, {useState} from "react";
import {createRechargeAction} from "./api/BcoinApi";
import {toast} from "react-toastify";

const BcoinScreen = () => {
    const [amount, setAmount] = useState()
    const [recharge, setRecharge] = useState()
    const handleRecharge = () => {
        if (amount === undefined) {
            toast("Chưa nhập số tiền")
            return
        }
        if (amount < 100) {
            toast("Nạp tối thiểu 100 Bcoin")
            return
        }
        createRechargeAction({amount: amount, setRechargeData: setRecharge})
    }

    return (
        <div className={"row"}>
            <div className={"col-6"}>
                <div className={"border p-4 rounded shadow-lg mt-5"}>
                    <div className={"mb-2"}>Nạp Bcoin</div>
                    <div className={"small mb-2"}>Đơn giá: 1.000 đ = 1 Bcoin</div>
                    <div className={"mb-2"}>
                        <input value={amount} onChange={(r) => {
                            setAmount(r?.currentTarget?.value)
                        }} className={"form-control mb-1"} placeholder={"Số tiền"}/>
                    </div>
                    <div className={"mt-3"}>
                        <a onClick={handleRecharge}
                           className={"cursor-pointer small bg-primary text-white border-none rounded mt-2 p-2"}>Xác
                            nhận nạp</a>
                    </div>

                    <div className={"my-2"}>
                        {recharge?.code === "00" &&
                            <div className={"text-success small"}>Tạo giao dịch nạp tiền thành công, vui lòng quét QR để hoàn tất</div>
                        }
                        {recharge?.data?.qrDataURL &&
                            <div className={"p-2"}>
                                <img src={recharge?.data?.qrDataURL} className={"w-100 rouned"}/>
                            </div>
                        }
                    </div>
                </div>
            </div>
            <div className={"col-6"}>
                <div className={"border p-4 rounded shadow-lg mt-5"}></div>
            </div>
        </div>
    )
}

export default BcoinScreen
