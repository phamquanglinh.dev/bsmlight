import React, {useState} from "react";
import AccountCustomField from "./tabs/AccountCustomField";
import IntegrationTab from "./tabs/IntegrationTab";

const Setting = () => {
    const [tab, setTab] = useState(window.location.hash)
    return (
        <div className={"container-fluid mt-2"}>
            <div className={"text-light h3 my-3"}>Cài đặt</div>
            <div>
                <button type="button" onClick={() => {
                    window.location.hash = "#account"
                    setTab(window.location.hash)
                }} className={"btn me-2 " + (tab === "#account" ? " btn-primary " : "btn-light")}>Khách hàng
                </button>
                <button onClick={() => {
                    window.location.hash = "#account_source"
                    setTab(window.location.hash)
                }} type="button"
                        className={"btn me-2 " + (tab === "#account_source" ? " btn-primary " : "btn-light")}>Nguồn
                    khách hàng
                </button>
                <button type="button"
                        onClick={() => {
                            window.location.hash = "#account_relation"
                            setTab(window.location.hash)
                        }}
                        className={"btn me-2 " + (tab === "#account_relation" ? " btn-primary " : "btn-light")}>Trạng
                    thái
                    khách hàng
                </button>
                <button type="button"
                        onClick={() => {
                            window.location.hash = "#integration"
                            setTab(window.location.hash)
                        }}
                        className={"btn me-2 " + (tab === "#integration" ? " btn-primary " : "btn-light")}>
                    Tích hợp
                </button>
            </div>
            <div className={"tabs-content my-2"}>
                <div>
                    {tab === '#account' ? <AccountCustomField/> : null}
                    {tab === '#integration' ? <IntegrationTab/> : null}
                </div>
            </div>
        </div>
    );
}
export default Setting
