import qs from "qs";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/integration"
const baseAxios = axios
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

export const getGoogleConnectUrl = () => {
    const url = baseUrl + "/google_form/connect_url"
    baseAxios.get(url).then((response) => {
        window.location.href = response.data.data.url
    }).catch((error) => {
        console.log(error)
    })
}

export const getGoogleFormConfig = ({setConfig}) => {
    const url = baseUrl + "/google_form/config"
    baseAxios.get(url).then((response) => {
        setConfig(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getSheetTabBySheetId = ({setTabs, sheetId}) => {
    const url = baseUrl + "/google_form/tabs/" + sheetId
    baseAxios.get(url).then((response) => {
        toast.success("Thành công")
        setTabs(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra, vui lòng thử lại")
        console.log(error)
    })
}

export const getSheetByTabIdAndSpreadsheetId = ({setSheet, tabId, spreadsheetId}) => {
    const url = baseUrl + "/google_form/tab"
    baseAxios.post(url, {
        spreadsheetId: spreadsheetId,
        tabId: tabId
    }).then((response) => {
        toast.success("Thành công")
        setSheet(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra, vui lòng thử lại")
        console.log(error)
    })
}

export const createApplicationCode = ({form, setCode}) => {
    const url = baseUrl + "/google_form/create_code"
    baseAxios.post(url, {
        form: form
    }).then((response) => {
        setCode(response.data.data.script)
        toast.success("Tạo code thành công")
    }).catch((error) => {
        toast.error("Có lỗi xảy ra, vui lòng thử lại")
        console.log(error)
    })
}

export const createOrUpdateCustomFields = ({name, form, setErrors, setShow, setForm}) => {
    if (name === undefined || name === null) {
        const url = baseUrl + "/custom_field/account/create"
        baseAxios.post(url, {
            form: form
        }).then((response) => {
            toast.success('Thêm mới thành công')
            setShow(false)
            setForm({})
        }).catch((error) => {
            toast.error("Không thành công. Vui lòng kiểm tra lại")
            console.log('a', error.response.data.errors)
            setErrors(error.response.data.errors)
        })
    } else {
        const url = baseUrl + "/custom_field/account/update"
        baseAxios.put(url, {
            form: form
        }).then((response) => {
            toast.success('Thêm mới thành công')
            setShow(false)
            setForm({})
        }).catch((error) => {
            toast.error("Không thành công. Vui lòng kiểm tra lại")
            console.log('a', error.response.data.errors)
            setErrors(error.response.data.errors)
        })
    }
}

export const getCustomFieldDetailByName = ({name, setCustomField}) => {
    const url = baseUrl + "/custom_field/account/detail/" + name
    baseAxios.get(url).then((response) => {
        setCustomField(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
    })
}

export const deleteCustomFieldByName = ({name, setShow}) => {
    const url = baseUrl + "/custom_field/account/delete/" + name
    baseAxios.delete(url).then((response) => {
        toast.success('Xoá thành công')
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
    })
}
