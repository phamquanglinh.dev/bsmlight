import React, {Component} from "react";
import ReactDOM from "react-dom";
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";
import Setting from "./Setting";

class CustomFieldProvider extends Component {
    render() {
        return (
            <div>
                <Setting/>
                <ToastContainer/>
            </div>
        )
    }
}

ReactDOM.render(<CustomFieldProvider/>, document.getElementById('custom-field-react'))
