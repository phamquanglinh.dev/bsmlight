import {useState} from "react";
import React from "react";
import AccountCustomField from "./AccountCustomField";
import GoogleForm from "../GoogleForm";

const IntegrationTab = () => {
    const [tab, setTab] = useState('google_form')
    return (
        <div className={"row pt-2"}>
            <div className={"col-2"}>
                <ul className="list-group">
                    <li className="list-group-item cursor-pointer">Google Form</li>
                    <li className="list-group-item cursor-pointer">Ladipage</li>
                    <li className="list-group-item cursor-pointer">Nhanh.vn</li>
                </ul>
            </div>
            <div className={"col-10"}>
                {tab === 'google_form' ? <GoogleForm/> : null}
            </div>
        </div>
    );
}
export default IntegrationTab
