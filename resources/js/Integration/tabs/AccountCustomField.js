import React, {useEffect, useState} from "react";
import {getAccountFields} from "../../CRM/api/CrmAccountAPI";
import CreateOrUpdateCustomField from "./CreateOrUpdateCustomField";
import {deleteCustomFieldByName} from "../api/integrationApi";

const AccountCustomField = () => {
    const [fields, setFields] = useState([])
    const [showTab, setShowTab] = useState(false)
    const [customFieldForUpdate, setCustomFieldForUpdate] = useState(null)

    useEffect(() => {
        getAccountFields({setFields})
    }, [showTab]);

    return (
        <div className={"mt-3"}>
            <div className={"mb-3"}>
                <div className={"d-flex justify-content-between align-items-center"}>
                    <div>Định nghĩa dữ liệu khách hàng</div>
                    <div>
                        <button
                            className={"btn btn-primary"}
                            onClick={() => {
                                setCustomFieldForUpdate(null)
                                setShowTab(true)
                            }}>Thêm mới
                        </button>
                    </div>
                </div>
            </div>
            <div className={"mb-2"}>
                <div className="table-responsive text-nowrap">
                    <table className="table table-bordered">
                        <thead>
                        <tr className={"bg-primary"}>
                            <th>#</th>
                            <th className={"text-white"}>Tên thuộc tính</th>
                            <th className={"text-white"}>Mã thuộc tính</th>
                            <th className={"text-white"}>Bắt buộc</th>
                            <th className={"text-white"}>Kiểu</th>
                            <th className={"text-white"}>Cho phép trùng</th>
                            <th className={"text-white"}>Cho phép chọn nhiều</th>
                            <th className={"text-white"}>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {fields.map((fieldData, key) =>
                            <tr key={key}>
                                <td>{key}</td>
                                <td>{fieldData.field_label}</td>
                                <td>{fieldData.field_name}</td>
                                <td>{fieldData.is_required === 0 ? "Không" : "Có"}</td>
                                <td>{fieldData.field_html_type}</td>
                                <td>{fieldData.is_duplicate === 0 ? "Không" : "Có"}</td>
                                <td>{fieldData.is_multiple === 0 ? "Không" : "Có"}</td>
                                <td>
                                    {fieldData.default === 0 ?
                                        <div>
                                            <span onClick={() => {
                                                setCustomFieldForUpdate(fieldData.field_name)
                                                setShowTab(true)
                                            }} className="mdi mdi-square-edit-outline"></span>
                                            <span
                                                onClick={() => {
                                                    const result = confirm('Có chắc muốn xoá !')
                                                    if (result) {
                                                        deleteCustomFieldByName({
                                                            name: fieldData.field_name,
                                                        })
                                                        setFields(fields.filter((f) => f !== fieldData))
                                                    }
                                                }}
                                                className="mdi mdi-delete-outline"></span>
                                        </div>
                                        : null}
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
            <div>
                <CreateOrUpdateCustomField customFieldName={customFieldForUpdate} showTab={showTab}
                                           setShowTab={setShowTab}/>
            </div>
        </div>
    );
}

export default AccountCustomField
