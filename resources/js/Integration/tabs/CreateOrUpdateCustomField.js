import React, {useEffect, useState} from "react";
import {createOrUpdateCustomFields, getCustomFieldDetailByName} from "../api/integrationApi";

const CreateOrUpdateCustomField = ({customFieldName, showTab, setShowTab}) => {
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState([])
    useEffect(() => {
        if (customFieldName !== null && customFieldName !== undefined) {
            getCustomFieldDetailByName({
                name: customFieldName,
                setCustomField: setForm
            })
        } else {
            setForm({
                field_label: "",
                field_name: "",
            })
        }
    }, [showTab, customFieldName])
    return (
        <div>
            <div className={"offcanvas offcanvas-end " + (showTab ? "show" : "")} tabIndex="-1" id="offcanvasEnd"
                 aria-labelledby="offcanvasEndLabel">
                <div className="offcanvas-header">
                    <h5 id="offcanvasEndLabel"
                        className="offcanvas-title">{customFieldName ? "Chỉnh sửa" : "Thêm mới"}</h5>
                    <button onClick={() => setShowTab(false)} type="button" className="btn-close text-reset"></button>
                </div>
                <div className="offcanvas-body my-auto mx-0 flex-grow-0 h-100">
                    <div className={"mb-3"}>
                        <div className="form-floating form-floating-outline">
                            <input value={form.field_label}
                                   onChange={(r) => {
                                       setForm(
                                           {
                                               ...form,
                                               field_label: r.currentTarget.value,
                                               field_name: createSlug(r.currentTarget.value)
                                           }
                                       )
                                   }} type="text" className="form-control"/>
                            <label>Tên thuộc tính</label>
                            <div className={"text-danger small mt-1"}>{errors?.field_name}</div>
                        </div>
                    </div>
                    <div className={"mb-3"}>
                        <div className="form-floating form-floating-outline">
                            <input value={form.field_name} disabled={true} type="text" className="form-control"/>
                            <label>Mã thuộc tính</label>
                        </div>
                    </div>

                    <div className={"mb-3"}>
                        <label className="switch switch-square switch-lg">
                            <input onChange={() => {
                                setForm({
                                    ...form,
                                    is_required: form.is_required === 1 ? 0 : 1
                                })
                            }} type="checkbox" className="switch-input" checked={form.is_required === 1}/>
                            <span className="switch-toggle-slider">
                                <span className="switch-on"></span>
                                <span className="switch-off"></span>
                            </span>
                            <span className="switch-label">Bắt buộc nhập</span>
                        </label>
                    </div>

                    <div className={"mb-3"}>
                        <label className="switch switch-square switch-lg">
                            <input onChange={() => {
                                setForm({
                                    ...form,
                                    is_duplicate: form.is_duplicate === 1 ? 0 : 1
                                })
                            }} type="checkbox" className="switch-input" checked={form.is_duplicate === 1}/>
                            <span className="switch-toggle-slider">
                                <span className="switch-on"></span>
                                <span className="switch-off"></span>
                            </span>
                            <span className="switch-label">Cho phép trùng</span>
                        </label>
                    </div>

                    <div className={"mb-3"}>
                        <div className="form-floating form-floating-outline mb-6">
                            <select
                                value={form.field_html_type}
                                onChange={(r) => {
                                    const value = r.target.value
                                    if (value === 'select' || value === 'checkbox') {
                                        let initLists;
                                        if (form.lists === undefined || form.lists === null) {
                                            initLists = [{
                                                option: ""
                                            }]
                                        } else {
                                            initLists = form.lists
                                        }


                                        setForm({
                                            ...form,
                                            has_list: 1,
                                            field_html_type: value,
                                            lists: initLists
                                        })
                                    } else {
                                        setForm({
                                            ...form,
                                            is_multiple: 0,
                                            lists: [{option: ""}],
                                            has_list: 0,
                                            field_html_type: value
                                        })
                                    }
                                }}
                                className="form-select" id="exampleFormControlSelect1"
                                aria-label="Default select example">
                                <option value="text">Text Input (Chữ đơn thuần)</option>
                                <option value="numeric">Numeric Input (Dạng nhập số)</option>
                                <option value="texarea">Textarea (Dạng đoạn văn bản dài)</option>
                                <option value="select">Select List (Dạng danh sách chon)</option>
                                <option value="checkbox">Checkbox (Dạng hộp kiểm)</option>
                            </select>
                            <label htmlFor="exampleFormControlSelect1">Dạng dữ liệu</label>
                        </div>
                    </div>

                    {form.has_list === 1 &&
                        <div>
                            <div className={"mb-3"}>
                                <label className="switch switch-square switch-lg">
                                    <input onChange={(e) => {
                                        setForm({
                                            ...form,
                                            is_multiple: form.is_multiple === 1 ? 0 : 1
                                        })
                                    }} type="checkbox" className="switch-input" checked={form.is_multiple === 1}/>
                                    <span className="switch-toggle-slider">
                                <span className="switch-on"></span>
                                <span className="switch-off"></span>
                            </span>
                                    <span className="switch-label">Cho phép chọn nhiều dữ liệu</span>
                                </label>
                            </div>

                            {form.lists.map((list, key) =>

                                <div key={key} className={"mb-3"}>
                                    <div className="form-password-toggle">
                                        <div className="input-group input-group-merge">
                                            <div className="form-floating form-floating-outline">
                                                <input type="text" className="form-control"
                                                       value={list.option}
                                                       onChange={(e) => {
                                                           const newList = [...form.lists]
                                                           newList[key].option = e.target.value
                                                           setForm({...form, lists: newList})
                                                       }}
                                                />
                                                <label htmlFor="basic-default-password12">Tên tuỳ chọn</label>
                                            </div>
                                            <span
                                                onClick={() => {
                                                    let newList = [...form.lists]
                                                    newList = newList.filter((item) => item !== list)
                                                    setForm({...form, lists: newList})
                                                }}
                                                className="input-group-text cursor-pointer"><i
                                                className="mdi mdi-trash-can"></i></span>
                                        </div>
                                    </div>
                                </div>
                            )}

                            <div
                                className={"cursor-pointer text-primary my-2"}
                                onClick={() => {
                                    setForm({
                                        ...form,
                                        lists: [...form.lists, {option: ""}]
                                    })
                                }}>
                                Thêm tuỳ chọn
                            </div>

                        </div>
                    }
                    {
                        (customFieldName === null || customFieldName === undefined) ?
                            <button onClick={() => {
                                createOrUpdateCustomFields({
                                    name: customFieldName,
                                    form: form,
                                    setErrors: setErrors,
                                    setShow: setShowTab,
                                    setForm: setForm
                                })
                            }} type="button" className="btn btn-primary mb-2 d-grid w-100">Thêm mới</button>
                            :
                            <button
                                onClick={() => {
                                    createOrUpdateCustomFields({
                                        name: customFieldName,
                                        form: form,
                                        setErrors: setErrors,
                                        setShow: setShowTab,
                                        setForm: setForm
                                    })
                                }}
                                type="button" className="btn btn-primary mb-2 d-grid w-100">Cập nhật</button>
                    }
                    <button onClick={() => {
                        setShowTab(false)
                    }} type="button" className="btn btn-outline-secondary d-grid w-100"
                            data-bs-dismiss="offcanvas">Đóng
                    </button>
                </div>
            </div>
        </div>
    );
}
export default CreateOrUpdateCustomField

function createSlug(slug) {
    //Đổi chữ hoa thành chữ thường
    slug = slug.toLowerCase();

    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "_");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '_');
    slug = slug.replace(/\-\-\-\-/gi, '_');
    slug = slug.replace(/\-\-\-/gi, '_');
    slug = slug.replace(/\-\-/gi, '_');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    return slug
}
