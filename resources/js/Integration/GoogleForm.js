import React, {useEffect, useState} from "react";
import {
    createApplicationCode,
    getGoogleConnectUrl,
    getGoogleFormConfig,
    getSheetByTabIdAndSpreadsheetId,
    getSheetTabBySheetId
} from "./api/integrationApi";
import {toast} from "react-toastify";
import {getAccountFields, getAccounts} from "../CRM/api/CrmAccountAPI";

const GoogleForm = () => {
    const [config, setConfig] = useState({})
    const [spreadSheetId, setSpreadSheetId] = useState("")
    const [tabId, setTabId] = useState("")
    const [tabs, setTabs] = useState([])
    const [columns, setColumns] = useState([])
    const [accountFields, setAccountFields] = useState([])
    const [form, setForm] = useState({})
    const [code, setCode] = useState("")
    useEffect(() => {
        getGoogleFormConfig({
            setConfig: setConfig
        });
        getAccountFields({setFields: setAccountFields})
    }, [])

    const getLoginUrl = () => {
        getGoogleConnectUrl()
    }

    const getTabs = () => {
        getSheetTabBySheetId({setTabs: setTabs, sheetId: spreadSheetId})
    }

    const getColumns = () => {
        getSheetByTabIdAndSpreadsheetId({
            setSheet: setColumns,
            tabId: tabId,
            spreadsheetId: spreadSheetId
        })
    }

    const createCode = () => {
        createApplicationCode({form: form, setCode: setCode})
    }

    function copyCode() {
        navigator.clipboard.writeText(code).then(() => {
            toast.success("Copy Thành công")
        }).catch(() => {
            toast.error("Copy Thất bại")
        })
    }

    return (
        <div className="container-fluid">
            <div className={"mt-4"}>
                {parseInt(config?.is_connected) === 0 ?
                    <div>
                        <button
                            onClick={getLoginUrl}
                            className={"btn btn-primary"}>Kết nối
                        </button>
                    </div>
                    : null}

                {parseInt(config?.is_connected) === 1 ?
                    <div className={"mt-4"}>
                        <div className={"row"}>
                            <div className={"col-9"}>
                                <div className="input-group mb-4">
                                    <input
                                        onChange={(r) => {
                                            setSpreadSheetId(r.target.value)
                                        }}
                                        value={spreadSheetId}
                                        type="text" className="form-control" placeholder="ID Của spreadsheets"/>
                                    <button
                                        onClick={() => {
                                            getTabs()
                                        }}
                                        className="btn btn-outline-primary" type="button"
                                        id="button-addon2">Lấy thông tin
                                    </button>
                                </div>

                                <div className="input-group mb-4">
                                    <select value={tabId}
                                            onChange={(e) => {
                                                const newTabId = e.target.value;
                                                console.log(e.target.value)
                                                setTabId(newTabId)
                                            }}
                                            className="form-select" id="inputGroupSelect02">
                                        <option selected>Chọn sheet...</option>
                                        {tabs.map((sheet, key) =>
                                            <option
                                                key={key}
                                                value={sheet.properties.title}>{sheet.properties.title}</option>
                                        )}
                                    </select>
                                    <button
                                        onClick={getColumns}
                                        className="btn btn-outline-primary" type="button">Chọn Sheet
                                    </button>
                                </div>

                                {columns.length > 0 ?
                                    <div className={"row mb-2"}>
                                        <div className={"col-6"}>Cột Google Spreadsheet</div>
                                        <div className={"col-6"}>Cột dữ liệu tương ứng trên BSM</div>
                                    </div> : null}
                                {columns.map((column, key) =>
                                    <div key={key} className={"row mb-2"}>
                                        <div className={"col-6"}>
                                            <div className="form-floating form-floating-outline">
                                                <input value={column.value} type="text" className="form-control"
                                                       disabled={true}/>
                                            </div>
                                        </div>
                                        <div className={"col-6"}>
                                            <div className="form-floating form-floating-outline">
                                                <select
                                                    onChange={(e) => {
                                                        const name = e.target.value;
                                                        setForm({
                                                            ...form, [key]: {
                                                                google: column,
                                                                bsm: name
                                                            }
                                                        })
                                                    }}

                                                    value={form[key]?.bsm ?? ""}

                                                    className="form-select p-2" id="inputGroupSelect02">
                                                    <option selected>Chọn thuộc tính ....</option>
                                                    {accountFields.map((accountField, accountKey) =>
                                                        <option key={accountKey}
                                                                value={accountField.field_name}>{accountField.field_label}</option>
                                                    )}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                )}
                                {columns.length > 0 ?
                                    <div className={"my-2"}>
                                        <button onClick={createCode} className={"btn btn-primary"}>Tạo mã</button>
                                    </div>
                                    : null}
                            </div>
                        </div>
                        {code !== "" ?
                            <div className={"my-2"}>
                                <div className={"p-2 rounded mb-2 col-9 bg-label-primary"}>
                                <pre>
                                    <code>
                                        {code}
                                    </code>
                                </pre>
                                </div>
                                <button onClick={copyCode} className={"btn btn-primary"}>Copy mã</button>
                            </div>
                            : null}
                    </div>
                    : null}
            </div>
        </div>
    );
}

export default GoogleForm
