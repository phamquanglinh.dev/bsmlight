import React, {Component} from "react";

import ReactDOM from "react-dom";
import GoogleForm from "./GoogleForm";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

class GoogleFormProvider extends Component {
    render() {
        return (
            <div>
                <GoogleForm/>
                <ToastContainer/>
            </div>
        )
    }
}

ReactDOM.render(<GoogleFormProvider/>, document.getElementById('google-form-react'))
