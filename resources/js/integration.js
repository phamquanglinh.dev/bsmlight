sessionStorage.setItem('csrf_token', document.querySelector('meta[name="csrf_token"]').content)
require('./bootstrap');
if ( document.getElementById('google-form-react') !== null) {
    require('./Integration/GoogleFormProvider');
}
if ( document.getElementById('custom-field-react') !== null) {
    require("./Integration/CustomFieldProvider")
}

