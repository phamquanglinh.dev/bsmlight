import React, {useEffect, useState} from "react";
import WorkingShifts from "./components/WorkingShifts";
import {useNavigate, useParams, useSearchParams} from "react-router-dom";
import {
    createStudyLog,
    getAllCardByClassroom,
    getAllClassroomAction,
    getAllShiftsAction,
    getAllSupporters,
    getAllTeachers, getStudyLogData, getUnAttendanceCards, updateStudyLog
} from "./api/StudyLogApi";
import CardLogs from "./components/CardLogs";
import DisableCardLogs from "./components/DisableCardLogs";
import StudyLogLessonScreen from "./components/StudyLogLessonScreen";

const StartCreateStudyLogScreen = () => {
    const navigate = useNavigate();
    const [teachers, setTeachers] = useState([]);
    const [supporters, setSupporters] = useState([]);
    const [params] = useSearchParams();
    const [currentClassroom, setCurrentClassroom] = useState({})
    const [errors, setErrors] = useState([])
    const [unAttendanceCards, setUnAttendanceCards] = useState([])
    const [data, setData] = useState({
        shifts: []
    });
    const {id} = useParams()

    const syncAttendanceCard = () => {
        setData({
            ...data,
            cardLogs: [...data?.cardLogs, ...unAttendanceCards]
        })
        setUnAttendanceCards([])
    }

    useEffect(() => {
        if (id) {
            getStudyLogData({setData, id})
            getUnAttendanceCards({setUnAttendanceCards, id})
        }
    }, [])

    const [calculate, setCalculate] = useState({})

    const setShifts = (shiftsData) => {
        setData((prevData) => ({
            ...prevData,
            shifts: shiftsData
        }));
    };

    const addShifts = () => {
        setData((prevData) => ({
            ...prevData,
            shifts: [
                ...prevData.shifts,
                {
                    id: Date.now()
                }
            ]
        }));
    };

    const setClassroom = (classrooms) => {
        setCurrentClassroom(classrooms.find((v) => v.id === data?.classroom_id))
    }

    useEffect(() => {
        getAllTeachers({setTeachers: setTeachers});
        getAllSupporters({setSupporters: setSupporters});
        setData({
            ...data,
            classroom_id: params.has('classroom_id') ? parseInt(params.get('classroom_id')) : undefined,
            schedule_id: params.has('schedule_id') ? parseInt(params.get('schedule_id')) : undefined,
            studylog_day: params.has('studylog_day') ? params.get('studylog_day') : undefined,
        });
    }, [1]);

    useEffect(() => {
        if (data.classroom_id) {
            getAllClassroomAction({setClassrooms: setClassroom})
        }
    }, [data.classroom_id]);

    useEffect(() => {
        if (data.schedule_id && !id) {
            getAllShiftsAction({setShifts, shiftId: data.schedule_id});
            getAllCardByClassroom({
                classroomId: data?.classroom_id, setCards: function (cards) {
                    setData((prevState) => {
                        return {...prevState, cardLogs: cards}
                    })
                }
            })
        }
    }, [data.schedule_id]);

    useEffect(() => {
        setCalculate({
            shifts: data?.shifts?.length,
            card: data?.cardLogs?.length,
            card_can_deg: data?.cardLogs?.filter((v) => v.can_deg === true)?.length,
            card_not_deg: data?.cardLogs?.filter((v) => v.can_deg === false)?.length,
            card_day: data?.cardLogs?.filter((v) => (v.day === 1 && v.can_deg === true)).length
        })
    }, [data])


    return (
        <div className={"position-relative"}>
            <div className={"row m-0 mb-4"}>
                <div className={"px-3"}>
                    <div className={"mb-2"}>
                        <span className={"fw-bold me-2"}>Lớp học:</span>
                        <span>{currentClassroom?.name}</span>
                    </div>
                    <div className={"mb-2"}>
                        <span className={"fw-bold me-2"}>Ngày học:</span>
                        <span>{data?.studylog_day}</span>
                    </div>
                    <div onClick={() => {
                        navigate(`/studylog/create?studylog_day=${data.studylog_day}&classroom_id=${data.classroom_id}&schedule_id=${data.schedule_id}`);
                    }} className={"text-primary cursor-pointer"}>Chọn lại
                    </div>
                </div>
            </div>
            <StudyLogLessonScreen data={data} setData={setData} errors={errors}/>
            <div className={"mt-2"}>
                <div className={"px-3 pb-3"}>

                </div>
                <div className={"my-2"}>
                    <div className={"mb-4 px-4 h4"}>Ca học</div>
                    {data?.shifts?.map((shift, key) => (
                        <WorkingShifts
                            key={shift.id}
                            setData={setData}
                            data={data}
                            index={key + 1}
                            shift={shift}
                            supporters={supporters}
                            teachers={teachers}
                            errors={errors}
                        />
                    ))}
                    <div className={"my-4 mb-5 px-2 text-left px-4"}>
                        <div className={"btn small bg-label-primary"} onClick={addShifts}>Thêm ca học</div>
                    </div>
                </div>
                <div className={"px-3 pb-2"}>
                    <hr/>
                </div>
                <div className={"my-2"}>
                    <div className={"mb-4 px-4 h4"}>Thẻ học có thể điểm danh</div>
                    {data?.cardLogs?.filter((v) => v?.can_deg === true).map((cardLog, key) =>
                        <CardLogs cardLog={cardLog} data={data} setData={setData} key={key} errors={errors}
                                  index={key}/>
                    )}
                </div>
                <div className={"my-2 p-3"}>
                    <div className={"mb-2 px-4 h4"}>Thẻ học đã hết số buổi điểm danh</div>
                    <div className={"row p-4 h-100"}>
                        {data?.cardLogs?.filter((v) => v?.can_deg === false).map((cardLog, key) =>
                            <DisableCardLogs cardLog={cardLog} data={data} setData={setData} key={key}/>
                        )}
                    </div>
                </div>
                {unAttendanceCards.length > 0 &&

                    <div className={"px-4"}>
                        <button onClick={syncAttendanceCard} className={"btn btn-label-warning"}>Cập nhật lại danh
                            sách
                        </button>
                        <div className={"my-2 small"}>Lấy danh sách thẻ học mới nhất của lớp, có thể sẽ có một số thẻ
                            học
                            chưa được điêm danh trong buổi học này
                        </div>
                    </div>
                }
                <div className={"pb-5"}></div>
            </div>
            <div className={'position-sticky w-100 px-4'} style={{bottom: 0}}>
                <div style={{boxShadow: "-1px -5px 20px 1px #00000073", backgroundColor: "#28243d"}}
                     className={"rounded p-3 w-100 p-2 d-flex justify-content-between align-items-center"}>
                    <div className={"text-white"}>
                        <span className={"me-3"}>Tổng quan: </span>
                        <span className={"me-2 p-2 bg-label-slack rounded"}>{calculate?.shifts} ca học</span>
                        <span className={"me-2 p-2 bg-label-secondary rounded"}>{calculate?.card} thẻ học</span>
                        <span className={"ms-1 me-3"}>|</span>
                        <span className={"me-2 p-2 bg-label-success rounded"}>{calculate?.card_can_deg} thẻ học có thể điểm danh</span>
                        <span className={"me-2 p-2 bg-label-danger rounded"}>{calculate?.card_not_deg} thẻ học không thể điểm danh</span>
                        <span className={"ms-1 me-3"}>|</span>
                        <span className={"me-2 p-2 bg-label-vimeo rounded"}>{calculate?.card_day} trừ buổi học</span>
                    </div>
                    {!id &&
                        <button onClick={() => {
                            createStudyLog({
                                studylog: data,
                                setErrors: setErrors
                            })
                        }} className={"btn btn-label-success"}>Điểm danh
                        </button>
                    }
                    {id &&
                        <button onClick={() => {
                            updateStudyLog({
                                studylog: data,
                                setErrors: setErrors,
                                id: id
                            })
                        }} className={"btn btn-label-success"}>Cập nhật
                        </button>
                    }
                </div>
            </div>
        </div>
    );
};

export default StartCreateStudyLogScreen;
