import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/s/studylog"
const baseAxios = axios.create({
    retries: 3,
    timeout: 100000,
    retryDelay: 100
})
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};

export const getAllClassroomAction = ({setClassrooms, search}) => {
    const url = baseUrl + "/classrooms" + (search ? "?search=" + search : '')
    baseAxios.get(url).then((response) => {
        setClassrooms(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllSchedulesAction = ({setSchedules, classroomId, studyLogDay}) => {
    const url = baseUrl + "/schedules/" + classroomId + "?day=" + studyLogDay
    baseAxios.get(url).then((response) => {
        setSchedules(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllShiftsAction = ({setShifts, shiftId: scheduleId}) => {
    const url = baseUrl + "/shifts/" + scheduleId
    baseAxios.get(url).then((response) => {
        setShifts(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllLessonPlanAction = ({setLessonPlans}) => {
    const url = baseUrl + "/lesson_plan/"
    baseAxios.get(url).then((response) => {
        setLessonPlans(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

/**
 *   Route::get('/s/supporters', [CalendarController::class, 'getSupporters']);
 *     Route::get('/s/teachers', [CalendarController::class, 'getTeachers']);
 * @param setTeachers
 */
export const getAllTeachers = ({setTeachers}) => {
    const url = window.location.origin + "/s/teachers"
    baseAxios.get(url).then((response) => {

        setTeachers(response.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllSupporters = ({setSupporters}) => {
    const url = window.location.origin + "/s/supporters"
    baseAxios.get(url).then((response) => {
        setSupporters(response.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllCardByClassroom = ({classroomId, setCards}) => {
    const url = baseUrl + "/cards/" + classroomId
    baseAxios.get(url).then((response) => {
        setCards(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}


export const handleExtendCardDeg = ({cardId, limitDeg, callback}) => {
    const url = baseUrl + "/cards/extends/" + cardId
    baseAxios.post(url, {
        limitDeg: limitDeg
    })
        .then((response) => {
            console.log(response.data.data)
            callback(response.data.data)
        })
        .catch((error) => {
            console.log(error)
            toast.error(error.response?.data?.message || 'Có lỗi xảy ra')
        })
}

export const createStudyLog = ({studylog, setErrors}) => {
    const url = baseUrl + "/create"
    baseAxios.post(url, studylog).then((response) => {
        toast.success('Thêm mới thành công')
        window.location.href = window.location.origin + "/studylog/list"
    }).catch((e) => {
        const response = e.response
        setErrors(response.data.errors)
        toast.error(response.data.message)
    })
}

export const updateStudyLog = ({studylog, id, setErrors}) => {
    const url = baseUrl + "/update/" + id
    baseAxios.put(url, studylog).then((response) => {
        toast.success('Cập nhật thành công')
        window.location.href = window.location.origin + "/studylog/show/" + id
    }).catch((e) => {
        const response = e.response
        setErrors(response.data.errors)
        toast.error(response.data.message)
    })
}

export const tempFile = async ({file, callback}) => {
    const url = window.location.origin + "/common/temp"

    try {
        const formData = new FormData();
        formData.append('file', file);

        const response = await fetch(url, {
            method: 'POST',
            body: formData,
        });
        if (!response.ok) {
            if (response.status === 422) {
                const errorData = await response.json();
                throw new Error(errorData.file.join(' '));
            }
            throw new Error('File upload failed');
        }
        const result = await response.json();
        const fileUrl = result.url;

        callback(fileUrl);
    } catch (error) {
        toast.error(error.message);
    }
};

export const getStudyLogData = ({setData, id}) => {
    const url = baseUrl + "/one/" + id
    baseAxios.get(url).then((response) => {
        setData(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getUnAttendanceCards = ({setUnAttendanceCards, id}) => {
    const url = baseUrl + '/card/un_attendance/' + id
    baseAxios.get(url).then((response) => {
        setUnAttendanceCards(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getDayValidOfClassroomId = ({data, callback}) => {
    const url = baseUrl + "/valid_days"
    baseAxios.post(url, data).then((response) => {
        callback(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}
