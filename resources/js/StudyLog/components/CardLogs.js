import React from "react";

/**
 * {
 *     "id": 109,
 *     "uuid": "BSM-CN.0002-StudyCard.0110",
 *     "attended": 66,
 *     "can_use_by": 0,
 *     "can_deg": false,
 *     "limit_deg": 0
 * }
 * @param card
 * @returns {Element}
 * @constructor
 */
const CardLogs = ({cardLog, data, setData, index, errors}) => {
    const updateCardLog = ({key, value}) => {
        const newCardLog = data?.cardLogs?.map((card) => {
            if (card.card_id === cardLog.card_id) {
                return {...cardLog, [key]: value}
            }

            return card
        })

        setData({
            ...data,
            cardLogs: newCardLog
        })
    }
    return (
        <div className="p-4 mx-4 border rounded shadow-lg position-relative mb-4">
              <span className={"bg-label-primary p-1 rounded position-absolute small"} style={{
                  top: "-0.6rem",
                  left: "-0.6rem"
              }}>{cardLog?.uuid}</span>
            <div className={"row"}>
                <div className={"col-4 p-3"}>
                    <div className={"rounded position-relative"}>
                        <div className={"d-flex align-items-center mb-2"}>
                            <div>
                                <img alt="1" className={"avatar-md me-2 rounded-circle"}
                                     src={cardLog?.student?.avatar}/>
                            </div>
                            <div>
                                <div>{cardLog?.student?.name}</div>
                                <div className={"small"}>{cardLog?.student?.uuid}</div>
                            </div>
                        </div>

                        <div className={"text-success"}>
                            Số buổi đã dùng: {cardLog?.attended}
                        </div>
                        {cardLog?.can_use_by > 0 &&
                            <div className={"text-warning"}>
                                Số buổi còn lại : {cardLog?.can_use_by}
                            </div>
                        }
                        {cardLog?.can_use_by <= 0 &&
                            <div>
                                <div className={"text-danger"}>
                                    Số buổi còn lại : {cardLog?.can_use_by}
                                </div>
                                <div className={"text-warning"}>
                                    Số buổi được điểm danh âm: {cardLog?.limit_deg}
                                </div>
                            </div>
                        }
                    </div>

                    <div className="p-6 my-2">
                        <label className="switch switch-square">
                            <input onChange={() => {
                                if (cardLog?.day === 1) {
                                    updateCardLog({
                                        key: "day",
                                        value: 0
                                    })
                                } else {
                                    updateCardLog({
                                        key: "day",
                                        value: 1
                                    })
                                }
                            }} type="checkbox" className="switch-input" checked={cardLog?.day === 1}/>
                            <span className="switch-toggle-slider">
                                <span className="switch-on"></span>
                                <span className="switch-off"></span>
                            </span>
                            <span className="switch-label">Trừ buổi học</span>
                        </label>
                    </div>

                    <div className="form-floating form-floating-outline my-4">
                        <select onChange={(r) => {
                            updateCardLog({
                                key: "status",
                                value: r.currentTarget.value
                            })
                        }} className="form-select">
                            <option selected={parseInt(cardLog?.status) === 0} value={0}>Đi học, đúng giờ</option>
                            <option selected={parseInt(cardLog?.status) === 1} value={1}>Đi học, muộn</option>
                            <option selected={parseInt(cardLog?.status) === 2} value={2}>Đi học, sớm</option>
                            <option selected={parseInt(cardLog?.status) === 3} value={3}>Vắng, có phép</option>
                            <option selected={parseInt(cardLog?.status) === 4} value={4}>Vắng, không phép</option>
                            <option selected={parseInt(cardLog?.status) === 5} value={5}>Không điểm danh</option>
                        </select>
                        <label htmlFor="exampleFormControlSelect1">Trạng thái</label>
                    </div>
                </div>
                <div className={"col-8"}>
                    <div className="form-floating form-floating-outline mb-6">
                        <textarea value={cardLog?.['teacher_note']}
                                  rows={5} className={"form-control"}
                                  style={{minHeight: "225px"}}
                                  onChange={(r) => {
                                      updateCardLog({
                                          key: "teacher_note",
                                          value: r.currentTarget.value
                                      })
                                  }}
                        >
                        </textarea>
                        <label htmlFor="exampleFormControlTextarea1">Lời nhắn cho PHHS</label>
                    </div>
                </div>
            </div>

            <div className={"text-danger small my-1"}>{errors?.["cardLogs." + (index) + ".status"]}</div>
            <div className={"text-danger small my-1"}>{errors?.["cardLogs." + (index) + ".id"]}</div>
            <div className={"text-danger small my-1"}>{errors?.["cardLogs." + (index) + ".day"]}</div>
        </div>
    )
}

export default CardLogs
