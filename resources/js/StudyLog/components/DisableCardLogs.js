import React, {useState} from "react";
import {handleExtendCardDeg} from "../api/StudyLogApi";

const DisableCardLogs = ({cardLog, data, setData}) => {
    const [showSetting, setShowSetting] = useState(false)
    const [limitDeg, setLimitDeg] = useState(cardLog?.limit_deg)

    function handleExtendCard() {
        handleExtendCardDeg({
            cardId: cardLog?.card_id,
            limitDeg: limitDeg,
            callback: function (updatedCardLog) {
                const newCardLogs = data?.cardLogs?.map((c) => {
                    if (c?.card_id === updatedCardLog?.card_id) {
                        console.log('up',updatedCardLog)
                        return updatedCardLog
                    }
                    return c
                })
                setData((prevData) => ({
                    ...prevData,
                    cardLogs: newCardLogs
                }));
                setShowSetting(false)
            }
        })
    }

    return (
        <div className={"col-4 p-1 h-100"}>
            <div className={"border p-2 rounded h-100"}>
                <div>{cardLog?.uuid}</div>
                <div>Học sinh: {cardLog?.student?.name}</div>
                <div>Số buổi đã dùng: {cardLog?.attended}</div>
                <div>Số buổi còn lại: <span className={"text-danger"}>
                    <span className={"fw-bold me-2"}>{cardLog?.can_use_by}</span>
                    {cardLog?.limit_deg > 0 &&
                        <small>(Được danh âm: {cardLog?.limit_deg} buổi)</small>
                    }</span></div>

                <hr/>
                {!showSetting &&
                    <div className={"d-flex my-1 justify-content-between"}>
                        {cardLog?.user_ability?.access &&
                            <span onClick={() => {
                                const url = window.location.origin + "/card/show/" + cardLog?.card_id
                                window.open(url, '_blank')
                            }} className={"mdi mdi-eye cursor-pointer"}>Xem</span>
                        }

                        {cardLog?.user_ability?.can_change_deg &&
                            <span onClick={() => {
                                setShowSetting(true)
                            }} className={"mdi mdi-receipt-text-plus ms-2 cursor-pointer"}>Cho phép điểm danh âm</span>
                        }
                    </div>
                }
                {showSetting &&
                    <div>
                        <div>
                            <div>
                                <label htmlFor="smallSelect" className="form-label">Số buổi cho phép điểm danh
                                    âm</label>
                                <select onChange={(r) => {
                                    setLimitDeg(r.currentTarget.value)
                                }} id="smallSelect" className="form-select form-select-sm">
                                    <option selected={limitDeg === 0} value={0}>0 buổi</option>
                                    <option selected={limitDeg === 5} value={5}>5 Buổi</option>
                                    <option selected={limitDeg === 10} value={10}>10 buổi</option>
                                    <option selected={limitDeg === 15} value={15}>15 buổi</option>
                                    <option selected={limitDeg === 20} value={20}>20 buổi</option>
                                    <option selected={limitDeg === 30} value={30}>30 buổi</option>
                                    <option selected={limitDeg === 40} value={40}>40 buổi</option>
                                    <option selected={limitDeg === 50} value={50}>50 buổi</option>
                                    <option selected={limitDeg === 100} value={100}>100 buổi</option>
                                    <option selected={limitDeg === 9999} value={9999}>Không giới hạn</option>
                                </select>
                            </div>
                        </div>
                        <div className={"d-flex justify-content-between my-2"}>
                            <span onClick={() => {
                                setShowSetting(false)
                            }} className={"cursor-pointer text-secondary me-2"}>Huỷ</span>
                            <span onClick={handleExtendCard} className={"text-success cursor-pointer"}>Xác nhận</span>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}
export default DisableCardLogs
