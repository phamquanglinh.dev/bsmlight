import {useEffect} from "react";
import React from "react";
import Select, {components} from "react-select";

const customOption = (props) => {
    return (
        <components.Option {...props}>
            <img
                src={props.data.avatar || 'https://i.pinimg.com/originals/64/64/1b/64641bf60f3cd96a6655d139ed87f024.gif'}
                alt=""
                style={{width: 36, height: 36, borderRadius: '50%', marginRight: 10}}
            />
            {props.data.label}
        </components.Option>
    );
};

const customSingleValue = (props) => {
    return (
        <components.SingleValue {...props}>
            {props?.data?.value &&
                <img
                    src={props.data.avatar || 'https://i.pinimg.com/originals/64/64/1b/64641bf60f3cd96a6655d139ed87f024.gif'}
                    alt="a"
                    style={{width: 36, height: 36, borderRadius: '50%', marginRight: 10}}
                />
            }
            {props.data.label}
        </components.SingleValue>
    );
};


export const customStyles = {
    control: (provided) => ({
        ...provided,
        backgroundColor: '#312d4b',
        borderColor: '#555',
        color: '#fff',
        padding: "0.3rem"
    }),
    option: (provided, state) => ({
        ...provided,
        backgroundColor: '#312d4b',
        color: '#fff'
    }),
    singleValue: (provided) => ({
        ...provided,
        color: '#fff'
    }),
    input: (provided) => ({
        ...provided,
        color: "#fff"
    })
};
const WorkingShifts = ({shift, teachers, supporters, index, data, setData, errors}) => {
    const updateShift = ({key, value}) => {
        console.log(key, value, shift)
        const newShifts = data.shifts.map((shiftItem) => {
            if (shiftItem.id === shift.id) {
                return {...shiftItem, [key]: value};
            }
            return shiftItem;
        });
        setData((prevData) => ({
            ...prevData,
            shifts: newShifts
        }));
    };

    const removeShift = () => {
        if (data?.shifts?.length < 2) {
            alert('Không thể xoá ca học duy nhất !')
        } else {
            const result = confirm('Bạn có chắc chắn muốn xoá ? ')

            if (result) {
                setData({
                    ...data,
                    shifts: data?.shifts.filter((v) => v !== shift)
                })
            }
        }
    }
    useEffect(() => {
        const date1 = new Date("2023-01-01 " + shift.start_time);
        const date2 = new Date("2023-01-01 " + shift.end_time);
        updateShift({
            key: "duration",
            value: (date2 - date1) / (1000 * 60)
        });
    }, [shift.start_time, shift.end_time]);
    return (
        <div className="p-4 mx-4 border rounded shadow-lg position-relative mb-4">
            <div className="position-absolute p-1 small text-white rounded bg-primary"
                 style={{top: "-0.6rem", left: "-0.6rem"}}>
                Ca học {index}
            </div>
            <div
                onClick={removeShift}
                className="position-absolute p-1 small text-white rounded bg-danger"
                style={{top: "-0.6rem", right: "-0.6rem"}}>
                <span className={"avatar mdi mdi-trash-can"}></span>
            </div>
            <div className="row">
                <div className="col-4">
                    <div className="input-group mb-3">
                        <span className="input-group-text" style={{padding: "0.8rem"}}>Thời gian</span>
                        <input
                            value={shift.start_time}
                            onChange={(e) => updateShift({key: 'start_time', value: e.target.value})}
                            type="time"
                            className="form-control p-2"
                            aria-label="Start time"
                        />
                        <input
                            value={shift.end_time}
                            onChange={(e) => updateShift({key: "end_time", value: e.target.value})}
                            type="time"
                            className="form-control p-2"
                            aria-label="End time"
                        />
                        <span className="input-group-text"
                              style={{padding: "0.8rem"}}>{(shift?.duration || 0) + " phút"}</span>
                    </div>
                    <div className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".start_time"]}</div>
                    <div className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".end_time"]}</div>

                    <div className="form-floating form-floating-outline mb-3">
                        <input
                            value={shift.room}
                            onChange={(e) => updateShift({key: "room", value: e.target.value})}
                            type="text"
                            className="form-control"
                            id="floatingInput"
                            placeholder="Phòng học"
                            aria-describedby="floatingInputHelp"
                        />
                        <label htmlFor="floatingInput">Phòng học</label>
                        <div className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".room"]}</div>
                    </div>

                    <div className="form-floating form-floating-outline mb-3">
                    <label>Giáo viên</label>
                        <Select
                            classNamePrefix="select"
                            value={teachers.find((v) => v.value === shift?.teacher_id)}
                            onChange={(r) => {
                                updateShift({
                                    key: "teacher_id",
                                    value: r?.value
                                })
                            }}
                            isClearable={true}
                            isSearchable={true}
                            name="color"
                            placeholder={"Chọn giáo viên"}
                            options={teachers}
                            styles={customStyles} // Sử dụng styles tùy chỉnh
                            components={{Option: customOption, SingleValue: customSingleValue}}
                        />
                        <div
                            className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".teacher_id"]}</div>
                    </div>
                    <div className="form-floating form-floating-outline mb-3">
                        <label>Trợ giảng</label>
                        <Select
                            components={{Option: customOption, SingleValue: customSingleValue}}
                            className="basic-single"
                            classNamePrefix="select"
                            onChange={(r) => {
                                updateShift({
                                    key: "supporter_id",
                                    value: r?.value
                                })
                            }}
                            value={supporters.find((v) => v.value === shift?.supporter_id)}
                            isClearable={true}
                            isSearchable={true}
                            name="supporter"
                            options={supporters}
                            styles={customStyles} // Sử dụng styles tùy chỉnh
                        />
                        <div
                            className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".supporter_id"]}</div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="form-floating form-floating-outline mb-6">
                        <textarea
                            value={shift.teacher_comment}
                            onChange={(e) => updateShift({key: "teacher_comment", value: e.target.value})}
                            className="form-control"
                            style={{minHeight: "250px"}}
                            rows={4}
                        ></textarea>
                        <label htmlFor="exampleFormControlTextarea1">Báo cáo của giáo viên cho admin</label>
                        <div
                            className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".teacher_comment"]}</div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="form-floating form-floating-outline mb-6">
                        <textarea
                            value={shift.supporter_comment}
                            onChange={(e) => updateShift({key: "supporter_comment", value: e.target.value})}
                            className="form-control"
                            style={{minHeight: "250px"}}
                            rows={4}
                        ></textarea>
                        <label htmlFor="exampleFormControlTextarea1">Báo cáo của trợ giảng cho admin</label>
                        <div
                            className={"text-danger small my-1"}>{errors?.["shifts." + (index - 1) + ".supporter_comment"]}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default WorkingShifts;
