import * as React from 'react';
import dayjs from 'dayjs';
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {PickersDay} from '@mui/x-date-pickers/PickersDay';
import {DateCalendar} from '@mui/x-date-pickers/DateCalendar';
import {DayCalendarSkeleton} from '@mui/x-date-pickers/DayCalendarSkeleton';
import {useEffect, useState} from "react";
import {getDayValidOfClassroomId} from "../api/StudyLogApi";

function ServerDay(props) {
    const {highlightedDays = [], day, outsideCurrentMonth, ...other} = props;

    const isSelected =
        !props.outsideCurrentMonth && highlightedDays.indexOf(props.day.date()) >= 0;

    return (
        <PickersDay
            sx={{color: isSelected ? "#56ca00" : "white", backgroundColor: isSelected ? "#37463f" : null}} {...other}
            outsideCurrentMonth={outsideCurrentMonth} day={day}/>
    );
}

const CalendarUi = ({data, setData}) => {
    const [isLoading, setIsLoading] = React.useState(false);
    const [highlightedDays, setHighlightedDays] = React.useState([]);
    const [currentMonth, setCurrentMonth] = useState(null)
    const [currentYear, setCurrentYear] = useState(null)

    function handleMonthChange(month) {
        setCurrentMonth((month.$M) + 1)
        setCurrentYear(month.$y)
    }

    useEffect(() => {
        getDayValidOfClassroomId({
            data: {
                month: currentMonth,
                classroom_id: data?.classroom_id,
                year: currentYear,
            },
            callback: setHighlightedDays
        })
    }, [data?.classroom_id, currentMonth, currentYear])

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateCalendar
                localeText={{ calendarWeekNumberHeaderText: 'Tuần' }}
                defaultValue={dayjs(data?.studylog_day)}
                loading={isLoading}
                onMonthChange={handleMonthChange}
                renderLoading={() => <DayCalendarSkeleton/>}
                dayOfWeekFormatter={formatDayOfWeek}
                slots={{
                    day: ServerDay,
                }}
                slotProps={{
                    day: {
                        highlightedDays,
                    },
                }}
                onChange={(date) => {
                    setData({...data, studylog_day: formatStaticsDate(date.toString())})
                }}
            />
        </LocalizationProvider>
    );
}

export default CalendarUi

const formatStaticsDate = (originalDateString) => {

    const originalDate = new Date(originalDateString);


    const year = originalDate.getFullYear();
    const month = String(originalDate.getMonth() + 1).padStart(2, '0'); // Months are zero-based
    const day = String(originalDate.getDate()).padStart(2, '0');


    return `${year}-${month}-${day}`
}

const formatDayOfWeek = (date) => {
    console.log(date)
    const daysOfWeek = ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'];
    return daysOfWeek[date.$W];
};
