import React, {useEffect, useRef, useState} from "react";
import {tempFile} from "../api/StudyLogApi";

const StudyLogLessonScreen = ({data, setData, errors}) => {
    const [label, setLabel] = useState({
        audio: data?.audio,
        file: data?.file,
        image: data?.image
    })
    const audioRef = useRef(null)
    const documentRef = useRef(null)
    const imageRef = useRef(null)

    useEffect(() => {
        setLabel({
            ...label,
            audio: data.audio,
            file: data.file,
            image: data.image
        })
    }, [data.audio, data.image, data.file]);
    return (
        <div className={"my-2 container-fluid"}>
            <div className={"rounded border shadow-lg p-4"}>

                <div className="form-floating form-floating-outline">
                    <input type="text" className="form-control" value={data?.title} onChange={(r) => {
                        setData({
                            ...data,
                            title: r.currentTarget.value
                        })
                    }}/>
                    <label htmlFor="floatingInput">Tiêu đề bài học</label>
                    <div className={"text-danger small my-1"}>{errors?.title}</div>
                </div>
                <div className="form-floating form-floating-outline mt-3 mb-6">
                    <textarea onChange={(r) => {
                        setData({
                            ...data, content: r.currentTarget.value
                        })
                    }} value={data?.content} style={{minHeight: "250px"}} className="form-control"></textarea>
                    <label>Nội dung bài học</label>
                    <div className={"text-danger small my-1"}>{errors?.content}</div>
                </div>

                <div className={"my-2 row"}>
                <div className={"col-3"}>
                    <div onClick={() => {
                        audioRef.current.click()
                    }} className={"p-2 border rounded cursor-pointer"}>
                            <span className="mdi me-2 mdi-volume-high">
                            </span>
                        <span>{label?.audio || 'Chưa chọn file'}</span>
                        <div className={"text-danger small my-1"}>{errors?.audio}</div>
                    </div>
                    <input onChange={(r) => {
                        console.log(r.currentTarget.files[0])
                            tempFile({
                                file: r.currentTarget.files[0],
                                callback: (url) => {
                                    setData({
                                        ...data,
                                        audio: url
                                    })
                                }
                            }).then()
                        }} ref={audioRef} hidden={true} type={"file"}/>
                    </div>
                    <div className={"col-3"}>
                        <div className={"d-flex"}>
                            <span className={"rounded-start mdi me-2 mdi-youtube p-2 border"}></span>
                            <input
                                className={"p-2 border rounded-end cursor-pointer w-100"}
                                style={{backgroundColor: "#28243d", marginLeft: "-8px"}}
                                onChange={(r) => {
                                    setData({
                                        ...data,
                                        video: r.currentTarget.value
                                    })
                                }}
                                value={data?.video}
                                placeholder={"Dán link Youtube"}
                                hidden={false} type={"input"}/>
                            <div className={"text-danger small my-1"}>{errors?.video}</div>
                        </div>
                    </div>
                    <div className={"col-3"}>
                        <div onClick={() => {
                            documentRef.current.click()
                        }} className={"p-2 border rounded cursor-pointer"}>
                            <span className="mdi me-2 mdi-file-document">
                            </span>
                            <span>{label?.file || 'Chưa chọn file'}</span>
                            <input
                                onChange={(r) => {
                                    console.log(r.currentTarget.files[0])
                                    tempFile({
                                        file: r.currentTarget.files[0],
                                        callback: (url) => {
                                            setData({
                                                ...data,
                                                file: url
                                            })
                                        }
                                    }).then()
                                }}
                                ref={documentRef} hidden={true} type={"file"}/>
                            <div className={"text-danger small my-1"}>{errors?.document}</div>
                        </div>
                    </div>
                    <div className={"col-3"}>
                        <div onClick={() => {
                            imageRef.current.click()
                        }} className={"p-2 border rounded cursor-pointer"}>
                            <span className="mdi me-2 mdi-image">
                            </span>
                            <span>{label?.image || 'Ảnh chụp lớp học'}</span>
                            <input
                                onChange={(r) => {
                                    console.log(r.currentTarget.files[0])
                                    tempFile({
                                        file: r.currentTarget.files[0],
                                        callback: (url) => {
                                            setData({
                                                ...data,
                                                image: url
                                            })
                                        }
                                    }).then()
                                }}
                                ref={imageRef} hidden={true} type={"file"}/>
                            <div className={"text-danger small my-1"}>{errors?.image}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default StudyLogLessonScreen
