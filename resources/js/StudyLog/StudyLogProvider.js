import React, {Component} from "react";

import ReactDOM from "react-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import CreateStudyLogScreen from "./CreateStudyLogScreen";
import {BrowserRouter as Router, useRoutes} from 'react-router-dom';
import StartCreateStudyLogScreen from "./StartCreateStudyLogScreen";

const App = () => {
    return useRoutes([
        {path: '/studylog/create/', element: <CreateStudyLogScreen/>},
        {path: '/studylog/create/start', element: <StartCreateStudyLogScreen/>},
        {path: '/studylog/edit/:id', element: <StartCreateStudyLogScreen/>},
    ]);
}

class StudyLogProvider extends Component {
    render() {
        return (
            <React.StrictMode>
                <Router>
                    <App/>
                    <ToastContainer/>
                </Router>
            </React.StrictMode>
        )
    }
}

ReactDOM.render(<StudyLogProvider/>, document.getElementById('studylog-react'))
