import React, {useEffect, useState} from "react";
import {useNavigate, useSearchParams} from "react-router-dom";
import {getAllClassroomAction, getAllSchedulesAction, getAllShiftsAction} from "./api/StudyLogApi";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import 'dayjs/locale/vi';
import CalendarUi from "./components/CalendarUi";

const CreateStudyLogScreen = () => {
    const navigate = useNavigate();
    const [params, setParams] = useSearchParams();
    const [data, setData] = useState({});
    const [classrooms, setClassrooms] = useState([]);
    const [schedules, setSchedules] = useState([]);
    const [shifts, setShifts] = useState([]);
    const [txtSearch, setTxtSearch] = useState('');
    const [weekHighLight, setWeekHighLight] = useState(0);
    const [selected, setSelected] = useState();
    const attendThisSchedule = () => {
        navigate(`/studylog/create/start?classroom_id=${data.classroom_id}&schedule_id=${data.schedule_id}&studylog_day=${data.studylog_day}`);
    }

    const attendOtherSchedule = () => {
        navigate(`/studylog/create/start?classroom_id=${data.classroom_id}&schedule_id=-1&studylog_day=${data.studylog_day}`);
    }

    const setUrl = ({key, value, deletedKey}) => {
        const newSearchParams = new URLSearchParams(params);
        newSearchParams.set(key, value);
        newSearchParams.delete(deletedKey);
        setParams(newSearchParams);
    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            getAllClassroomAction({setClassrooms: setClassrooms, search: txtSearch});
        }
    };

    useEffect(() => {
        if (params.get('classroom_id') !== null) {
            setData({
                ...classrooms,
                classroom_id: params.get('classroom_id')
            });
        }
        getAllClassroomAction({setClassrooms: setClassrooms});
    }, []);

    useEffect(() => {
        setData({
            ...data,
            classroom_id: params.has('classroom_id') ? parseInt(params.get('classroom_id')) : undefined,
            schedule_id: params.has('schedule_id') ? parseInt(params.get('schedule_id')) : undefined,
            studylog_day: params.has('studylog_day') ? params.get('studylog_day') : new Date().toISOString().slice(0, 10),
        });
    }, []);

    const formatStaticsDate = (originalDateString) => {
        const originalDate = new Date(originalDateString);
        const year = originalDate.getFullYear();
        const month = String(originalDate.getMonth() + 1).padStart(2, '0'); // Months are zero-based
        const day = String(originalDate.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    }

    useEffect(() => {
        if (data.classroom_id) {
            getAllSchedulesAction({
                setSchedules: setSchedules,
                classroomId: data.classroom_id,
                studyLogDay: data?.studylog_day
            });
            setShifts([]);
            setUrl({
                key: "classroom_id",
                value: data.classroom_id,
                deletedKey: "schedule_id"
            });
        }
    }, [data.classroom_id, data.studylog_day]);

    useEffect(() => {
        if (data.schedule_id) {
            getAllShiftsAction({
                setShifts: setShifts,
                shiftId: data.schedule_id,
                studylogDay: data?.studylog_day
            });
            setUrl({
                key: "schedule_id",
                value: data.schedule_id
            });
        }
    }, [data.schedule_id, data.classroom_id, data.studylog_day]);

    useEffect(() => {
        if (data.studylog_day) {
            setUrl({
                key: "studylog_day",
                value: data.studylog_day,
                deletedKey: "schedule_id"
            });
        }
    }, [data.studylog_day]);

    const adapter = React.useMemo(
        () => new AdapterDayjs({locale: dayjs.locale('vi')}),
        []
    );

    return (
        <div className={"container-fluid p-3 h-100 w-100 d-flex"}>
            <div className={"d-flex justify-content-center align-items-center w-100 h-100 "} style={{flex: 1}}>
                <div style={{width: "100rem"}} className={"shadow-lg p-3 border rounded"}>
                    <div className={"my-3 p-2 row"}>
                        <div className={"col-8"} style={{maxHeight: "75vh", overflowY: "scroll"}}>
                            <div className={"my-2 px-3"}>Bước 1: Chọn lớp học</div>
                            <div className={"px-3 py-2"} style={{position: "sticky", top: 0}}>
                                <input
                                    value={txtSearch}
                                    onChange={(r) => {
                                        setTxtSearch(r.target.value)
                                    }}
                                    onKeyUp={handleKeyPress}
                                    placeholder={"Tìm theo tên, mã lớp, gõ ENTER để hoàn thành"}
                                    className={"w-100 border-none form-control text-muted shadow-lg"}/>
                            </div>
                            <div className={"row"}>
                                {classrooms?.map((classroom, key) =>
                                    <div key={key} className={"col-3"}>
                                        <div onClick={() => {
                                            setData({
                                                ...data,
                                                classroom_id: classroom?.id,
                                                schedule_id: undefined
                                            })
                                        }}
                                             className={"p-2 border m-3 rounded shadow-lg cursor-pointer " + (data?.classroom_id === classroom?.id ? "border-success" : "")}>
                                            <div>
                                                <span className={"bg-label-success text-white p-1 rounded small"}>
                                                    <span className="mdi mdi-account-multiple me-1"></span>
                                                    <span>{classroom?.members}</span>
                                                </span>
                                            </div>
                                            <div className={"text-center mb-2"}>

                                                <img style={{
                                                    borderWidth: "0.2rem",
                                                    borderColor: "white",
                                                    borderStyle: "solid"
                                                }}
                                                     className={"avatar-xl rounded-circle"}
                                                     src={'https://img.freepik.com/premium-vector/classroom-icon-vector-image-can-be-used-online-education_120816-166773.jpg'}
                                                     alt={""}/>
                                            </div>
                                            <div className={"text-center"}>
                                                <div>{classroom?.name}</div>
                                                <div className={"my-1 small text-primary"}>({classroom?.uuid})</div>
                                                <div>{classroom?.weekdays?.map((day) =>
                                                    <span
                                                        className={"me-1 bg-label-success p-1 small"}>{parseInt(day?.week_day) === 8 ? "CN" : "T" + day?.week_day}</span>
                                                )}</div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className={"col-4 col-lg-4"}>
                            <div className={"my-2 px-3 pb-3"}>Bước 2: Chọn buổi học</div>
                            <div className={"border shadow-lg rounded"}>
                                <CalendarUi data={data} setData={setData}/>
                            </div>
                            {
                                data?.classroom_id &&
                                <div className={"my-3"}>
                                    <div className={"mb-3 fw-bold"}>Chọn lịch học</div>
                                    <div className={"row m-0"}>
                                        {schedules?.map((schedule, key) =>
                                            <div className={"col-6 p-1 small"}>
                                                <div onClick={() => {
                                                    setData({
                                                        ...data,
                                                        schedule_id: schedule?.id
                                                    })
                                                }} key={key}
                                                     className={"mb-2 p-2 border rounded shadow-lg cursor-pointer " + (schedule?.id === data?.schedule_id ? "border-success" : "")}>
                                                    <span>{schedule?.week_day_label}</span>
                                                    <div>
                                                        <span>{schedule?.start_time}</span> -
                                                        <span>{schedule?.end_time}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            }

                            {
                                (data?.schedule_id && shifts.length > 0) &&
                                <div className={"border-top mt-1 pt-3"}>
                                    <div className={"mb-1 fw-bold"}>Danh sách ca học</div>
                                    <div className={"small"}>
                                        {shifts?.map((shift, key) =>
                                            <div key={key} className={"p-2 border mb-1 rounded"}>
                                                <div>Thời
                                                    gian: {shift?.start_time} - {shift?.end_time} ({shift?.room})
                                                </div>
                                                <div>GV: {shift?.teacher?.name}</div>
                                                <div>TG: {shift?.supporter?.name}</div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            }

                            {(data?.schedule_id && shifts.length > 0) &&
                                <div className={"small d-flex justify-content-between my-2"}>
                                    <div
                                        onClick={attendThisSchedule}
                                        className={"text-success cursor-pointer"}>Điểm danh buổi học này
                                    </div>
                                    <div onClick={attendOtherSchedule} className={"text-primary cursor-pointer"}>Điểm
                                        danh buổi học khác
                                    </div>
                                </div>
                            }

                            {((data.classroom_id && data.studylog_day) && schedules.length < 1) &&
                                <div className={"text-center"}>
                                    <div onClick={attendOtherSchedule}
                                         className={"text-primary my-1 cursor-pointer"}>Tạo buổi học khác với cài đặt
                                        lịch lớp học
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreateStudyLogScreen;
