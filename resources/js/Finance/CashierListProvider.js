import React, {Component} from "react";
import ReactDOM from "react-dom";
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";
import CashierList from "./CashierList";


class CashierListProvider extends Component {
    render() {
        return (
            <div>
                <CashierList/>
                <ToastContainer/>
            </div>
        )
    }
}

ReactDOM.render(<CashierListProvider/>, document.getElementById('cashier-react'))
