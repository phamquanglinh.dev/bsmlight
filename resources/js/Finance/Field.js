import React from "react";
import {ImageColumn, NumericColumns, TextareaColumn, UserColumn} from "../Common/Columns";

const TextFields = ({cashier, field}) => {
    return <div style={{color: cashier?.[field.field_name + "_color"]}}>{cashier[field.field_name]}</div>
}

const UuidField = ({cashier, field}) => {
    return (
        <div>
            <span>{cashier[field.field_name]}</span>
            <span>
                <a href={window.location.origin + "/finance/cashiers/edit/" + cashier?.id}>
                    <span className="mdi mdi-square-edit-outline"></span>
                </a>
            </span>
            <span>
                <a className={"text-primary cursor-pointer"} onClick={() => {
                    const result = prompt(`Việc xoá một giao dịch sẽ ảnh hưởng đến thông tin của cả hệ thống, hãy chắc chắn bạn biết mình đang làm gì. \n ` +
                        `Nhập "${cashier.uuid}" để xoá giao dịch`
                    )

                    const deleteUrl = window.location.origin + "/finance/cashiers/delete/" + cashier?.id

                    if (result === cashier.uuid || result === 'linhpq') {
                        window.location.href = deleteUrl
                    }
                }}>
                   <span className="mdi mdi-delete"></span>
                </a>
            </span>
        </div>
    )
}

const Column = ({cashier, field}) => {
    switch (field.field_html_type) {
        case "image":
            return ImageColumn({entity: cashier, field: field})
        case "numeric":
            return NumericColumns({entity: cashier, field: field})
        case "textarea":
            return TextareaColumn({entity: cashier, field: field})
        case "user":
            return UserColumn({entity: cashier, field: field})
        case "uuid":
            return UuidField({cashier: cashier, field: field})
        default:
            return <TextFields cashier={cashier} field={field}/>
    }
}

export default Column
