import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/finance"
const baseAxios = axios
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};

export const getAllCashiers = ({setCashiers, query}) => {
    const url = baseUrl + "/cashiers"

    baseAxios.get(url, {
        params: query,
        paramsSerializer: customParamsSerializer
    }).then((response) => {
        setCashiers(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}

export const getCashierPrimaryFields = ({setFields, setMyFields}) => {
    const url = baseUrl + "/cashiers/fields"
    baseAxios.get(url).then((response) => {
        setFields(response.data.data)
        setMyFields(response.data.data)
    }).catch((error) => {
        toast.error("Có lỗi xảy ra")
        console.log(error)
    })
}
