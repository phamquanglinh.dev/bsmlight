import React, {useEffect, useRef, useState} from "react";
import {getAccounts} from "../CRM/api/CrmAccountAPI";
import Fields from "../components/crm/Field";
import {getAllCashiers, getCashierPrimaryFields} from "./api/CashierApi";
import Field from "./Field";
import CashierFilter from "./CashierFilter";
import AccountCustomFilter from "../components/crm/AccountCustomFilter";

const CashierList = () => {
    const scrollbarRef = useRef();

    const [fields, setFields] = useState([])
    const [myFields, setMyFields] = useState([])
    const [cashiers, setCashiers] = useState([])
    const [accountRelations, setAccountRelations] = useState([])
    const [queryData, setQueryData] = useState({
        limit: 10,
        offset: 1,
        fields: "id",
        orderBy: "transaction_day",
        direction: "desc"
    })

    useEffect(() => {
        getCashierPrimaryFields({setFields: setFields, setMyFields: setMyFields})
    }, [])

    useEffect(() => {
        setQueryData({
            ...queryData,
            fields: fields.map(field => field.field_name).join(',')
        })
    }, [myFields])

    useEffect(() => {
        console.log('fields', queryData.fields)
        getAllCashiers({
            setCashiers, query: queryData
        })
    }, [queryData])

    return (
        <div>
            <div className={"container-fluid"}>
                <div className={"mb-3 h3"}>Sổ quỹ</div>
            </div>
            <CashierFilter queryData={queryData} setQueryData={setQueryData}/>
            <div className={"container-fluid my-3"}>
                <div className="table-responsive text-nowrap">
                    <table className="table table-bordered rounded">
                        <thead>
                        <tr>
                            {myFields.filter(v => v.hide !== true).map((field, key) =>
                                (field.field_html_type !== 'uuid' ?
                                        <th className={"bg-primary text-dark"} key={key}>{field.field_label}</th> :
                                        <th style={{position: "sticky", left: 0}}
                                            className={"bg-primary text-dark fixed-left"}
                                            key={key}>{field.field_label}</th>
                                )
                            )}
                        </tr>
                        </thead>
                        <tbody>
                        {cashiers?.map((cashier, cashierKey) =>
                            <tr key={cashierKey}>
                                {myFields.filter(v => v.hide !== true).map((field, fieldKey) =>
                                    (field.field_html_type !== 'uuid' ? <td key={fieldKey}>
                                                <Field
                                                    cashier={cashier}
                                                    field={field}
                                                />
                                            </td> :
                                            <td style={{position: "sticky", left: 0, zIndex: 9999}}
                                                className={"fixed-left"}>
                                                <Field
                                                    cashier={cashier}
                                                    field={field}
                                                />
                                            </td>
                                    )
                                )}
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className={'container-fluid'}>
                <div className={"row justify-content-between align-items-center"}>
                    <div className={"col-3"}>
                        <a className={"cursor-pointer"}
                           onClick={() => setQueryData({
                               ...queryData,
                               offset: queryData > 1 ? queryData.offset - 1 : 1
                           })}
                        >
                            <span className="mdi mdi-arrow-left-drop-circle mdi-36px"></span>
                        </a>
                        <a className={"cursor-pointer"}>
                            <span
                                onClick={() => setQueryData({
                                    ...queryData,
                                    offset: queryData.offset + 1
                                })}
                                className="mdi mdi-arrow-right-drop-circle mdi-36px"></span>
                        </a>

                    </div>
                    <div className={"col-3"}>

                        <select
                            onChange={(e) => {
                                const newLimit = parseInt(e.target.value, 10);
                                setQueryData({
                                    ...queryData,
                                    limit: newLimit
                                })
                            }}
                            value={queryData.limit}
                            className={"form-control"}>
                            <option value={10}>Hiển thị 10 dữ liệu</option>
                            <option value={20}>Hiển thị 20 dữ liệu</option>
                            <option value={50}>Hiển thị 50 dữ liệu</option>
                            <option value={100}>Hiển thị 100 dữ liệu</option>
                            <option value={200}>Hiển thị 200 dữ liệu</option>
                            <option value={500}>Hiển thị 500 dữ liệu</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default CashierList
