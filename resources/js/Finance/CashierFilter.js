import React from "react";

const CashierFilter = ({queryData, setQueryData}) => {
    const updateFiltering = ({keys, value}) => {
        setQueryData({
            ...queryData,
            filters: {...queryData.filters, [keys]: value}
        })
    }

    return (
        <div className={"container-fluid"}>
            <div className={"d-flex flex-row justify-content-between"}>
                <div className={""}>
                    <button
                        onClick={() => {
                            updateFiltering({
                                keys: "cashier_type:handle",
                                value: undefined
                            })
                        }}
                        className={"btn me-2 " + (queryData?.filters?.['cashier_type:handle'] === undefined ? "bg-success text-white" : "bg-label-success")}
                    > Sổ quỹ
                    </button>
                    <button
                        onClick={() => {
                            updateFiltering({
                                keys: "cashier_type:handle",
                                value: "revenue"
                            })
                        }}
                        className={"btn me-2 " + (queryData?.filters?.['cashier_type:handle'] === "revenue" ? "bg-warning text-white" : "bg-label-warning")}>Sổ
                        thu
                    </button>
                    <button
                        onClick={() => {
                            updateFiltering({
                                keys: "cashier_type:handle",
                                value: "payment"
                            })
                        }}
                        className={"btn me-2 " + (queryData?.filters?.['cashier_type:handle'] === "payment" ? "bg-google-plus text-white" : "bg-label-google-plus")}>Sổ
                        chi
                    </button>
                    <button
                        onClick={() => {
                            updateFiltering({
                                keys: "cashier_type:handle",
                                value: "transfer"
                            })
                        }}
                        className={"btn me-2 " + (queryData?.filters?.['cashier_type:handle'] === "transfer" ? "bg-primary text-white" : "bg-label-primary")}>Sổ
                        chuyển
                    </button>
                    <button
                        onClick={() => {
                            updateFiltering({
                                keys: "cashier_type:handle",
                                value: "receive"
                            })
                        }}
                        className={"btn me-2 " + (queryData?.filters?.['cashier_type:handle'] === "receive" ? "bg-slack text-white" : "bg-label-slack")}>Sổ
                        nhận
                    </button>
                </div>
                <div className={"d-flex"}>
                    <a className={"btn btn-primary"} href={window.location.origin+"/finance/cashiers/create"}>Thêm giao dịch</a>
                </div>
            </div>
        </div>
    );
}
export default CashierFilter
