import React from "react";

const AccountRelationButton = ({accountRelation, queryData, setQueryData}) => {

    const isSelect = parseInt(queryData?.filters?.account_relation) === parseInt(accountRelation.id)

    return (
        <span
            onClick={() => setQueryData({
                ...queryData,
                filters: {
                    ...queryData.filters,
                    account_relation: accountRelation.id
                }
            })}
            className={"cursor-pointer p-2 rounded me-2 " + (isSelect === true ? "border-xl-solid" : null)}
            style={{background: accountRelation?.color, whiteSpace: "nowrap", color:"white"}}>
            {accountRelation?.relation_name}
        </span>
    );
}

export default AccountRelationButton
