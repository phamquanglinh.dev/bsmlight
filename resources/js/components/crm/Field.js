import React from "react";

const RelationFields = ({account, field}) => {
    return (
        <div className={"p-1 text-center fw-bold"} style={{color: account?.relation_color}}>{account[field.field_name]}</div>
    );
}
const TextFields = ({account, field}) => {
    return <div>{account[field.field_name]}</div>
}

const Fields = ({account, field}) => {
    switch (field.type) {
        case "relation":
            if (account.account_relation !== null)
                return <RelationFields account={account} field={field}/>
            else
                return <TextFields account={account} field={field}/>
        default:
            return <TextFields account={account} field={field}/>
    }
}

export default Fields
