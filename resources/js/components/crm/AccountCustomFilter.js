import React, {useEffect, useState} from "react";
import {toast} from "react-toastify";

const AccountCustomFilter = ({setMyFields, myFields, fields}) => {
    const [show, setShow] = useState(false)
    useEffect(() => {
        console.log('mfs', JSON.parse(sessionStorage.getItem('my_field')))
    }, []);

    function findFieldByName(myFields, fieldName) {
        return myFields.find(field => field.field_name === fieldName) || null;
    }

    return (
        <div>
            <button onClick={() => setShow(true)} className="btn btn-primary">
                Tuỳ chỉnh
            </button>
            <div className={"offcanvas offcanvas-end " + (show ? "show" : "")} tabIndex="-1" id="offcanvasEnd"
                 aria-labelledby="offcanvasEndLabel">
                <div className="offcanvas-header">
                    <h5 id="offcanvasEndLabel" className="offcanvas-title">Chọn data cần hiển thị</h5>
                    <button onClick={() => setShow(false)} type="button" className="btn-close text-reset"></button>
                </div>
                <div className="offcanvas-body my-auto mx-0 flex-grow-0">
                    {fields?.map((field, key) => (
                        <div key={key} className={"mb-2"}>
                            <label className="switch">
                                <input
                                    onChange={() => {
                                        if (findFieldByName(myFields, field.field_name)) {
                                            setMyFields(myFields.filter((myField) => myField.field_name !== field.field_name))
                                        } else {
                                            setMyFields([...myFields, field])
                                        }
                                    }}
                                    type="checkbox" className="switch-input"
                                    checked={findFieldByName(myFields, field.field_name) !== null}/>
                                <span className="switch-toggle-slider">
                                        <span className="switch-on"></span>
                                        <span className="switch-off"></span>
                                    </span>
                                <span className="switch-label">{field.field_label}</span>
                            </label>
                        </div>
                    ))}
                    <div className={"my-2"}></div>
                    <button onClick={() => {
                        sessionStorage.setItem('my_fields', JSON.stringify(myFields))
                        toast.success("Lưu thành công")
                        setShow(false)
                    }} type="button" className="btn btn-primary mb-2 d-grid w-100">Lưu thiết lập
                    </button>
                    <button onClick={() => setShow(false)} type="button"
                            className="btn btn-outline-secondary d-grid w-100"
                            data-bs-dismiss="offcanvas">Đóng
                    </button>
                </div>
            </div>
        </div>
    )
}
export default AccountCustomFilter
