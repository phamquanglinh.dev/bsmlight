import React from "react";
const CoverWeekDayInput = ({setCoverData, coverData, label, weekday}) => {
    return (
        <div className="form-check col-3">
            <input
                onChange={(r) => {
                    const check = r.currentTarget.checked
                    if (check) {
                        setCoverData({
                            ...coverData,
                            week_days: [...coverData.week_days, weekday]
                        })
                    } else {
                        const newWeekDays = coverData?.week_days?.filter((day) => day !== weekday)

                        setCoverData({
                            ...coverData,
                            week_days: newWeekDays
                        })
                    }
                }}
                className="form-check-input" type="checkbox" value=""
                id={"cover.week_days." + weekday} checked={coverData?.week_days?.includes(weekday)}/>
            <label className="form-check-label" htmlFor={"cover.week_days." + weekday}>
                {label}
            </label>
        </div>
    )
}
export default CoverWeekDayInput
