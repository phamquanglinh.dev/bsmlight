import React, {useEffect, useState} from "react";

const CalendarDay = ({date, showEvent, createCover, updateEvent}) => {
    const [currentDay, setCurrentDay] = useState(false)
    return (
        <div style={{minHeight: "8rem"}}>
            <div onClick={() => createCover(date)}
                 className={"d-flex align-items-center small cursor-pointer " + (date.current ? "text-success fw-bold" : "")}>
                <span className={"mdi mdi-plus-circle me-2"}></span>
                <span>{date.day}</span>
            </div>
            <div className={"mt-2 small"}>
                {
                    (date.events ?? []).map((shift, shiftKey) =>
                        <div key={shiftKey} onClick={() => updateEvent(shift?.id)}
                             className={"rounded mb-2 p-1 cursor-pointer " + (shift.type === "loop" ? "bg-label-success text-success" : "bg-label-warning text-warning")}>
                            <div>
                                {shift.type === "loop" ? <span className="mdi mdi-sync me-1"></span> :
                                    <span className="mdi mdi-cloud-upload-outline me-2"></span>
                                }
                                {shift.classroom}
                            </div>
                            <div>
                                <span className="mdi mdi-timer-play-outline me-1"></span>
                                {shift.start} - {shift.end} </div>
                            <div>
                                <span className="mdi mdi-human-male-board me-1"></span>

                                Teacher: {shift.teacherName} </div>
                            <div>
                                <span className="mdi mdi-account-box me-1"></span>
                                TA: {shift.supporterName} </div>
                        </div>
                    )
                }
            </div>
        </div>
    );
}

export default CalendarDay
