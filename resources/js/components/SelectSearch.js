import React, {useEffect, useRef, useState} from "react";

const SelectSearch = ({url, field, search, setCallback, callback, label, identity, initValue}) => {
    const [value, setValue] = useState()
    const [data, setData] = useState([])
    const [title, setTitle] = useState('name')
    const [description, setDescription] = useState('name')
    const [isSelected, setIsSelected] = useState(false)
    const inputRef = useRef(null);
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        console.log(isLoading && isSelected)
        document.addEventListener('click', handleClickOutside);
    }, []);

    useEffect(() => {
        if (callback[field] === undefined || callback[field] === null) {
            setIsSelected(false)
            setValue('')
        }

        if (initValue !== undefined) {
            setValue(initValue)
            setIsSelected(true)
        }
    }, [callback])

    const handleClickOutside = (e) => {
        if (inputRef.current && !inputRef.current.contains(e.target)) {
            setData([]);
        }
    };

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            setIsLoading(true)
            axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
            axios.post(url, {
                value: value,
                search: search
            }).then((response) => {
                setData(response.data?.collection)
                setTitle(response.data?.title)
                setDescription(response.data?.description)
            }).catch((error) => {
                console.log(error)
            })

            setIsLoading(false)
        }
    };

    return (
        <div className="mb-3 position-relative">
            <label className="form-label">{label}</label>
            <div className={"row"}>
                <div className={"col"}>
                    <div className={"input-group"}>
                        <input
                            value={value}
                            ref={inputRef}
                            onKeyUp={handleKeyPress}
                            onChange={(r) => {
                                setValue(r.target.value)
                            }}
                            type="text" className="form-control"
                            placeholder={"Nhập mã, tên để tìm, và ấn Enter để tìm"}
                            aria-describedby="defaultFormControlHelp"
                            readOnly={(isSelected && setIsLoading) ? true : null}
                        />

                        {isSelected ?
                            <button
                                onClick={() => {
                                    setData([])
                                    const newData = Object.keys(callback)
                                        .filter(key => key !== field)
                                        .reduce((obj, key) => {
                                            obj[key] = callback[key];
                                            return obj;
                                        }, {});
                                    console.log(newData)
                                    setCallback(newData)
                                    setIsLoading(false)
                                    setIsSelected(false)
                                    setValue('')
                                }}
                                className="btn btn-outline-primary" type="button"
                                id="button-addon1">
                                <span className="mdi mdi-close-circle"></span>
                            </button>
                            : null
                        }
                    </div>
                </div>

                {isLoading ?
                    <div className={"col"}>
                        <div className="spinner-border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div> : null
                }
            </div>
            <div className={"position-absolute w-100 " + (data.length === 0 ? "d-none" : "")}
                 style={{
                     height: "10rem",
                     overflowY: "scroll",
                     background: "#28243d",
                     zIndex: 1000000,
                     boxShadow: "2px 3px 9px 3px #0c0c0c8a"
                 }}>
                {data.map((item, key) =>
                    <div onClick={() => {
                        setCallback({
                            ...callback, [field]: item[identity]
                        })
                        setValue(item?.[description] + " - " + item?.[title])
                        setIsSelected(true)
                    }} className={"p-1 border cursor-pointer"} key={key}>
                        <div className={"fw-bold p-2 rounded"}>{item?.[title]}</div>
                        <div className={"p-2 rounded small"}>{item?.[description]}</div>
                    </div>
                )}
            </div>
        </div>
    )
}

export default SelectSearch
