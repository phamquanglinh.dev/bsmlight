import React, {useEffect, useRef, useState} from "react";

const SelectEntityFilter = ({setData, data, options, label, itemKey, avatar, title, description}) => {
    const [show, setShow] = useState(false);
    const inputRef = useRef(null);
    const [currentData, setCurrentData] = useState({});
    useEffect(() => {
        setCurrentData({})
    }, [options])
    useEffect(() => {
        const handleClickOutside = (e) => {
            setShow(e.target.id.includes(itemKey + "_"))
        };

        document.addEventListener('click', handleClickOutside);

        return () => {
            document.removeEventListener('click', handleClickOutside);
        };
    }, []);

    useEffect(() => {
        setShow(false);
    }, [currentData]);

    return (
        <div className="position-relative w-100">
            <div>
                {show === true ? (
                    <input
                        placeholder={label}
                        id={itemKey + "_"}
                        className="p-2 w-100 border-primary bg-none rounded"
                        style={{background: "none"}}
                    />
                ) : (
                    <div
                        id={itemKey + "__"}
                        ref={inputRef}
                        onClick={() => setShow(!show)}

                        className="cursor-pointer p-2 d-flex border rounded"
                    >
                        <div>
                            <img
                                id={itemKey + "___"}
                                className="avatar me-2 rounded-circle"
                                style={{borderWidth: "0.2rem", borderColor: "white", borderStyle: "solid"}}
                                alt="avatar"
                                src={currentData[avatar] || 'https://t4.ftcdn.net/jpg/03/59/58/91/360_F_359589186_JDLl8dIWoBNf1iqEkHxhUeeOulx0wOC5.jpg'}
                            />
                        </div>

                        <div>
                            <div id={itemKey + "____"}>{currentData[title] || 'Chọn...'}</div>
                            <div id={itemKey + "_____"} className="small">{currentData[description] || ''}</div>
                        </div>
                    </div>
                )}
            </div>
            <div className={"position-absolute w-100 border " + (show === true ? "d-block" : "d-none")}
                 style={{maxHeight: "30rem", overflowY: "scroll"}}>
                {options.map((item, key) => (
                    <div
                        onClick={() => {
                            setData({
                                ...data,
                                [itemKey]: item?.id
                            });
                            setCurrentData(item);
                            setShow(false);
                        }}
                        className={"cursor-pointer p-2 d-flex border-bottom"}
                        key={key}
                    >
                        <div>
                            <img
                                className="avatar me-2 rounded-circle"
                                style={{borderWidth: "0.2rem", borderColor: "white", borderStyle: "solid"}}
                                alt="avatar"
                                src={item[avatar]}
                            />
                        </div>

                        <div>
                            <div className={currentData === item ? 'fw-bold text-success' : ''}>{item[title]}</div>
                            <div className="small">{item[description]}</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default SelectEntityFilter;
