import React, {useEffect} from "react";
import SelectSearch from "./SelectSearch";

const CalendarFilter = ({filters, setFilters}) => {
    useEffect(() => {
        console.log(filters)
    }, [filters])
    return (
        <div className={"my-2 mt-4"}>
            <div className={"row"}>
                <div className={"col-4"}>
                    <SelectSearch
                        label={"Lớp học"}
                        callback={filters}
                        setCallback={setFilters}
                        field={"classroom_id"}
                        identity={"id"}
                        url={window.location.origin + "/f/classroom"}
                        search={"name,uuid"}
                    />
                </div>
                <div className={"col-4"}>
                    <SelectSearch
                        label={"Giáo viên"}
                        callback={filters}
                        setCallback={setFilters}
                        field={"teacher_id"}
                        identity={"id"}
                        url={window.location.origin + "/f/teacher"}
                        search={"name,uuid"}
                    />
                </div>
                <div className={"col-4"}>
                    <SelectSearch
                        label={"Trợ giảng"}
                        callback={filters}
                        setCallback={setFilters}
                        field={"supporter_id"}
                        identity={"id"}
                        url={window.location.origin + "/f/supporter"}
                        search={"name,uuid"}
                    />
                </div>
            </div>
        </div>
    )
}
export default CalendarFilter
