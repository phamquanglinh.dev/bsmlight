import {useEffect, useState} from "react";
import React from "react";

const NotificationTab = () => {
    const [notifications, setNotifications] = useState([]);
    const [page, setPage] = useState(1)
    const [show, setShow] = useState(false)
    const [tab, setTab] = useState('all')

    const updateNotification = ({updateData}) => {
        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
        axios.put(window.location.origin + "/s/notification", updateData).then((response) => {
            const newNotification = response.data
            const updatedNotification = notifications.map((notification) => {
                if (notification.id === newNotification.id) {
                    return newNotification
                }

                return notification
            })

            setNotifications(updatedNotification)
        }).catch((error) => {

        })
    }

    useEffect(() => {
        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
        axios.get(window.location.origin + "/s/notification?page=" + page + "&tab=" + tab).then((response) => {
            setNotifications(response.data)
        }).catch((error) => {

        })
    }, [page, tab]);

    return (
        <div>
            <div style={{width: "35rem"}} className={"offcanvas offcanvas-end " + (show === true ? "show" : "")}
                 tabIndex="-1" id={"vcl"}
                 aria-labelledby="offcanvasEndLabel">
                <div className="offcanvas-header">
                    <h5 id="offcanvasEndLabel" className="offcanvas-title">Thông báo hệ thống</h5>
                    <button type="button"
                            id={"vcl-close"}
                            className="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                </div>
                <div className="offcanvas-body my-auto mx-0 flex-grow-0 w-100 h-100">
                    <div>
                        <button
                            onClick={() => {
                                setTab('all')
                            }}
                            className={"btn me-2 " + (tab === 'all' ? 'btn-primary' : 'btn-light')}>Tất cả
                        </button>
                        <button
                            onClick={() => {
                                setTab('pin')
                            }}
                            className={"btn me-2 " + (tab === 'pin' ? 'btn-primary' : 'btn-light')}>Đã ghim
                        </button>
                        <button
                            onClick={() => {
                                setTab('unread')
                            }}
                            className={"btn me-2 " + (tab === 'unread' ? 'btn-primary' : 'btn-light')}>Chưa đọc
                        </button>
                        <button
                            onClick={() => {
                                setTab('read')
                            }}
                            className={"btn me-2 " + (tab === 'read' ? 'btn-primary' : 'btn-light')}>Đã đọc
                        </button>
                    </div>
                    {notifications?.map((notification, key) =>
                        <li key={key}
                            style={{
                                paddingTop: "1rem!important"
                            }}
                            className="border-bottom py-3 w-100 list-group-item list-group-item-action dropdown-notifications-item position-relative d-flex align-items-start">

                            <a className={"w-100 cursor-pointer"}>
                                <div className="d-flex align-items-center gap-2">
                                    <div className="d-flex flex-column flex-grow-1 overflow-hidden w-px-250">
                                        <h6
                                            onClick={() => {
                                                updateNotification({
                                                    updateData: {
                                                        ...notification, read: 1
                                                    }
                                                })
                                                window.location.href = notification?.url
                                            }}
                                            className={"mb-1 small " + (parseInt(notification?.pin) === 1 ? "text-warning" : "")}>
                                            {
                                                parseInt(notification.pin) ?
                                                    <span className="mdi mdi-pin"></span> : null
                                            }

                                            {notification?.title}</h6>
                                        <small
                                            onClick={() => {
                                                updateNotification({
                                                    updateData: {
                                                        ...notification, read: 1
                                                    }
                                                })
                                                window.location.href = notification?.url
                                            }}
                                            className="text-truncate text-body small">{notification?.description}</small>

                                        <div style={{right: "0.5rem"}} className={"position-absolute"}>
                                            <div className={"d-flex align-items-center"}>

                                                {parseInt(notification.read) === 0 &&
                                                    <div style={{width: "1rem", height: "1rem"}}
                                                         className={"bg-primary rounded-circle"}></div>
                                                }
                                                <div className="btn-group p-1 rounded-circle">
                                                    <span
                                                        type={"button"}
                                                        className={"text-primary p-0 w-100"}
                                                        style={{
                                                            margin: "-4px"
                                                        }}
                                                        data-bs-toggle="dropdown"
                                                    >
                                                        <i className="mdi mdi-dots-vertical p-0 m-0"></i>
                                                    </span>
                                                    <ul className="dropdown-menu" style={{width: '3rem!important'}}>
                                                        <li>
                                                            {parseInt(notification.read) === 1 ?
                                                                <span
                                                                    onClick={() => {
                                                                        updateNotification({
                                                                            updateData: {
                                                                                ...notification, read: 0
                                                                            }
                                                                        })
                                                                    }}
                                                                    className="dropdown-item w-75">Đánh dấu là chưa
                                                                    đọc</span> :
                                                                <span
                                                                    onClick={() => {
                                                                        updateNotification({
                                                                            updateData: {
                                                                                ...notification, read: 1
                                                                            }
                                                                        })
                                                                    }}
                                                                    className="dropdown-item w-75">Đánh dấu là đã
                                                                    đọc</span>}
                                                        </li>
                                                        {parseInt(notification?.pin) === 0 ?
                                                            <li>
                                                                <span
                                                                    onClick={() => {
                                                                        updateNotification({
                                                                            updateData: {
                                                                                ...notification, pin: 1
                                                                            }
                                                                        })
                                                                    }}
                                                                    className="dropdown-item w-75">Ghim</span>
                                                            </li> :
                                                            <li>
                                                                <span
                                                                    onClick={() => {
                                                                        updateNotification({
                                                                            updateData: {
                                                                                ...notification, pin: 0
                                                                            }
                                                                        })
                                                                    }}
                                                                    className="dropdown-item w-75">Bỏ ghim</span>
                                                            </li>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="text-start dropdown-notifications-actions">
                                    <small className="text-muted">{notification?.distance_time}</small>
                                </div>
                            </a>
                        </li>
                    )}
                    <div className={"text-center my-2"}>
                        <a className={"cursor-pointer text-primary"} onClick={() => {
                            setPage(page + 1)
                        }}>Xem thêm</a>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default NotificationTab
