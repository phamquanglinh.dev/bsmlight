import React, {Component} from "react";
import Calendar from "./Calendar";
import ReactDOM from "react-dom";
import NotificationTab from "./NotificationTab";

class Notification extends Component {
    render() {
        return (
            <div>
                <NotificationTab/>
            </div>
        )
    }
}

ReactDOM.render(<Notification/>, document.getElementById('notification-root'))
