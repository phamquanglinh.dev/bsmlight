import React, {Component} from "react";
import Calendar from "./Calendar";
import ReactDOM from "react-dom";

class App extends Component {
    render() {
        return (
            <div>
                <Calendar/>
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById('react-root'))
