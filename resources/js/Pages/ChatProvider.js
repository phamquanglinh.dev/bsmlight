import React, {Component} from "react";
import ReactDOM from "react-dom";
import ChatScreen from "./ChatScreen";

class ChatProvider extends Component {
    render() {
        return (
            <div>
                <ChatScreen/>
            </div>
        )
    }
}

ReactDOM.render(<ChatProvider/>, document.getElementById('beli-chat-root'))
