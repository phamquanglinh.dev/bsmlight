import {useEffect, useState} from "react";
import ChatTab from "./ChatTab";
import ChatList from "./ChatList";
import React from "react";
import {getSharedUsers} from "./api/ChatApi";

const ChatScreen = () => {
    // const [channel, setChannel] = useState(null)
    const [channel, setChannel] = useState(null)
    const [title, setTitle] = useState('Beli Chat')
    const [showChat, setShowChat] = useState(false)
    const [users, setUsers] = useState([])
    useEffect(() => {
        const localShowChat = localStorage.getItem('show_chat')
        const localChatName = localStorage.getItem('current_chat_name')
        if (localShowChat) {
            setShowChat(localShowChat === '1')
        }

        if (localChatName) {
            setChannel(localChatName)
            setTitle(localStorage.getItem('current_chat_title'))
        }

        getSharedUsers({setUsers: setUsers})
    }, [])

    useEffect(() => {
        localStorage.setItem('current_chat_title', title)
    }, [title]);

    useEffect(() => {
        if (channel !== null && typeof channel === 'string') {
            localStorage.setItem('current_chat_name', channel)
        } else {
            localStorage.removeItem('current_chat_name')
        }
    }, [channel])
    return (
        <div>
            {(showChat === true) ?
                <div className={"chat-container rounded shadow"}>
                    <div className={"d-flex bg-primary rounded-top p-1 py-2 justify-content-between"}>
                        <div className={"text-white fw-bold"}>
                            {title !== "Beli Chat" &&
                                <span onClick={() => {
                                    setChannel(null)
                                    setTitle("Beli Chat")
                                }} className="mdi mdi-backspace me-2 cursor-pointer"></span>
                            }
                            <span>{title}</span>
                        </div>
                        <span onClick={() => {
                            setShowChat(false)
                            localStorage.setItem('show_chat', '0')
                        }} className="mdi mdi-close-thick text-white cursor-pointer"></span>
                    </div>
                    {channel ? <ChatTab channel={channel} setChannel={setChannel} users={users} setTitle={setTitle}/> :
                        <ChatList setChannel={setChannel} users={users} setTitle={setTitle}/>}
                </div> :
                <div onClick={() => {
                    setShowChat(true)
                    localStorage.setItem('show_chat', '1')
                }} className={"chat-btn cursor-pointer"}>
                    <span className="mdi mdi-message mdi-36px text-primary"></span>
                </div>
            }
        </div>
    )
}
export default ChatScreen
