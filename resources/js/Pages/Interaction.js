import ReactDOM from "react-dom";
import React, {useEffect, useState} from "react";

export const Interaction = () => {
    const [studentId, setStudentId] = useState(localStorage.getItem('student_view_id'))
    const [page, setPage] = useState(1)
    const [logs, setLogs] = useState([])
    const getStudentCardLog = () => {
        const url = window.location.origin + "/s/interaction/" + studentId + "?page=" + page
        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
        axios.get(url).then((response) => {
            setLogs(response.data)
        }).catch((error) => {

        })
    }
    const style = {
        whiteSpace: 'pre-line' // Đảm bảo các dấu xuống dòng trong nội dung được hiển thị
    };


    useEffect(() => {
        getStudentCardLog()
    }, []);
    return (
        <div className="card-body pt-0" style={{height: "40rem", overflowY: "scroll"}}>
            <ul className="timeline mb-0 pt-2">
                {logs.map((log, key) =>
                    <li key={key} className="timeline-item timeline-item-transparent">
                        <span className="timeline-point timeline-point-primary"></span>
                        <div className="timeline-event">
                            <div className="timeline-header mb-3">
                                <h6 className="mb-0">{log?.title}</h6>
                                <small className="text-muted">{log?.time}</small>
                            </div>

                            <p className="mb-2">
                                <div style={style} dangerouslySetInnerHTML={{__html:log?.content}}></div>
                            </p>

                            {(log?.teacher_note) ?
                                <p className={"border p-1 rounded p-2 shadow"}>
                                    <div style={style} dangerouslySetInnerHTML={{__html: log?.teacher_note}}></div>
                                </p>
                                : null}
                        </div>
                    </li>
                )}
            </ul>
        </div>
    )
}

ReactDOM.render(<Interaction/>, document.getElementById('interaction-react'))
