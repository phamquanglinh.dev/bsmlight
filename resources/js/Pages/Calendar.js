import React, {useEffect, useState} from "react";
import CalendarDay from "../components/CalendarDay";
import CalendarFilter from "../components/CalendarFilter";
import CoverWeekDayInput from "../components/CoverWeekDayInput";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Select from 'react-select'
import SelectSearch from "../components/SelectSearch";

const Calendar = () => {
    const [calendarQuery, setCalendarQuery] = useState({
        month: new Date().getMonth() + 1,
        year: new Date().getFullYear(),
    });
    const [months, setMonths] = useState([]);
    const [shift, setShift] = useState(null);
    const [filters, setFilters] = useState({})
    const [coverData, setCoverData] = useState({})
    const [coverErrors, setCoverErrors] = useState({})
    const showEvent = (shift) => {
        setShift(shift)
    }
    const createCover = (date) => {
        setCoverData({
            week_days: [...coverData?.weekday ?? [], date?.weekday],
            start_cover_date: date?.original_format_day,
            end_cover_date: date?.original_format_day,
            start: null,
            end: null,
            classroom_id: null,
            supporter_id: null,
            teacher_id: null
        })
    }

    const getCover = ({id, setCoverData}) => {
        const url = window.location.origin + "/s/cover/" + id
        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
        axios.get(url).then((response) => {
            console.log(response.data)
            setCoverData(response.data)
        })
    }


    const updateCover = (id) => {
        getCover({id: id, setCoverData: setCoverData})
    }

    function deleteCoverAction() {
        const result = confirm('Bạn chắc chắn muốn xoá ?')

        const url = window.location.origin + "/s/cover/" + coverData?.id

        if (result) {
            axios.delete(url, coverData)
                .then(function (response) {
                    toast.success('Thêm thành công', {
                        position: "top-right",
                        autoClose: 2000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        theme: "dark",
                    });
                    setCoverData({
                        week_days: [],
                        start_cover_date: null,
                        end_cover_date: null,
                        start: null,
                        end: null,
                        classroom_id: null,
                        supporter_id: null,
                        teacher_id: null
                    })
                    getCalendar()
                })
                .catch(function (error) {
                    toast.error('Không thành công. Vui lòng thử lại', {
                        position: "top-right",
                        autoClose: 2000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme: "dark",
                    });
                    setCoverErrors(error?.response?.data?.errors)
                });
        }
    }

    const createCoverAction = () => {
        const url = window.location.origin + "/s/cover"
        console.clear()
        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
        console.log(coverData)
        axios.post(url, coverData)
            .then(function (response) {
                toast.success('Thêm thành công', {
                    position: "top-right",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    theme: "dark",
                });
                setCoverData({
                    week_days: [],
                    start_cover_date: null,
                    end_cover_date: null,
                    start: null,
                    end: null,
                    classroom_id: null,
                    supporter_id: null,
                    teacher_id: null
                })
                getCalendar()
            })
            .catch(function (error) {
                toast.error('Không thành công. Vui lòng thử lại', {
                    position: "top-right",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "dark",
                });
                setCoverErrors(error?.response?.data?.errors)
            });
    }

    function objectToParams(obj) {
        return Object.entries(obj).map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`).join('&');
    }

    const getCalendar = () => {
        const queryParams = objectToParams(calendarQuery);
        const url = window.location.origin + "/s/calendar?" + queryParams

        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

        axios.get(url)
            .then(function (response) {
                setMonths(response.data)
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    const getClassroomList = () => {
        const url = window.location.origin + "/s/classroom"

        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

        axios.get(url)
            .then(function (response) {
                setClassrooms(response.data)
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    const getTeachersList = () => {
        const url = window.location.origin + "/s/teachers"

        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

        axios.get(url)
            .then(function (response) {
                setTeachers(response.data)
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    const getSupporterList = () => {
        const url = window.location.origin + "/s/supporters"

        axios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

        axios.get(url)
            .then(function (response) {
                setSupporters(response.data)
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    useEffect(() => {
        getCalendar()
        getClassroomList()
        getTeachersList()
        getSupporterList()
    }, []);

    useEffect(() => {
        getCalendar()
    }, [calendarQuery])


    return (
        <div className={"container-fluid"}>

            <div className={"my-2"}>
                <span
                    onClick={() => {
                        if (calendarQuery?.month > 1) {
                            setCalendarQuery({
                                ...calendarQuery,
                                month: calendarQuery?.month - 1
                            })
                        } else {
                            setCalendarQuery({
                                ...calendarQuery,
                                year: calendarQuery?.year - 1,
                                month: 12
                            })
                        }
                    }}
                    className="mdi mdi-chevron-left fw-bold mdi-48px cursor-pointer"></span>
                <span>Tháng {calendarQuery?.month} - {calendarQuery?.year}</span>
                <span onClick={() => {
                    if (calendarQuery?.month < 12) {
                        setCalendarQuery({
                            ...calendarQuery,
                            month: calendarQuery?.month + 1
                        })
                    } else {
                        setCalendarQuery({
                            ...calendarQuery,
                            year: calendarQuery?.year + 1,
                            month: 1
                        })
                    }
                }}
                      className="mdi mdi-chevron-right fw-bold mdi-48px cursor-pointer"></span>

            </div>
            <div>
                <CalendarFilter
                    filters={calendarQuery}
                    setFilters={setCalendarQuery}
                />
            </div>
            <div className={"row m-0"}>
                {["Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7", "Chủ nhật"].map((head, key) =>
                    <div key={key} className={"col p-2 text-center border text-white bg-primary"}>{head}</div>
                )}
            </div>

            <div>
                {
                    months.map((week, weekKey) =>
                        <div key={weekKey} className={"row m-0"}>
                            {week.map((date, dayKey) =>
                                <div key={dayKey}
                                     className={"col p-2 border " + (date.active ? "" : "bg-disable-calendar")}>
                                    <CalendarDay
                                        updateEvent={updateCover}
                                        date={date} showEvent={showEvent} createCover={createCover}/>
                                </div>
                            )}
                        </div>
                    )
                }
            </div>

            <div>
                <div style={{width: "500px"}}
                     className={"offcanvas offcanvas-end " + ((coverData.start_cover_date !== undefined && coverData.start_cover_date !== null) ? "show" : null)}
                     tabIndex="-1" id="offcanvasEnd"
                     aria-labelledby="offcanvasEndLabel">
                    <div className="offcanvas-header">
                        <h5 id="offcanvasEndLabel"
                            className="offcanvas-title">{coverData?.id ? 'Cập nhật ' : 'Thêm '} lịch cover lớp học</h5>
                        <button onClick={() => setCoverData({})} type="button" className="btn-close text-reset"
                                data-bs-dismiss="offcanvas"
                                aria-label="Close"></button>
                    </div>

                    <div className="offcanvas-body h-100 my-auto mx-0 flex-grow-0">
                        <div>
                            <SelectSearch
                                label={'Chọn lớp học'}
                                identity={'id'}
                                field={'classroom_id'}
                                callback={coverData}
                                setCallback={setCoverData}
                                url={window.location.origin + "/f/classroom"}
                                search={'name,uuid'}
                                initValue={coverData?.classroom_label}
                            />
                            <div className={"small mt-1 text-danger"}>{coverErrors?.classroom_id}</div>
                        </div>
                        <div>
                            <SelectSearch
                                label={'Chọn giáo viên'}
                                identity={'id'}
                                field={'teacher_id'}
                                callback={coverData}
                                setCallback={setCoverData}
                                url={window.location.origin + "/f/teacher"}
                                search={'name,uuid'}
                                initValue={coverData?.teacher_label}
                            />
                            <div className={"small mt-1 text-danger"}>{coverErrors?.teacher_id}</div>
                        </div>

                        <div>
                            <SelectSearch
                                label={'Chọn trợ giảng'}
                                identity={'id'}
                                field={'supporter_id'}
                                callback={coverData}
                                setCallback={setCoverData}
                                url={window.location.origin + "/f/supporter"}
                                search={'name,uuid'}
                                initValue={coverData?.supporter_label}
                            />
                            <div className={"small mt-1 text-danger"}>{coverErrors?.supporter_id}</div>
                        </div>

                        <div className={"mb-3"}>
                            <div className={"row"}>
                                <div className={'col-6'}>
                                    <div className="form-floating form-floating-outline mb-6">
                                        <input
                                            onChange={(r) => {
                                                setCoverData({
                                                    ...coverData,
                                                    start_cover_date: r.target.value
                                                })
                                            }}
                                            value={coverData?.start_cover_date} className="form-control"
                                            type="date" id="html5-date-input"/>
                                        <label htmlFor="html5-date-input">Ngày bắt đầu</label>
                                        <div className={"small mt-1 text-danger"}>{coverErrors?.start_cover_date}</div>
                                    </div>
                                </div>
                                <div className={'col-6'}>
                                    <div className="form-floating form-floating-outline mb-6">
                                        <input
                                            onChange={(r) => {
                                                setCoverData({
                                                    ...coverData,
                                                    end_cover_date: r.target.value
                                                })
                                            }}
                                            value={coverData?.end_cover_date} className="form-control"
                                            type="date" id="html5-date-input"/>
                                        <label htmlFor="html5-date-input">Ngày kết thúc</label>
                                        <div className={"small mt-1 text-danger"}>{coverErrors?.end_cover_date}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className={'my-3'}>
                            <div className={"mb-1"}>Chọn ngày cover</div>
                            <div className={"row m-0"}>
                                <CoverWeekDayInput label={"T2"} weekday={2} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                                <CoverWeekDayInput label={"T3"} weekday={3} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                                <CoverWeekDayInput label={"T4"} weekday={4} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                                <CoverWeekDayInput label={"T5"} weekday={5} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                                <CoverWeekDayInput label={"T6"} weekday={6} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                                <CoverWeekDayInput label={"T7"} weekday={7} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                                <CoverWeekDayInput label={"CN"} weekday={8} coverData={coverData}
                                                   setCoverData={setCoverData}/>
                            </div>
                            <div className={"small mb-2 mt-1"}>
                                Các buổi cover sẽ được tự tạo từ ngày bắt đầu đến ngày kết thúc , dựa theo thứ đã chọn
                            </div>
                            <div className={"small mt-1 text-danger"}>{coverErrors?.week_days}</div>
                        </div>

                        <div className={"mb-3"}>
                            <div className={"row"}>
                                <div className={'col-6'}>
                                    <div
                                        className="form-floating form-floating-outline mb-6">
                                        <input
                                            onChange={(r) => {
                                                setCoverData({
                                                    ...coverData,
                                                    start: r.target.value
                                                })
                                            }}
                                            value={coverData?.start ?? "--:--"}
                                            className="form-control" type="time"
                                            id="html5-date-input"/>
                                        <label htmlFor="html5-date-input">Từ </label>
                                        <div className={"small mt-1 text-danger"}>{coverErrors?.start}</div>
                                    </div>
                                </div>
                                <div className={'col-6'}>
                                    <div className="form-floating form-floating-outline mb-6">
                                        <input
                                            onChange={(r) => {
                                                setCoverData({
                                                    ...coverData,
                                                    end: r.target.value
                                                })
                                            }}
                                            value={coverData?.end ?? "--:--"} className="form-control" type="time"
                                            id="html5-date-input"/>
                                        <label htmlFor="html5-date-input">Đến </label>
                                        <div className={"small mt-1 text-danger"}>{coverErrors?.end}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className={"row m-0"}>
                            <button type={'button'}
                                    onClick={createCoverAction}
                                    className="btn btn-outline-success col me-2"
                                    data-bs-dismiss="offcanvas">{coverData?.id ? 'Cập nhật' : "Thêm mới"}
                            </button>
                            <button type={'button'}
                                    onClick={deleteCoverAction}
                                    className="btn btn-outline-danger col me-2"
                                    data-bs-dismiss="offcanvas">Xoá
                            </button>
                            <button onClick={() => setCoverData({})} type="button"
                                    className="btn btn-outline-secondary col"
                                    data-bs-dismiss="offcanvas">Đóng
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer/>
        </div>
    )
}

export default Calendar
