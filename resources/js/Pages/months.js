export const monthsData = [
    [
        {
            day: "29/04/2024",
            weekday: 2,
            active: false
        },
        {
            day: "30/04/2024",
            weekday: 3,
            active: false
        },
        {
            day: "01/05/2024",
            weekday: 4,
            active: true
        },
        {
            day: "02/05/2024",
            weekday: 5,
            active: true,
            events: [
                {
                    classroom: "Lớp C001",
                    start: "19:00",
                    end: "21:00",
                    teacherName: "Sophia",
                    supporterName: "Lan Anh",
                },
            ]
        },
        {
            day: "03/05/2024",
            weekday: 6,
            active: true
        },
        {
            day: "04/05/2024",
            weekday: 7,
            active: true
        },
        {
            day: "05/05/2024",
            weekday: 8,
            active: true
        },
    ],
    [{
        day: "06/05/2024",
        weekday: 2,
        active: true
    },
        {
            day: "07/05/2024",
            weekday: 3,
            active: true
        },
        {
            day: "08/05/2024",
            weekday: 4,
            active: true
        },
        {
            day: "09/05/2024",
            weekday: 5,
            active: true
        },
        {
            day: "10/05/2024",
            weekday: 6,
            active: true,
            events: [
                {
                    classroom: "Lớp C001",
                    start: "19:00",
                    end: "21:00",
                    teacherName: "Sophia",
                    supporterName: "Lan Anh",
                },
            ]
        },
        {
            day: "11/05/2024",
            weekday: 7,
            active: true,
            current: true
        },
        {
            day: "12/05/2024",
            weekday: 1,
            active: true
        },],
    [
        {
            day: "13/05/2024",
            weekday: 2,
            active: true
        },
        {
            day: "14/05/2024",
            weekday: 3,
            active: true,
            events: [
                {
                    classroom: "Lớp C001",
                    start: "19:00",
                    end: "21:00",
                    teacherName: "Sophia",
                    supporterName: "Lan Anh",
                },
                {
                    classroom: "Lớp C002",
                    start: "22:00",
                    end: "23:00",
                    teacherName: "Anna",
                    supporterName: "Lan Anh",
                },
            ],
        },
        {
            day: "15/05/2024",
            weekday: 4,
            active: true
        },
        {
            day: "16/05/2024",
            weekday: 5,
            active: true
        },
        {
            day: "17/05/2024",
            weekday: 6,
            active: true
        },
        {
            day: "18/05/2024",
            weekday: 7,
            active: true,
            events: [
                {
                    classroom: "Lớp C001",
                    start: "19:00",
                    end: "21:00",
                    teacherName: "Sophia",
                    supporterName: "Lan Anh",
                },
                {
                    classroom: "Lớp C002",
                    start: "22:00",
                    end: "23:00",
                    teacherName: "Anna",
                    supporterName: "Lan Anh",
                },
            ]
        },
        {
            day: "19/05/2024",
            weekday: 1,
            active: true
        },
    ],
    [
        {
            day: "20/05/2024",
            weekday: 2,
            active: true
        },
        {
            "day": "21/05/2024",
            "weekday": 3,
            "active": true
        },
        {
            "day": "22/05/2024",
            "weekday": 4,
            "active": true
        },
        {
            "day": "23/05/2024",
            "weekday": 5,
            "active": true
        },
        {
            "day": "24/05/2024",
            "weekday": 6,
            "active": true
        },
        {
            "day": "25/05/2024",
            "weekday": 7,
            "active": true
        },
        {
            "day": "26/05/2024",
            "weekday": 1,
            "active": true
        },
    ],
    [
        {
            "day": "27/05/2024",
            "weekday": 2,
            "active": true
        },
        {
            "day": "28/05/2024",
            "weekday": 3,
            "active": true
        },
        {
            "day": "29/05/2024",
            "weekday": 4,
            "active": true
        },
        {
            "day": "30/05/2024",
            "weekday": 5,
            "active": true
        },
        {
            "day": "31/05/2024",
            "weekday": 6,
            "active": true
        },
        {
            "day": "01/06/2024",
            "weekday": 7,
            "active": false
        },
        {
            "day": "02/06/2024",
            "weekday": 8,
            "active": false
        }
    ]
];
