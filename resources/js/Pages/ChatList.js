import React, {useEffect, useState} from "react";
import {getAllChatRoom} from "./api/ChatApi";

const ChatList = ({setChannel, users, setTitle}) => {
    const [rooms, setRooms] = useState([])
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        getAllChatRoom({setRooms: setRooms, setLoading: setLoading})
    }, []);

    useEffect(() => {
        console.log("user_k", users)
    }, [users])

    const getUser = (uuid) => {
        return users.find((user) => user?.chat_uuid === uuid)
    }

    return (
        <div
            className={"p-1 mt-2 w-100" + (loading === true ? " d-flex justify-content-center align-items-center" : "")}
            style={{overflowY: "scroll", height: '30rem'}}>
            {loading === false &&
                <div className={"pb-5"}>
                    {rooms?.map((room, key) =>
                        <div onClick={() => {
                            setChannel(room?.name)
                            setTitle(room?.label || 'Chưa đặt tên')
                        }} key={key} className={"d-flex cursor-pointer align-items-center border-top p-1 w-100"}>
                            <div>
                                <img alt={"avg"} className={"avatar-md rounded-circle"} src={room?.avatar}/>
                            </div>
                            <div className={"ms-2"}>
                                <div className={"fw-bold"}>{room?.label || 'Chưa đặt tên'}</div>
                                {room?.current_message ?
                                    <div className={"small"}>
                                        {getUser(room?.current_message?.user_uuid)?.name}: {room?.current_message?.message}
                                    </div>
                                    :
                                    <div className={"small"}>Chưa có tin nhắn</div>
                                }
                            </div>
                        </div>
                    )}
                </div>
            }
            {
                loading === true &&
                <div>
                    <img alt={""} src={"https://cdn.pixabay.com/animation/2023/11/30/10/11/10-11-02-622_512.gif"}
                         style={{width: '2rem', height: '2rem'}}/>
                </div>
            }
        </div>
    )
}
export default ChatList
