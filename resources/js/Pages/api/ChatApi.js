import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/s/chat"

const baseAxios = axios.create({
    retries: 3,
    timeout: 100000,
    retryDelay: 100
})
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

export const getAllChatRoom = ({uuid, setRooms, setLoading}) => {
    setLoading(true)
    const url = baseUrl + "/channels"
    baseAxios.get(url).then((r) => {
        setRooms(r.data.data)
        setLoading(false)
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
        setLoading(false)
    })
}
export const getSharedUsers = ({setUsers}) => {
    const url = baseUrl + "/users"
    baseAxios.get(url).then((r) => {
        setUsers(r.data.data)
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
    })
}

export const getAllMessage = ({setMessages, channel}) => {
    const url = baseUrl + "/messages/" + channel
    baseAxios.get(url).then((r) => {
        setMessages(r.data.data)
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
    })
}

export const getUserPreference = ({setUserPreference}) => {
    const url = baseUrl + "/user"
    baseAxios.get(url).then((r) => {
        setUserPreference(r.data.data)
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
    })
}

export const sendPrivateMessage = ({messageData, setMessages, messages, setSending}) => {
    setSending(true)
    const url = baseUrl + "/messages"
    baseAxios.post(url, messageData).then((r) => {
        setMessages([r.data.data, ...messages])
        setSending(false)
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
        setSending(false)
    })
}

export const uploadImage = ({file, setAttachment, setUploading}) => {
    setUploading(true)
    const formData = new FormData();
    formData.append('file', file);
    const url = baseUrl + "/media/image"
    baseAxios.post(url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    }).then((r) => {
        setAttachment(r.data.data)
        setUploading(false)
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
        setUploading(false)
    })
}
