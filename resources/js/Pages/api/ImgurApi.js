export const handleUpload = async ({selectedFile, setAttachment}) => {
    if (!selectedFile) return;

    const formData = new FormData();
    formData.append('image', selectedFile);
    formData.append('type', 'file');
    formData.append('title', 'Simple upload');
    formData.append('description', 'This is a simple image upload in Imgur');

    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: 'https://api.imgur.com/3/image',
        headers: {
            'Authorization': 'Client-ID bfae53af93c8c2d', // Thay YOUR_CLIENT_ID bằng Client ID của bạn
            'Content-Type': 'multipart/form-data',
        },
        data: formData
    };

    try {
        const response = await axios.request(config);
        if (response.data.success) {
            setAttachment(response.data.data.link);
            alert('Upload thành công!');
        } else {
            alert('Upload thất bại!');
        }
    } catch (error) {
        console.error('Có lỗi xảy ra khi upload:', error);
        alert('Có lỗi xảy ra khi upload.');
    }
}
