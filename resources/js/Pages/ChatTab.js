import React, {useEffect, useRef, useState} from "react";
import {getAllMessage, getUserPreference, sendPrivateMessage, uploadImage} from "./api/ChatApi";
import {handleUpload} from "./api/ImgurApi";

const SendMessageBox = ({message, user}) => {

    return (
        <div className={"d-flex align-items-start justify-content-end my-3"}>
            <div className={"p-2 text-white bg-primary rounded"}>
                {(message?.type === '0' || message?.type === 0) &&
                    <div>{message?.message}</div>
                }
                {message?.type === '1' || message?.type === 1 &&
                    <div>
                        <img alt={""} onClick={()=> {window.open(message?.message)}} src={message?.message} style={{maxWidth:'8rem'}}/>
                    </div>
                }
                {message?.type === '2' || message?.type === 2 &&
                    <div>
                        <video controls={true} onClick={()=> {window.open(message?.message)}} src={message?.message} style={{maxWidth:'8rem'}}/>
                    </div>
                }
                <div className={"text-mini mt-1 justify-content-end w-100 d-flex"}>{message?.created_at}</div>
            </div>
            <img
                alt={"sn"}
                src={user?.avatar}
                className={"avatar-sm rounded-circle ms-2"}/>
        </div>
    )
}

const ReceiveMessageBox = ({message, user}) => {
    return (
        <div className={"d-flex align-items-start justify-content-start my-3"}>
            <img
                alt={"rv"}
                src={user?.avatar}
                className={"avatar-sm rounded-circle me-2"}/>
            <div className={"p-2 text-white bg-receive rounded"}>
                {(message?.type === '0' || message?.type === 0) &&
                    <div>{message?.message}</div>
                }
                {message?.type === '1' || message?.type === 1 &&
                    <div>
                        <img onClick={()=> {window.open(message?.message)}} src={message?.message} style={{maxWidth:'8rem'}} alt={""}/>
                    </div>
                }
                {message?.type === '2' || message?.type === 2 &&
                    <div>
                        <video controls={true} onClick={()=> {window.open(message?.message)}} src={message?.message} style={{maxWidth:'8rem'}}/>
                    </div>
                }
                <div className={"text-mini mt-1 justify-content-start w-100 d-flex"}>{message?.created_at}</div>
            </div>

        </div>
    )
}

const ChatTab = ({channel, setChannel, users}) => {
    const getUser = (uuid) => {
        return users.find((user) => user?.chat_uuid === uuid)
    }

    const endOfMessagesRef = useRef(null);

    const scrollToBottom = () => {
        endOfMessagesRef.current?.scrollIntoView({behavior: 'smooth'});
    };

    const [userPreference, setUserPreference] = useState({})

    const [socket, setSocket] = useState(null);

    const [currentMessage, setCurrentMessage] = useState('');

    const [receivedMessages, setReceivedMessages] = useState([]);

    const [messages, setMessages] = useState([])
    const [sending, setSending] = useState(false)

    useEffect(() => {
        if (channel !== null) {
            getAllMessage({setMessages, channel})
        }
    }, [channel])

    useEffect(() => {
        const ws = new WebSocket('wss://beli-chat.onrender.com/');

        setSocket(ws);

        ws.onopen = () => {
            console.log('WebSocket connection opened');
            ws.send(JSON.stringify({channel: channel}));
        };

        ws.onmessage = (event) => {
            const data = JSON.parse(event.data);
            const wsMessage = {
                id: data?.id,
                uuid: data?.user_uuid,
                message: data?.message,
                type: data?.type,
                attachment: data?.attachment,
                created_at: data?.created_at,
            }

            if (messages.find((m) => m?.id === wsMessage?.uuid)) {

            } else {
                setMessages([wsMessage, ...messages])
            }
        };

        ws.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        ws.onclose = (event) => {
            console.log('WebSocket connection closed:', event);
        };

        return () => {
            ws.close();
        };
    }, [messages]);

    useEffect(() => {
        getUserPreference({setUserPreference: setUserPreference})
    }, []);

    useEffect(() => {
        scrollToBottom();
    }, [messages, currentMessage]);

    useEffect(() => {
        setCurrentMessage('')
    }, [messages])

    const sendMessage = () => {
        if (currentMessage !== '' && currentMessage !== null) {
            sendPrivateMessage({
                messageData: {
                    channel: channel,
                    message: currentMessage,
                }, setMessages: setMessages, messages: messages, setSending: setSending
            })
        }
    }

    const [file, setFile] = useState(null);
    const [attachment, setAttachment] = useState({});
    const [uploading, setUploading] = useState(false);
    const fileRef = useRef(null)
    const handleFileChange = (event) => {
        setFile(event.target.files[0]);
    };

    useEffect(() => {
        // handleUpload({selectedFile: file, setAttachment: setAttachment}).then(r => console.log(r))
        uploadImage({
            file: file,
            setAttachment: setAttachment,
            setUploading: setUploading
        })
    }, [file])

    useEffect(() => {
        sendPrivateMessage({
            messageData: {
                channel: channel,
                message: attachment?.url,
                type: 1
            }, setMessages: setMessages, messages: messages, setSending: setSending
        })
    }, [attachment])

    return (
        <div>
            <div className={"p-2"} style={{overflowY: "scroll", height: "26rem"}}>
                <div className={"messages-area"}>
                    <div ref={endOfMessagesRef}/>
                    {sending && currentMessage !== '' &&
                        <SendMessageBox message={{message: currentMessage}} user={getUser(userPreference.chat_uuid)}/>
                    }
                    {messages?.map((message, key) =>
                        <div key={key} className={""}>
                            {message?.uuid === userPreference?.chat_uuid &&
                                <SendMessageBox message={message} user={getUser(message?.uuid)}/>}
                            {message?.uuid !== userPreference?.chat_uuid &&
                                <ReceiveMessageBox message={message} user={getUser(message?.uuid)}/>}
                        </div>
                    )}
                </div>
            </div>
            <div className={"chat_area d-flex p-2"}>
                 <span onClick={() => {
                     fileRef.current.click()
                 }} className="mdi mdi-image text-primary mdi-36px cursor-pointer me-2"></span>
                <textarea
                    value={currentMessage}
                    onChange={(r) => setCurrentMessage(r.currentTarget.value)}
                    className={"form-control rounded"} rows={1} placeholder={"Nhập tin nhắn"}></textarea>
                <span onClick={() => sendMessage()}
                      className="mdi mdi-send-circle text-primary mdi-36px cursor-pointer ms-2"></span>
                <div className={"d-none"}>
                    <input ref={fileRef} type="file" onChange={handleFileChange}/>
                </div>
            </div>
        </div>
    )
}
export default ChatTab
