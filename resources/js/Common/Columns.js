import React, {useState} from "react";

export const ImageColumn = ({entity, field}) => {
    const url = entity[field.field_name];
    const urls = window.location.origin + "/" + url
    return (
        <div>
            {url !== null && url !== "" &&
                <a href={urls}>Xem</a>
            }
        </div>
    );
}

export const NumericColumns = ({entity, field}) => {
    return (
        <div className={"fw-bold text-center"} style={{color: entity?.[field.field_name + "_color"]}}>
            {parseInt(entity[field.field_name]).toLocaleString("en-US")}
        </div>
    )
}

function formatText(text = "") {
    // Split the text into lines based on the newline characters
    const lines = text.split('\r\n');

    // Map each line to a paragraph or a break element
    return lines.map((line, index) => (
        <React.Fragment key={index}>
            {line}
            <br/>
        </React.Fragment>
    ));
}

export const TextareaColumn = ({entity, field}) => {
    const [show, setShow] = useState(false)
    const fullText = entity[field.field_name]
    let limitText = fullText
    if (fullText?.length > 30) {
        limitText = fullText.substring(0, 30) + '...';
    } else {
        limitText = fullText;
    }
    return (
        <div className={"position-relative cursor-pointer"}>
            <div onClick={() => {
                setShow(!show)
            }}>{!show ? limitText : formatText(fullText)}</div>
        </div>
    )
}

export const UserColumn = ({entity, field}) => {
    const name = entity[field?.options?.name]
    const avatar = entity[field?.options?.avatar]
    return (
        <div className={"d-flex"}>
            {
                avatar &&
                <img src={avatar} alt={""} style={{width: "2rem", height: "2rem"}} className={"rounded-circle"}/>
            }
            <div>{name}</div>
        </div>
    )
}
