import qs from "qs";
import axios from "axios";
import {toast} from "react-toastify";

const baseUrl = window.location.origin + "/s/bulk"
const baseAxios = axios.create({
    retries: 3,
    timeout: 100000,
    retryDelay: 100
})
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')

const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};


/**
 *   Route::get('/s/supporters', [CalendarController::class, 'getSupporters']);
 *     Route::get('/s/teachers', [CalendarController::class, 'getTeachers']);
 * @param setTeachers
 */
export const getAllCards = ({setCards}) => {
    const url = baseUrl + "/cards"
    baseAxios.get(url).then((response) => {
        setCards(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllStudents = ({setStudents}) => {
    const url = baseUrl + "/students"
    baseAxios.get(url).then((response) => {
        setStudents(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAllClassroom = ({setClassrooms}) => {
    const url = baseUrl + "/classrooms"
    baseAxios.get(url).then((response) => {
        setClassrooms(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const bulkCloneCards = ({data}) => {
    const url = baseUrl + "/cards/clone"

    baseAxios.post(url, data).then(() => {
        toast("Thành công")
        window.location.href = window.location.origin + "/card/list"
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
    })
}


export const bulkAddCardToClassroom = ({data}) => {
    const url = baseUrl + "/cards/add_to_classroom"

    baseAxios.post(url, data).then(() => {
        toast("Thành công")
        window.location.href = window.location.origin + "/classroom/edit/" + data?.classroom_id
    }).catch(() => {
        toast.error("Không thành công, vui lòng kiểm tra lại")
    })
}
