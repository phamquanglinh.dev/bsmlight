import React, {useEffect, useState} from "react";
import Select, {components} from "react-select";
import {bulkCloneCards, getAllCards, getAllStudents} from "./api/CardApi";

const customOption = (props) => {
    return (
        <components.Option {...props}>
            <div className={"d-flex"}>
                <div>{props.data.uuid}</div>
            </div>
        </components.Option>
    );
};

const customSingleValue = (props) => {
    return (
        <components.SingleValue {...props}>
            {props?.data?.id &&
                <div className={"d-flex"}>
                    <div>{props.data.uuid}</div>
                </div>
            }
        </components.SingleValue>
    );
};

const BulkCloneCard = () => {
    const [studentIds, setStudentIds] = useState([])
    const [cardId, setCardId] = useState(0)
    const [errors, setErrors] = useState([])
    const [cards, setCards] = useState([])
    const [students, setStudents] = useState([])
    const [search, setSearch] = useState("")
    const [linked, setLinked] = useState(false)
    const [filterStudents, setFilterStudents] = useState([])
    useEffect(() => {
        getAllCards({
            setCards: setCards
        })
        getAllStudents({
            setStudents: setStudents
        })
    }, []);

    useEffect(() => {
        setFilterStudents(students.filter((student) => {
            if (!search && !linked) {
                return student
            }
            if (search && !linked) {
                return student?.name.includes(search) || student?.uuid.includes(search)
            }
            if (linked && !search) {
                return student?.linked === 0
            }

            return (student?.name.includes(search) || student?.uuid.includes(search)) && (student?.linked === 0)
        }))
    }, [search, students, linked])


    const handleSelectAll = () => {
        const allStudentIds = students.map((student, key) => {
            return student?.id
        })

        setStudentIds(allStudentIds)
    }
    const handleUnSelectAll = () => {
        setStudentIds([])
    }
    const bulkClone = () => {
        bulkCloneCards({
            data: {
                card_id: cardId,
                student_ids: studentIds
            }
        })
    }

    return (
        <div className={"p-5"}>
            <div className="form-floating form-floating-outline mb-3">
                <label>Giáo viên</label>
                <Select
                    classNamePrefix="select"
                    value={cards.find((v) => v.id === cardId)}
                    onChange={(r) => {
                        setCardId(r.id)
                    }}
                    isClearable={true}
                    isSearchable={true}
                    name="color"
                    placeholder={"Chọn thẻ học"}
                    options={cards}
                    styles={customStyles} // Sử dụng styles tùy chỉnh
                    components={{Option: customOption, SingleValue: customSingleValue}}
                />
            </div>
            <div className={"my-3"}>
                <div className={"row m-0"}>
                    <div className={"p-1 col"}>
                        <input onChange={(r) => {
                            setSearch(r.currentTarget.value)
                        }} value={search} placeholder={"Tìm học sinh"} className={"form-control"}/>
                        <div className={"mt-3"}>
                            <div className="col-sm-6 p-6">
                                <label className="switch switch-square">
                                    <input onChange={() => {
                                        setLinked(!linked)
                                    }} type="checkbox" className="switch-input" checked={linked}/>
                                    <span className="switch-toggle-slider">
                                        <span className="switch-on"></span>
                                        <span className="switch-off"></span>
                                    </span>
                                    <span className="switch-label">Chọn những học sinh chưa gắn thẻ học</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className={"p-1 col"}>
                            <span onClick={handleSelectAll}
                                  className={" btn btn-label-primary me-2"}>Chọn tất cả</span>
                        <span onClick={handleUnSelectAll}
                              className={"btn btn-label-danger me-2"}>Bỏ chọn tất cả</span>
                        <span onClick={bulkClone}
                              className={"btn btn-label-success"}>Thêm hàng loạt</span>
                    </div>
                </div>
                <div className={"p-2"}>
                    {filterStudents?.map((student, key) =>
                        <div key={key} className={"d-flex"}>
                            <div className="form-check mt-4">
                                <input checked={studentIds.includes(student?.id)}
                                       onChange={() => {
                                           if (!studentIds.includes(student?.id)) {
                                               setStudentIds([...studentIds, student?.id])
                                           } else {
                                               setStudentIds(studentIds.filter((v) => v !== student?.id))
                                           }
                                       }}
                                       className="form-check-input"
                                       type="checkbox" value="" id={student?.uuid}/>
                                <label className="form-check-label" htmlFor={student?.uuid}>
                                    <img className={"avatar-xs me-2"} src={student?.avatar}/>
                                    {student?.uuid} - {student?.name}
                                </label>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
export default BulkCloneCard


const customStyles = {
    control: (provided) => ({
        ...provided,
        backgroundColor: '#312d4b',
        borderColor: '#555',
        color: '#fff',
        padding: "0.3rem"
    }),
    option: (provided, state) => ({
        ...provided,
        backgroundColor: '#312d4b',
        color: '#fff'
    }),
    singleValue: (provided) => ({
        ...provided,
        color: '#fff'
    }),
    input: (provided) => ({
        ...provided,
        color: "#fff"
    })
};
