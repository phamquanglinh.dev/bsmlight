import React, {Component} from "react";

import ReactDOM from "react-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {BrowserRouter as Router, useRoutes} from 'react-router-dom';
import BulkCloneCard from "./BulkCloneCard";
import BulkAddCardToClassroom from "./BulkAddCardToClassroom";

const App = () => {
    return useRoutes([
        {path: '/card/bulk/clone_card/', element: <BulkCloneCard/>},
        {path: '/card/bulk/add_card_to_classroom/', element: <BulkAddCardToClassroom/>},
    ]);
}

class StudyLogProvider extends Component {
    render() {
        return (
            <React.StrictMode>
                <Router>
                    <App/>
                    <ToastContainer/>
                </Router>
            </React.StrictMode>
        )
    }
}

ReactDOM.render(<StudyLogProvider/>, document.getElementById('card-react'))
