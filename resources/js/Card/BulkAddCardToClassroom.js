import React, {useEffect, useState} from "react";
import Select, {components} from "react-select";
import {bulkAddCardToClassroom, bulkCloneCards, getAllCards, getAllClassroom, getAllStudents} from "./api/CardApi";

const customOption = (props) => {
    return (
        <components.Option {...props}>
            <div className={"d-flex"}>
                <div>{props.data.uuid} - {props.data.name}</div>
            </div>
        </components.Option>
    );
};

const customSingleValue = (props) => {
    return (
        <components.SingleValue {...props}>
            {props?.data?.id &&
                <div className={"d-flex"}>
                    <div>{props.data.uuid} - {props.data.name}</div>
                </div>
            }
        </components.SingleValue>
    );
};

const BulkAddCardToClassroom = () => {
    const [cardIds, setCardIds] = useState([])
    const [classroomId, setClassroomId] = useState(0)
    const [errors, setErrors] = useState([])
    const [cards, setCards] = useState([])
    const [classrooms, setClassrooms] = useState([])
    const [search, setSearch] = useState("")
    const [filterCards, setFilterCards] = useState([])
    const [linked,setLinked] = useState(false)
    useEffect(() => {
        getAllCards({
            setCards: setCards
        })
        getAllClassroom({
            setClassrooms: setClassrooms
        })
    }, []);

    useEffect(() => {
        setFilterCards(cards.filter((card) => {
            if (!search && ! linked) {
                return card
            }

            if (search && !linked) {
                return card?.uuid.includes(search) || card?.student?.name.includes(search)
            }

            if (!search && linked) {
                return card?.linked === 0
            }

            if (search && linked) {
                return (card?.uuid.includes(search) || card?.student?.name.includes(search)) && (card?.linked === 0)
            }
        }))
    }, [search, cards, linked])

    const handleSelectAll = () => {
        const allCardIds = cards.map((card, key) => {
            return card?.id
        })

        setCardIds(allCardIds)
    }
    const handleUnSelectAll = () => {
        setCardIds([])
    }
    const bulkAdd = () => {
        bulkAddCardToClassroom({
            data: {
                classroom_id: classroomId,
                card_ids: cardIds
            }
        })
    }

    return (
        <div className={"p-5"}>
            <div className="form-floating form-floating-outline mb-3">
                <label>Lớp học</label>
                <Select
                    classNamePrefix="select"
                    value={classrooms.find((v) => v.id === classroomId)}
                    onChange={(r) => {
                        setClassroomId(r?.id)
                    }}
                    isClearable={true}
                    isSearchable={true}
                    name="color"
                    placeholder={"Chọn lớp học"}
                    options={classrooms}
                    styles={customStyles} // Sử dụng styles tùy chỉnh
                    components={{Option: customOption, SingleValue: customSingleValue}}
                />
            </div>
            <div className={"my-3"}>
                <div className={"row m-0"}>
                    <div className={"p-1 col"}>
                        <input onChange={(r) => {
                            setSearch(r.currentTarget.value)
                        }} value={search} placeholder={"Tìm thẻ học"} className={"form-control"}/>
                        <div className={"mt-3"}>
                            <div className="col-sm-6 p-6">
                                <label className="switch switch-square">
                                    <input onChange={() => {
                                        setLinked(!linked)
                                    }} type="checkbox" className="switch-input" checked={linked}/>
                                    <span className="switch-toggle-slider">
                                        <span className="switch-on"></span>
                                        <span className="switch-off"></span>
                                    </span>
                                    <span className="switch-label">Chọn những học sinh chưa gắn lớp học</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className={"p-1 col"}>
                            <span onClick={handleSelectAll}
                                  className={" btn btn-label-primary me-2"}>Chọn tất cả</span>
                        <span onClick={handleUnSelectAll}
                              className={"btn btn-label-danger me-2"}>Bỏ chọn tất cả</span>
                        <span onClick={bulkAdd}
                              className={"btn btn-label-success"}>Thêm hàng loạt</span>
                    </div>
                </div>
                <div className={"p-2"}>
                    {filterCards?.map((card, key) =>
                        <div key={key} className={"d-flex"}>
                            <div className="form-check mt-4">
                            <input checked={cardIds.includes(card?.id)}
                                       onChange={() => {
                                           if (!cardIds.includes(card?.id)) {
                                               setCardIds([...cardIds, card?.id])
                                           } else {
                                               setCardIds(cardIds.filter((v) => v !== card?.id))
                                           }
                                       }}
                                       className="form-check-input"
                                       type="checkbox" value="" id={card?.uuid}/>
                                <label className="form-check-label" htmlFor={card?.uuid}>
                                    <img className={"avatar-xs me-2"} src={card?.student?.avatar}/>
                                    {card?.uuid} - {card?.student?.name} - {card?.student?.uuid}
                                </label>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
export default BulkAddCardToClassroom


const customStyles = {
    control: (provided) => ({
        ...provided,
        backgroundColor: '#312d4b',
        borderColor: '#555',
        color: '#fff',
        padding: "0.3rem"
    }),
    option: (provided, state) => ({
        ...provided,
        backgroundColor: '#312d4b',
        color: '#fff'
    }),
    singleValue: (provided) => ({
        ...provided,
        color: '#fff'
    }),
    input: (provided) => ({
        ...provided,
        color: "#fff"
    })
};
