import React, {useEffect, useRef, useState} from "react";
import {getAccountFields, getAccountRelation, getAccounts} from "./api/CrmAccountAPI";
import AccountRelationButton from "../components/crm/AccountRelationButton";
import 'react-perfect-scrollbar/dist/css/styles.css';
import Fields from "../components/crm/Field";
import AccountCustomFilter from "../components/crm/AccountCustomFilter"; // Import CSS for styling

const AccountList = () => {
    const scrollbarRef = useRef();

    const handleHorizantalScroll = (element, speed, distance, step) => {
        let scrollAmount = 0;
        const slideTimer = setInterval(() => {
            element.scrollLeft += step;
            scrollAmount += Math.abs(step);
            if (scrollAmount >= distance) {
                clearInterval(slideTimer);
            }
            if (element.scrollLeft === 0) {
                // setArrowDisable(true);
            } else {
                // setArrowDisable(false);
            }
        }, speed);
    };

    const [fields, setFields] = useState([])
    const [myFields, setMyFields] = useState([])
    const [accounts, setAccounts] = useState([])
    const [accountRelations, setAccountRelations] = useState([])
    const [queryData, setQueryData] = useState({
        limit: 10,
        offset: 1,
        fields: "id",
        orderBy: "created_at",
        direction: "desc"
    })

    useEffect(() => {
        getAccountFields({setFields: setFields, setMyFields: setMyFields, myFields: myFields})
        getAccountRelation(
            {
                queryData: {
                    fields: "id,relation_name,color",
                    orderBy: "id",
                    direction: "asc",
                    limit: 99
                },
                setAccountRelations: setAccountRelations
            }
        )

    }, [])

    useEffect(() => {
        console.log("myFields")
        setQueryData({
            ...queryData,
            fields: fields.map(field => field.field_name).join(',')
        })
    }, [myFields])

    useEffect(() => {
        getAccounts({queryData, setAccounts})
    }, [queryData])

    return (
        <div>
            <div className={"container-fluid"}>
                <div className={"mb-3 h3"}>Khách hàng</div>
            </div>

            <div className={"container-fluid"}>
                <div className={"row justify-content-between align-items-center"}>
                    <div className={"col-10"}>
                        <div className={"d-flex align-items-center"}>
                            <a onClick={() => {
                                handleHorizantalScroll(scrollbarRef.current, 25, 100, -10);
                            }} className={"me-2 cursor-pointer"}>
                                <span className="mdi mdi-arrow-left-drop-circle mdi-36px"></span>
                            </a>
                            <div ref={scrollbarRef} className={"relation_scroll py-2"}>
                                <span
                                    onClick={() => setQueryData({
                                        ...queryData,
                                        filters: Object.fromEntries(
                                            Object.entries(queryData.filters).filter(([key]) => key !== "account_relation")
                                        )
                                    })}
                                    className={"cursor-pointer bg-primary text-white p-2 rounded me-2"}
                                    style={{whiteSpace: "nowrap"}}
                                >
                                    Tất cả
                                </span>
                                {accountRelations.map((accountRelation, key) =>
                                    <AccountRelationButton key={key} queryData={queryData} setQueryData={setQueryData}
                                                           accountRelation={accountRelation}/>
                                )}
                            </div>
                            <a
                                onClick={() => {
                                    handleHorizantalScroll(scrollbarRef.current, 25, 100, 10);
                                }}
                                className={"ms-2 cursor-pointer"}>
                                <span className="mdi mdi-arrow-right-drop-circle mdi-36px"></span>
                            </a>
                        </div>
                    </div>
                    <div className={"col-2"}>
                        <a className={"btn btn-primary"} href={window.location.origin + "/account/create"}>
                            Thêm mới
                        </a>
                    </div>
                </div>
            </div>

            <div className={"container-fluid my-3"}>
                <div className="table-responsive text-nowrap">
                    <table className="table table-bordered rounded">
                        <thead>
                        <tr>
                            {myFields.map((field, key) =>
                                <th className={"bg-primary text-dark"} key={key}>{field.field_label}</th>
                            )}
                        </tr>
                        </thead>
                        <tbody>
                        {accounts.map((account, accountKey) =>
                            <tr key={accountKey}>
                                {myFields.map((field, fieldKey) =>
                                    <td key={fieldKey}>
                                        <Fields
                                            account={account}
                                            field={field}
                                        />
                                    </td>
                                )}
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>

            <div className={'container-fluid'}>
                <div className={"row justify-content-between align-items-center"}>
                    <div className={"col-3"}>
                        <a className={"cursor-pointer"}
                           onClick={() => setQueryData({
                               ...queryData,
                               offset: queryData > 1 ? queryData.offset - 1 : 1
                           })}
                        >
                            <span className="mdi mdi-arrow-left-drop-circle mdi-36px"></span>
                        </a>
                        <a className={"cursor-pointer"}>
                            <span
                                onClick={() => setQueryData({
                                    ...queryData,
                                    offset: queryData.offset + 1
                                })}
                                className="mdi mdi-arrow-right-drop-circle mdi-36px"></span>
                        </a>

                    </div>
                    <div className={"col-4"}>
                        <AccountCustomFilter setMyFields={setMyFields} myFields={myFields} fields={fields}/>
                    </div>
                    <div className={"col-3"}>

                        <select
                            onChange={(e) => {
                                const newLimit = parseInt(e.target.value, 10);
                                setQueryData({
                                    ...queryData,
                                    limit: newLimit
                                })
                            }}
                            value={queryData.limit}
                            className={"form-control"}>
                            <option value={10}>Hiển thị 10 dữ liệu</option>
                            <option value={20}>Hiển thị 20 dữ liệu</option>
                            <option value={50}>Hiển thị 50 dữ liệu</option>
                            <option value={100}>Hiển thị 100 dữ liệu</option>
                            <option value={200}>Hiển thị 200 dữ liệu</option>
                            <option value={500}>Hiển thị 500 dữ liệu</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AccountList;
