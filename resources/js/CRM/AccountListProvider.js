import React, {Component} from "react";

import ReactDOM from "react-dom";
import AccountList from "./AccountList";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
class AccountListProvider extends Component {
    render() {
        return (
            <div>
                <AccountList/>
                <ToastContainer/>
            </div>
        )
    }
}

ReactDOM.render(<AccountListProvider/>, document.getElementById('list-account-react'))
