import qs from "qs";
import axios from "axios";
const baseUrl = window.location.origin + "/crm"
const baseAxios = axios
baseAxios.defaults.headers.common['X-CSRF-TOKEN'] = sessionStorage.getItem('csrf_token')
const customParamsSerializer = (params) => {
    return qs.stringify(params, {arrayFormat: 'brackets'});
};

export const getAccounts = ({queryData, setAccounts}) => {
    const url = baseUrl + "/accounts"
    const configs = {
        params: queryData,
        paramsSerializer: customParamsSerializer
    };
    baseAxios.get(url, configs).then((response) => {
        setAccounts(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

export const getAccountFields = ({setFields, setMyFields, myFields}) => {
    console.log("sf", setFields)
    console.log("smf", setMyFields)
    const url = baseUrl + "/fields"
    const configs = {
        params: {
            module: "account"
        }
    };
    baseAxios.get(url, configs).then((response) => {
        const newFields = response.data.data
        setFields(newFields)
        if (setMyFields) {
            if (!sessionStorage.getItem('my_fields')) {
                sessionStorage.setItem("my_fields", JSON.stringify(response.data.data))
            } else {
                const oldMyField = JSON.parse(sessionStorage.getItem('my_fields'))
                const newFieldNames = new Set(newFields.map(field => field.name));
                const filteredOldMyField = oldMyField.filter(field => newFieldNames.has(field.name));
                sessionStorage.setItem('my_fields', JSON.stringify(filteredOldMyField));
            }
            setMyFields(JSON.parse(sessionStorage.getItem('my_fields')))
        }
    }).catch((error) => {

        console.log(error)
    })
}

export const getAccountRelation = ({queryData, setAccountRelations}) => {
    const url = baseUrl + "/account_relations"
    const configs = {
        params: queryData,
        paramsSerializer: customParamsSerializer
    };
    baseAxios.get(url, configs).then((response) => {
        setAccountRelations(response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}
