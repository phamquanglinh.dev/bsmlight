<?php

use App\Http\Controllers\SystemLogApiController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\WebhookController;
use App\Http\Middleware\DupplicateMidleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('telegram/webhook', [TelegramController::class, 'handleWebhook']);


Route::post('getfly/system_log', [SystemLogApiController::class, 'create']);

Route::get('webhook/account', [WebhookController::class, 'webhookCreateAccount']);

Route::middleware([DupplicateMidleware::class])->any('webhook/account/google_sheet', [WebhookController::class, 'webhookCreateAccountFromGoogleSheet']);
