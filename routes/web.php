<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AccountRelationController;
use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\BcoinController;
use App\Http\Controllers\BeliChatIntegrationController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\BulkApiController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\CashierController;
use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CustomFieldController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\FundController;
use App\Http\Controllers\GoogleChatController;
use App\Http\Controllers\GoogleFormController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\InteractionController;
use App\Http\Controllers\LessonPlantController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\Salary\SalaryCriteriaController;
use App\Http\Controllers\SalaryGroupController;
use App\Http\Controllers\SalarySheetController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SalarySnapController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\SearchAllController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudyLogApiController;
use App\Http\Controllers\StudyLogController;
use App\Http\Controllers\SupporterController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\SystemLogController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\TransactionController;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\FrontendMiddeware;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/forgot_password', [ResetPasswordController::class, "forgotPasswordView"])->name('password.request');
Route::post('/forgot_password', [ResetPasswordController::class, "sendPasswordConfirmation"])->name('password.send');
Route::get('/reset_password', [ResetPasswordController::class, "resetPasswordView"])->name('password');
Route::post('/reset_password', [ResetPasswordController::class, "updatePasswordWithToken"])->name('password.update');

Route::get('/login', [AuthenticateController::class, "loginView"])->name('login');
Route::get('/register', [AuthenticateController::class, "registerView"])->name('registration');
Route::post('/register', [AuthenticateController::class, "register"])->name('register');
Route::post('/login', [AuthenticateController::class, "login"])->name('authentication');
Route::get('/logout', [AuthenticateController::class, "logout"])->name('logout');

Route::get('/auth/facebook', [AccountController::class, 'redirectToFacebook']);
Route::get('/facebook/callback', [AccountController::class, 'handleFacebookCallback']);
Route::get("/facebook/delete", [AccountController::class, "deleteAuthFaceBook"]);

Route::get('/auth/google', [AccountController::class, 'redirectToGoogle']);
Route::get('/auth/google/callback', [AccountController::class, 'handleGoogleCallback']);
Route::get("/auth/google/delete", [AccountController::class, "deleteAuthGoogle"]);


Route::get("/edit/user/{id}", [AccountController::class, "edit"])->name("user.edit");
Route::put("/update/user/{id}", [AccountController::class, "update"])->name("user.update");

Route::middleware(['auth'])->group(function () {
    Route::get('/verification', [AuthenticateController::class, 'verificationView']);
    Route::post('/update_email', [AuthenticateController::class, 'updateEmail']);
    Route::get('/resend_email', [AuthenticateController::class, 'sendVerificationEmail']);
    Route::post('/verify', [AuthenticateController::class, 'verify']);

    Route::middleware(['greeting', 'host'])->group(function () {
        Route::get('/', function () {
            return view('welcome');
        })->name('index');

        Route::get('/calendar', [ClassroomController::class, 'calendarView']);

        Route::prefix('student')->group(function () {
            Route::get('/create', [StudentController::class, "create"])->name('student.create');
            Route::post('/store', [StudentController::class, "store"])->name('student.store');
            Route::get('/list', [StudentController::class, "list"])->name('student.list');
            Route::get('/edit/{id}', [StudentController::class, "edit", "id"])->name('student.edit');
            Route::post('/update/{id}', [StudentController::class, "update", "id"])->name('student.update');
            Route::get('/delete/{id}', [StudentController::class, "delete", "id"])->name('student.delete');
            Route::get('/show/{id}', [StudentController::class, "show", "id"])->name('student.show');
            Route::get('/unlink/{id}', [StudentController::class, "unlink", "id"])->name('student.unlink');
        });

        Route::prefix('teacher')->group(function () {
            Route::get('/create', [TeacherController::class, "create"])->name('teacher.create');
            Route::post('/store', [TeacherController::class, "store"])->name('teacher.store');
            Route::get('/list', [TeacherController::class, "list"])->name('teacher.list');
            Route::get('/edit/{id}', [TeacherController::class, "edit", "id"])->name('teacher.edit');
            Route::post('/update/{id}', [TeacherController::class, "update", "id"])->name('teacher.update');
            Route::get('/delete/{id}', [TeacherController::class, "delete", "id"])->name('teacher.delete');
        });

        Route::prefix('supporter')->group(function () {
            Route::get('/create', [SupporterController::class, "create"])->name('supporter.create');
            Route::post('/store', [SupporterController::class, "store"])->name('supporter.store');
            Route::get('/list', [SupporterController::class, "list"])->name('supporter.list');
            Route::get('/edit/{id}', [SupporterController::class, "edit", "id"])->name('supporter.edit');
            Route::post('/update/{id}', [SupporterController::class, "update", "id"])->name('supporter.update');
            Route::get('/delete/{id}', [SupporterController::class, "delete", "id"])->name('supporter.delete');
        });

        Route::prefix('staff')->group(function () {
            Route::get('/create', [StaffController::class, "create"])->name('staff.create');
            Route::post('/store', [StaffController::class, "store"])->name('staff.store');
            Route::get('/list', [StaffController::class, "list"])->name('staff.list');
            Route::get('/edit/{id}', [StaffController::class, "edit", "id"])->name('staff.edit');
            Route::post('/update/{id}', [StaffController::class, "update", "id"])->name('staff.update');
            Route::get('/delete/{id}', [StaffController::class, "delete", "id"])->name('staff.delete');
        });

        Route::prefix('custom_field')->group(function () {
            Route::get('/create', [CustomFieldController::class, "create"])->name('custom_field.create');
            Route::post('/store', [CustomFieldController::class, "store"])->name('custom_field.store');
            Route::get('/list', [CustomFieldController::class, "list"])->name('custom_field.list');
            Route::get('/edit/{id}', [CustomFieldController::class, "edit", "id"])->name('custom_field.edit');
            Route::post('/update/{id}', [CustomFieldController::class, "update", "id"])->name('custom_field.update');
            Route::get('/delete/{id}', [CustomFieldController::class, "delete", "id"])->name('custom_field.delete');
        });

        Route::prefix('card')->group(function () {
            Route::get('/create', [CardController::class, "create"])->name('card.create');
            Route::post('/store', [CardController::class, "store"])->name('card.store');
            Route::get('/list', [CardController::class, "list"])->name('card.list');
            Route::get('/edit/{id}', [CardController::class, "edit", "id"])->name('card.edit');
            Route::post('/update/{id}', [CardController::class, "update", "id"])->name('card.update');
            Route::get('/delete/{id}', [CardController::class, "delete", "id"])->name('card.delete');
            Route::get('/show/{id}', [CardController::class, "show", "id"])->name('card.show');
            Route::get('/bulk/clone_card', [CardController::class, "bulkCloneCard"]);
            Route::get('/bulk/add_card_to_classroom', [CardController::class, "bulkAddCardToClassroom"]);
        });

        Route::prefix('classroom')->group(function () {
            Route::get('/create', [ClassroomController::class, "create"])->name('classroom.create');
            Route::post('/store', [ClassroomController::class, "store"])->name('classroom.store');
            Route::get('/list', [ClassroomController::class, "list"])->name('classroom.list');
            Route::get('/edit/{id}', [ClassroomController::class, "edit", "id"])->name('classroom.edit');
            Route::post('/update/{id}', [ClassroomController::class, "update", "id"])->name('classroom.update');
            Route::get('/delete/{id}', [ClassroomController::class, "delete", "id"])->name('classroom.delete');
            Route::get('/user-calendar', [ClassroomController::class, 'getUserCalendar']);
            Route::get('/cover/{classroomId}', [ClassroomController::class, 'getCoverView', 'classroomId']);
            Route::post('/cover/{classroomId}', [ClassroomController::class, 'updateCoverClassroom', 'classroomId']);
        });

        Route::prefix('role')->group(function () {
            Route::get('/create', [RoleController::class, "create"])->name('role.create');
            Route::post('/store', [RoleController::class, "store"])->name('role.store');
            Route::get('/list', [RoleController::class, "list"])->name('role.list');
            Route::get('/update/{id}', [RoleController::class, "list"])->name('role.list');
        });

        Route::prefix('studylog')->group(function () {
//            Route::get('/create', [StudyLogController::class, "create"])->name('studylog.create');
            Route::get('/create', [StudyLogApiController::class, "createStudyLogView"])->name('studylog.create');
            Route::get('/edit/{id}', [StudyLogApiController::class, "createStudyLogView"])->name('studylog.edit');
            Route::get('/create/start', [StudyLogApiController::class, "createStudyLogView"])->name('studylog.create');
            Route::any('/store/{step?}', [StudyLogController::class, "store"])->name('studylog.store');
            Route::get('/list', [StudyLogController::class, "list"])->name('studylog.list');

            Route::get('/show/{id}', [StudyLogController::class, "show", "id"])->name('studylog.show');
            Route::post('/update/{id}', [StudyLogController::class, "update", "id"])->name('studylog.update');
            Route::get('/delete/{id}', [StudyLogController::class, "delete", "id"])->name('studylog.delete');

            Route::get('/submit/{id}', [StudyLogController::class, "submit", "id"])->name('studylog.submit');
            Route::get('/cancel/{id}', [StudyLogController::class, "cancel", "id"])->name('studylog.cancel');
            Route::get('/recover/{id}', [StudyLogController::class, "recover", "id"])->name('studylog.recover');
            Route::get('/confirm/{id}', [StudyLogController::class, "confirm", "id"])->name('studylog.confirm');
            Route::get('/confirm/log/{id}/alt/{user_id}', [
                StudyLogController::class,
                "confirmUser",
                "id"
            ])->name('studylog.confirm_user');
            Route::get('/confirm/logs/{id}/', [
                StudyLogController::class,
                "confirmAllUser",
                "id"
            ])->name('studylog.confirm_all_users');
            Route::get('/accept/{id}', [StudyLogController::class, "accept", "id"])->name('studylog.accept');
            Route::get('/rollback/{id}', [
                StudyLogController::class,
                "rollbackAccept",
                "id"
            ])->name('studylog.rollback');
            Route::get('/reject/{id}', [StudyLogController::class, "reject", "id"])->name('studylog.reject');
            Route::any('quick', [StudyLogController::class, 'quickCreate']);
        });

        Route::prefix('branch')->withoutMiddleware(['host'])->group(function () {
            Route::get('/create', [BranchController::class, "create"])->name('branch.create');
            Route::get('/access/{branchId}', [BranchController::class, "access", "branchId"])->name('branch.access');
            Route::post('/store', [BranchController::class, "store"])->name('branch.store');
            Route::get('/list', [BranchController::class, "list"])->name('branch.list');
            Route::get('/edit/{id}', [BranchController::class, "edit", "id"])->name('branch.edit');
            Route::post('/update/{id}', [BranchController::class, "update", "id"])->name('branch.update');
            Route::get('/delete/{id}', [BranchController::class, "destroy", "id"])->name('branch.delete');
        });

        Route::prefix('fund')->group(function () {
            Route::get('list', [FundController::class, 'list'])->name('fund.list');
            Route::get('edit/{id}', [FundController::class, 'edit', 'id'])->name('fund.edit');
            Route::get('paid/{id}', [FundController::class, 'paidFundView', 'id'])->name('fund.paidView');
            Route::post('paid/{id}', [FundController::class, 'paidFund', 'id'])->name('fund.paid');
            Route::post('update/{id}', [FundController::class, 'update', 'id'])->name('fund.update');
        });

        Route::prefix("lesson_plant")->group(function () {
            Route::get("/list", [LessonPlantController::class, "list"])->name('lesson_plant.list');
            Route::get("/create", [LessonPlantController::class, "create"])->name('lesson_plant.create');
            Route::post("/store", [LessonPlantController::class, "store"])->name('lesson_plant.store');
            Route::get("/edit/{id}", [LessonPlantController::class, "edit", "id"])->name('lesson_plant.edit');
            Route::get("/show/{id}", [LessonPlantController::class, "show", "id"])->name('lesson_plant.show');
            Route::post("/update/{id}", [LessonPlantController::class, "update", "id"])->name('lesson_plant.update');
            Route::get("/delete/{id}", [LessonPlantController::class, "delete", "id"])->name('lesson_plant.delete');
            Route::get("/accept/{id}", [LessonPlantController::class, "accept", "id"])->name('lesson_plant.accept');
        });

        //

        Route::get('system_log/list', [SystemLogController::class, 'list']);
        Route::post('system_log/access', [SystemLogController::class, 'handleAccessToBsm']);

        Route::prefix('transaction')->group(function () {
            Route::get('/create/card', [
                TransactionController::class,
                "createCardTransaction"
            ])->name('transaction.create.card');
            Route::post('/create/card', [
                TransactionController::class,
                "storeCardTransaction"
            ])->name('transaction.store.card');
            Route::get('/edit/{id}/card/{cardId}', [
                TransactionController::class,
                "editCardTransaction",
                'id',
                'cardId'
            ])->name('transaction.edit.card');
            Route::post('/update/{id}', [
                TransactionController::class,
                "updateCardTransaction",
                'id'
            ])->name('transaction.update.card');
            Route::any('/delete/{id}', [
                TransactionController::class,
                "deleteCardTransaction",
                'id'
            ])->name('transaction.delete.card');
            Route::get('/accept/{id}', [TransactionController::class, "accept"])->name('transaction.accept');
            Route::get('/deny/{id}', [TransactionController::class, "deny"])->name('transaction.deny');
        });

        Route::get('{any}/import', [ImportController::class, "importView"]);

        Route::prefix('salary-criteria')->group(function () {
            Route::get('list', [SalaryCriteriaController::class, 'list'])->name('salary-criteria.list');
            Route::post('one', [SalaryCriteriaController::class, 'one'])->name('salary-criteria.one');
            Route::get('sheet-user-classroom/{classroom_id}', [
                SalaryCriteriaController::class,
                'getSheetUserByClassroom'
            ]);
            Route::get('create', [SalaryCriteriaController::class, 'create'])->name('salary-criteria.create');
            Route::post('store', [SalaryCriteriaController::class, 'store'])->name('salary-criteria.store');
            Route::get('delete/{id}', [SalaryCriteriaController::class, 'delete'])->name('salary-criteria.delete');
        });

        Route::prefix('salary-sheet')->group(function () {
            Route::get('list', [SalarySheetController::class, 'list'])->name('salary-sheet.list');
            Route::get('create', [SalarySheetController::class, 'create'])->name('salary-sheet.create');
            Route::post('store', [SalarySheetController::class, 'store'])->name('salary-sheet.store');
            Route::get('edit/{id}', [SalarySheetController::class, 'edit'])->name('salary-sheet.edit');
            Route::post('update/{id}', [SalarySheetController::class, 'update'])->name('salary-sheet.update');
            Route::get('delete/{id}', [SalarySheetController::class, 'delete'])->name('salary-sheet.delete');
            Route::get('show/{id}', [SalarySheetController::class, 'show'])->name('salary-sheet.delete');
        });

        Route::prefix('salary-snap')->group(function () {
            Route::get('all', [SalarySnapController::class, 'all'])->name('salary-snap.list');
            Route::get('list', [SalarySnapController::class, 'list'])->name('salary-snap.list');
            Route::get('show/{id?}', [SalarySnapController::class, 'show', 'id'])->name('salary-snap.show');
            Route::get('edit/{id?}', [SalarySnapController::class, 'edit', 'id'])->name('salary-snap.edit');
            Route::post('request/{id?}', [SalarySnapController::class, 'request', 'id'])->name('salary-snap.request');
            Route::get('delete/{id}', [SalarySnapController::class, 'delete', 'id'])->name('salary-snap.delete');
            Route::post('manual/update', [
                SalarySnapController::class,
                'manualUpdate'
            ])->name('salary-snap.manual-update');
            Route::get('close/{id}', [SalarySnapController::class, 'closeSalarySnap', "id"])->name('salary-snap.close');
        });

        Route::get('export/{entity}', [ImportController::class, 'exportEntity', 'entity']);

        Route::prefix('salary-group')->group(function () {
            Route::get('list', [SalaryGroupController::class, 'list'])->name('salary-group.list');
            Route::get('show/{id?}', [SalaryGroupController::class, 'show', 'id'])->name('salary-group.show');
            Route::get('paid/{id}', [
                SalaryGroupController::class,
                'requestPaid',
                'id'
            ])->name('salary-group.request-paid');
            Route::post('update/{id}/json', [SalaryGroupController::class, 'updateJson', 'id'])->name('salary-group.update-json');
            Route::get('paid/{id}', [SalaryGroupController::class, 'requestPaid', 'id'])->name('salary-group.request-paid');
            Route::post('paid/{id}', [SalaryGroupController::class, 'paid', 'id'])->name('salary-group.paid');
            Route::get('cancel/{id}', [
                SalaryGroupController::class,
                'requestCancel',
                'id'
            ])->name('salary-group.request-cancel');
            Route::post('cancel/{id}', [SalaryGroupController::class, 'cancel', 'id'])->name('salary-group.cancel');
        });

        Route::prefix('permission')->group(function () {
            Route::get('setting', [PermissionController::class, 'permissionSettingView']);
        });
        Route::prefix('chat')->group(function () {
            Route::get('connect', [GoogleChatController::class, 'connect'])->name('chat.connect');
            Route::get('callback', [GoogleChatController::class, 'callback'])->name('chat.callback');
        });

        Route::get('salary/transaction/{id}', [TransactionController::class, 'editSalaryTransaction', 'id']);
        Route::post('salary/transaction/{id}', [TransactionController::class, 'updateSalaryTransaction', 'id'])->name('transaction.update.salary');
    });
    Route::prefix('comment')->group(function () {
        Route::post('/store', [CommentController::class, "store"]);
    });

    Route::get('switch/profile', [StudentController::class, 'getProfilesView']);
    Route::any('switch/profile/{userId}', [StudentController::class, 'switchProfilesView']);

    Route::post('notification/save-token', [
        NotificationController::class,
        'saveDesktopFcmTokenAction'
    ])->name('notification.save-token');
    Route::withoutMiddleware(['auth'])->delete('notification/remove-token', [
        NotificationController::class,
        'removeDesktopFcmTokenAction'
    ])->name('notification.remove-token');
    Route::get('notification/create', [
        NotificationController::class,
        'createNotificationView'
    ])->name('notification.create');
    Route::post('notification/store', [NotificationController::class, 'storeNotification'])->name('notification.store');
    Route::any('search-all', [SearchAllController::class, 'searchAll']);

});

Route::prefix('static')->group(function () {
    Route::post('schedule', [ScheduleController::class, "staticAddSchedule"])->name('schedule.static.add');
    Route::post('shift', [ScheduleController::class, "staticAddShift"])->name('shift.static.add');
    Route::post('shiftTemplate', [
        ScheduleController::class,
        "staticAddShiftTemplate"
    ])->name('shiftTemplate.static.add');
    Route::post('cardTemplate', [ScheduleController::class, "staticAddCardTemplate"])->name('cardTemplate.static.add');
    Route::post('role', [RoleController::class, 'getOneRole']);
});

Route::middleware(['auth'])->group(function () {
    Route::get('calendar', [CalendarController::class, 'index']);
    Route::get('/s/calendar', [CalendarController::class, 'getCalendar']);
    Route::get('/s/classroom', [CalendarController::class, 'getClassroom']);
    Route::get('/s/supporters', [CalendarController::class, 'getSupporters']);
    Route::get('/s/teachers', [CalendarController::class, 'getTeachers']);
    Route::post('/s/cover', [CalendarController::class, 'storeCover']);
    Route::delete('/s/cover/{id}', [CalendarController::class, 'deleteCover']);
    Route::get('/s/cover/{id}', [CalendarController::class, 'getOneCoverAction']);
    Route::get('/s/notification', [NotificationController::class, 'getAllUserNotificationJson']);
    Route::put('/s/notification', [NotificationController::class, 'updateNotificationJson']);
    Route::get('/s/interaction/{studentId}', [InteractionController::class, 'getStudentCommentLogData']);
    Route::get('/s/user_info', [InteractionController::class, 'getUserInfoAction']);
    Route::get('/s/chat/channels', [BeliChatIntegrationController::class, 'getAllChannelsAction']);
    Route::get('/s/chat/users', [BeliChatIntegrationController::class, 'getAllUsers']);
    Route::get('/s/chat/user', [BeliChatIntegrationController::class, 'getCurrentUsers']);
    Route::get('/s/chat/messages/{channel}', [BeliChatIntegrationController::class, 'getAllMessages', 'channel']);
    Route::post('/s/chat/messages', [BeliChatIntegrationController::class, 'createMessage']);
    Route::post('/s/chat/media/image', [MediaController::class, 'uploadImage']);
    Route::any('classroom/init_chat/{classroomId}', [BeliChatIntegrationController::class, 'createChatByClassroom','classroomId']);
});

Route::middleware(['auth'])->group(function () {
    Route::any('f/classroom', [ClassroomController::class, 'filterClassroomAction']);
    Route::any('f/teacher', [TeacherController::class, 'filterTeacherAction']);
    Route::any('f/supporter', [SupporterController::class, 'filterSupporterAction']);
});

Route::middleware([Authenticate::class, FrontendMiddeware::class])->prefix('crm')->group(function () {
    Route::get('accounts', [AccountController::class, 'getAllAccountAction']);
    Route::get('account/{accountId}', [AccountController::class, 'getOneAccountAction']);
    Route::post('account', [AccountController::class, 'createAccountAction']);
    Route::put('account/{accountId}', [AccountController::class, 'updateAccountAction', 'accountId']);
    Route::delete('account/{accountId}', [AccountController::class, 'deleteAccountAction', 'accountId']);

    Route::get('account_relations', [AccountRelationController::class, 'getAllAccountRelationAction']);
    Route::get('account_relation/{accountRelationId}', [
        AccountRelationController::class,
        'getOneAccountRelationAction'
    ]);
    Route::post('account_relation', [AccountRelationController::class, 'createAccountRelationAction']);
    Route::put('account_relation/{accountRelationId}', [
        AccountRelationController::class,
        'updateAccountRelationAction',
        'accountRelationId'
    ]);
    Route::delete('account_relation/{accountRelationId}', [
        AccountRelationController::class,
        'deleteAccountRelationAction',
        'accountRelationId'
    ]);

    Route::get('/fields', [CustomFieldController::class, 'getCrmModuleFields']);
});

Route::middleware([Authenticate::class, FrontendMiddeware::class])->prefix('finance')->group(function () {
    Route::get('cashiers', [CashierController::class, 'getAllCashierAction']);
    Route::get('cashiers/fields', [CashierController::class, 'getCashierFields']);
});


Route::middleware([Authenticate::class, FrontendMiddeware::class])->prefix('integration')->group(function () {
    Route::get('google_form/connect_url', [GoogleFormController::class, 'getConnectUrlAction']);
    Route::get('google_form/config', [GoogleFormController::class, 'getGoogleFormConfigAction']);
    Route::get('google_form/tabs/{sheetId}', [GoogleFormController::class, 'getGoogleFormTabsAction']);
    Route::post('google_form/tab', [GoogleFormController::class, 'getGoogleFormSheetDetailAction']);
    Route::post('google_form/create_code', [GoogleFormController::class, 'createGoogleScriptCode']);


});

Route::middleware(['auth'])->group(function () {
    Route::get('accounts/list', [AccountController::class, "listView"]);

    Route::get('integration/google_form', [GoogleFormController::class, "getView"]);
    Route::get('integration/custom_field', [CustomFieldController::class, 'getCustomFieldView']);
    Route::post('integration/custom_field/account/create', [CustomFieldController::class, 'createAccountCustomField']);
    Route::get('integration/custom_field/account/detail/{name}', [
        CustomFieldController::class,
        'getOneAccountCustomField'
    ]);
    Route::put('integration/custom_field/account/update', [CustomFieldController::class, 'updateAccountCustomField']);
    Route::delete('integration/custom_field/account/delete/{name}', [
        CustomFieldController::class,
        'deleteAccountCustomField'
    ]);

    Route::prefix('finance')->group(function () {
        Route::get('cashiers/list', [CashierController::class, 'listView']);
        Route::get('cashiers/create', [CashierController::class, 'createTransactionView'])->name('cashiers.create');
        Route::post('cashiers/store', [CashierController::class, 'storeOtherTransaction'])->name('cashiers.store');
        Route::get('cashiers/edit/{id}', [CashierController::class, 'editCashierView', 'id'])->name('cashiers.edit');
        Route::post('cashiers/update/{id}', [
            CashierController::class,
            'updateCashierView',
            'id'
        ])->name('cashiers.update');
        Route::post('cashiers/delete/{id}', [
            CashierController::class,
            'deleteCashierView',
            'id'
        ])->name('cashiers.delete');

        Route::post('transaction-type/store', [
            CashierController::class,
            'storeTransactionType'
        ])->name('cashiers.transactionType.store');
    });

    Route::middleware([Authenticate::class, FrontendMiddeware::class])->prefix('s/studylog')->group(function () {
        #Getter
        Route::get('classrooms', [StudyLogApiController::class, 'getAllClassroomAction']);
        Route::get('schedules/{classroomId}', [StudyLogApiController::class, 'getAllSchedulesAction', 'classroomId']);
        Route::get('shifts/{scheduleId}', [StudyLogApiController::class, 'getAllShiftsAction', 'scheduleId']);
        Route::get('lesson_plan', [StudyLogApiController::class, 'getAllLessonPlantAction']);
        Route::get('cards/{classroomId}', [StudyLogApiController::class, 'getAllCardsAction', 'classroomId']);
        Route::post('cards/extends/{cardId}', [StudyLogApiController::class, 'extendDegCardAction', 'cardId']);
        Route::get('one/{id}', [StudyLogApiController::class, 'getOneStudyLogAction', 'id']);
        #
        Route::post('create', [StudyLogApiController::class, 'createStudyLogAction']);
        Route::put('update/{id}', [StudyLogApiController::class, 'updateStudyLogAction', 'id']);
        Route::get('card/un_attendance/{id}', [StudyLogApiController::class, 'getUnAttendedCardAction', 'id']);
        Route::post('valid_days', [StudyLogApiController::class, 'getAllDayValidDays']);
        Route::post('create', [StudyLogApiController::class, 'createStudyLogAction']);
        Route::put('update/{id}', [StudyLogApiController::class, 'updateStudyLogAction', 'id']);
        Route::get('card/un_attendance/{id}', [StudyLogApiController::class, 'getUnAttendedCardAction', 'id']);
        Route::post('valid_days', [StudyLogApiController::class, 'getAllDayValidDays']);
        Route::post('create', [StudyLogApiController::class, 'createStudyLogAction']);
        Route::put('update/{id}', [StudyLogApiController::class, 'updateStudyLogAction', 'id']);
        Route::post('valid_days', [StudyLogApiController::class, 'getAllDayValidDays']);
    });

    Route::prefix("/s/report/")->group(function () {
        Route::get('cash_flow_pie', [ReportController::class, 'getCashFlowPieChart']);
        Route::get('cash_flow_statistic', [ReportController::class, 'getCashFlowStatistics']);
        Route::get('education_gross_bar', [ReportController::class, 'getEducationGrossBarChart']);
        Route::get('education_gross_statistic', [ReportController::class, 'getEducationGrossStatistic']);
        Route::get('education_gross_data', [ReportController::class, 'getEducationGrossData']);
    });
    Route::prefix("report")->group(function () {
        Route::get('cash_flow', [ReportController::class, 'getCashFlowView']);
        Route::get('education_gross', [ReportController::class, 'getEducationGrossView']);
    });


    Route::prefix('/s')->group(function () {
        Route::get('/import/ability', [ImportController::class, 'importAbility']);
        Route::get('/{entity}/import/template', [ImportController::class, 'downloadTemplate', 'entity']);
        Route::get('/{entity}/import/titles', [ImportController::class, 'getAllImportTitle', 'entity']);
        Route::post('/student/import/validate', [ImportController::class, 'handleValidateStudentImport']);
        Route::post('/classroom/import/validate', [ImportController::class, 'handleValidateClassroomImport']);
        Route::post('/teacher/import/validate', [ImportController::class, 'handleValidateTeacherImport']);
        Route::post('/supporter/import/validate', [ImportController::class, 'handleValidateSupporterImport']);
        Route::post('/staff/import/validate', [ImportController::class, 'handleValidateStaffImport']);
        Route::post('/student/import', [ImportController::class, 'importStudent']);
        Route::post('/classroom/import', [ImportController::class, 'importClassroom']);
        Route::post('/teacher/import', [ImportController::class, 'importTeacher']);
        Route::post('/supporter/import', [ImportController::class, 'importSupporter']);
        Route::post('/staff/import', [ImportController::class, 'importStaff']);
    });
    Route::middleware([Authenticate::class, FrontendMiddeware::class])->prefix('s/bulk')->group(function () {
        Route::get('/cards', [BulkApiController::class, 'getAllCard']);
        Route::get('/students', [BulkApiController::class, 'getAllStudent']);
        Route::get('/classrooms', [BulkApiController::class, 'getAllClassroom']);
        Route::post('/cards/clone', [BulkApiController::class, 'bulkCloneCardAction']);
        Route::post('/cards/add_to_classroom', [BulkApiController::class, 'addCardToClassroom']);
    });
});


Route::get('test', function () {
    Artisan::call('salary:calculate');
})->name('test');

Route::middleware('auth')->prefix('bcoin')->group(function () {
    Route::get('connect', [BcoinController::class, 'connectView']);
    Route::post('connect', [BcoinController::class, 'connect']);
    Route::any('disconnect', [BcoinController::class, 'disconnect']);
});

Route::middleware([Authenticate::class, FrontendMiddeware::class])->prefix('s/bcoin')->group(function () {
    Route::post('recharge', [BcoinController::class, 'createRecharge']);
});
