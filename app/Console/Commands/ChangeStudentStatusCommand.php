<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ChangeStudentStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:change_student_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Thay đổi trạng thái học sinh';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $lastThreeMonth = Carbon::now()->subMonths(3)->toDateTimeString();

        $lastSixMonth = Carbon::now()->subMonths(6)->toDateTimeString();

        DB::table('users')
            ->leftJoin('cards', 'users.id', '=', 'cards.student_id')
            ->leftJoin('card_logs', 'cards.id', '=', 'card_logs.card_id')
            ->where('users.role', 2)
            ->where('users.student_status', '!=', 2)
            ->where('users.student_status', '!=', 3)
            ->where('card_logs.created_at', '<=', $lastThreeMonth)
            ->update(['users.student_status' => 2]);

        DB::table('users')
            ->leftJoin('cards', 'users.id', '=', 'cards.student_id')
            ->leftJoin('card_logs', 'cards.id', '=', 'card_logs.card_id')
            ->where('users.role', 2)
            ->where('users.student_status', '!=', 3)
            ->where('card_logs.created_at', '<=', $lastSixMonth)
            ->update(['users.student_status' => 3]);

        return 0;
    }
}
