<?php

namespace App\Console\Commands;

use App\Models\Card;
use Illuminate\Console\Command;

class ResetCardUuid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset card uui';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cards = Card::query()->withoutGlobalScope('branch')->select(['id', 'branch'])->get();
        foreach ($cards as $card) {
            $uuid = $card['id'] < 100000
                ? sprintf('%04d', $card['id'] + 1)
                : $card['id'] + 1;

            $newUUID = $card['branch'] . "-" . "StudyCard" . "." . $uuid;

            $card->update([
                'uuid' => $newUUID
            ]);
        }
        return "OKE Cọc vl";
    }
}
