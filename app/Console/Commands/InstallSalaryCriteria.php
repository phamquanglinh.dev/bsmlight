<?php

namespace App\Console\Commands;

use App\Models\Branch;
use App\Models\SalaryCriteria;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InstallSalaryCriteria extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salary:install {branch?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $branch = $this->argument('branch') ?? null;

        if (!$branch) {
            $branches = DB::table('branches as b')
                ->leftJoin('salary_criteria as s', 'b.uuid', '=', 's.branch')
                ->whereNull('s.branch')
                ->select('b.uuid')
                ->get()->pluck('uuid')->toArray();
        } else {
            $branches = [$branch];
        }

        foreach ($branches as $branch) {
            $this->alert('Khởi tạo module lương cho chi nhánh ' . $branch);

            foreach ($this->defineDefaultCriterias() as $criteria) {
                $criteria['branch'] = $branch;

                DB::table('salary_criteria')->updateOrInsert([
                    'criteria_code' => $criteria['criteria_code'],
                    'branch' => $criteria['branch'],
                    'criteria_type' => $criteria['criteria_type']
                ], $criteria);
            }
        }

        return 0;
    }

    private function defineDefaultCriterias(): array
    {
        return [
            [
                'criteria_name' => 'Số buổi học điểm danh muộn',
                'criteria_code' => SalaryCriteria::CRITERIA_LATE_STUDY_LOG_NUM,
                'criteria_type' => SalaryCriteria::TYPE_PUNISH,
                'is_bsm_criteria' => 1,
                'criteria_description' => 'Số buổi học nhân sự đó được giao điểm danh nhưng điểm danh muộn',
                'unit_type' => SalaryCriteria::UNIT_INTEGER,
                'criteria_rate' => 3000,
            ],
            [
                'criteria_name' => 'Số buổi học sinh đi học',
                'criteria_code' => SalaryCriteria::CRITERIA_STUDENT_ATTEND_NUM,
                'criteria_type' => SalaryCriteria::TYPE_REWARD,
                'is_bsm_criteria' => 1,
                'criteria_description' => 'Là số buổi học sinh đi học của các buổi học trong tháng',
                'unit_type' => SalaryCriteria::UNIT_INTEGER,
                'criteria_rate' => 1500,
            ],
            [
                'criteria_name' => '% Doanh Thu',
                'criteria_code' => SalaryCriteria::CRITERIA_REVENUE_PERCENT,
                'criteria_type' => SalaryCriteria::TYPE_REWARD,
                'is_bsm_criteria' => 1,
                'criteria_description' => 'Doanh thu do chính nhân sự đó tạo ra tính theo buổi học đã chạy trong tháng',
                'unit_type' => SalaryCriteria::UNIT_PERCENT,
                'criteria_rate' => 5,
            ],
        ];
    }
}
