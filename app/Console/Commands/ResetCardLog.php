<?php

namespace App\Console\Commands;

use App\Models\Card;
use App\Models\CardLog;
use Illuminate\Console\Command;

class ResetCardLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reset_card_log_deg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**
         * @var CardLog[] $cardLogs
         * @var Card $card
         */
        $cardLogs = CardLog::query()
            ->where('can_deg', null)
            ->orderBy('id')
            ->get(['id', 'card_id']);

        foreach ($cardLogs as $cardLog) {
            $card = Card::query()->withoutGlobalScopes()->where('id', $cardLog['card_id'])->first();

            $cardLog->update([
                'can_deg' => $card->getCanDegAttribute() ? 1 : 0
            ]);
            $this->alert($cardLog['id']);
        }

        return 0;
    }
}
