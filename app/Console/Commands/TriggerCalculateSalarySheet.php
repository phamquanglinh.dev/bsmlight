<?php

namespace App\Console\Commands;

use App\Helper\SalaryCalculateService;
use App\Models\SalaryCriteria;
use App\Models\SalarySheet;
use App\Models\SalarySheetDetail;
use App\Models\SalarySnap;
use App\Models\SalarySnapDetail;
use App\Models\StudyLog;
use App\Models\WorkingShift;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TriggerCalculateSalarySheet extends Command
{
    private SalaryCalculateService $salaryCalculateService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salary:calculate {salary_sheet_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tính toán tạo thẻ lương';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param SalaryCalculateService $salaryCalculateService
     * @return void
     */
    public function handle(
        SalaryCalculateService $salaryCalculateService
    ): void
    {
        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;

        $this->salaryCalculateService = $salaryCalculateService;

        $salarySheetId = $this->argument()['salary_sheet_id'];

        if ($salarySheetId) {
            /**
             * @var SalarySheet $salarySheet
             */
            $salarySheet = SalarySheet::query()->withoutGlobalScopes()->where('id', $salarySheetId)->first();

            $this->handleCreateSalarySnapBySalarySheet($salarySheet, $currentYear, $currentMonth);

            return;
        }

        /**
         * @var SalarySheet[] $salarySheets
         */
        $salarySheets = SalarySheet::query()->withoutGlobalScopes()
            ->where(function($query) use ($currentYear) {
                $query->where('loop_type', 1)
                    ->where('period_year', $currentYear);
            })
            ->orWhere(function($query) use ($currentYear, $currentMonth) {
                $query->where('loop_type', 5)
                    ->where('period_year', $currentYear)
                    ->whereRaw("JSON_CONTAINS(JSON_EXTRACT(attributes, '$.period_months'), '\"$currentMonth\"')");
            })
            ->whereNull('deleted_at')
            ->get();

        foreach ($salarySheets as $salarySheet) {
            $this->handleCreateSalarySnapBySalarySheet($salarySheet, $currentYear, $currentMonth);
        }
    }

    private function handleCreateSalarySnapBySalarySheet(SalarySheet $salarySheet, int $currentYear, int $currentMonth)
    {
        $period = Carbon::create($currentYear, $currentMonth)->toDate();

        $snapData = [
            'classroom_id' => $salarySheet['classroom_id'],
            'snap_salary_name' => $salarySheet['sheet_name'],
            'snap_salary_rate' => $salarySheet['salary_rate'],
            'snap_salary_fund_percent' => $salarySheet['fund_percent'],
            'period' => $period,
            'salary_sheet_id' => $salarySheet['id'],
            'user_id' => $salarySheet['user_id'],
            'branch' => $salarySheet['branch'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];

        $snapData['snap_salary_value'] = $this->calculateSnapSalary($salarySheet['user_id'], $salarySheet['salary_rate'], $period, $salarySheet['classroom_id']);

        $snapDetailData = [];

        foreach ($salarySheet->getSalarySheetDetails()->get() as $sheetDetail) {
            /**
             * @var SalarySheetDetail $sheetDetail
             */

            $snapDetailData[] = [
                'classroom_id' => $sheetDetail['classroom_id'],
                'snap_criteria_name' => $sheetDetail['criteria_name'],
                'snap_criteria_code' => $sheetDetail['criteria_code'],
                'snap_criteria_type' => $sheetDetail['criteria_type'],
                'snap_criteria_rate' => $sheetDetail['criteria_rate'],
                'salary_sheet_detail_id' => $sheetDetail['id'],
                'user_id' => $salarySheet['user_id'],
                'salary_sheet_id' => $salarySheet['id'],
                'snap_value' => $sheetDetail['criteria_code'] !== SalaryCriteria::CRITERIA_MANUAL ? $this->salaryCalculateService->calculateCriteriaToAmount($sheetDetail, $salarySheet['user_id']) : 0,
                'snap_unit_type' => $sheetDetail['unit_type'],
                'period' => $period,
                'branch' => $salarySheet['branch'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }


        $oldSnap = SalarySnap::query()->withoutGlobalScopes()->where([
            'salary_sheet_id' => $snapData['salary_sheet_id'],
            'user_id' => $salarySheet['user_id']
        ])->first();

        if ($oldSnap) {
            $oldSnap->update($snapData);
        } else {
            $snapData['uuid'] = SalarySnap::makeUUID($salarySheet['branch']);

            SalarySnap::query()->withoutGlobalScopes()->create($snapData);
        }

        $mappedOldSnapDetails = SalarySnapDetail::query()->where([
            'user_id' => $salarySheet['user_id'],
            'salary_sheet_id' => $salarySheet['id'],
        ])->get()->mapWithKeys(function (SalarySnapDetail $snapDetail) {
            return [$snapDetail['salary_sheet_detail_id'] => $snapDetail];
        });

        foreach ($snapDetailData as $snapDetail) {
            $oldSnapDetail = $mappedOldSnapDetails[$snapDetail['salary_sheet_detail_id']] ?? null;

            if ($oldSnapDetail) {
                unset($mappedOldSnapDetails[$oldSnapDetail['salary_sheet_detail_id']]);

                if ($snapDetail['snap_criteria_code'] == SalaryCriteria::CRITERIA_MANUAL) {
                    unset($snapDetail['snap_value']);
                }

                $oldSnapDetail->update($snapDetail);

                continue;
            }

            SalarySnapDetail::query()->create($snapDetail);
        }

        foreach ($mappedOldSnapDetails as $mappedOldSnapDetail) {
            SalarySnapDetail::query()->where([
                'salary_sheet_detail_id' => $mappedOldSnapDetail['salary_sheet_detail_id']
            ])->delete();
        }
    }


    private function calculateSnapSalary($userId, $salaryRate, DateTime $period, int $classroomId = null)
    {
        $firstDay = Carbon::now()->firstOfMonth();
        $lastDay = Carbon::now()->lastOfMonth();

        /**
         * @var WorkingShift[] $workingShifts
         */
        $builder = WorkingShift::query();

        if ($classroomId) {
            $builder->whereHas('StudyLog', function (Builder $studylog) use ($classroomId) {
                $studylog->where('classroom_id', $classroomId);
            });
        }

        $workingShifts = $builder->where(function (Builder $builder) use ($userId) {
            $builder->where('staff_id', $userId)
                ->orWhere('teacher_id', $userId)
                ->orWhere('supporter_id', $userId);
        })->whereHas('StudyLog', function (Builder $studyLog) use ($period, $firstDay, $lastDay) {
            $studyLog->where('status', StudyLog::ACCEPTED)
                ->whereRaw("DATE(studylog_day) >= ?", [$firstDay])
                ->whereRaw("DATE(studylog_day) <= ?", [$lastDay]);
        })->get();
        $hour = 0;
        foreach ($workingShifts as $workingShift) {
            $hour += $workingShift->getDurationHourAttribute();
        }

        return $hour * $salaryRate;
    }
}
