<?php

namespace App\Console\Commands;

use App\Helper\UUID;
use App\Models\Branch;
use App\Models\Card;
use App\Models\Classroom;
use App\Models\Staff;
use App\Models\Student;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class resetUUid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uuid:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset UUID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return true
     */
    public function handle(): bool
    {
        $this->resetTeacher();
        $this->resetStudent();
        $this->resetSupporter();
        $this->resetStaff();
        $this->resetClassroom();
        $this->resetCard();
        $this->fixWrongBranch();

        return true;
    }

    private function resetTeacher()
    {
        /**
         * @var Teacher[] $teachers
         */
        $teachers = Teacher::query()->withoutGlobalScopes()->where('role', '=', User::TEACHER_ROLE)->get();

        $teacherUUID = [];

        foreach ($teachers as $teacher) {
            $prefix = $teacher->{'teacher_source'} == 1 ? "GVVN" : "GVNN";

            $newUUID = $teacherUUID[$teacher->{'branch'}] ?? 1;

            $fUUID = sprintf('%04d', $newUUID);

            $teacher->update([
                'uuid' => "{$teacher->{'branch'}}-{$prefix}.{$fUUID}"
            ]);

            $teacherUUID[$teacher->{'branch'}] = $newUUID + 1;
        }
    }

    private function resetStudent(): void
    {
        /**
         * @var Student[] $students
         */
        $students = Student::query()->withoutGlobalScopes()->where('role', '=', User::STUDENT_ROLE)->get();

        $studentUUID = [];

        foreach ($students as $student) {
            $newUUID = $studentUUID[$student->{'branch'}] ?? 1;

            $fUUID = sprintf('%04d', $newUUID);

            $student->update([
                'uuid' => "{$student->{'branch'}}-HS.{$fUUID}"
            ]);

            $studentUUID[$student->{'branch'}] = $newUUID + 1;
        }
    }

    private function resetSupporter()
    {
        /**
         * @var Student[] $supporters
         */
        $supporters = Supporter::query()->withoutGlobalScopes()->where('role', '=', User::SUPPORTER_ROLE)->get();

        $supporterUuid = [];

        foreach ($supporters as $supporter) {
            $newUUID = $supporterUuid[$supporter->{'branch'}] ?? 1;

            $fUUID = sprintf('%04d', $newUUID);

            $supporter->update([
                'uuid' => "{$supporter->{'branch'}}-TG.{$fUUID}"
            ]);

            $supporterUuid[$supporter->{'branch'}] = $newUUID + 1;
        }
    }

    private function resetStaff(): void
    {
        /**
         * @var Student[] $staffs
         */
        $staffs = Staff::query()->withoutGlobalScopes()->where('role', '=', User::STAFF_ROLE)->get();

        $staffUuid = [];

        foreach ($staffs as $staff) {
            $newUUID = $staffUuid[$staff->{'branch'}] ?? 1;

            $fUUID = sprintf('%04d', $newUUID);

            $staff->update([
                'uuid' => "{$staff->{'branch'}}-NV.{$fUUID}"
            ]);

            $staffUuid[$staff->{'branch'}] = $newUUID + 1;
        }
    }

    private function resetClassroom()
    {
        /**
         * @var Student[] $classrooms
         */
        $classrooms = Classroom::query()->withoutGlobalScopes()->get();

        $staffUuid = [];

        foreach ($classrooms as $classroom) {
            $newUUID = $staffUuid[$classroom->{'branch'}] ?? 1;

            $fUUID = sprintf('%04d', $newUUID);

            $classroom->update([
                'uuid' => "{$classroom->{'branch'}}-Lop.{$fUUID}"
            ]);

            $staffUuid[$classroom->{'branch'}] = $newUUID + 1;
        }
    }

    private function resetCard(): void
    {
        $cards = Card::query()->withoutGlobalScopes()->get();

        $cardUuid = [];

        foreach ($cards as $card) {
            $newUUID = $cardUuid[$card->{'branch'}] ?? 1;

            $fUUID = sprintf('%04d', $newUUID);

            $card->update([
                'uuid' => "{$card->{'branch'}}-StudyCard.{$fUUID}"
            ]);

            $cardUuid[$card->{'branch'}] = $newUUID + 1;
        }
    }

    private function fixWrongBranch(): void
    {
        /**
         * @var Card[] $cards
         */
        $cards = Card::query()->withoutGlobalScopes()->get(['branch', 'id']);

        foreach ($cards as $card) {
            if (!str_contains($card->{'branch'}, 'BSM')) {
                Card::query()->withoutGlobalScopes()->where('id', $card->{'id'})->update([
                    'branch' => 'BSM-' . $card->{'branch'}
                ]);
            }
        }

        $branches = Branch::query()->withoutGlobalScopes()->get(['uuid', 'id']);

        foreach ($branches as $branch) {
            $branch->update([
                'uuid' => 'BSM-CN.' . sprintf('%04d',$branch->{'id'})
            ]);
        }
    }
}
