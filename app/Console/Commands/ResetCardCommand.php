<?php

namespace App\Console\Commands;

use App\Models\Card;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ResetCardCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'card:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cards = Card::query()->withoutGlobalScopes()->get();

        $x = 0;

        foreach ($cards as $card) {
            $paidAmount = $card->{'paid_fee'};

            $totalTransaction = Transaction::query()->where([
                'object_type' => Transaction::CARD_TRANSACTION_TYPE,
                'object_id' => $card->{'id'},
                'status' => 1
            ])->sum('amount');

            $notHaveTransaction = $paidAmount - $totalTransaction;

            Transaction::query()->create([
                'created_by' => 0,
                'uuid' => now()->timestamp + $x,
                'transaction_type' => 'new',
                'object_type' => Transaction::CARD_TRANSACTION_TYPE,
                'object_id' => $card->{'id'},
                'amount' => $notHaveTransaction,
                'notes' => 'INIT BY SYSTEM',
                'status' => 1,
                'approve' => 0,
                'created_at' => Carbon::now()->subYears(20),
                'updated_at' => Carbon::now()->subYears(20),
            ]);
            $x += 1;
        }

        Card::query()->withoutGlobalScopes()->update([
            'paid_fee' => 0
        ]);
    }
}
