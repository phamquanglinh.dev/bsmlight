<?php

namespace App\Console\Commands;

use App\Jobs\SendSOSNotification;
use App\Models\Card;
use App\Models\Staff;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SosCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $mainQuery = DB::table(DB::raw('(SELECT
                                    c.id,
                                    c.branch,
                                    c.uuid,
                                    c.van,
                                    c.original_days,
                                    c.bonus_days,
                                    cl.staff_id,
                                    (c.original_fee - c.promotion_fee) AS total_fee,
                                    u.name,
                                    COUNT(clg.id) AS attended,
                                    (COUNT(clg.id) + c.van) AS van_and_attended
                                FROM
                                    cards AS c
                                LEFT JOIN
                                    classrooms AS cl ON cl.id = c.classroom_id
                                LEFT JOIN
                                    users AS u ON u.id = cl.staff_id
                                LEFT JOIN
                                    card_logs AS clg ON c.id = clg.card_id
                                LEFT JOIN
                                    studylogs AS stlogs ON stlogs.id = clg.studylog_id
                                WHERE
                                    u.role = 5
                                    AND stlogs.`status` = 5
                                    AND clg.day = 1
                                GROUP BY
                                    c.id,
                                    c.branch,
                                    c.uuid,
                                    c.van,
                                    c.original_days,
                                    c.bonus_days,
                                    cl.staff_id,
                                    u.name
                                ) AS main_query'))
            ->select([
                'main_query.*',
                DB::raw('(SELECT SUM(trans.amount)
                             FROM transactions AS trans
                             WHERE trans.object_id = main_query.id
                             AND trans.object_type = "card"
                             AND trans.status = 1) AS total_transaction_amount')
            ])->get();

        foreach ($mainQuery as $card) {
            $card = (array)$card;

            if ($card['total_fee'] <= 0) {
                continue;
            }

            $can_use_day_by_paid = $card['total_transaction_amount'] / $card['total_fee'];

            $can_use_day = $can_use_day_by_paid - $card['van_and_attended'];

            if ($can_use_day <= 0) {
                SendSOSNotification::dispatch($card);
            }
        }
        return 0;
    }
}
