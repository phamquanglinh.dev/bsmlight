<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use Illuminate\Console\Command;

class TransactionDayReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction_day:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $transactions = Transaction::query()->get();

        foreach ($transactions as $transaction) {
            if ($transaction->{'transaction_day'} == null) {
                $transaction->update([
                    'transaction_day' => $transaction->{'created_at'},
                ]);
            }
        }

        return 1;
    }
}
