<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\AccountRelation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InitialAccountRelation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relation:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $accountRelations = [
            [
                'relation_name' => 'Chưa liên hệ',
                'color' => '#ff0000'
            ],
            [
                'relation_name' => 'Chưa được trả lời',
                'color' => '#ea9999'
            ],
            [
                'relation_name' => 'Chưa chốt được hẹn gặp',
                'color' => '#93c47d'
            ],
            [
                'relation_name' => 'Chưa gặp',
                'color' => '#38761d'
            ],
            [
                'relation_name' => 'Chưa test',
                'color' => '#6d9eeb'
            ],
            [
                'relation_name' => 'Chưa học thử',
                'color' => '#0000ff'
            ],
            [
                'relation_name' => 'Chưa tư vấn',
                'color' => '#b4a7d6'
            ],
            [
                'relation_name' => 'Chưa đồng ý đăng ký',
                'color' => '#9900ff'
            ],
            [
                'relation_name' => 'Chưa vào tiền',
                'color' => '#9900ff'
            ],
            [
                'relation_name' => 'Đã vào tiền',
                'color' => '#00ff00'
            ],
            [
                'relation_name' => 'Từ chối',
                'color' => '#ff0000'
            ],
            [
                'relation_name' => 'Lead Lạnh',
                'color' => '#b4a7d6'
            ],
        ];

        DB::table('account_relations')->truncate();

        DB::table("account_relations")->insert($accountRelations);

        return 1;
    }
}
