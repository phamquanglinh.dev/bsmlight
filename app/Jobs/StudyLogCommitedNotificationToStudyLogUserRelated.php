<?php

namespace App\Jobs;

use App\Helper\DesktopNotification;
use App\Helper\Object\NotificationObject;
use App\Models\StudyLog;
use App\Models\SystemLog;
use App\Models\WorkingShift;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class StudyLogCommitedNotificationToStudyLogUserRelated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private StudyLog $studyLog;
    private int $userId;
    private string $branch;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        StudyLog $studyLog,
        int $userId,
        string $branch,
    )
    {
        $this->studyLog = $studyLog;
        $this->userId = $userId;
        $this->branch = $branch;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle(): void
    {
        $userIds = [];

        $studyLog = $this->studyLog;

        $uuid = Carbon::parse($studyLog['created_at'])->timestamp;
        /**
         * @var WorkingShift[] $workingShifts
         */
        $workingShifts = WorkingShift::query()->where('studylog_id', $studyLog['id'])->get();

        foreach ($workingShifts as $workingShift) {
            $userIds[] = $workingShift->supporter_id;
            $userIds[] = $workingShift->teacher_id;
            $userIds[] = $workingShift->staff_id;
        }

        $userIds = array_unique($userIds);

        SystemLog::query()->create([
            'title' => 'Buổi học #' . Carbon::parse($studyLog['created_at'])->timestamp . " đã được gửi lên",
            'object_type' => 'studylog',
            'active_type' => 'submit',
            'object_id' => $studyLog['id'],
            'user_id' => $this->userId,
            'ip_address' => 'System',
            'branch' => $this->branch,
        ]);

        Log::debug('hi');

        DesktopNotification::sendNotificationForAll(new NotificationObject(
            title: 'Buổi học được gửi lên',
            body: "Buổi học #{$uuid} đã được gửi lên, hãy kiểm tra và xác nhận thông tin", user_ids: $userIds,
            thumbnail: '',
            ref: url('/') . '/studylog/show/' . $studyLog['id'],
            attributes: []
        ));
    }
}
