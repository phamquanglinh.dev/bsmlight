<?php

namespace App\Jobs;

use App\Helper\DesktopNotification;
use App\Helper\Object\NotificationObject;
use App\Models\Notification;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSOSNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private array $cardData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $cardData)
    {
        $this->cardData = $cardData;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        $card = $this->cardData;

        $url = ('/card/show/' . $card['id']);

        $notification = Notification::query()->where('url', "like", "%$url%")
            ->where('title', 'like', '%SOS%')
            ->where('user_id', $card['staff_id'])
            ->where('read', 0);

        if ($notification->count() > 0) {
            return;
        }

        $notification = new Notification([
            'url' => $url,
            'title' => "SOS: Thẻ học {$card['uuid']} đã hết số buổi",
            'user_id' => $card['staff_id'],
            'read' => 0,
            'description' => 'Thẻ học đã dùng hết số buổi theo thanh toán, liên hệ với HS để renew',
            'pin' => 1,
            'thumbnail' => ''
        ]);

        $notification->save();

        DesktopNotification::sendNotificationForSingleUser(new NotificationObject(
            title: $notification['title'],
            body: $notification['description'],
            user_ids: [$notification['user_id']],
            thumbnail: '', ref: env('APP_URL') . "" . $url,
            attributes: []
        ), createNotification: 0);
    }
}
