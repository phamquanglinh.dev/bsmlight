<?php

namespace App\Providers;

use App\Events\ClassroomCovered;
use App\Events\ClassroomWasCreated;
use App\Events\ClassroomWasUpdated;
use App\Listeners\CreateSalarySheet;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        ClassroomWasCreated::class => [
            CreateSalarySheet::class
        ],
        ClassroomWasUpdated::class => [
            CreateSalarySheet::class
        ],
        ClassroomCovered::class => [
            CreateSalarySheet::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
