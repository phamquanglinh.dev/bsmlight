<?php

namespace App\Composer;

use App\Models\UserConfig;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TutorialComposer
{
    public function compose(View $view)
    {
        $abilities = [
            'teacher' => 0,
            'supporter' => 0,
            'staff' => 0,
            'classroom' => 0,
            'student' => 0
        ];

        $teacherImported = UserConfig::query()->where('branch', Auth::user()->{'branch'})->where('type', 'teacher_imported')->first();
        $supporterImported = UserConfig::query()->where('branch', Auth::user()->{'branch'})->where('type', 'supporter_imported')->first();
        $staffImported = UserConfig::query()->where('branch', Auth::user()->{'branch'})->where('type', 'staff_imported')->first();
        $classroomImported = UserConfig::query()->where('branch', Auth::user()->{'branch'})->where('type', 'classroom_imported')->first();
        $studentImported = UserConfig::query()->where('branch', Auth::user()->{'branch'})->where('type', 'student_imported')->first();

        if (!$studentImported) {
            $abilities['student'] = 1;
        }

        if (!$classroomImported) {
            $abilities['classroom'] = 1;
            $abilities['student'] = 2;
        }

        if (!$teacherImported) {
            $abilities['teacher'] = 1;
            $abilities['classroom'] = 2;
        }

        if (!$supporterImported) {
            $abilities['supporter'] = 1;
            $abilities['classroom'] = 2;
        }

        if (!$staffImported) {
            $abilities['staff'] = 1;
            $abilities['classroom'] = 2;
        }


        $view->with('importAbilities', $abilities);
    }
}
