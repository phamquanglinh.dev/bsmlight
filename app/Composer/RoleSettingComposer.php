<?php

namespace App\Composer;

use App\Helper\Fields;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class RoleSettingComposer
{
    private function getRolesWithDefault()
    {
        $defaults = [
            'default_staff' => 'Nhân viên mặc định',
            'default_student' => 'Học sinh mặc định',
            'default_teacher' => 'Giáo viên mặc định',
            'default_supporter' => 'Trợ giảng mặc định',
        ];


        $customRoles = Role::query()->where('branch', Auth::user()->{'branch'})->get();

        foreach ($customRoles as $role) {
            $defaults[$role['id']] = $role['name'];
        }

        return $defaults;
    }

    public function compose(View $view)
    {
        $view->with('userSelectField', new Fields([
            'name' => 'users',
            'label' => 'Đối tượng áp dụng',
            'type' => 'select-multiple',
            'options' => $this->getAllUsersWithType(),
            'nullable' => true
        ]));

        $view->with('field', new Fields([
            'name' => 'roles',
            'label' => 'Copy từ mẫu vai trò',
            'type' => 'select',
            'options' => $this->getRolesWithDefault(),
            'nullable' => true
        ]));


    }

    private function getAllUsersWithType(): array
    {
        $defaultUserType = [
            'staff' => 'Tất cả nhân viên',
            'student' => 'Tất cả học sinh',
            'teacher' => 'Tất cả giáo viên',
            'supporter' => 'Tất cả trợ giảng'
        ];

        $users = User::query()->where('branch', Auth::user()->{'branch'})->get();

        foreach ($users as $user) {
            $defaultUserType[$user['id']] = $user['uuid'] . ' ' . $user['name'];
        }

        return $defaultUserType;
    }
}
