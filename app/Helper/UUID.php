<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 21/05/2024 4:58 pm
 */

namespace App\Helper;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Model;

class UUID
{
    public static function make(string $branch, Model $model, string $prefix): string
    {
        $lastRecord = $model::query()->where('branch', $branch)->orderBy('id', 'DESC')->first();

        if (! $lastRecord) {
            $newUUId = 1;
        } else {
            $dataUUid = explode('.', $lastRecord->{'uuid'});

            if ((int)(last($dataUUid))) {
                $oldUuid = last($dataUUid);
            } else {
                $oldUuid = 0;
            }

            $newUUId = (int) $oldUuid + 1;
        }
        if ($newUUId < 10000000) {
            $formattedNewUUid = sprintf('%04d', $newUUId);
        } else {
            $formattedNewUUid = $newUUId;
        }

        return "{$branch}-$prefix.{$formattedNewUUid}";
    }
}
