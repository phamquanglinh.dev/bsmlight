<?php

namespace App\Helper;

class QueryHelper
{
    private static array $models = [];

    public static function addModel(string $model): void
    {
        self::$models[] = $model;
    }

    public  static function isWithOutAppend(string $model): bool
    {
        return in_array($model, self::$models);
    }
}
