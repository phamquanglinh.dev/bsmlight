<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 29/03/2024 5:36 pm
 */

namespace App\Helper;

use App\Models\UserConfig;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class GoogleClientHelper
{

    public function makeLoginUrl($scopes, $callback): string
    {
        $clientId = env('GOOGLE_CLIENT_ID');

        return 'https://accounts.google.com/o/oauth2/auth?' . http_build_query([
                'client_id' => $clientId,
                'redirect_uri' => $callback,
                'scope' => implode(' ', $scopes),
                'response_type' => 'code',
                'access_type' => 'offline',
                'approval_prompt' => 'force'
            ]);
    }

    public function authWithCode(string $code, string $callback): array|null
    {
        $clientId = env('GOOGLE_CLIENT_ID');
        $clientSecret = env('GOOGLE_CLIENT_SECRET');

        $client = new Client();

        try {
            $response = $client->post('https://oauth2.googleapis.com/token', [
                'form_params' => [
                    'code' => $code,
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret,
                    'redirect_uri' => $callback,
                    'grant_type' => 'authorization_code'
                ]
            ]);
        } catch (\Exception|GuzzleException $exception) {
            return null;
        }

        $tokens = json_decode($response->getBody(), true);

        if (isset($tokens['error'])) {
            return null;
        }

        return $tokens;
    }

    /**
     * @throws GuzzleException
     */
    public function authWithRefreshToken(string $refreshToken)
    {
        $clientId = env('GOOGLE_CLIENT_ID');
        $clientSecret = env('GOOGLE_CLIENT_SECRET');

        $client = new Client();

        try {
            $response = $client->request('POST', 'https://oauth2.googleapis.com/token', [
                'form_params' => [
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret,
                    'refresh_token' => $refreshToken,
                    'grant_type' => 'refresh_token'
                ]
            ]);
        } catch (\Exception $exception) {
            throw new BadRequestException('Lỗi kết nối');
        }

        $body = $response->getBody();

        return json_decode($body, true);
    }
}
