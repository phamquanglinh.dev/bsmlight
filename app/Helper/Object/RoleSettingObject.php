<?php

namespace App\Helper\Object;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

class RoleSettingObject
{
    public function __construct(
        private readonly Collection|array $roles
    )
    {

    }

    /**
     * @return RoleObject[]
     */
    public function getRoles(): array
    {
        return $this->roles->map(function (Role $role) {
            return new RoleObject(
                name: $role['name'],
                permissions: $role->Permissions()->get(),
                users: $role->Users()->get()
            );
        })->toArray();
    }


}
