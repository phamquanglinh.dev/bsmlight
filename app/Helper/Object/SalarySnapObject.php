<?php

namespace App\Helper\Object;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;


class SalarySnapObject
{
    /**
     * $uuid
     * $snap_salary_rate
     * $snap_salary_value
     * $snap_salary_fund_percent
     * $period
     * $salary_sheet_id
     * $user_id
     * $classroom_id
     * $snap_salary_kpi
     * $fund_value
     * $total_value
     * $close_details
     * $branch
     */

    public function __construct(
        private readonly string     $uuid,
        private readonly float      $snap_salary_rate,
        private readonly float      $snap_salary_fund_percent,
        private readonly string     $period,
        private readonly int        $salary_sheet_id,
        private readonly int        $user_id,
        private readonly User       $user,
        private readonly int        $classroom_id,
        private readonly float      $snap_salary_kpi,
        private readonly float      $fund_value,
        private readonly float      $total_value,
        private readonly Collection $salarySnapDetails,
        private readonly string     $branch,
        private readonly float      $snap_salary_value
    )
    {
    }

    public function getSnapSalaryValue(): float
    {
        return $this->snap_salary_value;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getSnapSalaryRate(): float
    {
        return $this->snap_salary_rate;
    }

    public function getSnapSalaryFundPercent(): float
    {
        return $this->snap_salary_fund_percent;
    }

    public function getPeriod(): string
    {
        return $this->period;
    }

    public function getSalarySheetId(): int
    {
        return $this->salary_sheet_id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getClassroomId(): int
    {
        return $this->classroom_id;
    }

    public function getSnapSalaryKpi(): float
    {
        return $this->snap_salary_kpi;
    }

    public function getFundValue(): float
    {
        return $this->fund_value;
    }

    public function getTotalValue(): float
    {
        return $this->total_value;
    }

    public function getSalarySnapDetails(): Collection
    {
        return $this->salarySnapDetails;
    }

    public function getBranch(): string
    {
        return $this->branch;
    }
}
