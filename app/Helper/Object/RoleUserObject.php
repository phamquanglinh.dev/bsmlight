<?php

namespace App\Helper\Object;

class RoleUserObject
{
    public function __construct(
        private readonly int $id,
        private readonly string $name,
        private readonly string $avatar,
    )
    {

    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }
}
