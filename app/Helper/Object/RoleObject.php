<?php

namespace App\Helper\Object;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class RoleObject
{
    public function __construct(
        private readonly string           $name,
        private readonly Collection|array $permissions,
        private readonly Collection|array $users,
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return RolePermissionObject[]
     */
    public function getPermissions(): array
    {
        return $this->permissions->map(function (Permission $permission) {
            return new RolePermissionObject(
                id: $permission['id'],
                name: $permission['name']
            );
        })->toArray();
    }

    /**
     * @return RoleUserObject[]
     */
    public function getUsers(): array
    {
        return $this->users->map(function (User $user) {
            return new RoleUserObject(
                id: $user['id'],
                name: $user['name'], avatar: $user['avatar']
            );
        })->toArray();
    }
}
