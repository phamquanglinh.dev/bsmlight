<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 06/04/2024 4:16 pm
 */

namespace App\Helper\Object;

class ShiftEvent
{
    public function __construct(
        private readonly string $classroomName,
        private readonly string $start,
        private readonly string $end,
        private readonly string $teacher,
        private readonly string $supporter,
        private readonly string $room,
        private readonly int $weekDay,
    ) {}

    public function getWeekDay(): int
    {
        return $this->weekDay;
    }

    public function getClassroomName(): string
    {
        return $this->classroomName;
    }

    public function getStart(): string
    {
        return $this->start;
    }

    public function getEnd(): string
    {
        return $this->end;
    }

    public function getTeacher(): string
    {
        return $this->teacher;
    }

    public function getSupporter(): string
    {
        return $this->supporter;
    }

    public function getRoom(): string
    {
        return $this->room;
    }
}
