<?php

use App\Models\Fund;
use App\Models\Permission;
use App\Models\SalaryGroup;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

#SCOPE

function defineScope(): array
{
    return [
        User::HOST_ROLE => [
            'student',
            'teacher',
            'supporter',
            'staff',
            'classroom',
            'card',
            'transaction',
            'inventory',
            'studylog',
            'custom_field',
            'salary-criteria',
            'salary',
            'salary-sheet',
            'salary-snap'
        ],
        User::STUDENT_ROLE => [

        ],
        User::TEACHER_ROLE => [

        ],
        User::STAFF_ROLE => [
            'student',
            'teacher',
            'supporter',
            'studylog',
            'transaction',
            'classroom',
            'card',
        ],
        User::SUPPORTER_ROLE => [

        ]
    ];
}

function definePermission(): array
{
    return [
        User::HOST_ROLE => [

        ],
        User::TEACHER_ROLE => [
            'create studylog',
            'list student',
            'show student',
            'list studylog',
            'create studylog',
            'edit studylog',
            'list studylog',
            'list classroom',
            'show classroom',
        ],
        User::SUPPORTER_ROLE => [
            'create studylog',
            'list student',
            'show student',
            'list studylog',
            'create studylog',
            'edit studylog',
            'list studylog',
            'list classroom',
            'show classroom',
        ],
        User::STUDENT_ROLE => [
            'list transaction',
            'show card',
            'list card',
            'list studylog',
            'list classroom',
            'show classroom'
        ],
        User::STAFF_ROLE => [

        ]
    ];
}

if (!function_exists('check_permission')) {
    function check_permission(string $key): bool
    {
        /**
         * @var User $user
         */
        $user = User::query()->where('id', Auth::user()->{'id'})->first();

        if ($user->role == User::HOST_ROLE) {
            return true;
        }

        $role = $user->role;

        $myScope = defineScope()[$role];

        $module = explode(' ', $key)[1];

        if (in_array($module, $myScope)) {
            return true;
        }

        if (in_array($key, definePermission()[$role])) {
            return true;
        }

        $permission = Permission::query()->where('key', $key)->first();

        if (!$permission) {
            return false;
        }

        if (in_array($permission['module'], defineScope()[$role])) {
            return true;
        }

        if (in_array($key, definePermission()[$role])) {
            return true;
        }

        if ($user->permissions()->where('key', $key)->first()) {
            return true;
        }

        if (force_permission($key)) {
            return true;
        }

        return false;
    }
}

if (!function_exists('force_permission')) {
    function force_permission($permissionName): bool
    {
        /**
         * @var User $user
         */
        $user = User::query()->where('id', Auth::user()->{'id'})->first();
        if ($user->role == User::HOST_ROLE) {
            return true;
        }

        $permission = Permission::query()->where('key', $permissionName)->first();

        if (!$permission) {
            return false;
        }

        $existRole = $user->Roles()->whereHas('Permissions', function (Builder $permission) use ($permissionName) {
            $permission->where('key', $permissionName);
        })->exists();

        if ($existRole) {
            return true;
        }

        return false;
    }
}

if (!function_exists('uploads')) {
    function uploads(UploadedFile $file = null, $folder = '', $customName = null): ?string
    {
        if (!$file) {
            return null;
        }
        $fileName = Str::random(10) . "_" . $file->getClientOriginalName();

        if ($customName !== null) {
            $fileName = $customName;
        }

        return "uploads/" . $file->storeAs("{$folder}", $fileName, 'upload');
    }
}

if (!function_exists('is_module')) {
    function is_module(string $module): string
    {
        $currentUrl = Route::current()->uri();

        return str_contains($currentUrl, $module);
    }
}

if (!function_exists('excel_date')) {
    function excel_date($excelDate): Carbon
    {
        try {
            $timestamp = ($excelDate - 25569) * 86400;

            return Carbon::createFromTimestamp($timestamp);
        } catch (Exception $exception) {
            try {
                return Carbon::createFromFormat("d/m/Y", $excelDate);
            } catch (Exception $exception) {
                return Carbon::now();
            }
        }
    }
}

if (!function_exists('array_pull')) {
    function array_pull(array &$array, string $key)
    {
        $value = $array[$key] ?? null;

        unset($array[$key]);

        return $value;
    }
}

if (!function_exists('rawSql')) {
    function rawSql(string $sql, array $bindings): string
    {
        $count = substr_count($sql, '?');

        for ($i = 0; $i < $count; $i++) {
            $binding = $bindings[$i];
            $binding = is_string($binding) ? "'{$binding}'" : $binding;

            $sql = preg_replace('/\?/', $binding, $sql, 1);
        }
        return $sql;
    }
}

if (!function_exists('getDaysOfWeekInMonth')) {
    function getDaysOfWeekInMonth($year, $month, $dayOfWeek): array
    {
        $firstDayOfMonth = Carbon::createFromDate($year, $month, 1);
        $lastDayOfMonth = $firstDayOfMonth->copy()->endOfMonth();

        $days = [];

        for ($currentDay = $firstDayOfMonth; $currentDay->lte($lastDayOfMonth); $currentDay->addDay()) {
            if ($currentDay->dayOfWeek == $dayOfWeek) {
                $days[] = $currentDay->copy();
            }
        }

        return $days;
    }
}

if (!function_exists('n')) {
    function n($value)
    {
        return str_replace(',', '', $value);
    }
}


if (!function_exists('silence')) {
    function silence($callback, $default = null)
    {
        try {
            return $callback();
        } catch (Exception $e) {
            return $default;
        }
    }
}

if (!function_exists('getSalaryGroupTransaction')) {
    function getSalaryGroupTransaction(int $salaryGroupId): array
    {
        $results = DB::table('transactions')
            ->select('transactions.uuid', 'transactions.id', 'transactions.amount', 'transactions.notes', 'transactions.transaction_day', 'v2_salaries.salary_group_id')
            ->leftJoin('v2_salaries', 'v2_salaries.id', '=', 'transactions.object_id')
            ->where('transactions.object_type', 'salary_paid')
            ->where('v2_salaries.salary_group_id', $salaryGroupId)
            ->orderBy('transactions.transaction_day', 'desc')
            ->get();

        return $results->toArray();
    }
}

if (!function_exists('getFundTransaction')) {
    function getFundTransaction(int $fundId): array
    {
        $results = DB::table('transactions')->where(
            [
                'object_id' => $fundId,
                'object_type' => Fund::OBJECT_TYPE
            ]
        )->orderBy('transaction_day', 'desc')->get();

        return $results->toArray();
    }
}

if (!function_exists('avatar')) {
    function avatar(string $url): string
    {
        return asset('avatars/' . $url);
    }
}

if (!function_exists('has_import')) {
    function has_import($entity): bool
    {
        return in_array($entity, [
//            'student',
//            'staff',
//            'teacher',
//            'supporter',
//            'classroom'
        ]);
    }
}
if (!function_exists('convertExcelTimeToPHP')) {
    function convertExcelTimeToPHP($excel_time): string
    {
        $seconds_in_a_day = 24 * 60 * 60;

        // Chuyển đổi giá trị thời gian của Excel thành giây và làm tròn để tăng độ chính xác
        $seconds = round($excel_time * $seconds_in_a_day);

        // Chuyển đổi giây thành định dạng thời gian
        $time = gmdate("H:i", $seconds);

        return $time;
    }
}

if (!function_exists('array_not_null')) {
    function array_not_null(array $array): array
    {
        foreach ($array as $key => $value) {
            if (is_null($value)) {
                unset($array[$key]);
            }
        }

        return $array;
    }
}
