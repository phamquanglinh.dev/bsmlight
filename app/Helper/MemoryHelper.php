<?php

namespace App\Helper;

class MemoryHelper
{
    public static int $is_frontend = 0;

    public static array $params;

    public static function addParam(string $key, mixed $value): void
    {
        static::$params[$key] = $value;
    }

    public function getParam($key): mixed
    {
        return static::$params[$key] ?? false;
    }
}
