<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 06/04/2024 4:14 pm
 */

namespace App\Helper;

use App\Helper\Object\ShiftEvent;
use App\Models\Classroom;
use App\Models\ClassroomShift;
use Carbon\Carbon;

class CalendarViewModel
{
    /**
     * @return ShiftEvent[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  06/04/2024 16:35:39
     */
    public function getShiftEvents(): array
    {
        $shiftEvents = [];

        /**
         * @var Classroom $classroom
         */
        $classrooms = Classroom::query()->get();

        foreach ($classrooms as $classroom) {
            /**
             * @var ClassroomShift $shift
             */
            foreach ($classroom->Shifts()->get() as $shift) {
                $shiftEvents[] = new ShiftEvent(
                    classroomName : $classroom['name'],
                    start : $shift['start_time'],
                    end : $shift['end_time'],
                    teacher : $shift->teacher?->uuid . '-' . $shift->teacher?->name,
                    supporter : $shift->supporter?->uuid . '-' . $shift->supporter?->name,
                    room : $shift['room'],
                    weekDay : $shift->schedule?->week_day
                );
            }
        }

        return $shiftEvents;
    }

    /**
     * @return array
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  06/04/2024 16:36:03
     */
    public function getMonthEvent(): array
    {
        $month = [];

        $days = $this->getDaysInMonth(Carbon::now()->year, Carbon::now()->month);

        foreach ($days as $day) {
            $month[$day->day] = [];

            foreach ($this->getShiftEvents() as $event) {
                $weekDay = $day->weekday();

                if ($weekDay == 0) {
                    $weekDay = 8;
                } else {
                    $weekDay += 1;
                }

                $month[$day->day]['week_day'] = $weekDay;

                if ($event->getWeekDay() == $weekDay) {
                    $month[$day->day]['events'][] = $event;
                }
            }
        }

        return $month;
    }

    /**
     * @param $year
     * @param $month
     *
     * @return Carbon[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  06/04/2024 16:29:34
     */
    private function getDaysInMonth($year, $month): array
    {
        $daysInMonth = [];

        // Tạo một đối tượng Carbon từ năm và tháng được cung cấp
        $date = Carbon::create($year, $month, 1);

        // Lặp qua tất cả các ngày trong tháng
        while ($date->month == $month) {
            $daysInMonth[] = $date->copy();
            $date->addDay();
        }

        return $daysInMonth;
    }

}
