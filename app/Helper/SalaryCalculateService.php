<?php

namespace App\Helper;

use App\Models\CardLog;
use App\Models\SalaryCriteria;
use App\Models\SalarySheetDetail;
use App\Models\StudyLog;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use PhpParser\Modifiers;

class SalaryCalculateService
{
    private Carbon $firstDayOfMonth;
    private Carbon $lastDayOfMonth;

    public function __construct()
    {
        $this->firstDayOfMonth = Carbon::now()->startOfMonth();
        $this->lastDayOfMonth = Carbon::now()->endOfMonth();
    }

    public function calculateCriteriaToAmount(SalarySheetDetail $salarySheetDetail, int $userId): ?float
    {
        switch ($salarySheetDetail->criteria_code) {
            case SalaryCriteria::CRITERIA_REVENUE_PERCENT:
                return 0;
            case SalaryCriteria::CRITERIA_LATE_STUDY_LOG_NUM:
                return $this->calculate_CRITERIA_LATE_STUDY_LOG_NUM($salarySheetDetail, $userId);
            case SalaryCriteria::CRITERIA_STUDENT_ATTEND_NUM:
                return $this->calculate_CRITERIA_STUDENT_ATTEND_NUM($salarySheetDetail, $userId);
            default:
                return null;
        }
    }

    private function calculate_CRITERIA_LATE_STUDY_LOG_NUM(SalarySheetDetail $salarySheetDetail, int $userId): int
    {
        $currentMonth = now()->format('Y-m');

        $builder = StudyLog::query();

        if ($salarySheetDetail->classroom_id != null) {
            $builder->where('classroom_id', $salarySheetDetail->classroom_id);
        }

        $studyLog = $builder->whereRaw("DATE(studylog_day) <= DATE_ADD(created_at, INTERVAL 48 HOUR)")
            ->where(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'), $currentMonth)->where('created_by', $userId)
            ->whereRaw("DATE(studylog_day) >= ?", [$this->firstDayOfMonth->toDateString()])
            ->whereRaw("DATE(studylog_day) <= ?", [$this->lastDayOfMonth->toDateString()]);

        return $studyLog->count();
    }

    private function calculate_CRITERIA_STUDENT_ATTEND_NUM(SalarySheetDetail $salarySheetDetail, int $userId)
    {
        $currentMonth = now()->format('Y-m');

        $builder = CardLog::query();

        if ($salarySheetDetail->classroom_id != null) {
            $builder->whereHas('StudyLog', function (Builder $studyLog) use ($salarySheetDetail) {
                $studyLog->where('classroom_id', $salarySheetDetail->classroom_id);
            });
        }

        $cardLog = $builder->whereHas('StudyLog', function (Builder $studyLog) use ($salarySheetDetail, $userId) {
            $studyLog->whereHas('WorkingShifts', function (Builder $workingShift) use ($userId) {
                $workingShift->where('staff_id', $userId)->orWhere('teacher_id', $userId)
                    ->orWhere('supporter_id', $userId);
            })->whereRaw("DATE(studylog_day) >= ?", [$this->firstDayOfMonth->toDateString()])
                ->whereRaw("DATE(studylog_day) <= ?", [$this->lastDayOfMonth->toDateString()])
                ->where('classroom_id', $salarySheetDetail->classroom_id);
        })
            ->where('day', 1)->where(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'), $currentMonth);

        return $cardLog->count();
    }
}
