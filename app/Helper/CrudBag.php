<?php

namespace App\Helper;

use App\Models\Card;
use App\Models\CardLog;
use App\Models\SalaryCriteria;
use App\Models\StudyLog;
use App\Models\User;
use App\Models\WorkingShift;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class CrudBag
{
    const BEFORE_CONTENT = "before_content";
    const DISABLE_ALL_TOP_BTN = 'disable_all_top_btn';
    const SHOW_FILTER = 'show_filter';
    const BULK_CARD = 'bulk_card';
    const DISABLE_ACTION = 'disable_action';
    const CUSTOM_LABEL_ACTION = 'label_action';
    const CUSTOM_LABEL_ACTION_BTN = 'label_action_btn';

    const HEAD_HINT = 'head_hint';
    const ORDER_MODE = 'order_mode';
    const PLACEHOLDER_SEARCH = 'placeholder_search';
    const BEFORE_TEXT = 'before_text';
    const CARD_TRANSACTION_PRINT = 'card_transaction_print';
    /**
     * @var array
     */
    public array $fields = [];
    private string $label = '';
    private string $entity = '';
    private array $columns = [];
    private array $filters = [];
    private array $statistics = [];
    private ?string $searchValue = null;

    private bool $hasFile = false;

    public function isHasFile(): bool
    {
        return $this->hasFile;
    }

    public function setHasFile(bool $hasFile): void
    {
        $this->hasFile = $hasFile;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public function getSearchValue(): ?string
    {
        return $this->searchValue;
    }

    public function setSearchValue(string $searchValue = null): void
    {
        $this->searchValue = $searchValue;
    }

    /**
     * @return Statistic[]
     */
    public function getStatistics(): array
    {
        return $this->statistics;
    }

    /**
     * @return Filter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    public function addFilter(array $data = []): void
    {
        $this->filters[] = new Filter($data);
    }

    public function addStatistic(array $data = []): void
    {
        $this->statistics[] = new Statistic($data);
    }

    private ?int $id = null;

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getEntity(): string
    {
        return $this->entity;
    }

    public function setEntity(string $entity): void
    {
        $this->entity = $entity;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    private string $action;

    /**
     * @param $data
     * @return void
     */
    public function addFields($data): void
    {
        $this->fields[] = new Fields($data);
    }

    /**
     * @return Fields[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function addColumn(array $data = []): void
    {
        $this->columns[] = new Column($data);
    }

    /**
     * @return Column[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    private array $params = [];

    public function setParam(string $key, $value): void
    {
        $this->params[$key] = $value;
    }

    public function getParam(string $key)
    {
        return $this->params[$key] ?? null;
    }

    public function handleColumns(Request $request, CrudBag $crudBag): CrudBag
    {
        $crudBag->addColumn([
            'label' => 'Buổi điểm danh',
            'type' => 'studylog-head',
            'name' => 'supportId',
            'attributes' => [
                'show' => true,
                'entity' => 'studylog',
                'options' => StudyLog::statusListOptions(),
                'bg' => StudyLog::statusBackgroundOptions()
            ],
            'fixed' => 'first'
        ]);
        $crudBag->addColumn([
            'name' => 'created_at_string',
            'label' => 'Thời gian tạo',
            'type' => 'text',
        ]);
        $crudBag->addColumn([
            'name' => 'creator',
            'label' => 'Người tạo',
            'type' => 'entity',
            'attributes' => [
                'avatar' => 'avatar',
                'name' => 'name',
                'uuid' => 'uuid',
                'id' => 'id',
                'entity' => 'creator',
                'model' => 'user'
            ]
        ]);
        $crudBag->addColumn([
            'name' => 'classroomEntity',
            'type' => 'entity',
            'label' => 'Lớp học',
            'entity' => 'classroomEntity',
            'attributes' => [
                'avatar' => 'avatar',
                'name' => 'name',
                'uuid' => 'uuid',
                'id' => 'id',
                'entity' => 'classroomEntity',
                'model' => 'classroom'
            ]
        ]);
        $crudBag->addColumn([
            'name' => 'studylog_day_string',
            'label' => 'Ngày tháng năm buổi học'
        ]);
        $crudBag->addColumn([
            'name' => 'title',
            'label' => 'Tiêu đề buổi học'
        ]);
        $crudBag->addColumn([
            'name' => 'statistics',
            'type' => 'statistics',
            'label' => 'Thống kê buổi học',
            'attributes' => [
                'statistics_fields' => [
                    [
                        'name' => 'attendances',
                        'label' => 'Đi học',
                        'color' => 'text-success'
                    ],
                    [
                        'name' => 'left',
                        'label' => 'Vắng',
                        'color' => 'text-danger'
                    ],
                    [
                        'name' => 'calculated',
                        'label' => 'Trừ buổi',
                        'color' => 'text-success'
                    ],
                    [
                        'name' => 'not_calculated',
                        'label' => 'Không trừ buổi',
                        'color' => 'text-danger'
                    ]
                ]
            ]
        ]);
        $crudBag->addColumn([
            'name' => 'teachers',
            'type' => 'array',
            'label' => 'Giáo viên',
        ]);
        $crudBag->addColumn([
            'name' => 'supporters',
            'type' => 'array',
            'label' => 'Trợ giảng'
        ]);

        return $crudBag;
    }

    public function handleQuery(Request $request, Builder $query): Builder
    {
        if (Auth::user()->{'role'} == User::HOST_ROLE) {
            $query->whereHas('classroom', function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'});
            })
                ->where('status', '!=', StudyLog::CANCELED);

            return $query->orderBy('created_at', 'DESC');
        }

        $query->where(function (Builder $query) {
            $query->where('status', '!=', StudyLog::CANCELED)
                ->where(function (Builder $builder) {
                    $builder->where('created_by', Auth::user()->id)
                        ->orWhere(function (Builder $query) {
                            $query->whereHas('CardLogs', function (Builder $query) {
                                $query->where('student_id', Auth::user()->id);
                            })->orWhereHas('WorkingShifts', function (Builder $query) {
                                $query->where('teacher_id', Auth::user()->id)->orWhere('supporter_id', Auth::user()->id)
                                    ->orWhere('staff_id', Auth::user()->id);
                            });
                        })->orWhere(function (Builder $builder) {
                            $builder->whereHas('Classroom', function (Builder $classroom) {
                                $classroom->where('staff_id', Auth::id());
                            });
                        });
                });
        });

        return $query->orderBy('created_at', 'DESC');
    }

    public function hardDelete(int $id): Redirector|Application|RedirectResponse
    {
        WorkingShift::query()->where('studylog_id', $id)->delete();
        CardLog::query()->where('studylog_id', $id)->delete();
        StudyLog::query()->where('id', $id)->delete();

        return redirect('/studylog/list')->with('success', 'Xoá thành công');
    }
}
