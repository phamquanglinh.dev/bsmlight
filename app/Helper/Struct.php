<?php

namespace App\Helper;

use App\Models\CustomFieldStruct;

class Struct
{
    public static array $accountCustomFields = [
        [
            'field_name' => 'a.born_year',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Năm sinh',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.birthday',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Sinh nhật',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.school',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Trường học hiện tại',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.address',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Địa chỉ',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.parent_name',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Tên bố hoặc mẹ',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.paren_phone',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Số điện thoại phụ huynh',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.link_facebook',
            'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Link facebook của phụ huynh',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
        [
            'field_name' => 'a.last_feedback',
            'field_html_type' => CustomFieldStruct::HTML_DATETIME_TYPE,
            'is_required' => 0,
            'is_duplicate' => 0,
            'is_multiple' => 0,
            'field_label' => 'Feedback gần nhất',
            'module' => CustomFieldStruct::MODULE_ACCOUNT,
            'default' => 1,
        ],
    ];
}
