<?php

namespace App\Helper;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\MessageBag;

class HttpResponse
{
    public function responseMessage(string $message): JsonResponse
    {
        return response()->json([
            'message' => $message
        ]);
    }

    public function responseCollection(array $collection = []): JsonResponse
    {
        return response()->json([
            'data' => $collection,
            'total' => count($collection)
        ]);
    }

    public function responseCreated(int $identity): JsonResponse
    {
        return response()->json([
            'message' => 'Thêm mới thành công',
            'id' => $identity
        ]);
    }

    public function validateResponse(MessageBag $errors)
    {
        return response()->json([
            "message" => "Không thành công. Vui lòng kiểm tra lại",
            "errors" => $errors->toArray()
        ], 422);
    }

    public function notFoundResponse(): JsonResponse
    {
        return response()->json([
            "message" => "Không tìm thấy",
        ], 404);
    }

    public function responseData(array $data): JsonResponse
    {
        return response()->json([
            "data" => $data
        ]);
    }

    public function badResponse(string $string): JsonResponse
    {
        return response()->json([
            'message' => $string
        ],400);
    }
}
