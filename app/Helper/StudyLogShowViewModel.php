<?php

namespace App\Helper;

use App\Helper\Object\CardLogObject;
use App\Helper\Object\CommentObject;
use App\Helper\Object\StudyLogAcceptedObject;
use App\Helper\Object\StudyLogObject;
use App\Helper\Object\WorkingShiftObject;
use App\Models\CardLog;
use App\Models\Comment;
use App\Models\StudyLog;
use App\Models\User;
use App\Models\WorkingShift;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Builder;

class StudyLogShowViewModel
{
    /**
     * @param StudyLog $studyLog
     * @param Collection|array $cardLogs
     * @param Collection|array $workingShifts
     * @param Collection|array $comments
     * @param StudyLogAcceptedObject[] $studyLogAcceptedUsers
     */
    public function __construct(
        private readonly StudyLog         $studyLog,
        private readonly Collection|array $cardLogs,
        private readonly Collection|array $workingShifts,
        private readonly Collection|array $comments,
        private readonly array            $studyLogAcceptedUsers,
        private readonly ?string          $submit_time,
        private readonly ?string          $cancel_time,
        private readonly ?string          $accept_time,
        private readonly ?string          $created_time
    )
    {
    }

    public function getCreatedTime(): ?string
    {
        return $this->created_time;
    }

    public function getSubmitTime(): ?string
    {
        return $this->submit_time;
    }

    public function getCancelTime(): ?string
    {
        return $this->cancel_time;
    }

    public function getAcceptTime(): ?string
    {
        return $this->accept_time;
    }

    /**
     * @return CommentObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 02/01/2024 8:45 am
     */
    public function getComments(): array
    {
        return $this->comments->map(fn(Comment $comment) => new CommentObject(
            user_id: $comment['user_id'],
            user_name: $comment->user?->name,
            user_avatar: $comment->user?->avatar,
            comment_time: Carbon::parse($comment['created_at'])->toDateTimeLocalString('minutes'),
            type: $comment['type'],
            content: $comment['content'],
            self: $comment['user_id'] == Auth::id()
        ))->toArray();
    }

    /**
     * @return StudyLogAcceptedObject[]
     */
    public function getStudyLogAcceptedUsers(): array
    {
        return $this->studyLogAcceptedUsers;
    }

    /**
     * @return WorkingShiftObject[]
     */

    public function getWorkingShifts(): array
    {
        return $this->workingShifts->map(fn(WorkingShift $workingShift) => new WorkingShiftObject(
            id: $workingShift->id,
            start_time: $workingShift->start_time ?? '',
            end_time: $workingShift->end_time ?? '',
            room: $workingShift->room ?? '',
            teacher_id: $workingShift->teacher_id ?? '',
            supporter_id: $workingShift->supporter_id ?? '',
            staff_id: $workingShift->staff_id ?? '',
            teacher_name: $workingShift->teacher?->name ?? '',
            supporter_name: $workingShift->supporter?->name ?? '',
            staff_name: $workingShift->staff?->name ?? '',
            teacher_avatar: $workingShift->teacher?->avatar ?? '',
            supporter_avatar: $workingShift->supporter?->avatar ?? '',
            staff_avatar: $workingShift->staff?->avatar ?? '',
            teacher_comment: $workingShift->teacher_comment ?? '',
            supporter_comment: $workingShift->supporter_comment ?? '',
            teacher_timestamp: $workingShift->teacher_timestamp ?? '',
            supporter_timestamp: $workingShift->supporter_timestamp ?? '',
        ))->toArray();
    }

    /**
     * @return CardLogObject[]
     */
    public function getCardLogs(): array
    {
        return $this->cardLogs->map(fn(CardLog $cardLog) => new CardLogObject(
            id: $cardLog->card->id ?? '',
            uuid: $cardLog->card?->uuid ?? '',
            studentName: $cardLog->student->name ?? '',
            studentUuid: $cardLog->student->uuid ?? '',
            studentAvatar: $cardLog->student->avatar ?? '',
            status_text: $cardLog->StatusList()[$cardLog->status] ?? '',
            day: $cardLog->day ?? '',
            teacher_comment: $cardLog->teacher_note ?? '',
            supporter_comment: $cardLog->supporter_note ?? '',
            attended_days: $cardLog->Card()?->first()?->getVanAndAttendedDaysAttribute() ?? 0,
            can_use_day: $cardLog->Card()?->first()?->getCanUseDayAttribute() ?? 0,
            can_deg: $cardLog['can_deg'] ?? $cardLog->Card()?->first()?->getCanDegAttribute(),
        ))->toArray();
    }

    /**
     * @return StudyLogObject
     */

    public function getStudyLog(): StudyLogObject
    {
        $studyLog = $this->studyLog;

        return new StudyLogObject(
            id: $studyLog->id,
            title: $studyLog->title ?? '',
            status_text: $studyLog->statusList()[$studyLog->status] ?? '',
            status: $studyLog->status,
            status_background: $studyLog->statusBackground()[$studyLog->status] ?? '',
            classroomName: $studyLog->classroom->name ?? '',
            classroomUuid: $studyLog->classroom->uuid ?? '',
            classroomAvatar: $studyLog->classroom->avatar ?? '',
            week_day: $studyLog['week_day'] ?? '',
            schedule_text: $studyLog->schedule_text ?? 'Buổi học tuỳ chọn',
            link: $studyLog->link ?? '',
            photo: $studyLog->photo ?? '',
            video: $studyLog->video ?? '',
            studylog_image: $studyLog->studylog_image ?? '',
            notes: $studyLog->content ?? ''
        );
    }

    public function getUserAbilityAction(): array
    {
        $studyLog = $this->studyLog;

        $rule = [
            'commit' => false,
            'self_confirm' => false,
            'alt_confirm' => false,
            'cancel' => false,
            'confirm' => false,
            'refuse' => false,
            'edit' => false,
            'delete' => false,
            'restore' => false,
            'submit' => false,
            'rollback' => false
        ];

        switch ($this->studyLog['status']) {
            case StudyLog::DRAFT_STATUS:
                if (Auth::id() == $studyLog['created_by']) {
                    $rule['commit'] = true;
                }

                if (in_array(Auth::user()->{'role'}, [
                    User::STAFF_ROLE,
                    User::SUPPORTER_ROLE,
                    User::HOST_ROLE,
                    User::TEACHER_ROLE
                ])) {
                    $rule['commit'] = true;
                }

                if (Auth::id() == $studyLog['created_by']) {
                    $rule['edit'] = true;
                }
                break;
            case StudyLog::WAITING_CONFIRM:
                if (Auth::id() == $studyLog['created_by']) {
                    $rule['edit'] = true;
                }
                $workingShifts = $studyLog->WorkingShifts()->get();
                foreach ($workingShifts as $workingShift) {
                    if (Auth::id() == $workingShift['teacher_id'] || Auth::id() == $workingShift['supporter_id']) {
                        $hasConfirm = DB::table('studylogs_accepts')->where('user_id', Auth::id())->where('studylog_id', $studyLog['id'])->first();
                        if (!$hasConfirm) {
                            $rule['self_confirm'] = true;
                            $rule['edit'] = true;
                        }
                    }
                }

                if (Auth::user()->{'role'} == User::STAFF_ROLE || Auth::user()->{'role'} == User::HOST_ROLE) {
                    $rule['alt_confirm'] = true;

                    $rule['edit'] = true;
                }

                if (Auth::id() == $studyLog['created_by']) {
                    $rule['cancel'] = true;
                }
                break;
            case StudyLog::WAITING_ACCEPT:
                if (Auth::user()->{'role'} == User::HOST_ROLE) {
                    $rule['confirm'] = true;
                    $rule['refuse'] = true;
                } else {
                    if (check_permission('accept studylog')) {
                        $rule['confirm'] = true;
                        $rule['refuse'] = true;
                    }
                }
                break;
        }

        if (Auth::user()->{'role'} == User::HOST_ROLE) {
            $rule['edit'] = true;
            $rule['delete'] = true;
        }

        if ($studyLog->status == StudyLog::ACCEPTED) {
            if (Auth::user()->{'role'} == User::HOST_ROLE) {
                $rule['rollback'] = true;
            }
        }

        if (in_array(Auth::user()->{'role'}, [
            User::STAFF_ROLE,
            User::HOST_ROLE,
            User::TEACHER_ROLE,
            User::SUPPORTER_ROLE
        ])) {
            if (in_array($studyLog->status, [
                StudyLog::DRAFT_STATUS,
                StudyLog::WAITING_ACCEPT,
                StudyLog::WAITING_CONFIRM,
                StudyLog::CANCELED
            ])) {
                $rule['delete'] = true;
            }
        }

        return $rule;
    }
}
