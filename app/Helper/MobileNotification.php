<?php

namespace App\Helper;

use App\Helper\Object\NotificationObject;
use App\Models\UserFcm;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class MobileNotification
{
    public static function sendMobilePushNotification(
        NotificationObject $notificationObject,
        array              $userId
    ): void
    {
        $client = new Client();
        $url = "https://exp.host/--/api/v2/push/send";

        foreach ($userId as $id) {
            $tokens = UserFcm::query()->where('user_id', $id)->whereIn('type', [UserFcm::IOS, UserFcm::ANDROID])->get();

            foreach ($tokens as $token) {
                $message = [
                    'to' => $token['token'],
                    'sound' => 'default',
                    'title' => $notificationObject->getTitle(),
                    'body' => $notificationObject->getBody(),
                    'data' => $notificationObject->getAttributes() ?? ["name" => "LOL"]
                ];
                Log::debug(json_encode($message));
                $headers = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ];

                try {
                    $response = $client->request('POST', $url, [
                        'headers' => $headers,
                        'json' => $message
                    ]);

                } catch (GuzzleException $e) {
                    Log::error($e->getMessage());
                }
            }
        }
    }
}
