<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $salary_sheet_id
 * @property string $criteria_name
 * @property string $criteria_code
 * @property int $criteria_type
 * @property int $unit_type
 * @property string $criteria_description
 * @property int $classroom_id
 */
class SalarySheetDetail extends Model
{
    use HasFactory;

    /**
     * salary_sheet_id
     * criteria_name
     * criteria_code
     * criteria_type
     * criteria_rate
     * unit_type
     * criteria_description
     *
     */

    protected $table = 'salary_sheet_detail';

    protected $fillable = [
        'id',
        'salary_sheet_id',
        'criteria_name',
        'criteria_code',
        'criteria_type',
        'criteria_rate',
        'is_bsm_criteria',
        'unit_type',
        'criteria_description',
        'branch',
        'classroom_id'
    ];


    public function getSalarySheet(): BelongsTo
    {
        return $this->belongsTo(SalarySheet::class, 'salary_sheet_id', 'id');
    }
}
