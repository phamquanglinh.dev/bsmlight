<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 3:43 pm 29/02/2024
 * @property string $criteria_name
 * @property string $criteria_code
 * @property int $criteria_type
 * @property int $is_bsm_criteria
 * @property int $unit_type
 * @property float $criteria_rate
 */
class SalaryCriteria extends Model
{
    const CRITERIA_STUDENT_ATTEND_NUM = 'student_attend_num';
    const CRITERIA_LATE_STUDY_LOG_NUM = 'late_study_log_num';
    const CRITERIA_REVENUE_PERCENT = 'revenue_percent';
    const CRITERIA_MANUAL = 'manual';

    /**
     * Thưởng
     */
    public const TYPE_REWARD = 1;

    /**
     * Phạt
     */
    public const TYPE_PUNISH = 2;
    const UNIT_INTEGER = 1;
    const UNIT_PERCENT = 2;

    use HasFactory;
    use SoftDeletes;

    protected $table = 'salary_criteria';

    protected $guarded = ['id'];

    public static function getCriteriaTypeOptions(): array
    {
        return [
            self::TYPE_REWARD => 'Thưởng',
            self::TYPE_PUNISH => 'Phạt',
        ];
    }

    public static function getUnitOptions(): array
    {
        return [
            self::UNIT_INTEGER => 'Tính theo số lượng (X * số lượng)',
            self::UNIT_PERCENT => 'Tính theo phần trăm (X * số lượng / 100)',
        ];
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('branch', function (Builder $query) {
            $query->where('branch', Auth::user()->{'branch'});
        });
    }
}
