<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class WorkingShift extends Model
{
    use HasFactory;

    public const TEMPLATE = [
        'duration' => 0,
        'teacher_id' => '',
        'supporter_id' => '',
        'start_time' => '',
        'end_time' => '',
        'room' => '',
        'teacher_timestamp' => '',
        'supporter_timestamp' => '',
        'teacher_comment' => '',
        'supporter_comment' => '',
    ];
    const UNVERIFIED = 0;
    const VERIFIED = 1;

    public function Teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function Supporter(): BelongsTo
    {
        return $this->belongsTo(Supporter::class, 'supporter_id', 'id');
    }

    public function Staff(): BelongsTo
    {
        return $this->belongsTo(Staff::class, 'staff_id', 'id');
    }


    protected $table = 'working_shifts';
    protected $guarded = ['id'];

    public function getDurationHourAttribute(): float
    {
        $start = Carbon::parse($this->start_time);
        $end = Carbon::parse($this->end_time);

        return $end->diffInMinutes($start) / 60;
    }

    public function StudyLog(): BelongsTo
    {
        return $this->belongsTo(StudyLog::class, 'studylog_id', 'id');
    }
}
