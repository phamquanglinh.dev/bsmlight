<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Transaction extends Model
{
    use HasFactory;

    const RENEW_TYPE = 'renew';
    const NEW_TYPE = 'new';
    const SALARY_PAID_OBJECT_TYPE = 'salary_paid';
    const RECEIVE_TYPE = 'receive';
    const TRANSFER_TYPE = 'transfer';

    protected $table = 'transactions';

    protected $guarded = ['id'];

    protected $appends = [
        'object_type_label',
        'relation_object'
    ];

    public const CARD_TRANSACTION_TYPE = 'card';
    public const ACTIVE_STATUS = 1;
    public const PENDING_STATUS = 0;
    public const CANCEL_STATUS = 2;

    public function affiliates(): HasMany
    {
        return $this->hasMany(Transaction::class, 'transaction_id', 'id');
    }

    public function Creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function approve(): BelongsTo
    {
        return $this->belongsTo(User::class, 'approve', 'id');
    }

    public static function defineDefaultTransactionObjectTypeLabel()
    {
        return [
            static::CARD_TRANSACTION_TYPE => 'Thẻ học'
        ];
    }

    public function getObjectTypeLabelAttribute(): string
    {
        return Transaction::defineDefaultTransactionObjectTypeLabel()[$this->getAttribute('object_type')] ?? '-';
    }

    public function getRelationObjectAttribute(): ?array
    {
        switch ($this->getAttribute('object_type')) {
            case static::CARD_TRANSACTION_TYPE:
                $card = Card::query()->where('id', $this->getAttribute('object_id'))->first();
                if (!$card) {
                    return null;
                }

                return [
                    'uuid' => $card['uuid'],
                    'id' => $card['id'],
                    'url' => '/card/show/' . $card['id']
                ];

            default:
                return null;
        }
    }

    public function approver(): BelongsTo
    {
        return $this->belongsTo(User::class, 'approve', 'id');
    }

    public function setApproveAttribute($value)
    {
        if (!$value) {
            $this->attributes['approve'] = 0;
        } else {
            $this->attributes['approve'] = $value;
        }
    }

    public function setTransactionDayAttribute($value): void
    {
        if (!$value) {
            $this->attributes['transaction_day'] = $this->{'created_at'};
        } else {
            $this->attributes['transaction_day'] = $value;
        }
    }


}
