<?php

namespace App\Models;

use App\Helper\UUID;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property string $name
 * @property string $email
 * @property int $verified_code
 * @property int $verified
 * @property string $password
 * @property mixed $uuid
 * @property int $role
 * @property mixed $branch
 * @property int $id
 * @property string $avatar
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    public const  STUDENT_ROLE = 2;
    public const  HOST_ROLE = 1;
    public const TEACHER_ROLE = 3;

    public const SUPPORTER_ROLE = 4;

    public const STAFF_ROLE = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function newUuid(string $branch, string $code): string
    {
        $model = new User();

        switch ($code) {
            case 'NV':
                $model = new Staff();
                break;
            case 'GVNN':
            case 'GGVN':
                $model = new Teacher();
                break;
            case 'HS':
                $model = new Student();
                break;
            case 'TG':
                $model = new Supporter();
                break;
            default:
                break;
        }

        return UUID::make($branch, $model, $code);
    }

    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new ResetPassword($token));
    }

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, "user_permission", "user_id", "permission_id");
    }

    public function StudyLogAccept(): HasMany
    {
        return $this->hasMany(StudyLogAccept::class, 'studylog_id', 'id');
    }

    public function getRoleLabel(): string
    {
        switch ($this->role) {
            case User::STUDENT_ROLE:
                return 'Học sinh';
            case User::HOST_ROLE:
                return 'HOST';
            case User::STAFF_ROLE:
                return 'Nhân viên';
            case User::TEACHER_ROLE:
                return 'Giáo vien';
            case User::SUPPORTER_ROLE:
                return 'Trợ giảng';
            default:
                return '';
        }
    }

    public function Roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function getAvatarAttribute(): string
    {
        $avatarPath = $this->attributes['avatar'] ?? null;
        if ($avatarPath) {
            return asset($avatarPath);
        }
        return asset('demo/assets/img/avatars/default-avatar.gif');

    }

    public static function avatarStudentSelect(): array
    {
        return [
            avatar('1.webp'),
            avatar('2.webp'),
            avatar('3.gif'),
            avatar('4.gif'),
            avatar('5.gif'),
            avatar('6.gif'),
        ];
    }

    public function Branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class,'branch', 'uuid');
    }
}
