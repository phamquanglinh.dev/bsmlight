<?php

namespace App\Models;

use App\Models\Traits\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Cashier extends Model
{
    use HasFactory;
    use Entity;


    protected $table = 'transactions';
    protected $guarded = ['id'];

    public static function defineRetrievableField(): array
    {
        return [
            'id' => true,
            'created_by' => true,
            'uuid' => true,
            'transaction_type' => true,
            'object_type' => true,
            'object_id' => true,
            'amount' => true,
            'object_image' => true,
            'notes' => true,
            'status' => true,
            'approve' => true,
            'transaction_day' => true,

            'creator_display_name' => true,
            'creator_avatar' => true,
            'approve_display_name' => true,
            'approve_avatar' => true,
            'object_uuid' => true,
            'object_type_label' => true,
            'created_at' => true,
            'affect_user' => true,
            'transaction_type_group' => true,
            'transaction_type_detail' => true,
            'balance' => true,
            'timestamp' => true
        ];
    }

    public static function calculateCashierAmount(): array
    {
        $balance = 0;

        $balanceLog = [];

        $cashiers = Cashier::query()->orderBy('transaction_day', 'ASC')->get(['id', 'amount', 'object_type', 'transaction_type']);

        foreach ($cashiers as $cashier) {
            if ($cashier->object_type == Transaction::CARD_TRANSACTION_TYPE) {
                $balance = $balance + $cashier->amount;
            }

            if ($cashier->object_type == Transaction::SALARY_PAID_OBJECT_TYPE) {
                $balance = $balance - $cashier->amount;
            }

            $transactionType = TransactionType::query()->where('id', $cashier->{'transaction_type'})->first();

            if ($transactionType) {
                switch ($transactionType->group) {
                    case TransactionType::GROUP_A:
                    case TransactionType::GROUP_B:
                        $balance = $balance + $cashier->amount;
                        break;
                    case TransactionType::GROUP_C:
                    case TransactionType::GROUP_D:
                        $balance = $balance - $cashier->amount;
                        break;
                }
            }

            $balanceLog[$cashier->id] = $balance;
        }

        return $balanceLog;
    }

    public static function primaryFields()
    {
        return [
            [
                'field_name' => 'id',
                'hide' => true
            ],
            [
                'field_name' => 'uuid',
                'field_label' => 'Mã giao dịch',
                'field_html_type' => 'uuid',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'object_uuid',
                'field_label' => 'Mã đối ứng',
                'field_html_type' => 'text',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'timestamp',
                'field_label' => 'timestamp',
                'field_html_type' => 'text',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'created_by',
                'field_label' => 'Người tạo',
                'field_html_type' => 'user',
                'options' => [
                    'name' => 'creator_display_name',
                    'avatar' => 'creator_avatar',
                ],
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'creator_display_name',
                'hide' => true,
            ],
            [
                'field_name' => 'creator_avatar',
                'hide' => true,
            ],
            [
                'field_name' => 'transaction_day',
                'field_label' => 'Ngày giao dịch',
                'field_html_type' => 'text',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'affect_user',
                'field_label' => 'Người nộp hoặc nhận',
                'field_html_type' => 'user',
                'options' => [
                    'name' => 'affect_user_name',
                    'avatar' => 'affect_user_avatar',
                ]
            ],
            [
                'field_name' => 'notes',
                'field_label' => 'Ghi chú',
                'field_html_type' => 'textarea',
                'is_required' => 0,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'transaction_type_group',
                'field_label' => 'Loại nhóm giao dịch',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'transaction_type_detail',
                'field_label' => 'Loại giao dịch cụ thể',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'transaction_type',
                'field_label' => 'Loại hình',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'amount',
                'field_label' => 'Số tiền (vnđ)',
                'field_html_type' => 'numeric',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'object_image',
                'field_label' => 'Biên lai',
                'field_html_type' => 'image',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'balance',
                'field_label' => 'Số dư sau mỗi giao dịch',
                'field_html_type' => 'numeric',
                'is_required' => 1,
                'default' => 0,
                'is_multiple' => 0
            ]
        ];

    }

    public static function defineExternalField(): array
    {
        return [
            'creator_display_name' => 'created_by',
            'creator_avatar' => 'created_by',
            'approve_display_name' => 'approve',
            'approve_avatar' => 'approve',
            'object_uuid' => 'object_id',
            'object_type_label' => 'object_type',
            'affect_user' => 'object_id',
            'transaction_type_group' => 'transaction_type',
            'transaction_type_detail' => 'transaction_type',
            'balance' => 'id',
            'timestamp' => 'created_at'
        ];
    }

    public function handleQueryProcess(Request $request)
    {
        $fields = $request->get('fields') ? explode(',', $request->get('fields')) : ['id'];

        $externalFields = array_intersect($fields, array_keys(static::defineExternalField()));

        $this->setAttribute('uuid', "GIAODICH." . $this->{'uuid'});

        if (in_array('creator_display_name', $externalFields) && $this->{'created_by'} != null) {

            $user = User::query()->where('id', $this->{'created_by'})->first();
            if (!$user) {
                $this->setAttribute('creator_display_name', 'Đã xoá');
            } else {
                $this->setAttribute('creator_display_name', $user->{'name'});
            }
        }

        if (in_array('creator_avatar', $externalFields) && $this->{'created_by'} != null) {

            $user = User::query()->where('id', $this->{'created_by'})->first();
            if (!$user) {
                $this->setAttribute('creator_avatar', '');
            } else {
                $this->setAttribute('creator_avatar', $user->{'avatar'});
            }
        }

        if (in_array('approve_display_name', $externalFields) && $this->{'approve'} != null) {

            $user = User::query()->where('id', $this->{'approve'})->first();
            if (!$user) {
                $this->setAttribute('approve_display_name', 'Đã xoá');
            } else {
                $this->setAttribute('approve_display_name', $user->{'name'});
            }

        }

        if (in_array('approve_avatar', $externalFields) && $this->{'approve'} != null) {

            $user = User::query()->where('id', $this->{'approve'})->first();
            if (!$user) {
                $this->setAttribute('approve_avatar', '');
            } else {
                $this->setAttribute('approve_avatar', $user->{'avatar'});
            }

        }

        if (in_array('object_uuid', $externalFields) && $this->{'object_type'} != null && $this->{'object_id'} != null) {
            if ($this->{'object_type'} == Transaction::CARD_TRANSACTION_TYPE) {
                $card = Card::query()->where('id', $this->{'object_id'})->first();
                if (!$card) {
                    $this->setAttribute('object_uuid', 'Đã xoá');
                } else {
                    $this->setAttribute('object_uuid', $card->{'uuid'});
                }
            }

            if ($this->{'object_type'} == Transaction::SALARY_PAID_OBJECT_TYPE) {
                $salary = Salary::query()->where('id', $this->{'object_id'})->first();
                if (!$salary) {
                    $this->setAttribute('object_uuid', 'Đã xoá');
                } else {
                    $this->setAttribute('object_uuid', $salary->{'uuid'});
                }
            }

        }

        if (in_array('object_type_label', $externalFields) && $this->{'object_type'} != null && $this->{'object_id'} != null) {
            if ($this->{'object_type'} == Transaction::CARD_TRANSACTION_TYPE) {
                $this->setAttribute('object_type_label', 'Thẻ học');
            }
            if ($this->{'object_type'} == Transaction::SALARY_PAID_OBJECT_TYPE) {
                $this->setAttribute('object_type_label', 'Chi lương');
            }
        }

        if (in_array('affect_user', $externalFields) && $this->{'object_type'} != null && $this->{'object_id'} != null) {
            if ($this->{'object_type'} == Transaction::CARD_TRANSACTION_TYPE) {
                $card = Card::query()->where('id', $this->{'object_id'})->first();
                if (!$card) {
                    $this->setAttribute('affect_user', 0);
                } else {
                    $student = Student::query()->where('id', $card->{'student_id'})->first();
                    if (!$student) {
                        $this->setAttribute('affect_user', 0);
                    } else {
                        $this->setAttribute('affect_user_name', $student->{'name'});
                        $this->setAttribute('affect_user_avatar', $student->{'avatar'});
                        $this->setAttribute('affect_user', $student->{'id'});
                    }
                }
            }
            if ($this->{'object_type'} == Transaction::SALARY_PAID_OBJECT_TYPE) {
                $salary = Salary::query()->where('id', $this->{'object_id'})->first();
                if ($salary) {
                    $user = User::query()->where('id', $salary->{'user_id'})->first();
                    if ($user) {
                        $this->setAttribute('affect_user_name', $user->{'name'});
                        $this->setAttribute('affect_user_avatar', $user->{'avatar'});
                        $this->setAttribute('affect_user', $user->{'id'});
                    }
                }
            }
        }

        if ($this->{'amount'} != null && $this->{'object_type'} != null) {
            if ($this->{"object_type"} == Transaction::CARD_TRANSACTION_TYPE) {
                $this->setAttribute('amount_color', "green");
            }

            if ($this->{"object_type"} == Transaction::SALARY_PAID_OBJECT_TYPE) {
                $this->setAttribute('amount_color', "red");
            }
        }

        if ($this->{'created_at'}) {
            $this->setAttribute('timestamp', Carbon::parse($this->{'created_at'})->isoFormat('DD/MM/YYYY HH:mm:ss'));
        }

        if ($this->{'object_type'} != null) {
            if ($this->{'object_type'} == Transaction::CARD_TRANSACTION_TYPE) {
                $this->setAttribute('transaction_type_group', 'A. Thu');
                $this->setAttribute('transaction_type_detail', 'A.2. Thu học phí Ngoại Ngữ (Tiếng Anh, Tiếng Trung,...)');
                $this->setAttribute('transaction_type_group_color', 'yellow');
                $this->setAttribute('transaction_type_detail_color', 'yellow');
            }

            if ($this->{'object_type'} == Transaction::SALARY_PAID_OBJECT_TYPE) {
                $this->setAttribute('transaction_type_group', 'C. Chi');
                $this->setAttribute('transaction_type_group_color', 'red');
                $this->setAttribute('transaction_type_detail_color', 'red');
                $salary = Salary::query()->where('id', $this->{'object_id'})->first();
                if ($salary) {
                    $user = User::query()->where('id', $salary->{'user_id'})->first();
                    if ($user->{'role'} == User::TEACHER_ROLE) {
                        $this->setAttribute('transaction_type_detail', 'C.1. Chi lương giáo viên.');
                        $this->setAttribute('transaction_type', "renew");
                    }
                    if ($user->{'role'} == User::SUPPORTER_ROLE) {
                        $this->setAttribute('transaction_type_detail', 'C.2. Chi lương trợ giảng');
                        $this->setAttribute('transaction_type', "renew");
                    }
                }
            }
            /**
             * @var TransactionType $transactionType
             */
            $transactionType = TransactionType::query()->where('id', $this->{'transaction_type'})->first();

            if ($transactionType) {
                $this->setAttribute('transaction_type_group', TransactionType::groupOptions()[$transactionType->{'group'}]);
                $this->setAttribute('transaction_type_group_color', TransactionType::groupColor()[$transactionType->{'group'}]);
                $this->setAttribute('transaction_type_detail', $transactionType->{'name'});
                $this->setAttribute('transaction_type_detail_color', TransactionType::groupColor()[$transactionType->{'group'}]);
                $this->setAttribute('amount_color', TransactionType::groupColor()[$transactionType->{'group'}]);
                $this->setAttribute('transaction_type', $transactionType->hasSameType() ? "renew" : "new");
            }

        }
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        static::addGlobalScope('branch', function (Builder $builder) {
            $builder->whereHas('creator', function (Builder $builder) {
                $builder->withTrashed()->where('branch', Auth::user()->{"branch"});
            });
        });
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function handleBalance(array $calculateCashierAmount)
    {
        $this->setAttribute('balance', $calculateCashierAmount[$this->{'id'}]);
        if ($calculateCashierAmount[$this->{'id'}] > 0) {
            $this->setAttribute('balance_color', 'green');
        }
        if ($calculateCashierAmount[$this->{'id'}] < 0) {
            $this->setAttribute('balance_color', 'red');
        }
        if ($calculateCashierAmount[$this->{'id'}] == 0) {
            $this->setAttribute('balance_color', 'yellow');
        }
    }

    public static function customQuery(Request $request, Builder &$builder): void
    {
        $filters = $request->get('filters') ? $request->get('filters') : [];
        if (!empty($filters['cashier_type:handle'])) {
            $cashierType = $filters['cashier_type:handle'];

            switch ($cashierType) {
                case "revenue":
                    $builder->where(function (Builder $builder) {
                        $builder->whereIn('object_type', [
                            Transaction::CARD_TRANSACTION_TYPE,
                        ])->orWhereHas('TransactionType', function (Builder $builder) {
                            $builder->where('group', TransactionType::GROUP_A);
                        });
                    });
                    break;
                case "payment":
                    $builder->where(function (Builder $builder) {
                        $builder->whereIn('object_type', [
                            Transaction::SALARY_PAID_OBJECT_TYPE,
                        ])->orWhereHas('TransactionType', function (Builder $builder) {
                            $builder->where('group', TransactionType::GROUP_C);
                        });
                    });
                    break;
                case "receive":
                    $builder->where(function (Builder $builder) {
                        $builder->whereIn('object_type', [

                        ])->orWhereHas('TransactionType', function (Builder $builder) {
                            $builder->where('group', TransactionType::GROUP_B);
                        });
                    });
                    break;
                case "transfer":
                    $builder->whereIn('object_type', [

                    ])->orWhereHas('TransactionType', function (Builder $builder) {
                        $builder->where('group', TransactionType::GROUP_D);
                    });
                    break;
                default:
                    break;
            }
        }
    }

    public function TransactionType(): BelongsTo
    {
        return $this->belongsTo(TransactionType::class, 'transaction_type', 'id');
    }
}
