<?php

namespace App\Models;

use App\Models\Traits\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class AccountRelation extends Model
{
    use HasFactory;
    use Entity;
    use SoftDeletes;

    protected $table = 'account_relations';
    protected $guarded = ['id'];

    public static function defineFillableField(): array
    {
        return [
            'relation_name' => true,
            'color' => true,
        ];
    }

    public static function defineRetrievableField(): array
    {
        return [
            'id' => true,
            'relation_name' => true,
            'color' => true,
        ];
    }

    public static function boot(): void
    {
        parent::boot();

        static::addGlobalScope('branch', function (Builder $builder) {
            $builder->where('branch', Auth::user()->{'branch'})->orWhere('branch', '=', null);
        });
    }
}
