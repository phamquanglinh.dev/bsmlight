<?php

namespace App\Models\Bcoin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BcoinWallet extends Model
{
    use HasFactory;

    protected $table = "wallets";

    protected $guarded = ['id'];

    protected $connection = 'bcoin';
}
