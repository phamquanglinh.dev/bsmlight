<?php

namespace App\Models\Bcoin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class BcoinAccount extends Model
{
    use HasFactory;

    protected $connection = 'bcoin';

    protected $table = 'users';

    protected $guarded = ['id'];

    public function Wallet(): HasOne
    {
        return $this->hasOne(BcoinWallet::class, 'user_id', 'id');
    }
}
