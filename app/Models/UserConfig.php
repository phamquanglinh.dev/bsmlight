<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserConfig extends Model
{
    use HasFactory;

    const GOOGLE_FORM = "google_form";
    const ACCOUNT_FIELD_TYPE = "account_field";

    protected $table = 'user_configs';

    protected $guarded = ['id'];
    const GOOGLE_CHAT_TYPE = 'google_chat';
}
