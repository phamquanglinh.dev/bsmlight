<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CoverShift extends Model
{
    use HasFactory;

    protected $table = "cover_shifts";

    protected $guarded = ['id'];

    protected $casts = [
        'week_days' => 'array',
        'start_cover_date' => 'datetime',
        'end_cover_date' => 'datetime'
    ];

    public function Classroom(): BelongsTo
    {
        return $this->belongsTo(Classroom::class, 'classroom_id');
    }

    public function Teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function Supporter(): BelongsTo
    {
        return $this->belongsTo(Supporter::class, 'supporter_id');
    }
}
