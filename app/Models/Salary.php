<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory;

    const PENDING_STATUS = 0;

    protected $table = 'v2_salaries';

    protected $guarded = ['id'];

    public function getPaidAttribute(): int
    {
        return Transaction::query()
            ->where('object_type', Transaction::SALARY_PAID_OBJECT_TYPE)
            ->where('object_id', $this->getAttribute('id'))
            ->sum('amount');
    }

    public function getUnpaidAttribute(): int
    {
        return $this->getAttribute('total_value') - $this->getPaidAttribute();
    }

    public function Classroom()
    {
        return $this->belongsTo(Classroom::class, 'classroom_id', 'id');
    }

    public function getClassroomEntityAttribute()
    {
        return $this->Classroom()->first()->makeHidden(Classroom::getAppends());
    }

    public function backgroundStatus(): string
    {
        if ($this->getAttribute('status') == -1) {
            return 'bg-label-danger';
        }

        if ($this->getUnpaidAttribute() == 0) {
            return 'bg-label-success';
        }

        if ($this->getPaidAttribute() == 0) {
            return 'bg-label-secondary';
        }

        return 'bg-label-warning';
    }

    public function status()
    {
        if ($this->getAttribute('status') == -1) {
            return 'Đã hủy chi lương';
        }

        if ($this->getUnpaidAttribute() == 0) {
            return 'Đã chi lương xong';
        }

        if ($this->getPaidAttribute() == 0) {
            return 'Chưa chi lương';
        }

        return 'Đang chi lương';
    }

    public function isCancelled(): bool
    {
        return $this->getAttribute('status') == -1;
    }
}
