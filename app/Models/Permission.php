<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    public static function defineEntry(): array
    {
        return [
            'student' => 'học sinh',
            'teacher' => 'giáo viên',
            'supporter' => 'trợ giảng',
            'staff' => 'nhân viên',
            'card' => 'thẻ học',
            'transaction' => 'giao dịch',
            'classroom' => 'lớp học',
            'studylog' => 'buổi điểm danh',
            'branch' => 'chi nhánh',
            'permission' => 'phân quyền',
            'salary_sheet' => 'phiếu lương',
            'salary_criteria' => 'chỉ tiêu lương',
            'cashier' => 'sổ quỹ'
        ];
    }

    public static function defineAction()
    {
        return [
            'list' => 'Xem tất cả',
            'show' => 'Xem chi tiết',
            'create' => 'Thêm mới',
            'update' => 'Cập nhật',
            'delete' => 'Xóa'
        ];
    }

    public static function defineSinglePermission(): array
    {
        return [
            'accept studylog' => 'Duyệt buổi điểm danh',
            'accept transaction' => 'Duyệt giao dịch',
        ];
    }

    public static function makePermissionData(): array
    {
        $permissionData = [];
        foreach (Permission::defineEntry() as $entryKey => $entry) {
            foreach (static::defineAction() as $actionKey => $action) {
                $permissionData[] = [
                    'name' => "$action $entry",
                    "key" => "$actionKey $entryKey",
                    "module" => $entryKey,
                    "description" => ''
                ];
            }
        }

        foreach (static::defineSinglePermission() as $key => $permission) {
            $module = explode(' ', $key)[1];

            $permissionData[] = [
                "name" => $permission,
                "key" => $key,
                'module' => $module,
                'description' => ''
            ];
        }

        return $permissionData;
    }

    public static function groupByModule(): array
    {
        $permissionByModule = [];

        $permissions = Permission::query()->get();

        foreach (static::defineEntry() as $entryKey => $entry) {
            $permissionByModule[$entryKey]['label'] = $entry;
        }

        foreach ($permissions as $permission) {
            $permissionByModule[$permission['module']]['permissions'][] = [
                'name' => $permission['name'],
                'key' => $permission['key'],
            ];
        }

        return $permissionByModule;
    }
}
