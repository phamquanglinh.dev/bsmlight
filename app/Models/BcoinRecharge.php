<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BcoinRecharge extends Model
{
    use HasFactory;

    const STATUS_WAITING = 0;

    protected $connection = 'bcoin';

    protected $table = 'recharges';

    protected $guarded = ['id'];
}
