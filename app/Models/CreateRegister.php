<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreateRegister extends Model
{
    use HasFactory;

    protected $table = "register_key";

    protected $guarded = ["id"];
}
