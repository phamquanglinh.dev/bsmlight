<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 2:16 pm 29/02/2024
 **/

/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 11:22 am 01/03/2024
 * @property string $sheet_name
 * @property integer $loop_type
 * @property integer $period_year
 * @property string $description
 * @property float $salary_rate
 * @private float $fund_percent
 */
class SalarySheet extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $guarded = ['id'];
    const LOOP_TYPE_MONTH = 1;
    const LOOP_TYPE_MONTH_SETTING = 5;

    protected $table = 'salary_sheet';


    protected $appends = [
        'user',
        'classroom'
    ];

    public static function loopTypeOptions(): array
    {
        return [
            self::LOOP_TYPE_MONTH => 'Lặp lại hàng tháng',
            self::LOOP_TYPE_MONTH_SETTING => 'Chỉ áp dụng tháng nhất định'
        ];
    }

    public static function sheetUserOptions(): array
    {
        $options = [
            'teacher' => 'Tất cả giáo viên trong chi nhánh',
            'supporter' => 'Tất cả trợ giảng trong chi nhánh',
            'foreign' => 'Tất cả giáo viên nước ngoài trong chi nhánh',
        ];

        $validUsers = User::query()->where('branch', Auth::user()->{'branch'})
            ->whereIn('role', [User::SUPPORTER_ROLE, User::TEACHER_ROLE])
            ->get()->mapWithKeys(function ($user) {
                return [$user['id'] => $user['uuid'] . '-' . $user['name']];
            })->toArray();

        return $options + $validUsers;
    }

    public static function getOrDefaultSheetDetail(SalarySheet $salarySheet = null): array
    {
        if (!$salarySheet) {
            return [];
        }

        return $salarySheet->getSalarySheetDetails()->get()->mapWithKeys(function (SalarySheetDetail $sheetDetail) {
            return [$sheetDetail['id'] => $sheetDetail];
        })->toArray();
    }

    /**
     * @return HasMany
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 29/02/2024 2:16 pm
     */
    public function getSalarySheetDetails(): HasMany
    {
        return $this->hasMany(SalarySheetDetail::class, 'salary_sheet_id', 'id');
    }

    /**
     * @return BelongsTo
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 29/02/2024 2:16 pm
     */
    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Classroom(): BelongsTo
    {
        return $this->belongsTo(Classroom::class, 'classroom_id', 'id');
    }

    public function getUserAttribute(): Model
    {
        return $this->User()->first() ?? new User();
    }

    public function getClassroomAttribute(): Model
    {
        return $this->Classroom()->first() ?? new Classroom();
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        static::addGlobalScope('branch', function (Builder $builder) {
            $builder->where('branch', Auth::user()->{'branch'});
        });
    }

}
