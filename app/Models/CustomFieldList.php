<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomFieldList extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "custom_field_list";

    protected $fillable = [
        'custom_field_id',
        'field_option',
    ];
}
