<?php

namespace App\Models;

use App\Models\Traits\HasSystemLog;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Student extends User
{
    use HasFactory;
    use SoftDeletes;
    use HasSystemLog;

    const MALE = 1;
    const FEMALE = 2;
    protected $table = 'users';

    const STUDENT_LEARNING = 1;
    const STUDENT_ARRIVED = 2;
    const STUDENT_STOPPED = 3;

    public static function StudentStatusLabel(): array
    {
        return [
            self::STUDENT_LEARNING => 'Đang học',
            self::STUDENT_ARRIVED => 'Đang bảo lưu',
            self::STUDENT_STOPPED => 'Đã nghỉ'
        ];
    }

    public static function StudentStatusBg(): array
    {
        return [
            self::STUDENT_LEARNING => 'bg-success',
            self::STUDENT_ARRIVED => 'bg-primary',
            self::STUDENT_STOPPED => 'bg-secondary'
        ];
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope('role', function ($builder) {
            $builder->where('role', User::STUDENT_ROLE);
        });

        static::addGlobalScope('branch', function ($builder) {
            $builder->where('branch', Auth::user()->{'branch'});
        });
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function profile(): HasOne
    {
        return $this->hasOne(StudentProfile::class, "user_id", "id");
    }

    protected $appends = [
        'gender',
        'english_name',
        'school',
        'facebook',
        'address',
        'user_ref',
        'extra_information',
        'cards',
        'private_chat',
        'zalo_private_chat',
        'birthday',
        'age',
        'grade',
        'level',
        'sibling',
        'last_login',
        'login_warning'
    ];

    public function getSiblingAttribute(): array
    {
        return [];
    }

    public function getAgeAttribute(): int
    {
        $birthday = Carbon::parse($this->profile?->birthday);

        if ($birthday) {
            return $birthday->diffInYears(Carbon::now());
        }

        return 0;
    }

    public function getGradeAttribute(): string
    {
        $age = $this->getAgeAttribute();
        if ($age <= 3) {
            return "--";
        }
        if ($age <= 5) {
            return "Lớp $age tuổi";
        }
        if ($age <= 17) {
            return "Lớp " . $age - 5;
        }

        return "Khác";
    }

    public function getLevelAttribute(): string
    {
        $age = $this->getAgeAttribute();
        if ($age <= 3) {
            return '--';
        }

        if ($age <= 5) {
            return 'Mầm non';
        }
        if ($age <= 10) {
            return 'Tiểu học';
        }
        if ($age <= 14) {
            return "THCS";
        }
        if ($age <= 17) {
            return "THPT";
        }

        if ($age <= 21) {
            return "Cao Đẳng - Đại Học";
        }

        return 'Người đi làm';
    }

    public function getZaloPrivateChatAttribute(): int
    {
        return 1;
    }

    public function getGenderAttribute($value)
    {
        return $this->profile?->gender;
    }

    public function getEnglishNameAttribute($value)
    {
        return $this->profile?->english_name;
    }

    public function getSchoolAttribute($value)
    {
        return $this->profile?->school;
    }

    public function getFacebookAttribute($value)
    {
        return $this->profile?->facebook;
    }

    public function getAddressAttribute($value)
    {
        return $this->profile?->address;
    }

    public function getUserRefAttribute($value)
    {
        return $this->profile?->user_ref;
    }

    public function getExtraInformationAttribute($key)
    {
        $extraInformation = json_decode($this->profile?->extra_information, true);

        return $extraInformation[$key] ?? null;
    }

    public function getBirthdayAttribute(): string
    {
        if (! $this->profile?->birthday) {
            return '';
        }

        return silence(fn() =>Carbon::createFromFormat('Y-d-m',$this->profile?->birthday)->isoFormat('DD-MM-YYYY'), $this->profile?->birthday);
    }

    public function getCardsAttribute(): array
    {
        return $this->hasMany(Card::class, "student_id", 'id')->get()->map(function (Card $card) {
            return [
                'id' => $card['id'],
                'uuid' => $card['uuid']
            ];
        })->toArray();
//        return [
//            [
//                'id' => 1,
//                'uuid' => 'B.000-StudyCard.0001'
//            ],
//            [
//                'id' => 2,
//                'uuid' => 'B.000-StudyCard.0001'
//            ]
//        ];
    }

    public function getPrivateChatAttribute(): string
    {
        return "";
    }

    public function getCustomField(string $name)
    {
        $extraInformation = $this->profile?->extra_information;

        if (! $extraInformation) {
            return null;
        }

        $extraInformation = json_decode($extraInformation, true);

        return $extraInformation[$name] ?? null;
    }

    public function Cards(): HasMany
    {
        return $this->hasMany(Card::class, 'student_id', 'id');
    }

    public function Parallels(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'parallels', 'student_id', 'parallel_id');
    }

    public function getOriginStudent()
    {
        return Parallel::query()->where('parallel_id', $this->id)->first()?->getAttribute('parallel_id') ?? null;
    }
}
