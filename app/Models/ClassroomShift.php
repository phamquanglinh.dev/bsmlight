<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ClassroomShift extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    const TEMPLATE = [
        'start_time' => '',
        'end_time' => '',
        'teacher_id' => null,
        'supporter_id' => null,
        'room' => ''
    ];

    public function Classroom(): BelongsTo
    {
        return $this->belongsTo(Classroom::class, 'classroom_id');
    }

    public function Teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function Supporter(): BelongsTo
    {
        return $this->belongsTo(Supporter::class, 'supporter_id', 'id');
    }

    public function Schedule(): BelongsTo
    {
        return $this->belongsTo(ClassroomSchedule::class, 'classroom_schedule_id', 'id');
    }
}
