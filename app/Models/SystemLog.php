<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property string $title
 * @property string $user_id
 * @property string $object_id
 * @property string $object_type
 * @property string $url
 * @property string $user_name
 * @property string $user_avatar
 * @property string $user_role_label
 */
class SystemLog extends Model
{
    use HasFactory;

    const OBJECT_TYPE_STUDYLOG = 'studylog';
    const ACTION_TYPE_ATTENDANCE = 'attendance';
    protected $table = 'system_logs';
    protected $guarded = ['id'];

    protected $appends = [
        'url',
        'object_type_label'
    ];

    const OBJECT_TYPE_USER = 'user';
    const OBJECT_TYPE_CARD = 'card';
    const ACTION_TYPE_LOGIN = 'login';
    const ACTION_TYPE_ACCESS = 'access';
    const ACTION_TYPE_BE_ATTENDANCE = 'be_attendance';

    public static function roleToString(): array
    {
        return [
            User::HOST_ROLE => 'host',
            User::STAFF_ROLE => 'staff',
            User::TEACHER_ROLE => 'teacher',
            User::SUPPORTER_ROLE => 'supporter',
            User::STUDENT_ROLE => 'student'
        ];
    }

    public function getUrlAttribute(): string
    {
        switch ($this->object_type) {
            case self::OBJECT_TYPE_USER:
                $user = User::query()->where('id', $this->object_id)->first();
                if (! $user) {
                    return "#";
                }
                $stringRole = SystemLog::roleToString()[$user['role']];

                return url('') . '/' . $stringRole . '/show/' . $user['id'];
            case self::OBJECT_TYPE_CARD:
                $card = Card::query()->where('id', $this->object_id)->first();
                if (! $card) {
                    return "#";
                }

                return url('') . '/card/show/' . $card['id'];
            default:
                return "#";
        }
    }

    public function getObjectTypeLabelAttribute(): string
    {
        switch ($this->object_type) {
            case self::OBJECT_TYPE_USER:
                $user = User::query()->where('id', $this->object_id)->first();
                if (! $user) {
                    return "Đã bị xóa";
                }

                return $user['uuid'] . '-' . $user['name'];

            case self::OBJECT_TYPE_CARD:
                $card = Card::query()->where('id', $this->object_id)->first();
                if (! $card) {
                    return "Đã bị xóa";
                }

                return $card['uuid'];
            default:
                return "#";
        }
    }

    public function getUserIdUrl()
    {
        $user = User::query()->where('id', $this->user_id)->first();
        $stringRole = SystemLog::roleToString()[$user['role']];

        return url('') . '/' . $stringRole . '/show/' . $user['id'];
    }

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
