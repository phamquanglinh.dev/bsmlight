<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemLogTest extends Model
{
    use HasFactory;

    protected $table = 'system_logs_test';

    protected $guarded = ['id'];
}
