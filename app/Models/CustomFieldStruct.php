<?php

namespace App\Models;

use App\Models\Traits\Entity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomFieldStruct extends Model
{
    use Entity;
    use HasFactory;
    use SoftDeletes;

    protected $table = 'custom_field_struct';
    protected $guarded = ['id'];
    const HTML_TEXT_TYPE = 'text';
    const HTML_NUMERIC_TYPE = 'numeric';
    const HTML_TEXTAREA_TYPE = 'textarea';
    const HTML_CHECKBOX_TYPE = 'checkbox';
    const HTML_SELECT_TYPE = 'select';
    const HTML_DATETIME_TYPE = 'datetime';
    const MODULE_ACCOUNT = 'account';
    const HTML_LINK_TYPE = 'link_type';

    public static function defineFillableField(): array
    {
        return [
            'field_name' => true,
            'field_label' => true,
            'field_html_type' => true,
            'is_required' => true,
            'is_duplicate' => true,
            'is_multiple' => true,
        ];
    }

    public static function defineRetrievableField(): array
    {
        return [
            'id' => true,
            'field_name' => true,
            'field_label' => true,
            'field_html_type' => true,
            'is_required' => true,
            'is_duplicate' => true,
            'is_multiple' => true,
        ];
    }

    protected $casts = [
        'is_required' => 'integer',
        'is_duplicate' => 'integer',
        'is_multiple' => 'integer',
        'default' => 'integer'
    ];
}
