<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SalaryGroup extends Model
{
    const STATUS_UNKNOWN = -1;
    const STATUS_IN_PROCESS = 1;
    const STATUS_FINISHED = 2;
    const STATUS_CANCELED = 3;
    use HasFactory;

    protected $appends = [
        'user_entity',
        'salary_count',
        'total_paid', # Tổng số tiền đã thanh toán
        'total_unpaid', # Tổng số tiền chưa thanh toán
        'total_amount', # Tổng số tiền cần thanh toán,
        'total_with_fund',
    ];

    protected $table = 'salary_groups';

    protected $guarded = ['id'];

    public static function statusLabel()
    {
        return [
            SalaryGroup::STATUS_UNKNOWN => 'Chưa thanh toán',
            SalaryGroup::STATUS_IN_PROCESS => 'Đang thanh toán',
            SalaryGroup::STATUS_FINISHED => 'Thanh toán hết',
            SalaryGroup::STATUS_CANCELED => 'Hủy thanh toán'
        ];
    }

    public static function bgStatusLabel()
    {
        return [
            SalaryGroup::STATUS_UNKNOWN => 'bg-secondary',
            SalaryGroup::STATUS_IN_PROCESS => 'bg-warning',
            SalaryGroup::STATUS_FINISHED => 'bg-success',
            SalaryGroup::STATUS_CANCELED => 'bg-danger'
        ];
    }

    public static function bgLBStatusLabel()
    {
        return [
            SalaryGroup::STATUS_UNKNOWN => 'bg-label-secondary',
            SalaryGroup::STATUS_IN_PROCESS => 'bg-label-warning',
            SalaryGroup::STATUS_FINISHED => 'bg-label-success',
            SalaryGroup::STATUS_CANCELED => 'bg-label-danger'
        ];
    }

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function Salaries(): HasMany
    {
        return $this->hasMany(Salary::class, 'salary_group_id', 'id');
    }

    public function getUserEntityAttribute()
    {
        return $this->User()->first();
    }

    public function getSalaryCountAttribute(): int
    {
        return Salary::query()->where('salary_group_id', $this->getAttribute('id'))->count();
    }

    public function getTotalPaidAttribute(): int
    {
        return Transaction::query()
            ->where('object_type', Transaction::SALARY_PAID_OBJECT_TYPE)
            ->leftJoin('v2_salaries', 'v2_salaries.id', '=', 'transactions.object_id')
            ->where('v2_salaries.salary_group_id', $this->getAttribute('id'))
            ->sum('amount');
    }

    public function getTotalUnpaidAttribute(): int
    {
        return $this->getTotalAmountAttribute() - $this->getTotalPaidAttribute();
    }

    public function getTotalAmountAttribute(): int
    {
        return Salary::query()
            ->where('status', '!=', -1)
            ->where('salary_group_id', $this->getAttribute('id'))->sum('total_value');
    }

    public function getTotalFundAttribute(): int
    {
        return Salary::query()
            ->where('status', '!=', -1)
            ->where('salary_group_id', $this->getAttribute('id'))
            ->sum('fund_value');
    }

    public function getLabel()
    {
        $userEntity = $this->User()->first();

        $period = Carbon::parse($this->getAttribute('period'));

        return "{$userEntity->getAttribute('name')} ({$userEntity->getAttribute('uuid')}) - Tháng {$period->month} năm {$period->year}";
    }

    public function handleStatusProcess(): static
    {
        $paid = $this->getTotalPaidAttribute();

        if ($paid > 0) {
            $status = SalaryGroup::STATUS_IN_PROCESS;
        }

        $unpaid = $this->getTotalUnpaidAttribute();

        if ($unpaid == 0) {
            $status = SalaryGroup::STATUS_FINISHED;
        }

        if (!isset($status)) {
            $status = SalaryGroup::STATUS_UNKNOWN;
        }

        $this->setAttribute('status', $status);

        return $this;
    }

    public function getTotalWithFundAttribute(): int
    {
        return $this->getTotalAmountAttribute() + $this->getTotalFundAttribute();
    }

}
