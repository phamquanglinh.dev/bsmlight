<?php

namespace App\Models\Traits;

use App\Models\Account;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

trait Entity
{
    public static function handleQuery(Request $request, Builder &$builder): void
    {
        $fields = $request->get('fields') ? explode(',', $request->get('fields')) : ['id'];
        $limit = $request->get('limit') ? $request->get('limit') : 10;
        $offset = $request->get('offset') ? $request->get('offset') : 0;
        $orderBy = $request->get('orderBy') ? $request->get('orderBy') : "id";
        $direction = $request->get('direction') ? $request->get('direction') : "DESC";
        $filters = $request->get('filters') ? $request->get('filters') : [];

        foreach ($fields as $field) {
            if (!in_array($field, array_keys(array_merge(static::defineRetrievableField(), static::defineCustomFields())))) {
                throw new BadRequestException("Trường $field không hợp lệ hoặc không được phép lấy");
            }
        }

        $primaryField = array_filter($fields, function ($field) {
            return !in_array($field, array_keys(array_merge(static::defineExternalField(), static::defineCustomFields())));
        });

        $requiredFields = array_values(static::defineExternalField());

        $fields = array_unique(array_merge($requiredFields, $primaryField, ['id']));

        $builder->select($fields);

        static::handleCondition($filters, $builder);

        $builder->orderBy($orderBy, $direction);

        $builder->limit($limit)->offset(($offset - 1) * $limit);
    }

    public static function handleCondition(array $filters, Builder &$builder): void
    {
        foreach ($filters as $field => $value) {
            switch ($field) {
                case preg_match('/:contain$/', $field) == 1:
                    $builder->where(str_replace(":contain", "", $field), 'like', "%{$value}%");
                    break;
                case preg_match('/:gt$/', $field) == 1:
                    $builder->where(str_replace(":gt", "", $field), '>', $value);
                    break;
                case preg_match('/:lt$/', $field) == 1:
                    $builder->where(str_replace(":gt", "", $field), '<', $value);
                    break;
                case preg_match('/:lte$/', $field) == 1:
                    $builder->where(str_replace(":gt", "", $field), '<=', $value);
                    break;
                case preg_match('/:gte$/', $field) == 1:
                    $builder->where(str_replace(":gt", "", $field), '>=', $value);
                    break;
                case preg_match('/:in$/', $field) == 1:
                    $builder->whereIn(str_replace(":in", "", $field), $value);
                    break;
                default:
                    if (preg_match('/:handle$/', $field) == 1) {
                        break;
                    }
                    $builder->where($field, $value);
                    break;
            }
        }
    }

    public static function defineFillableField(): array
    {
        return [];
    }

    public static function defineSavableField(): array
    {
        return [];
    }

    public static function defineRetrievableField(): array
    {
        return [];
    }

    public static function defineExternalField(): array
    {
        return [];
    }

    public function setIfEmpty($attribute, $value): void
    {
        if (empty($this->{$attribute})) {
            $this->{$attribute} = $value;
        }
    }

    public function populate(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, array_keys(static::defineFillableField())) && !empty(static::defineFillableField())) {
                throw new BadRequestException("{$key} không được phép nhập");
            }
        }

        $this->fill($data);
    }

    public function handleQueryProcess(Request $request)
    {

    }

    public static function defineCustomFields(): array
    {
        return [];
    }
}
