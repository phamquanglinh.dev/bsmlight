<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 28/03/2024 1:01 pm
 */

namespace App\Models\Traits;

use App\Models\SystemLog;
use Carbon\Carbon;

/**
 * @property int $id
 * @property string $branch
 */
trait HasSystemLog
{
    public function getLastLoginAttribute(): string
    {
        $systemLog = SystemLog::query()->where([
            'object_id' => $this->id,
            'object_type' => SystemLog::OBJECT_TYPE_USER,
            'active_type' => SystemLog::ACTION_TYPE_LOGIN,
            'branch' => $this->branch
        ])->first();

        if (! $systemLog) {
            return 'Chưa đăng nhập';
        }

        return Carbon::parse($systemLog->created_at)->isoFormat('DD/MM/YYYY hh:mm:ss');
    }

    public function getLoginWarningAttribute(): bool
    {
        $systemLog = SystemLog::query()->where([
            'object_id' => $this->id,
            'object_type' => SystemLog::OBJECT_TYPE_USER,
            'active_type' => SystemLog::ACTION_TYPE_LOGIN,
            'branch' => $this->branch
        ])->orderBy('created_at', 'DESC')->first();

        if (! $systemLog) {
            return false;
        }

        if (Carbon::parse($systemLog['created_at'])->diffInDays(now()) >= 7) {
            return true;
        }

        return false;
    }

    public function getLastBeAttendanceAttribute(): string
    {
        $systemLog = SystemLog::query()->where([
            'object_id' => $this->id,
            'object_type' => SystemLog::OBJECT_TYPE_CARD,
            'active_type' => SystemLog::ACTION_TYPE_BE_ATTENDANCE,
            'branch' => $this->branch
        ])->orderBy('created_at', 'DESC')->first();

        if (! $systemLog) {
            return '-';
        }

        return Carbon::parse($systemLog->created_at)->isoFormat('DD/MM/YYYY hh:mm:ss');
    }

    public function getBeAttendanceWarningAttribute(): bool
    {
        $systemLog = SystemLog::query()->where([
            'object_id' => $this->id,
            'object_type' => SystemLog::OBJECT_TYPE_CARD,
            'active_type' => SystemLog::ACTION_TYPE_BE_ATTENDANCE,
            'branch' => $this->branch
        ])->orderBy('created_at', 'DESC')->first();

        if (! $systemLog) {
            return false;
        }

        if (Carbon::parse($systemLog['created_at'])->diffInDays(now()) >= 7) {
            return true;
        }

        return false;
    }
}
