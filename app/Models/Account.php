<?php

namespace App\Models;

use App\Models\Traits\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Account extends Model
{
    use HasFactory;
    use Entity;

    protected $table = "accounts";

    protected $guarded = ["id"];

    public static function validationRulesForCreate(): array
    {
        return [
            'account_name' => 'bail|string|required',
            'account_phone' => 'bail|string|nullable|required_if:account_email,!=,null',
            'account_email' => 'bail|email|nullable|required_if:account_phone,!=,null',
            'description' => 'bail|string|nullable',
            'gender' => 'bail|integer|in:1,2|nullable',
            'account_type' => 'bail|integer|exists:account_types,id|nullable',
            'account_source' => 'bail|integer|exists:account_sources,id|nullable',
            'account_relation' => 'bail|integer|exists:account_relations,id|nullable',
            'account_manager' => 'bail|integer|exists:users,id|nullable',
            'ref_id' => 'bail|integer|nullable',
            'ref_type' => 'bail|integer|nullable|required_if:ref_id,!=,null',
        ];
    }

    public static function validationRulesForUpdate(): array
    {
        return [
            'account_name' => 'bail|string|filled',
            'account_phone' => 'bail|string|nullable',
            'account_email' => 'bail|email|filled',
            'description' => 'bail|string|nullable',
            'gender' => 'bail|integer|in:1,2|nullable',
            'account_type' => 'bail|integer|exists:account_types,id|filled',
            'account_source' => 'bail|integer|exists:account_sources,id|filled',
            'account_relation' => 'bail|integer|exists:account_relations,id|filled',
            'account_manager' => 'bail|integer|exists:users,id|filled',
        ];
    }

    public static function makeUUID(string $branch): string
    {
        $accountId = Account::query()->withoutGlobalScopes()->where('branch', $branch)->orderBy('id', 'DESC')->first()?->id ?? 0;

        $accountId++;

        if ($accountId < 100000) {
            $accountId = sprintf('%04d', $accountId);
        }

        return "KH/{$branch}/{$accountId}";
    }

    public static function defineFillableField(): array
    {
        return [
            'account_name' => true,
            'account_phone' => true,
            'account_email' => true,
            'description' => true,
            'gender' => true,
            'account_type' => true,
            'account_source' => true,
            'account_relation' => true,
            'account_manager' => true,
            'ref_id' => true,
            'ref_type' => true,
        ];
    }

    public static function defineRetrievableField(): array
    {
        return [
            'id' => true,
            'account_name' => true,
            'account_phone' => true,
            'account_email' => true,
            'description' => true,
            'gender' => true,
            'account_type' => true,
            'account_source' => true,
            'account_relation' => true,
            'account_manager' => true,
            'ref_id' => true,
            'ref_type' => true,
            'relation_name' => true,
            'source_name' => true,
            'type_name' => true,
            'manager_name' => true,
            'ref_name' => true,
            'gender_text' => true,
            'created_at' => true
        ];
    }

    public static function defineCustomFields(): array
    {
        return CustomFieldStruct::query()
            ->where('module', CustomFieldStruct::MODULE_ACCOUNT)
            ->where(function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'})
                    ->orWhere('default', 1);
            })
            ->get(['field_name'])->mapWithKeys(function (CustomFieldStruct $customFieldStruct) {
                return [$customFieldStruct['field_name'] => true];
            })->toArray();
    }

    public static function primaryFields(): array
    {
        return [
            [
                'field_name' => 'account_name',
                'field_label' => 'Tên khách hàng',
                'field_html_type' => 'entity',
                'is_required' => 1,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'account_phone',
                'field_label' => 'Số điện thoại',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'account_email',
                'field_label' => 'Email',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'description',
                'field_label' => 'Mô tả',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'gender_text',
                'field_label' => 'Giới tính',
                'field_html_type' => CustomFieldStruct::HTML_TEXT_TYPE,
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'relation_name',
                'field_label' => 'Trạng thái',
                'field_html_type' => 'relation',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'source_name',
                'field_label' => 'Nguồn KH',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'manager_name',
                'field_label' => 'Người quản lý',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'ref_name',
                'field_label' => 'Người giới thiệu',
                'field_html_type' => 'text',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
            [
                'field_name' => 'created_at',
                'field_label' => 'Thời gian tạo',
                'field_html_type' => 'datetime',
                'is_required' => 0,
                'default' => 1,
                'is_multiple' => 0
            ],
        ];
    }

    public static function defineExternalField(): array
    {
        return [
            'relation_name' => 'account_relation',
            'source_name' => 'account_source',
            'type_name' => 'account_type',
            'manager_name' => 'account_manager',
            'ref_name' => 'ref_id',
            'gender_text' => 'gender'
        ];
    }

    public static function boot(): void
    {
        parent::boot();

        static::addGlobalScope('branch', function (Builder $builder) {
            $builder->where('branch', Auth::user()->{'branch'});
        });
    }

    public function handleQueryProcess(Request $request): void
    {
        $fields = $request->get('fields') ? explode(',', $request->get('fields')) : ['id'];

        $externalFields = array_intersect($fields, array_keys(static::defineExternalField()));

        if (in_array('relation_name', $externalFields) && $this->{'account_relation'} != null) {

            $accountRelation = AccountRelation::query()->where('id', $this->{'account_relation'})->first();

            $this->setAttribute('relation_name', $accountRelation->{'relation_name'});
            $this->setAttribute('relation_color', $accountRelation->{'color'});
        }


        if (in_array('source_name', $externalFields) && $this->{'account_source'} != null) {

            $accountSource = AccountSource::query()->where('id', $this->{'account_source'})->first();

            $this->setAttribute('source_name', $accountSource->{'source_name'});
        }

        if (in_array('type_name', $externalFields) && $this->{'account_type'} != null) {

            $accountSource = AccountType::query()->where('id', $this->{'account_type'})->first();

            $this->setAttribute('type_name', $accountSource->{'type_name'});
        }

        if (in_array('manager_name', $externalFields) && $this->{'account_manager'} != null) {

            $accountSource = User::query()->where('id', $this->{'account_manager'})->first();

            $this->setAttribute('manager_name', $accountSource->{'name'});
        }

        if (in_array('gender_text', $externalFields) && $this->{'gender'} != null) {
            $gender = $this->{'gender'} == 1 ? "Nam" : "Nữ";
            $this->setAttribute('gender_text', $gender);
        }

        $customFields = array_intersect($fields, array_keys(static::defineCustomFields()));

        $this->handleCustomFields($customFields);
    }

    private function handleCustomFields(array $customFields): void
    {
        foreach ($customFields as $field) {
            $customField = CustomFieldStruct::query()
                ->where('field_name', $field)->where('module', CustomFieldStruct::MODULE_ACCOUNT)
                ->first();

            if (!$customField) {
                continue;
            }

            $customFieldValue = CustomFieldValue::query()
                ->where('custom_field_id', $customField->{'id'})
                ->where('object_id', $this->{'id'})
                ->where('object_type', CustomFieldStruct::MODULE_ACCOUNT)->first();

            if (!$customFieldValue) {
                continue;
            }

            switch ($customField->{'field_html_type'}) {
                default:
                    $this->setAttribute($field, $customFieldValue->{'value'});
                    break;
            }
        }
    }
}
