<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $salary_sheet_id
 * @property int $user_id
 */
class SalarySheetUser extends Model
{
    use HasFactory;

    protected $table = 'salary_sheet_user';

    protected $guarded = ['id'];

    public function SalarySheet(): BelongsTo
    {
        return $this->belongsTo(SalarySheet::class, 'salary_sheet_id', 'id');
    }
}
