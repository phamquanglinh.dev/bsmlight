<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BcoinSetting extends Model
{
    use HasFactory;

    protected $connection = 'bcoin';

    protected $table = 'settings';

    protected $visible = ['id', 'name', 'value'];

    public static function getMappedSettings(): array
    {
        return BcoinSetting::query()->get()->mapWithKeys(function ($setting) {
            return [$setting['name'] => $setting['value']];
        })->toArray();
    }
}
