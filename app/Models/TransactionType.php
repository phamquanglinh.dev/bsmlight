<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'transaction_types';

    const GROUP_A = 1;
    const GROUP_B = 2;
    const GROUP_C = 3;
    const GROUP_D = 4;

    public static function groupOptions(): array
    {
        return [
            self::GROUP_A => 'A.Thu',
            self::GROUP_B => 'B.Nhận',
            self::GROUP_C => 'C.Chi',
            self::GROUP_D => 'D.Chuyển',
        ];
    }

    public static function groupColor(): array
    {
        return [
            self::GROUP_A => 'yellow',
            self::GROUP_B => 'orange',
            self::GROUP_C => 'red',
            self::GROUP_D => 'pink',
        ];
    }

    public function hasSameType(): bool
    {
        return Transaction::query()->where('transaction_type', $this->{'id'})->count() > 1;
    }
}
