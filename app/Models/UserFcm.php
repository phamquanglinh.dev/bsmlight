<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFcm extends Model
{
    use HasFactory;

    const WEB = 0;
    const ANDROID = 1;
    const IOS = 2;

    protected $table = 'user_fcm';

    protected $guarded = ['id'];
}
