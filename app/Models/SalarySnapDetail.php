<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $snap_criteria_name
 * @property int $snap_criteria_type
 * @property float $snap_criteria_rate
 * @property int $snap_unit_type
 * @property float $snap_value
 * @property float $amount
 * @property int $user_id
 * @property int $salary_sheet_id
 * @property mixed $period
 * @property mixed $snap_criteria_code
 */
class SalarySnapDetail extends Model
{
    use HasFactory;

    protected $table = 'salary_snap_detail';
    protected $guarded = ['id'];

    public function getAmountAttribute(): float
    {
        $value = $this->snap_criteria_rate * $this->snap_value;

        if ($this->snap_unit_type == SalaryCriteria::UNIT_PERCENT) {
            $value = $value / 100;
        }

        if ($this->snap_criteria_type == SalaryCriteria::TYPE_PUNISH) {
            $value = 0 - $value;
        }

        return $value;
    }
}
