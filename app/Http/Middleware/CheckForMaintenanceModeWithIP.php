<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;

class CheckForMaintenanceModeWithIP
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app()->isDownForMaintenance()) {
            $allowedIps = ['42.114.121.247']; // Thay thế bằng các IP được phép truy cập

            if (!in_array($request->ip(), $allowedIps)) {
                abort(503);
            }
        }

        return $next($request);
    }
}
