<?php

namespace App\Http\Middleware;

use App\Helper\MemoryHelper;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FrontendMiddeware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure(Request): (Response|RedirectResponse) $next
     * @return RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        MemoryHelper::$is_frontend = 1;

        return $next($request);
    }
}
