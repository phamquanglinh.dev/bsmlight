<?php

namespace App\Http\Middleware;

use App\Http\Controllers\CardController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudyLogApiController;
use App\Http\Controllers\StudyLogController;
use App\Http\Controllers\TransactionController;
use App\Models\Bcoin\BcoinAccount;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserConfig;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class BcoinMiddleWare
{

    private function mappedActionApplyBcoin(): array
    {
        return [
            CardController::class => [
                'store' => 0.625,
                'update' => 0.625,
            ],

            TransactionController::class => [
                'accept' => 0.625
            ],

            StudyLogController::class => [
                'accept' => 0.625
            ]
        ];
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure(Request): (Response|RedirectResponse) $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
        if ($request->getRequestUri() == '/') {
            return $next($request);
        }

        if (Auth::check()) {
            $controller = $request->route()->getController()::class;

            $function = $request->route()->getActionMethod();

            if (isset($this->mappedActionApplyBcoin()[$controller][$function])) {
                return $this->validateBcoin($controller, $function, $next, $request);
            }
        }
        return $next($request);
    }

    private function validateBcoin(string $controller, string $function, Closure $next, Request $request)
    {
        $cost = $this->mappedActionApplyBcoin()[$controller][$function];

        $bcoinUserName = UserConfig::query()->where('type', 'bcoin_account')->where('branch', Auth::user()->{'branch'})->first();

        if (!$bcoinUserName) {
            return redirect()->to('bcoin/connect')->with('error', 'Tài khoản chưa kết nối bcoin, chưa thể sử dụng tính năng');
        }

        $bcoinAccount = BcoinAccount::query()->where('username', $bcoinUserName['settings'])->first();

        if (!$bcoinAccount) {
            return redirect()->to('bcoin/connect')->with('error', 'Tài khoản Bcoin không hợp lệ, vui lòng kiểm tra hoặc kết nối lại');
        }

        $wallet = $bcoinAccount->{'wallet'};

        if (! $wallet) {
            return redirect()->to('bcoin/connect')->with('error', 'Tài khoản Bcoin không hợp lệ, vui lòng kiểm tra hoặc kết nối lại');
        }

        if ($wallet->{'balance'} < $cost) {
            return redirect()->to('bcoin/connect')->with('error', 'Số dư tài khoản không đủ, vui lòng nạp thêm Bcoin');
        }

        $wallet->{'balance'} -= $cost;

        $wallet->save();

        return $next($request);
    }
}
