<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DupplicateMidleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $key = 'request_' . sha1(json_encode($request->input()));

        if (Cache::has($key)) {
            return response()->json(['error' => 'Too many requests. Try again later.'], 429);
        }

        Cache::put($key, true, 5);

        return $next($request);
    }
}
