<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Models\LessonPlant;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;

class LessonPlantController extends Controller
{

    public function list(Request $request)
    {
        if (Auth::user()->{'role'} == User::STUDENT_ROLE) {
            abort(403);
        }

        $crudBag = new CrudBag();
        $crudBag->setLabel('Giáo án');
        $crudBag->setEntity('lesson_plant');

        $this->handleColumns($crudBag);

        if (Auth::user()->{'role'} != User::HOST_ROLE) {
            $crudBag->setParam(CrudBag::DISABLE_ACTION, true);
        }

        $builder = LessonPlant::query();

        $listViewModel = new ListViewModel($builder->paginate($request->get('perPage') ?? 10));

        return view("list", [
            'crudBag' => $crudBag,
            'listViewModel' => $listViewModel
        ]);
    }

    public function create()
    {
        if (Auth::user()->{'role'} == User::STUDENT_ROLE) {
            abort(403);
        }

        $crudBag = new CrudBag();

        $crudBag->setLabel("Giáo án");

        $crudBag->setAction('lesson_plant.store');

        $this->handleFields($crudBag);

        return view("create", [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'audio' => 'string|nullable',
            'link' => 'string|nullable',
            'video' => 'string|nullable',
            'content' => 'string|nullable'
        ]);

        $input = $request->input();

        LessonPlant::query()->create([
            'name' => $input['name'],
            'title' => $input['title'],
            'audio' => $input['audio'],
            'link' => $input['link'],
            'video' => $input['video'],
            'content' => $input['content'],
            'created_by' => Auth::id(),
            'status' => 0
        ]);

        return redirect()->to('/lesson_plant/list');
    }

    /**
     */
    public function edit(int $id): Factory|View|Application
    {
        /**
         * @var LessonPlant $lessonPlant
         */
        $lessonPlant = LessonPlant::query()->where('id', $id)->firstOrFail();

        $crudBag = new CrudBag();

        $this->handleFields($crudBag, $lessonPlant);

        $crudBag->setLabel("Giáo án");

        $crudBag->setId($id);

        $crudBag->setAction('lesson_plant.update');

        return view("create", [
            'crudBag' => $crudBag,
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, int $id)
    {
        $lessonPlant = LessonPlant::query()->where('id', $id)->firstOrFail();

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'audio' => 'string|nullable',
            'link' => 'string|nullable',
            'video' => 'string|nullable',
            'content' => 'string|nullable'
        ]);

        $input = $request->input();

        $lessonPlant->update([
            'name' => $input['name'] ?? $lessonPlant->{'name'},
            'title' => $input['title'] ?? $lessonPlant->{'title'},
            'audio' => $input['audio'] ?? $lessonPlant->{'audio'},
            'link' => $input['link'] ?? $lessonPlant->{'link'},
            'video' => $input['video'] ?? $lessonPlant->{'video'},
            'content' => $input['content'] ?? $lessonPlant->{'content'},
            'created_by' => Auth::id(),
            'status' => 0,
            'confirm_by' => null
        ]);

        return redirect()->to('/lesson_plant/list');
    }

    public function delete()
    {

    }

    public function accept(int $id)
    {
        $lessonPlant = LessonPlant::query()->where('id', $id)->firstOrFail();

        if (!in_array(Auth::user()->{'role'}, [User::HOST_ROLE, User::STAFF_ROLE])) {
            abort(403);
        }

        $lessonPlant->update([
            'status' => 1,
            'confirm_by' => Auth::id()
        ]);

        return redirect()->back()->with('success', 'Duyệt thành công');
    }

    private function handleColumns(CrudBag &$crudBag)
    {
        $crudBag->addColumn([
            'label' => 'Tên giáo án',
            'name' => 'name',
            'fixed' => 'first',
            'attributes' => [
                'entity' => 'lesson_plant',
                'edit' => true,
                'show' => true
            ]
        ]);

        $crudBag->addColumn([
            'label' => 'Người tạo',
            'name' => 'created_by',
            'type' => 'entity',
            'attributes' => [
                'id' => 'id',
                'avatar' => 'avatar',
                'entity' => 'creator_entity',
                'name' => 'name',
                'uuid' => 'uuid',
                'model' => "Creator"
            ]
        ]);

        $crudBag->addColumn([
            'label' => 'Ngày tải lên',
            'name' => 'created_at',
            'fixed' => 'date',
        ]);

        $crudBag->addColumn([
            'label' => 'Trạng thái',
            'name' => 'status',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    0 => 'Chưa duyệt',
                    1 => 'Đã duyệt',
                    2 => 'Từ chối'
                ],
                'bg' => [
                    0 => 'bg-secondary',
                    1 => 'bg-success',
                    2 => 'bg-danger'
                ]
            ]
        ]);

        $crudBag->addColumn([
            'label' => 'Người duyệt',
            'name' => 'confirm_by',
            'type' => 'entity',
            'attributes' => [
                'id' => 'id',
                'avatar' => 'avatar',
                'entity' => 'confirmer_entity',
                'name' => 'name',
                'uuid' => 'uuid',
                'model' => "Confirmer"
            ]
        ]);
    }

    private function handleFields(CrudBag &$crudBag, LessonPlant $lessonPlant = null)
    {
        $crudBag->addFields([
            'name' => 'name',
            'label' => 'Tên giáo án',
            'required' => true,
            'value' => $lessonPlant?->{'name'}
        ]);

        $crudBag->addFields([
            'name' => 'title',
            'label' => 'Tiêu đề',
            'required' => true,
            'value' => $lessonPlant?->{'title'}
        ]);

        $crudBag->addFields([
            'name' => 'audio',
            'label' => 'Link audio',
            'value' => $lessonPlant?->{'audio'}
        ]);
        $crudBag->addFields([
            'name' => 'video',
            'label' => 'Link video',
            'value' => $lessonPlant?->{'video'}
        ]);
        $crudBag->addFields([
            'name' => 'link',
            'label' => 'Link tài liệu',
            'value' => $lessonPlant?->{'link'}
        ]);
        $crudBag->addFields([
            'name' => 'content',
            'label' => 'Nội dung',
            'type' => 'textarea',
            'class' => 'col-10',
            'value' => $lessonPlant?->{'content'}
        ]);
    }

    public function show(int $id)
    {
        $lessonPlant = LessonPlant::query()->where('id', $id)->firstOrFail();

        return \view("lesson_plant_show", [
            'lessonPlant' => $lessonPlant
        ]);
    }
}
