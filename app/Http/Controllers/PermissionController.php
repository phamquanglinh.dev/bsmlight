<?php

namespace App\Http\Controllers;

use App\Helper\Object\RoleSettingObject;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    public function permissionSettingView(): View
    {
        $modules = Permission::groupByModule();

        $roles = Role::query()->where('branch', Auth::user()->{'branch'})->get();
        return view('permission.permission-setting', [
            'modules' => $modules,
            'roleSetting' => new RoleSettingObject($roles)
        ]);
    }
}
