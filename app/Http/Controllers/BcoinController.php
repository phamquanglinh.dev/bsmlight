<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\Bcoin\BcoinAccount;
use App\Models\BcoinRecharge;
use App\Models\BcoinSetting;
use App\Models\User;
use App\Models\UserConfig;
use GuzzleHttp\Client;
use HttpRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Psr\Http\Message\ResponseInterface;


class BcoinController extends Controller
{
    public function connectView(): View
    {
        if (Auth::user()->role != User::HOST_ROLE) {
            abort(403);
        }

        $bcoinUserInfo = [
            'is_connected' => false
        ];

        $bcoinUserName = UserConfig::query()->where('type', 'bcoin_account')->where('branch', Auth::user()->{'branch'})->first();

        if ($bcoinUserName) {
            $bcoinAccount = BcoinAccount::query()->where('username', $bcoinUserName->{'settings'})->with('wallet')->first();

            if ($bcoinAccount) {
                $bcoinUserInfo = [
                    'name' => $bcoinAccount['name'],
                    'username' => $bcoinAccount['username'],
                    'balance' => $bcoinAccount['wallet']['balance'],
                    'is_connected' => true
                ];
            }
        }

        return \view('bcoin.connect', [
            'bcoinUserInfo' => $bcoinUserInfo,
        ]);
    }

    /**
     */
    public function connect(Request $request): RedirectResponse
    {
        if (Auth::user()->role != User::HOST_ROLE) {
            abort(403);
        }

        $input = $request->input();

        $notification = Validator::make($input, [
            'b_username' => 'required|string',
            'b_password' => 'required|string'
        ]);

        if ($notification->fails()) {
            return redirect()->back()->withInput()->withErrors($notification->errors());
        }

        $bcoinAccount = BcoinAccount::query()->where('username', $input['b_username'])->first();

        if (!$bcoinAccount) {
            return redirect()->back()->withInput()->withErrors(['b_username' => 'Tài khoản không tồn tại']);
        }

        if (!Hash::check($input['b_password'], $bcoinAccount->password)) {
            return redirect()->back()->withInput()->withErrors(['b_password' => 'Mật khẩu không chính xác']);
        }

        $userConfig = new UserConfig();

        $userConfig->fill([
            'user_id' => Auth::id(),
            'type' => 'bcoin_account',
            'branch' => Auth::user()->{'branch'},
            'settings' => $bcoinAccount['username']
        ]);


        $userConfig->save();

        return redirect()->back()->with('success', 'Kết nối thành công');
    }

    public function disconnect(): RedirectResponse
    {
        if (Auth::user()->role != User::HOST_ROLE) {
            abort(403);
        }

        $userConfig = UserConfig::query()->where('type', 'bcoin_account')->where('branch', Auth::user()->{'branch'})->first();

        $userConfig?->delete();

        return redirect()->back()->with('success', 'Ngắt kết nối thành công');
    }

    public function createRecharge(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        if (Auth::user()->role != User::HOST_ROLE) {
            return response()->json([
                'message' => 'Không có quyền nạp'
            ], 403);
        }

        $input = $request->input();

        $notification = Validator::make($input, [
            'amount' => 'required|numeric',
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification);
        }

        $settings = BcoinSetting::getMappedSettings();

        $client = new Client();

        $url = 'https://api.vietqr.io/v2/generate';

        $headers = [
            'x-client-id' => config('viet_qr_client_id'),
            'x-api-key' => config('viet_qr_api_key'),
        ];

        $rechargeCode = Str::random(9);

        $bcoinUserName = UserConfig::query()->where('type', 'bcoin_account')->where('branch', Auth::user()->{'branch'})->first();

        if (!$bcoinUserName) {
            return $httpResponse->badResponse('Chưa kết nối Bcoin');
        }

        $bcoinAccount = BcoinAccount::query()->where('username', $bcoinUserName->{'settings'})->with('wallet')->first();

        if (!$bcoinAccount) {
            return $httpResponse->badResponse('Chưa kết nối Bcoin');
        }

        $body = [
            'accountNo' => $settings['default_bank_account'],
            'accountName' => $settings['default_bank_name'],
            'acqId' => $settings['default_bank_code'],
            'amount' => $input['amount'] * 1000,
            'addInfo' => "BCOIN {$rechargeCode} {$bcoinAccount->username} {$input['amount']}",
            'format' => 'text',
            "template" => 'print'
        ];

        $response = $this->tryApi(
            fn() => $client->post($url, [
                'headers' => $headers,
                'json' => $body
            ])
        );

        $responseData = json_decode($response->getBody()->getContents(), true);

        $recharge = new BcoinRecharge();

        $recharge->fill([
            'wallet_id' => $bcoinAccount->wallet?->id,
            'amount' => $input['amount'],
            'user_id' => $bcoinAccount['id'],
            'status' => BcoinRecharge::STATUS_WAITING,
            'type' => $rechargeCode,
        ]);

        $recharge->save();

//        event(new RechargeCreated($recharge));

        return $httpResponse->responseData($responseData);
    }

    public function tryApi(\Closure $callback): ResponseInterface|JsonResponse
    {
        try {
            return $callback();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }
}
