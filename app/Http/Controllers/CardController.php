<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Helper\Object\CommentObject;
use App\Helper\Object\TransactionObject;
use App\Models\Card;
use App\Models\Classroom;
use App\Models\Comment;
use App\Models\Staff;
use App\Models\Student;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class CardController extends Controller
{
    public function list(Request $request)
    {
        Cache::put('search_card' . Auth::id(), $request->getQueryString());
        $mode = $request->get('mode') ?? Card::ORDER_BY_STUDENT;
        $crudBag = new CrudBag();
        $crudBag->setLabel('Thẻ học');
        $crudBag->setSearchValue($request->get('search'));

        $crudBag->setEntity('card');

        $this->handleColumn($request, $crudBag);

        $this->handleFiltering($crudBag, $request);

        $query = Card::query();

        if (Auth::user()->{'role'} == User::STUDENT_ROLE) {
            $query->where('student_id', Auth::id());
        }
        $crudBag->setParam(CrudBag::ORDER_MODE, $mode);
        $crudBag->setParam(CrudBag::BULK_CARD, true);
        $this->handleQuery($query, $crudBag);

        $this->handleStatistic($crudBag, $query, $request->get('perPage') ?? 10, $request->get('page') ?? 1);

        if ($request->get('renew:handle') == -1 || $request->get('renew:handle') == null) {
            $listViewModel = new ListViewModel($query->paginate($request->get('perPage') ?? 10));
        } else {
            $perPage = $request->get('perPage') ?? 10;

            $renewType = $request->get('renew:handle');

            $collection = $query->take($perPage)->get()->filter(function (Card $card) use ($renewType) {
                if ($renewType == 'S') {
                    return $card->getCanDegAttribute();
                }

                if ($renewType == 'A-B-C') {
                    return in_array($card->getRenewTypeAttribute(), ['A', 'B', 'C']);
                }

                return $card->getRenewTypeAttribute() == $renewType;
            });

            $currentPage = $request->get('page');

            $pagedData = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();

            $paginatedObjects = new Collection($pagedData);

            $paginator = new LengthAwarePaginator(
                $paginatedObjects,
                count($collection),
                $perPage,
                $currentPage
            );

            $listViewModel = new ListViewModel($paginator);
        }


        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => $listViewModel,
            'mode' => $request->get('mode') ?? '1'
        ]);
    }

    public function create()
    {
        return view('create', [
            'crudBag' => $this->handleFields()
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'van' => 'integer|nullable',
            'van_date' => 'date|nullable',
            'student_id' => 'integer|nullable|not_in:0',
            'classroom_id' => 'integer|nullable|not_in:0',
            'original_days' => 'string|required|min:1',
            'bonus_days' => 'string|nullable',
            'bonus_reason' => 'string|nullable',
            'original_fee' => 'string|required|min:1',
            'promotion_fee' => 'string|nullable|min:1',
            'fee_reason' => 'string|nullable',
            'payment_plant' => 'string|nullable',
            'drive_link' => 'string|nullable',
            'commitment' => 'string|nullable',
            'allow_deg' => 'nullable',
            'limit_deg' => 'string|nullable',
        ]);
        $dataToCreate = [
            'uuid' => Card::generateUUID(Auth::user()->{'branch'}),
            'branch' => Auth::user()->{'branch'},
            'van' => $request->{'van'} ?? 0,
            'van_date' => $request->{'van_date'},
            'student_id' => $request->{'student_id'},
            'classroom_id' => $request->{'classroom_id'},
            'original_days' => str_replace('.', '', $request->{'original_days'}),
            'bonus_days' => str_replace(',', '', $request->{'bonus_days'} ?? 0),
            'bonus_reason' => $request->{'bonus_reason'},
            'original_fee' => str_replace(',', '', $request->{'original_fee'}),
            'promotion_fee' => str_replace(',', '', $request->{'promotion_fee'} ?? 0),
            'fee_reason' => $request->{'fee_reason'},
            'payment_plant' => $request->{'payment_plant'},
            'drive_link' => $request->{'drive_link'},
            'commitment' => $request->{'commitment'},
        ];

        if ($request->get('allow_deg') == 1) {
            $dataToCreate['allow_deg'] = '1';
            $dataToCreate['limit_deg'] = $request->get('limit_deg') ?? 0;
        }

        Card::query()->create($dataToCreate);
        $card_href_ = Cache::get('search_card' . Auth::id()) ?? "";
        return redirect('/card/list?=&' . $card_href_)->with('success', 'Thêm mới thành công');
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, int $id)
    {
        $card = Card::query()->where('id', $id)->firstOrFail();

        $this->validate($request, [
            'van' => 'integer|nullable',
            'van_date' => 'date|nullable',
            'student_id' => 'integer|nullable|not_in:0',
            'classroom_id' => 'integer|nullable|not_in:0',
            'original_days' => 'string|required|min:1',
            'bonus_days' => 'string|nullable',
            'bonus_reason' => 'string|nullable',
            'original_fee' => 'string|required|min:1',
            'promotion_fee' => 'string|nullable|min:1',
            'fee_reason' => 'string|nullable',
            'payment_plant' => 'string|nullable',
            'drive_link' => 'string|nullable',
            'commitment' => 'string|nullable',
            'allow_deg' => 'nullable',
            'limit_deg' => 'string|nullable',
        ]);

        $dataToUpdate = [
            'branch' => Auth::user()->{'branch'},
            'van' => $request->{'van'} ?? 0,
            'van_date' => $request->{'van_date'},
            'student_id' => $request->{'student_id'},
            'classroom_id' => $request->{'classroom_id'},
            'original_days' => str_replace('.', '', $request->{'original_days'}),
            'bonus_days' => str_replace(',', '', $request->{'bonus_days'} ?? 0),
            'bonus_reason' => $request->{'bonus_reason'},
            'original_fee' => str_replace(',', '', $request->{'original_fee'}),
            'promotion_fee' => str_replace(',', '', $request->{'promotion_fee'} ?? 0),
            'fee_reason' => $request->{'fee_reason'},
            'payment_plant' => $request->{'payment_plant'},
            'drive_link' => $request->{'drive_link'},
            'commitment' => $request->{'commitment'},
        ];

        if ($request->get('allow_deg') == 1) {
            $dataToUpdate['allow_deg'] = '1';
            $dataToUpdate['limit_deg'] = $request->get('limit_deg') ?? 0;
        }

        $card->update(array_filter($dataToUpdate, function ($value) {
            return $value != null;
        }));
        $card_href_ = Cache::get('search_card' . Auth::id()) ?? "";
        return redirect('/card/list?=&' . $card_href_)->with('success', 'Cập nhật thành công');
    }

    public function delete(int $id)
    {
        $card = Card::query()->where('id', $id)->where('branch', Auth::user()->{'branch'})->firstOrFail();
        $card->delete();

        return redirect()->to('/card/list')->with('success', 'Xoá thành công');
    }

    public function show(int $id)
    {
        $card = Card::query()->where('id', $id)->firstOrFail();

        $commentRecords = Comment::query()
            ->where('object_id', $id)
            ->where('object_type', Comment::CARD_COMMENT)
            ->orderBy('created_at', 'desc')->get();

        $comments = $commentRecords->map(function (Comment $comment) {
            return new CommentObject(
                user_id: $comment['user_id'],
                user_name: $comment->user?->name,
                user_avatar: $comment->user?->avatar,
                comment_time: Carbon::parse($comment['created_at'])->toDateTimeLocalString('minutes'),
                type: $comment['type'],
                content: $comment['content'],
                self: $comment['user_id'] == Auth::id()
            );
        });

        $builder = Transaction::query()->where('object_type', 'card')
            ->where('object_id', $id);

        $newTransactionCount = $builder->clone()->where('status', 0)->count();

        $transactions = $builder->where('created_by', '!=', 0)
            ->orderBy('created_at', 'DESC')
            ->get()
            ->map(fn(Transaction $transaction) => new TransactionObject(
                id: $transaction['id'],
                uuid: $transaction['uuid'],
                type: $transaction['transaction_type'],
                note: $transaction['notes'],
                amount: $transaction['amount'],
                new: $transaction['status'] == 0,
                accepted: $transaction['status'] == 1,
                created_at: Carbon::parse($transaction['created_at'])->isoFormat('DD/MM/YYYY HH:mm:ss'),
                image: $transaction['object_image'],
                creator_name: $transaction->creator?->name ?? "SYS_ADMIN",
                creator_uuid: $transaction->creator?->uuid ?? "",
                creator_avatar: $transaction->creator?->avatar ?? "https://www.belienglish.com/assets/images/3.png",
                created_by: $transaction['created_by'],
                approve_name: $transaction->approver()?->first()?->name ?? "",
                approve_avatar: $transaction->approver()?->first()?->avatar ?? "",
                transaction_day: $transaction['transaction_day'] != null ?
                    Carbon::parse($transaction['transaction_day'])->isoFormat('DD/MM/YYYY HH:mm:ss') :
                    Carbon::parse($transaction['created_at'])->isoFormat('DD/MM/YYYY HH:mm:ss'),
            )
            )->toArray();

        return view('cards.show', [
            'card' => $card,
            'comments' => $comments,
            'transactions' => $transactions,
            'newTransactionCount' => $newTransactionCount
        ]);
    }

    public function edit(int $id)
    {
        return view('create', [
            'crudBag' => $this->handleFields($id)
        ]);
    }

    private function handleQuery(Builder $query, CrudBag $crudBag): void
    {
        if ($crudBag->getSearchValue() != '') {
            $searchValue = '%' . $crudBag->getSearchValue() . '%';

            $query->where(function ($subQuery) use ($searchValue) {
                $subQuery->where('uuid', 'like', $searchValue);
            });

            $query->orWhereHas('Student', function ($subQuery) use ($searchValue) {
                $subQuery->where('name', 'like', $searchValue)
                    ->orWhere('uuid', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('profile', function (Builder $builder) use ($searchValue) {
                        $builder->where('english_name', 'like', "%{$searchValue}%");
                    });;
            });
        }

        foreach ($crudBag->getFilters() as $filter) {
            if ($filter->getValue() == "") {
                continue;
            }
            switch ($filter->getName()) {
                case 'paid_status:handle':
                    if ($filter->getValue() == Card::UNPAID_SELECT) {
                        $query->whereColumn('paid_fee', '<', DB::raw('original_fee - promotion_fee'));
                    } elseif ($filter->getValue() == Card::PAID_SELECT) {
                        $query->whereColumn('paid_fee', '>=', DB::raw('original_fee - promotion_fee'));
                    }
                    break;
                case 'classroom':
                    $query->where('classroom_id', $filter->getValue());
                    break;
                case 'staff';
                    $query->whereHas('Classroom', function (Builder $subQuery) use ($filter) {
                        $subQuery->where('staff_id', $filter->getValue());
                    });
                    break;
                case 'teacher':
                    $query->whereHas('Classroom', function (Builder $subQuery) use ($filter) {
                        $subQuery->whereHas('Shifts', function (Builder $builder) use ($filter) {
                            $builder->where('teacher_id', $filter->getValue());
                        });
                    });
                    break;
                case 'supporter':
                    $query->whereHas('Classroom', function (Builder $subQuery) use ($filter) {
                        $subQuery->whereHas('Shifts', function (Builder $builder) use ($filter) {
                            $builder->where('supporter_id', $filter->getValue());
                        });
                    });
                    break;
                default:
                    break;
            }
        }
        if ($crudBag->getParam(CrudBag::ORDER_MODE) == Card::ORDER_BY_STUDENT) {

            $query->orderBy('student_id', 'asc');
            $query->orderBy('id', 'DESC');

        } else {
            $query->orderBy('id', 'desc');
        }
    }

    private function handleColumn(Request $request, CrudBag &$crudBag, Card $card = null): void
    {
        /**
         * Mã thẻ học + Trạng thái thẻ học $uuid + $card_status
         * Học sinh được thụ hưởng (mã, ảnh , tên, tên tiếng anh) $student_id
         * Lớp đang được xếp (mã , ảnh, tên ) $classroom_id
         * Tổng số buổi đăng ký $total_days
         * Tổng số tiền cần thanh toán $total_fee
         * Đơn giá một buổi $daily_fee
         * Tổng số tiền đã thanh toán $paid_fee
         * Số buổi được sử dụng theo thanh toán $can_use_day_by_paid
         * Số tiền chưa thanh toán $unpaid_fee
         * Van và số buổi bị trừ khi điểm danh $van + $attended_days
         * Số buổi còn lại có thể sử dụng $can_use_day
         *  Phân loại renew $renew_type
         * Số học phí còn lại có thể sử dụng $can_use_fee
         * Điểm feedback $feedback_score
         *  Ngày tháng năm lấy feedback $latest_feedback_date
         * Trạng thái tiến độ sale $sale_status
         * Ngày tháng năm cập nhật sale  $sale_updated_at
         */
//        $crudBag->addColumn([
//            'name' => 'uuid',
//            'label' => 'Thẻ học',
//            'type' => 'group-card',
//            'fixed' => 'first',
//            'attributes' => [
//                'edit' => true,
//                'entity' => 'card',
//                'transaction' => true,
//                'show' => true
//            ],
//        ]);

        $crudBag->addColumn([
            'name' => 'student_id',
            'fixed' => 'first',
            'label' => 'Học sinh',
            'type' => 'card-head',
            'attributes' => [
                'entity' => 'student_entity',
                'model' => 'student',
                'id' => 'id',
                'avatar' => 'avatar',
                'name' => 'name',
                'uuid' => 'uuid',
                'card_id' => 'card_id',
                'card_uuid' => 'card_uuid'
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'classroom_id',
            'label' => 'Lớp đang được xếp',
            'type' => 'entity',
            'attributes' => [
                'entity' => 'classroom_entity',
                'model' => 'classroom',
                'id' => 'id',
                'name' => 'name',
                'uuid' => 'uuid',
                'avatar' => 'avatar',
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'last_be_attendance',
            'type' => 'last_be_attendance',
            'label' => 'Điểm danh lần cuối',
        ]);

        $crudBag->addColumn([
            'name' => 'total_days',
            'label' => 'Tổng số buổi đăng ký',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'total_fee',
            'label' => 'Tổng số tiền cần thanh toán',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'daily_fee',
            'label' => 'Đơn giá một buổi',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'paid_fee',
            'label' => 'Tổng số đã thanh toán',
            'type' => 'card_transaction_logs'
        ]);

        $crudBag->addColumn([
            'name' => 'can_use_day_by_paid',
            'label' => 'Số buổi được sử dụng theo thanh toán',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'unpaid_fee',
            'label' => 'Số tiền chưa thanh toán',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'van_and_attended_days',
            'label' => 'Van và số buổi bị trừ khi điểm danh',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'last_be_attendance',
            'label' => 'Ngày điểm danh gần nhất',
            'type' => 'text'
        ]);

        $crudBag->addColumn([
            'name' => 'van_date',
            'label' => 'Ngày chốt điểm danh ở hệ thống cũ',
            'type' => 'date'
        ]);

        $crudBag->addColumn([
            'name' => 'can_use_day',
            'label' => 'Số buổi còn lại có thể sử dụng',
            'type' => 'number'
        ]);
        /**
         * <0 buổi = SOS: màu đỏ
         * =0 buổi = X: màu nâu
         *
         * 0-6 buổi = A: màu tím
         *
         * 7-12 buổi = B: màu xanh lá
         *
         * 13-24 buổi = C: màu xanh lá nhạt
         *
         * 25-48 buổi = D: màu xám
         *
         * >48 buổi = E: màu trắng
         */
        $crudBag->addColumn([
            'name' => 'renew_type',
            'label' => 'Phân loại renew',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    'SOS' => 'SOS',
                    'X' => 'X',
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                ],
                'bg' => [
                    'SOS' => 'bg-danger card-danger',
                    'X' => 'bg-info',
                    'A' => 'bg-warning',
                    'B' => 'bg-success',
                    'C' => 'bg-orange',
                    'D' => 'bg-purple',
                    'E' => 'bg-secondary',
                ],
                'tooltips' => [
                    'SOS' => 'SOS: Ít hơn 0 buổi (học quá số buổi cho phép)',
                    'X' => 'X: bằng 0 (Đã hết số buổi )',
                    'A' => 'A: ít hơn 7 buổi (cần thông báo tái tục renew)',
                    'B' => 'B: ít hơn 13 buổi (cần feedback để chuân bị renew) ',
                    'C' => 'C: Ít hơn 25 buổi (cần lấy feedback để kịp khiến học sinh thích học và tiến bộ',
                    'D' => 'D: Từ 25 đến 48 buổi',
                    'E' => 'E: Còn trên 48 buổi',
                ]
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'can_use_fee',
            'label' => 'Số học phí còn lại có thể sử dụng',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'feedback_score',
            'label' => 'Điểm feedback',
            'type' => 'text'
        ]);

        $crudBag->addColumn([
            'name' => 'latest_feedback_date',
            'label' => 'Ngày tháng năm lấy feedback',
            'type' => 'text'
        ]);

        $crudBag->addColumn([
            'name' => 'sale_status',
            'label' => 'Trạng thái tiến độ sale',
            'type' => 'text'
        ]);

        $crudBag->addColumn([
            'name' => 'sale_updated_at',
            'label' => 'Ngày tháng năm cập nhật sale',
            'type' => 'text'
        ]);

//        return $crudBag;
    }

    private function handleFields(int $id = null)
    {
        $crudBag = new CrudBag();

        if ($id) {
            /**
             * @var Card $card
             */
            $card = Card::query()->where('id', $id)->where('branch', Auth::user()->{'branch'})->firstOrFail();
            $crudBag->setId($id);
        }

        $crudBag->setAction(isset($card) ? "card.update" : 'card.store');
        $crudBag->setLabel('Thẻ học');
        $crudBag->setHasFile(true);

        $crudBag->addFields([
            'name' => 'van',
            'label' => 'Số buổi đã dùng trước khi sử dụng BSM',
            'type' => 'text',
            'value' => $card['van'] ?? null
        ]);
        $crudBag->addFields([
            'name' => 'van_date',
            'label' => 'Ngày chốt điểm danh ở hệ thống cũ (VAN)',
            'type' => 'date',
            'value' => $card['van_date'] ?? null
        ]);

        $studentSelects = Student::query()->get()->mapWithKeys(function ($student) {
            $englishName = $student->english_name ?? '';
            $englishNamePrefix = $englishName ? '-' : '';

            return [$student->id => $student->uuid . "-" . $student->name . $englishNamePrefix . $englishName];
        })->all();

        $classroomSelects = Classroom::query()->get(['name', 'id', 'uuid'])->mapWithKeys(function ($classroom) {
            return [$classroom->id => $classroom->uuid . "-" . $classroom->name];
        })->all();

        $crudBag->addFields([
            'name' => 'student_id',
            'label' => 'Học sinh gắn với thẻ học',
            'type' => 'select',
            'nullable' => true,
            'options' => $studentSelects,
            'placeholder' => 'Học sinh gắn với thẻ học',
            'value' => $card->student_id ?? null
        ]);

        $crudBag->addFields([
            'name' => 'classroom_id',
            'label' => 'Lớp học gắn với thẻ học',
            'type' => 'select',
            'nullable' => true,
            'options' => $classroomSelects,
            'placeholder' => 'Học sinh gắn với thẻ học',
            'value' => $card->classroom_id ?? null
        ]);

        $crudBag->addFields([
            'name' => 'drive_link',
            'label' => 'Lên PDF đơn đăng ký',
            'type' => 'text',
            'value' => $card->drive_link ?? null
        ]);

        $crudBag->addFields([
            'name' => 'commitment',
            'label' => 'Cam kết đầu ra nếu có',
            'type' => 'text',
            'value' => $card->commitment ?? null
        ]);

        $crudBag->addFields([
            'name' => 'original_days',
            'label' => 'Số buổi đăng ký gốc',
            'type' => 'number',
            'suffix' => 'buổi',
            'required' => true,
            'class' => 'col-4 mb-2',
            'value' => $card->original_days ?? null
        ]);

        $crudBag->addFields([
            'name' => 'bonus_days',
            'label' => 'Số buổi được tặng thêm',
            'type' => 'number',
            'suffix' => 'buổi',
            'class' => 'col-3 mb-2',
            'value' => $card->bonus_days ?? null
        ]);

        $crudBag->addFields([
            'name' => 'total_days',
            'label' => 'Số buổi thực tế đăng ký',
            'type' => 'handle',
            'suffix' => 'buổi',
            'class' => 'col-3 mb-2',
            'attributes' => [
                'js' => asset('/demo/js/handle-total-day.js'),
                'identity' => 'total_days'
            ],
            'value' => $card->total_days ?? null
        ]);

        $crudBag->addFields([
            'name' => 'bonus_reason',
            'label' => 'Lý do tặng',
            'type' => 'textarea',
            'class' => 'col-10 mb-2',
            'value' => $card->bonus_reason ?? null
        ]);

        $crudBag->addFields([
            'name' => 'original_fee',
            'label' => 'Học phí gốc',
            'type' => 'number',
            'required' => true,
            'suffix' => 'đ',
            'value' => $card->original_fee ?? null
        ]);

        $crudBag->addFields([
            'name' => 'promotion_fee',
            'label' => 'Học phí ưu đãi',
            'type' => 'number',
            'required' => false,
            'suffix' => 'đ',
            'class' => 'col-3 mb-2',
            'value' => $card->promotion_fee ?? null
        ]);

        $crudBag->addFields([
            'name' => 'promotion_percent',
            'label' => '% Ưu đãi',
            'type' => 'handle',
            'class' => 'col-2 mb-2',
            'attributes' => [
                'js' => asset('/demo/js/handle-promotion-percent.js'),
                'identity' => 'promotion-percent'
            ],
            'value' => isset($card) ? $card->calPromotionPercent() : null
        ]);

        $crudBag->addFields([
            'name' => 'fee_reason',
            'label' => 'Lý do ưu đãi',
            'type' => 'textarea',
            'class' => 'col-10 mb-2',
            'value' => $card->fee_reason ?? null
        ]);

        $crudBag->addFields([
            'name' => 'total_fee',
            'label' => 'Học phí thực tế cần đóng',
            'type' => 'handle',
            'attributes' => [
                'js' => asset('/demo/js/handle-total-fee.js'),
                'identity' => 'total_fee'
            ],
            'value' => number_format($card->total_fee ?? 0)
        ]);

        $crudBag->addFields([
            'name' => 'daily_fee',
            'label' => 'Đơn giá buổi học',
            'type' => 'handle',
            'attributes' => [
                'js' => asset('/demo/js/handle-daily-fee.js'),
                'identity' => 'daily_fee'
            ],
            'value' => number_format($card->daily_fee ?? 0)
        ]);

        $crudBag->addFields([
            'name' => 'payment_plant',
            'label' => 'Kế hoạch thanh toán (nếu có)',
            'type' => 'textarea',
            'class' => 'col-10 mb-2',
            'value' => $card->payment_plan ?? null
        ]);

        $crudBag->addFields([
            'name' => 'allow_deg',
            'type' => 'checkbox',
            'label' => 'Cho phép điểm danh âm',
            'value' => $card->allow_deg ?? null,
            'class' => 'col-10 mb-2'
        ]);

        $crudBag->addFields([
            'name' => 'limit_deg',
            'type' => 'select',
            'label' => 'Số buổi được phép âm',
            'value' => $card->limit_deg ?? 0,
            'options' => [
                0 => 'Không chọn',
                5 => '5 buổi',
                10 => '10 buổi',
                15 => '15 buổi',
                20 => '20 buổi',
                30 => '30 buổi',
                40 => '40 buổi',
                50 => '50 buổi',
                75 => '75 buổi',
                100 => '100 buổi',
                999999 => 'Không giới hạn',
            ]
        ]);

        return $crudBag;
    }

    public function handleFiltering(CrudBag &$crudBag, Request $request)
    {
        $crudBag->addFilter([
            'name' => 'paid_status:handle',
            'value' => $request->get('paid_status:handle') ?? -1,
            'label' => 'Trạng thái thanh toán',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    Card::UNPAID_SELECT => 'Chưa thanh toán hết',
                    Card::PAID_SELECT => 'Đã thanh toán hết'
                ]
            ],
        ]);

        $crudBag->addFilter([
            'name' => 'renew:handle',
            'value' => $request->get('renew:handle') ?? -1,
            'label' => 'Trạng thái thanh toán',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    Card::RENEW_SOS => 'SOS',
                    Card::RENEW_X => 'X',
                    Card::RENEW_A => 'A',
                    Card::RENEW_B => 'B',
                    Card::RENEW_C => 'C',
                    Card::RENEW_D => 'D',
                    Card::RENEW_E => 'E',
                    'S' => 'SOS (Có thể điểm danh)'
                ]
            ],
        ]);

        $crudBag->addFilter([
            'name' => 'mode',
            'value' => $request->get('mode') ?? Card::ORDER_BY_STUDENT,
            'label' => 'Chế độ xem',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    Card::ORDER_BY_STUDENT => 'Xem theo học sinh',
                    Card::ORDER_BY_CARD => 'Xem theo thẻ học'
                ]
            ],
        ]);

        $crudBag->addFilter([
            'name' => 'classroom',
            'value' => $request->get('classroom'),
            'label' => 'Lớp',
            'type' => 'select2',
            'attributes' => [
                'options' => Classroom::query()->get()->mapWithKeys(function (Classroom $classroom) {
                    return [$classroom->{'id'} => "{$classroom->uuid} - $classroom->name"];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'staff',
            'value' => $request->get('staff'),
            'label' => 'Nhân viên',
            'type' => 'select2',
            'attributes' => [
                'options' => Staff::query()->get()->mapWithKeys(function (Staff $staff) {
                    return [$staff->{'id'} => "{$staff->uuid} - $staff->name"];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'teacher',
            'value' => $request->get('teacher'),
            'label' => 'Giáo viên',
            'type' => 'select2',
            'attributes' => [
                'options' => Teacher::query()->get()->mapWithKeys(function (Teacher $staff) {
                    return [$staff->{'id'} => "{$staff->uuid} - $staff->name"];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'supporter',
            'value' => $request->get('supporter'),
            'label' => 'Trợ giảng',
            'type' => 'select2',
            'attributes' => [
                'options' => Supporter::query()->get()->mapWithKeys(function (Supporter $staff) {
                    return [$staff->{'id'} => "{$staff->uuid} - $staff->name"];
                })
            ]
        ]);
    }

    private function handleRenewStatus(Builder &$query, $lte = -10000, $gte = 999999999)
    {
//        $query
//            ->selectRaw("*, ((paid_fee / (original_fee - promotion_fee) * (original_days + bonus_days)) - van - (SELECT COUNT(*) FROM card_logs WHERE card_logs.card_id = cards.id AND card_logs.day = 1)) as ME")
//            ->whereRaw("((paid_fee / (original_fee - promotion_fee) * (original_days + bonus_days)) - van - (SELECT COUNT(*) FROM card_logs WHERE card_logs.card_id = cards.id AND card_logs.day = 1)) $gte")
//            ->whereRaw("((paid_fee / (original_fee - promotion_fee) * (original_days + bonus_days)) - van - (SELECT COUNT(*) FROM card_logs WHERE card_logs.card_id = cards.id AND card_logs.day = 1)) $lte")
//            ->whereNull('cards.deleted_at')
//            ->where('branch', 'CN.0002');
//        dd($query->toSql());
    }

    private function handleStatistic(CrudBag &$crudBag, Builder $query, int $perPage, int $page)
    {
        $count = [];
        /**
         * @var Card $card
         */
        foreach ($query->take($perPage)->skip(($page - 1) * $perPage)->get() as $card) {
            $count[$card->getRenewTypeAttribute()][] = $card;
        }
        /**
         * 'SOS' => 'SOS: Ít hơn 0 buổi (học quá số buổi cho phép)',
         * 'X' => 'X: bằng 0 (Đã hết số buổi )',
         * 'A' => 'A: ít hơn 7 buổi (cần thông báo tái tục renew)',
         * 'B' => 'B: ít hơn 13 buổi (cần feedback để chuân bị renew) ',
         * 'C' => 'C: Ít hơn 25 buổi (cần lấy feedback để kịp khiến học sinh thích học và tiến bộ',
         * 'D' => 'D: Từ 25 đến 48 buổi',
         * 'E' => 'E: Còn trên 48 buổi',
         */
        $crudBag->addStatistic([
            'label' => 'SOS',
            'value' => count($count['SOS'] ?? []) ?? 0,
            'badge' => 'Số buổi ít hơn 0',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'X',
            'value' => count($count['X'] ?? []) ?? 0,
            'badge' => 'Số buổi bằng 0',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'A',
            'value' => count($count['A'] ?? []) ?? 0,
            'badge' => 'Ít hơn 7 buổi ',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'B',
            'value' => count($count['B'] ?? []) ?? 0,
            'badge' => 'Ít hơn 13 buổi',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'C',
            'value' => count($count['C'] ?? []) ?? 0,
            'badge' => 'Ít hơn 25 buổi ',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'D',
            'value' => count($count['D'] ?? []) ?? 0,
            'badge' => 'Từ 25 đến 48 buổi',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'E',
            'value' => count($count['E'] ?? []) ?? 0,
            'badge' => 'Còn trên 48 buổi',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);


    }

    public function bulkCloneCard(): View
    {
        return view("cards.react");
    }

    public function bulkAddCardToClassroom(): View
    {
        return view("cards.react");
    }
}
