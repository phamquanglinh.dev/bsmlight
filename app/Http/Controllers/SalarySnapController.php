<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Helper\Object\CommentObject;
use App\Helper\Object\SalarySnapObject;
use App\Models\Classroom;
use App\Models\Comment;
use App\Models\RequestEdit;
use App\Models\Salary;
use App\Models\SalaryGroup;
use App\Models\SalarySheet;
use App\Models\SalarySnap;
use App\Models\SalarySnapDetail;
use App\Models\Student;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class SalarySnapController extends Controller
{
    public function all(Request $request): Factory|\Illuminate\Contracts\View\View|Application
    {
        $crudBag = new CrudBag();
        $crudBag->setLabel('Tính thẻ lương');
        $crudBag->setSearchValue($request->get('search'));
        $crudBag->setEntity('salary-snap');
        $this->handleColumn($request, $crudBag);
        $crudBag->setParam('disable_all_top_btn', true);
        $this->handleFilter($crudBag, $request);
        $query = SalarySnap::query();

        $this->handleQuery($query, $crudBag);

        $this->handleStatistic($query, $crudBag);

        $listViewModel = new ListViewModel($query->paginate($request->get('perPage') ?? 10));

        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => $listViewModel
        ]);
    }

    public function list(Request $request)
    {
        $salarySheetId = $request->get('salary_sheet_id');
        $periodMonth = $request->get('period_month') ?? Carbon::now()->month;

        $period = Carbon::create(Carbon::now()->year, $periodMonth, 1);

        $salarySheet = SalarySheet::query()->where('id', $salarySheetId)->firstOrFail();
        $salarySnaps = SalarySnap::query()->where([
            'salary_sheet_id' => $salarySheetId,
            'period' => $period->toDateString()
        ])->get();

        return view("salary-snap.list", [
            'salarySnaps' => $salarySnaps,
            'salarySheet' => $salarySheet,
            'periodMonth' => $periodMonth
        ]);
    }

    public function show(Request $request, int $id = null): View
    {
        $salary = null;

        /**
         * @var SalarySnap $salarySnap
         */
        $periodMonth = $request->get('period_month') ?? Carbon::now()->month;
        $userId = $request->get('user_id') ?? Auth::id();

        $period = Carbon::create(Carbon::now()->year, $periodMonth, 1);

        if ($id) {
            $salarySnap = SalarySnap::query()->where('id', $id)->firstOrFail();
        } else {
            $salarySnap = SalarySnap::query()->where([
                'period' => $period,
                'user_id' => $userId
            ])->firstOrFail();
        }

        if ($request->get('close')) {
            /**
             * @var Salary $salary
             */
            $salary = Salary::query()->where('uuid', $salarySnap['uuid'])->firstOrFail();

            $salarySnapObject = $this->handleMakeSalarySnapObject($salary);

        } else {
            $salarySnapObject = new SalarySnapObject(
                uuid: $salarySnap['uuid'],
                snap_salary_rate: $salarySnap->snap_salary_rate,
                snap_salary_fund_percent: $salarySnap->snap_salary_fund_percent,
                period: $salarySnap->period,
                salary_sheet_id: $salarySnap->salary_sheet_id,
                user_id: $salarySnap->user_id,
                user: $salarySnap->User()->first(),
                classroom_id: $salarySnap->classroom_id,
                snap_salary_kpi: $salarySnap->snap_salary_kpi,
                fund_value: $salarySnap->fund_value,
                total_value: $salarySnap->total_value,
                salarySnapDetails: $salarySnap->SalarySnapDetails(),
                branch: $salarySnap->branch,
                snap_salary_value: $salarySnap->snap_salary_value
            );
        }

        $comments = Comment::query()->where([
            'object_id' => $salarySnap['id'],
            'object_type' => Comment::SALARY_SNAP_COMMENT
        ])->get()->map(fn(Comment $comment) => new CommentObject(
            user_id: $comment['user_id'],
            user_name: $comment->user?->name,
            user_avatar: $comment->user?->avatar,
            comment_time: \Illuminate\Support\Carbon::parse($comment['created_at'])->toDateTimeLocalString('minutes'),
            type: $comment['type'],
            content: $comment['content'],
            self: $comment['user_id'] == Auth::id()
        ));

        return view("salary-snap.show", [
            'salarySnap' => $salarySnap,
            'salarySnapObject' => $salarySnapObject,
            'periodMonth' => $periodMonth,
            'comments' => $comments,
            'close' => $request->get('close') ?? false,
            'closeTime' => $salary ? $salary->created_at : null
        ]);
    }

    public function edit(Request $request, int $id = null)
    {
        $salarySnap = SalarySnap::query()->where('id', $id)->firstOrFail();

        $crudBag = new CrudBag();

        if (Auth::user()->{'role'} != User::HOST_ROLE) {
            $crudBag->setParam(CrudBag::CUSTOM_LABEL_ACTION_BTN, 'Yêu cầu chỉnh sửa');
        } else {
            $crudBag->setParam(CrudBag::CUSTOM_LABEL_ACTION_BTN, 'Chỉnh sửa');
        }

        $crudBag->setId($id);

        $crudBag->setAction('salary-snap.request');

        $crudBag->setLabel('Tinh the luong');

        $crudBag->addFields([
            'name' => 'snap_salary_rate',
            'label' => 'Lương theo giờ',
            'type' => 'text',
            'value' => $salarySnap->{'snap_salary_rate'}
        ]);

        $crudBag->addFields([
            'name' => 'snap_salary_fund_percent',
            'label' => 'Quỹ gắn bó',
            'type' => 'text',
            'value' => $salarySnap->{'snap_salary_fund_percent'}
        ]);

        return \view('create', [
            'crudBag' => $crudBag
        ]);
    }

    public function request(Request $request, int $id)
    {
        if (Auth::user()->{'role'} != User::HOST_ROLE) {
            RequestEdit::query()->create([
                'status' => 1,
                'salary_id' => $id,
                'snap_salary_rate' => $request->get('snap_salary_rate'),
                'snap_salary_fund_percent' => $request->get('snap_salary_fund_percent'),
                'user_id' => Auth::id(),
            ]);
        } else {
            /**
             * @var SalarySnap $salarySnap
             */
            $salarySnap = SalarySnap::query()->where('id', $id)->firstOrFail();

            if ($salarySnap->snap_salary_value != 0) {
                $time = $salarySnap->snap_salary_value / $salarySnap->snap_salary_rate;
                $salarySnap->snap_salary_value = $time * $request->get('snap_salary_rate');
            }

            $salarySnap->snap_salary_rate = $request->get('snap_salary_rate');

            $salarySnap->snap_salary_fund_percent = $request->get('snap_salary_fund_percent');

            $salarySnap->save();
        }

        return redirect('/salary-snap/all')->with('success', 'Thành công');
    }

    public function manualUpdate(Request $request)
    {
        /**
         * @var SalarySnapDetail $salarySnapDetail
         */
        $salarySnapDetail = SalarySnapDetail::query()->where('id', $request->get('id'))->firstOrFail();

        $salarySnapDetail->update([
            'snap_value' => $request->get('snap_value')
        ]);
        /**
         * @var SalarySnap $salarySnap
         */
        $salarySnap = SalarySnap::query()->where([
            'user_id' => $salarySnapDetail->user_id,
            'salary_sheet_id' => $salarySnapDetail->salary_sheet_id,
            'period' => $salarySnapDetail->period
        ])->firstOrFail();

        $salaryKpi = $salarySnap->getSnapSalaryKpiAttribute();
        $total = $salarySnap->getTotalValueAttribute();

        return response()->json([
            'amount' => number_format($salarySnapDetail->getAmountAttribute()) . ' đ',
            'value' => number_format($salarySnapDetail->snap_value),
            'salary_kpi' => number_format($salaryKpi) . " đ",
            'total' => number_format($total) . " đ",
        ]);
    }

    public function closeSalarySnap(int $id)
    {
        /**
         * uuid: $salary['snap_salary_uuid'],
         * snap_salary_rate: $salary['salary_rate'],
         * snap_salary_fund_percent: $salary['salary_fund'],
         * period:  $salary['period'],
         * salary_sheet_id:  0,
         * user_id:  $salary['user_id'],
         * user: User::query()->where('id',$salary['user_id'])->firstOrFail(),
         * classroom_id:  0,
         * snap_salary_kpi:  $salary['snap_salary_kpi'],
         * fund_value:  $salary['salary_fund'],
         * total_value:  $salary['salary_actually_total'],
         * salarySnapDetails:  Collection::make($salarySnapDetail),
         * branch:  $salary['branch'],
         * snap_salary_value:  $salary['snap_salary_value']
         */
        /**
         * @var SalarySnap $salarySnap
         */
        $salarySnap = SalarySnap::query()->where('id', $id)->firstOrFail();

        $snapDetails = $salarySnap->SalarySnapDetails()->map(fn(SalarySnapDetail $salarySnapDetail) => [
            'salary_sheet_detail_id' => $salarySnapDetail['salary_sheet_detail_id'],
            'snap_criteria_name' => $salarySnapDetail->snap_criteria_name,
            'snap_criteria_type' => $salarySnapDetail->snap_criteria_type,
            'snap_criteria_code' => $salarySnapDetail->snap_criteria_code,
            'snap_criteria_rate' => $salarySnapDetail->snap_criteria_rate,
            'user_id' => $salarySnapDetail['user_id'],
            'salary_sheet_id' => $salarySnapDetail['salary_sheet_id'],
            'snap_value' => $salarySnapDetail['snap_value'],
            'snap_unit_type' => $salarySnapDetail['snap_unit_type'],
            'period' => $salarySnapDetail['period'],
            'branch' => $salarySnapDetail['branch'],
            'classroom_id' => $salarySnapDetail['classroom_id']

        ])->toArray();

        $fundId = $this->makeOfGetGroupId($salarySnap['user_id'], $salarySnap['period']);

        $dataToClose = [
            'uuid' => $salarySnap['uuid'],
            'snap_salary_rate' => $salarySnap['snap_salary_rate'],
            'snap_salary_fund_percent' => $salarySnap['snap_salary_fund_percent'],
            'period' => $salarySnap['period'],
            'salary_sheet_id' => $salarySnap['salary_sheet_id'],
            'user_id' => $salarySnap['user_id'],
            'classroom_id' => $salarySnap['classroom_id'],
            'snap_salary_kpi' => $salarySnap->snap_salary_kpi,
            'fund_value' => $salarySnap->fund_value,
            'total_value' => $salarySnap->total_value,
            'salary_snap_details' => json_encode($snapDetails),
            'branch' => $salarySnap['branch'],
            'snap_salary_value' => $salarySnap['snap_salary_value'],
            'salary_group_id' => $fundId
        ];

        Salary::query()->where('uuid', $salarySnap['uuid'])->delete();

        Salary::query()->create($dataToClose);

        $dataToCreate = [
            'user_id' => Auth::id(),
            'object_type' => Comment::SALARY_SNAP_COMMENT,
            'object_id' => $salarySnap->id,
            'content' => 'Chốt thẻ lương',
            'type' => Comment::LOG_TYPE,
        ];

        Comment::query()->create($dataToCreate);

        return redirect()->back()->with('success', 'Chốt thành công');
    }

    private function handleMakeSalarySnapObject(Salary $salary)
    {
        $salarySnapDetail = array_map(function ($data) {
            $detail = new SalarySnapDetail();
            foreach ($data as $property => $value) {
                $detail->{$property} = $value;
            }
            return $detail;
        }, json_decode($salary['salary_snap_details'], 1));

        return new SalarySnapObject(
            uuid: $salary['uuid'],
            snap_salary_rate: $salary['snap_salary_rate'],
            snap_salary_fund_percent: $salary['snap_salary_fund_percent'],
            period: $salary['period'],
            salary_sheet_id: $salary['salary_sheet_id'],
            user_id: $salary['user_id'],
            user: User::query()->where('id', $salary['user_id'])->firstOrFail(),
            classroom_id: $salary['classroom_id'],
            snap_salary_kpi: $salary['snap_salary_kpi'],
            fund_value: $salary['fund_value'],
            total_value: $salary['total_value'],
            salarySnapDetails: Collection::make($salarySnapDetail),
            branch: $salary['branch'],
            snap_salary_value: $salary['snap_salary_value']
        );
    }

    private function handleColumn(Request $request, CrudBag &$crudBag)
    {
        $crudBag->addColumn([
            'name' => 'uuid',
            'label' => 'Thẻ lương',
            'type' => 'group_salary',
            'attributes' => [
                'entity' => 'salary-snap',
                'show' => true,
                'edit' => true
            ],
            'fixed' => 'first'
        ]);

        $crudBag->addColumn([
            'name' => 'classroom_id',
            'label' => 'Lớp đang được áp dụng',
            'type' => 'entity',
            'attributes' => [
                'entity' => 'classroom_entity',
                'model' => 'classroom',
                'id' => 'id',
                'name' => 'name',
                'uuid' => 'uuid',
                'avatar' => 'avatar',
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'period',
            'label' => 'Tháng áp dụng'
        ]);

        $crudBag->addColumn([
            'name' => 'snap_salary_rate',
            'label' => 'Lương theo giờ',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'snap_salary_value',
            'label' => 'Lương gốc được nhận',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'snap_salary_kpi',
            'label' => 'Thưởng/Phạt KPI',
            'type' => 'number'
        ]);

        /**
         * $snap_salary_kpi
         * $fund_value
         * $total_value
         * $close_details
         */

        $crudBag->addColumn([
            'name' => 'fund_value',
            'label' => 'Quỹ gắn bó',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'total_value',
            'label' => 'Thực nhận',
            'type' => 'number'
        ]);

        $crudBag->addColumn([
            'name' => 'is_close',
            'type' => 'select',
            'label' => 'Trạng thái',
            'attributes' => [
                'options' => [
                    0 => 'Chưa chốt',
                    1 => 'Đã chốt'
                ],
                'bg' => [
                    0 => 'bg-danger',
                    1 => 'bg-success'
                ]
            ]
        ]);
    }

    private function handleQuery(Builder &$query, CrudBag $crudBag)
    {
        $filters = $crudBag->getFilters();

        foreach ($filters as $filter) {
            if (!$filter->getValue()) {
                continue;
            }
            switch ($filter->getName()) {
                case 'user_id':
                    $query->where('user_id', $filter->getValue());
                    break;
                case 'classroom_id':
                    $query->where('classroom_id', $filter->getValue());
                    break;
                case 'period':
                    $query->where('period', $filter->getValue() . "-01");
                    break;
                default:
                    break;
            }
        }

        $query->orderBy('created_at', 'desc');
    }

    public function delete(int $id)
    {
        $salarySnap = SalarySnap::query()->findOrFail($id);

        SalarySnapDetail::query()->where([
            'user_id' => $salarySnap['user_id'],
            'classroom_id' => $salarySnap['classroom_id'],
        ])->delete();

        $salarySnap->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
    }

    private function makeOfGetGroupId(int $userId, string $period): int
    {
        $salaryFund = SalaryGroup::query()->where('user_id', $userId)->where('period', $period)->first();

        if (!$salaryFund) {
            $salaryFund = SalaryGroup::query()->create([
                'user_id' => $userId,
                'period' => $period,
                'status' => -1
            ]);
        }

        return $salaryFund->id;
    }

    private function handleFilter(CrudBag &$crudBag, Request $request)
    {
        if ($request->hasAny(['user_id', 'classroom_id'])) {
            $crudBag->setParam('show_filter', true);
        }

        $crudBag->addFilter([
            'name' => 'user_id',
            'label' => 'Nhân sự',
            'type' => 'select2',
            'attributes' => [
                'options' => User::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function ($user) {
                    return [$user->id => "{$user->uuid} ({$user->name})"];
                }),
                'class' => 'col-md-4'
            ],
            'value' => $request->get('user_id'),

        ]);

        $crudBag->addFilter([
            'name' => 'classroom_id',
            'label' => 'Lớp',
            'type' => 'select2',
            'attributes' => [
                'options' => Classroom::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function ($classroom) {
                    return [$classroom->id => "{$classroom->uuid} ({$classroom->name})"];
                }),
                'class' => 'col-md-4'
            ],
            'value' => $request->get('classroom_id'),

        ]);

        $crudBag->addFilter([
            'name' => 'period',
            'type' => 'period',
            'value' => $request->get('period'),
            'label' => 'Period',
        ]);
    }

    private function handleStatistic(Builder $query, CrudBag &$crudBag)
    {
        $crudBag->setParam('show_statistic', true);

        $crudBag->addStatistic([
            'label' => 'Tổng tiền',
            'value' => number_format($query->sum('snap_salary_value'))." đ",
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'Tổng số thẻ lương',
            'value' => number_format($query->count()),
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);
    }
}
