<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\CustomFieldBag;
use App\Helper\Fields;
use App\Helper\ListViewModel;
use App\Helper\Object\CustomFieldShow;
use App\Helper\Object\UserProfileObject;
use App\Helper\ProfileBag;
use App\Models\CustomFields;
use App\Models\Home;
use App\Models\Parallel;
use App\Models\Student;
use App\Models\StudentProfile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class StudentController extends Controller
{
    private CrudBag $crudBag;

    public function __construct(CrudBag $crudBag)
    {
        $this->crudBag = $crudBag;
    }

    public function unlink(int $id): RedirectResponse
    {
        if (!check_permission('edit student')) {
            abort(403);
        }

        $student = Student::query()->where('id', $id)->firstOrFail();

        $student->update([
            'home_id' => null
        ]);

        return redirect()->back()->with('success', 'Thành công');
    }

    public function create(): View
    {
        if (!check_permission('create student')) {
            abort(403);
        }

        $this->crudBag->setEntity('student');
        $this->crudBag->setLabel('Học sinh');
        $this->crudBag->setAction('student.store');
        $this->crudBag = $this->handleFields($this->crudBag);
        $this->crudBag->setParam(CrudBag::HEAD_HINT, 'Mật khẩu mặc định là bsm123456@');

        return view('create', [
            'crudBag' => $this->crudBag
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        if (!check_permission('create student')) {
            abort(403);
        }

        $parallel = $request->get('parallel') ?? null;

        $this->validate($request, [
            'name' => 'required|string|max:40',
            'english_name' => 'string|nullable',
            'avatar' => 'string',
            'gender' => 'integer|in:1,2|required',
            'facebook' => 'string|nullable',
            'email' => 'email|nullable',
            'address' => 'string|nullable',
            'school' => 'string|nullable',
            'user_ref' => 'integer|nullable',
            'phone' => 'string|nullable'
        ]);

        $dataToCreateStudent = $request->only([
            'name',
            'avatar',
            'email',
            'phone',
            'student_status'
        ]);

        $dataToCreateProfile = $request->only([
            'english_name',
            'gender',
            'facebook',
            'address',
            'school',
            'user_ref',
            'address',
            'birthday'
        ]);

        $customFields = $request->get('custom_field') ?? [];

        $dataToCreateStudent['uuid'] = User::newUuid(Auth::user()->{"branch"}, "HS");
        $dataToCreateStudent['password'] = Hash::make('bsm123456@');
        $dataToCreateStudent['branch'] = Auth::user()->{'branch'};
        $dataToCreateStudent['role'] = User::STUDENT_ROLE;

        DB::transaction(function () use ($dataToCreateStudent, $dataToCreateProfile, $customFields, $parallel) {
            $user = User::query()->create($dataToCreateStudent);

            if (!empty($parallel)) {
                $parallelUser = User::query()->where('id', $parallel)->get(['home_id', 'id'])->first();;
                if ($parallelUser->{'home_id'}) {
                    $user->update([
                        'home_id' => $parallelUser->{'home_id'}
                    ]);
                } else {
                    $newHome = Home::query()->create([
                        'branch' => Auth::user()->{'branch'}
                    ]);

                    $user->update([
                        'home_id' => $newHome->{'id'}
                    ]);

                    $parallelUser->update([
                        'home_id' => $newHome->{'id'}
                    ]);
                }
            }

            $dataToCreateProfile['user_id'] = $user['id'];
            StudentProfile::query()->create($dataToCreateProfile);
            $this->saveCustomFields($customFields, $user->id);
        });

        return redirect('student/list')->with('success', 'Thêm mới thành công');
    }

    public function list(Request $request): View
    {
        Cache::put("student_list_" . Auth::id(), $request->getQueryString());

        if (!check_permission('list student')) {
            abort(403);
        }

        $perPage = $request->get('perPage') ?? 10;
        $crudBag = new CrudBag();
        $crudBag->setParam(CrudBag::PLACEHOLDER_SEARCH, 'Tìm kiếm bằng tên, mã, hoặc tên tiếng Anh, hoặc sđt, email...');
        $crudBag->setEntity('student');
        $crudBag->setLabel('Học sinh');
        $crudBag->setSearchValue($request->get('search'));
        $crudBag = $this->handleFiltering($crudBag, $request);
        $crudBag = $this->handleStatistic($crudBag, $request);
        $crudBag = $this->handleColumn($crudBag, $request);

        /**
         * @var LengthAwarePaginator $students
         */
        $builder = Student::query();

        $this->handleBuilder($builder, $crudBag);

        $students = $builder->paginate($perPage);

        return \view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => new ListViewModel($students)
        ]);
    }

    public function edit(int $id): View
    {
        if (!check_permission('edit student')) {
            abort(403);
        }
        $this->crudBag->setEntity('student');
        $this->crudBag->setLabel('Học sinh');
        $this->crudBag->setAction('student.update');
        $this->crudBag->setId($id);

        $student = Student::query()->where('id', $id)->firstOrFail();

        if ($student->{'home_id'}) {
            $home = Home::query()->where('id', $student->{'home_id'})->first();
            if ($home) {
                $this->crudBag->setParam('Home', $home);
            }
            $this->crudBag->setParam(CrudBag::BEFORE_CONTENT, 'home.info');
        }

        /**
         * @var Student $student
         */
        $this->crudBag = $this->handleFields($this->crudBag, $student);

        return view('create', [
            'crudBag' => $this->crudBag
        ]);
    }

    public function update(int $id, Request $request): RedirectResponse
    {
        if (!check_permission('edit student')) {
            abort(403);
        }

        $history = Cache::get('student_list_' . Auth::id());

        $student = Student::query()->where('id', $id)->firstOrFail();

        $parallel = $request->get('parallel') ?? null;

        $this->validate($request, [
            'name' => 'required|string|max:40',
            'english_name' => 'string|nullable',
            'avatar' => 'string|nullable',
            'gender' => 'integer|in:1,2|required',
            'facebook' => 'string|nullable',
            'email' => 'email|nullable',
            'address' => 'string|nullable',
            'school' => 'string|nullable',
            'user_ref' => 'integer|nullable',
            'phone' => 'string|nullable',
        ]);

        $dataToCreateStudent = $request->only([
            'name',
            'avatar',
            'email',
            'phone',
            'student_status'
        ]);

        $dataToCreateProfile = $request->only([
            'english_name',
            'gender',
            'facebook',
            'address',
            'school',
            'user_ref',
            'address',
            'birthday'
        ]);
        $customFields = $request->get('custom_field') ?? [];
        DB::transaction(function () use ($dataToCreateStudent, $dataToCreateProfile, $id, $customFields, $student, $parallel) {
            $student->update($dataToCreateStudent);

            if (!empty($parallel)) {
                $parallelUser = User::query()->where('id', $parallel)->get(['home_id', 'id'])->first();;
                if ($parallelUser->{'home_id'}) {
                    $student->update([
                        'home_id' => $parallelUser->{'home_id'}
                    ]);
                } else {
                    $newHome = Home::query()->create([
                        'branch' => Auth::user()->{'branch'}
                    ]);

                    $student->update([
                        'home_id' => $newHome->{'id'}
                    ]);

                    $parallelUser->update([
                        'home_id' => $newHome->{'id'}
                    ]);
                }
            }

            StudentProfile::query()->where('user_id', $id)->update($dataToCreateProfile);
            $this->saveCustomFields($customFields, $id);
        });

        return redirect()->to('student/list?' . $history)->with('success', 'Chỉnh sửa thành công');
    }

    public function delete(int $id): RedirectResponse
    {
        if (!check_permission('delete student')) {
            abort(403);
        }
        $student = Student::query()->where('id', $id)->firstOrFail();

        DB::transaction(function () use ($student) {
            StudentProfile::query()->where('user_id', $student->id)->delete();
            $student->delete();
        });

        return redirect()->back()->with('success', 'Xóa thành công');
    }

    private function handleFiltering(CrudBag $crudBag, Request $request): CrudBag
    {
        $crudBag->addFilter([
            'name' => 'gender:eq',
            'value' => $request->get('gender:eq') ?? -1,
            'label' => 'Giới tính',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    Student::MALE => 'Nam',
                    Student::FEMALE => 'Nữ'
                ]
            ],
        ]);

        $crudBag->addFilter([
            'name' => 'birthday_month:handle',
            'label' => 'Tháng sinh nhật',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    1 => 'Tháng 1',
                    2 => 'Tháng 2',
                    3 => 'Tháng 3',
                    4 => 'Tháng 4',
                    5 => 'Tháng 5',
                    6 => 'Tháng 6',
                    7 => 'Tháng 7',
                    8 => 'Tháng 8',
                    9 => 'Tháng 9',
                    10 => 'Tháng 10',
                    11 => 'Tháng 11',
                    12 => 'Tháng 12'
                ]
            ],
            'value' => $request->get('birthday_month:handle')
        ]);

        $crudBag->addFilter([
            'label' => 'Lớp học trên BSM',
            'name' => 'classroom:handle',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    1 => 'Lop C001',
                    2 => 'Lop C002'
                ]
            ],
            'value' => $request->get('classroom:handle')
        ]);

//        $crudBag->addFilter([
//            'label' => 'Lớp',
//            'name' => 'grade:handle',
//            'type' => 'select',
//            'attributes' => [
//                'options' => [
//                    1 => 'Lớp 1',
//                    2 => 'Lớp 2',
//                    3 => 'Lớp 3',
//                    4 => 'Lớp 4',
//                    5 => 'Lớp 5',
//                    6 => 'Lớp 6',
//                    7 => 'Lớp 7',
//                    8 => 'Lớp 8',
//                    9 => 'Lớp 9',
//                    10 => 'Lớp 10',
//                    11 => 'Lớp 11',
//                    12 => 'Lớp 12',
//                    13 => 'Cấp cao hơn'
//                ],
//            ],
//            'value' => $request->get('grade:handle')
//        ]);

//        $crudBag->addFilter([
//            'label' => 'Cấp học',
//            'name' => 'level:handle',
//            'type' => 'select',
//            'attributes' => [
//                'options' => [
//                    0 => 'Mầm non',
//                    1 => 'Tiểu học',
//                    2 => 'THCS',
//                    3 => 'THPT',
//                    4 => 'Cao Đẳng - Đại Học',
//                    5 => 'Khác'
//                ],
//            ],
//            'value' => $request->get('level:handle')
//        ]);


        $crudBag->addFilter([
            'label' => 'Tuổi',
            'name' => 'age:eq',
            'type' => 'text',
            'value' => $request->get('age:eq')
        ]);

        $crudBag->addFilter([
            'label' => 'Nhan vien phu trach',
            'name' => 'staff:handle',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    1 => 'Anh C',
                    2 => 'Anh B',
                ]
            ],
            'value' => $request->get('staff:handle')
        ]);

        return $crudBag;
    }

    private function handleBuilder(Builder $builder, CrudBag $crudBag): void
    {
        if ($crudBag->getSearchValue() != '') {
            $builder->where(function (Builder $subBuilder) use ($crudBag) {
                $subBuilder->where('name', 'like', "%" . $crudBag->getSearchValue() . "%")
                    ->orWhere('uuid', 'like', '%' . $crudBag->getSearchValue() . '%')
                    ->orWhere('email', 'like', "%{$crudBag->getSearchValue()}%")
                    ->orWhere('phone', 'like', "%{$crudBag->getSearchValue()}%")
                    ->orWhereHas('profile', function (Builder $builder) use ($crudBag) {
                        $builder->where('english_name', 'like', "%{$crudBag->getSearchValue()}%");
                    });
            });
        }

        if (Auth::user()->{"role"} == User::TEACHER_ROLE) {
            $builder->whereHas('Cards', function (Builder $card) {
                $card->whereHas('Classroom', function (Builder $classroom) {
                    $classroom->whereHas('Shifts', function (Builder $shifts) {
                        $shifts->where('teacher_id', Auth::id());
                    })->orWhereHas('StudyLogs', function (Builder $studyLogs) {
                        $studyLogs->whereHas('WorkingShifts', function (Builder $workingShift) {
                            $workingShift->where('teacher_id', Auth::id());
                        });
                    });
                });
            });
        }

        if (Auth::user()->{"role"} == User::SUPPORTER_ROLE) {
            $builder->whereHas('Cards', function (Builder $card) {
                $card->whereHas('Classroom', function (Builder $classroom) {
                    $classroom->whereHas('Shifts', function (Builder $shifts) {
                        $shifts->where('supporter_id', Auth::id());
                    })->orWhereHas('StudyLogs', function (Builder $studyLogs) {
                        $studyLogs->whereHas('WorkingShifts', function (Builder $workingShift) {
                            $workingShift->where('supporter_id', Auth::id());
                        });
                    });
                });
            });
        }

        if (Auth::user()->{'role'} == User::STAFF_ROLE) {
            if (check_permission('all student')) {
                $builder->whereHas('Cards', function (Builder $card) {
                    $card->whereHas('Classroom', function (Builder $classroom) {
                        $classroom->withoutGlobalScopes()->where('staff_id', Auth::id());
                    });
                });
            }
        }

        foreach ($crudBag->getFilters() as $filter) {
            if ($filter->getValue() != null && $filter->getValue() != '-1') {
                switch ($filter->getName()) {
                    case "name:contains":
                        $builder->where('name', 'like', "%" . $filter->getValue() . "%");
                        break;
                    case "gender:eq":
                        $builder->whereHas('profile', function (Builder $profile) use ($filter) {
                            $profile->where('gender', $filter->getValue());
                        });
                        break;
                    case "birthday_month:handle":
                        $builder->whereHas('profile', function (Builder $profile) use ($filter) {
                            $profile->whereRaw('MONTH(birthday) = ' . $filter->getValue());
                        });
                        break;
                    case "age:eq":
                        $builder->whereHas('profile', function (Builder $profile) use ($filter) {
                            $currentYear = now()->year;
                            $birthYear = $currentYear - $filter->getValue();
                            $profile->whereRaw('YEAR(birthday) = ?', [$birthYear]);
                        });
                        break;
                    default:
                        break;
                }
            }
        }

        $builder->orderBy('created_at', 'desc');
    }

    private function handleStatistic(CrudBag $crudBag, Request $request): CrudBag
    {
        $crudBag->addStatistic([
            'label' => 'Học sinh',
            'value' => Student::query()->count() ?? 0,
            'badge' => 'Thời điểm hiện tại',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);

        $crudBag->addStatistic([
            'label' => 'Học sinh',
            'value' => Student::query()->where('student_status', Student::STUDENT_LEARNING)->count() ?? 0,
            'badge' => 'Đang học',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);
        $crudBag->addStatistic([
            'label' => 'Học sinh',
            'value' => Student::query()->where('student_status', Student::STUDENT_ARRIVED)->count() ?? 0,
            'badge' => 'Bảo lưu',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);
        $crudBag->addStatistic([
            'label' => 'Học sinh',
            'value' => Student::query()->where('student_status', Student::STUDENT_STOPPED)->count() ?? 0,
            'badge' => 'Đã nghỉ',
            'image' => asset('demo/assets/img/illustrations/illustration-1.png')
        ]);
        return $crudBag;
    }

    private function handleColumn(CrudBag $crudBag, Request $request): CrudBag
    {
        $crudBag->addColumn([
            'name' => 'uuid',
            'type' => 'text',
            'label' => 'Mã học sinh',
            'fixed' => 'first',
            'attributes' => [
                'entity' => 'student',
                'edit' => true,
                'show' => true
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'name',
            'type' => 'profile',
            'label' => 'Họ tên',
            'attributes' => [
                'avatar' => 'avatar',
                'address' => 'address',
                'identity' => 'id'
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'english_name',
            'type' => 'text',
            'label' => 'Tên tiếng anh'
        ]);

        $crudBag->addColumn([
            'name' => 'cards',
            'type' => 'hasMany',
            'label' => 'Danh sách thẻ học',
            'attributes' => [
                'relation.id' => 'id',
                'relation.label' => 'uuid',
                'relation.entity' => 'card'
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'zalo_private_chat',
            'type' => 'link',
            'label' => 'Link Zalo Chăm sóc 1-1'
        ]);

        $crudBag->addColumn([
            'name' => 'student_status',
            'label' => 'Trạng thái học sinh',
            'type' => 'select',
            'attributes' => [
                'options' => Student::StudentStatusLabel(),
                'bg' => Student::StudentStatusBg()
            ]
        ]);

//        $crudBag->addColumn([
//            'name' => 'private_chat',
//            'type' => 'link',
//            'label' => 'Chat trên hệ thống'
//        ]);

        $crudBag->addColumn([
            'name' => 'gender',
            'type' => 'select',
            'label' => 'Giới tính',
            'attributes' => [
                'options' => [
                    Student::MALE => 'Nam',
                    Student::FEMALE => 'Nữ'
                ],
                'bg' => [
                    Student::MALE => 'bg-danger',
                    Student::FEMALE => 'bg-success'
                ]
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'birthday',
            'type' => 'text',
            'label' => 'Ngày sinh'
        ]);
        $crudBag->addColumn([
            'name' => 'age',
            'type' => 'text',
            'label' => 'Tuổi',
            'attributes' => [
                'bg' => [
                    '1' => 'bg-success',
                    '2' => 'bg-success',
                    '3' => 'bg-success',
                    '4' => 'bg-success',
                    '5' => 'bg-success',
                    '6' => 'bg-warning',
                    '7' => 'bg-warning',
                    '8' => 'bg-warning',
                    '9' => 'bg-warning',
                    '10' => 'bg-warning',
                    '11' => 'bg-orange',
                    '12' => 'bg-orange',
                    '13' => 'bg-orange',
                    '14' => 'bg-orange',
                    '15' => 'bg-danger',
                    '16' => 'bg-danger',
                    '17' => 'bg-danger',
                    '18' => 'bg-purple',
                    '19' => 'bg-purple',
                    '20' => 'bg-purple',
                    '21' => 'bg-purple',
                ]
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'grade',
            'type' => 'text',
            'label' => 'Lớp',
            'attributes' => [
                'bg' => [
                    'Lớp 3 tuổi' => 'bg-success',
                    'Lớp 4 tuổi' => 'bg-success',
                    'Lớp 5 tuổi' => 'bg-success',
                    'Lớp 1' => 'bg-warning',
                    'Lớp 2' => 'bg-warning',
                    'Lớp 3' => 'bg-warning',
                    'Lớp 4' => 'bg-warning',
                    'Lớp 5' => 'bg-warning',

                    'Lớp 6' => 'bg-orange',
                    'Lớp 7' => 'bg-orange',
                    'Lớp 8' => 'bg-orange',
                    'Lớp 9' => 'bg-orange',
                    'Lớp 10' => 'bg-danger',
                    'Lớp 11' => 'bg-danger',
                    'Lớp 12' => 'bg-danger',
                    'Khác' => 'bg-purple',
                ]
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'level',
            'type' => 'text',
            'label' => 'Cấp hoc',
            'attributes' => [
                'bg' => [
                    'Mầm non' => 'bg-success',
                    'Tiểu học' => 'bg-warning',
                    'THCS' => 'bg-orange',
                    'THPT' => 'bg-danger',
                    'Cao Đẳng - Đại Học' => 'bg-purple',
                    'Người đi làm' => 'bg-green',
                ]
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'phone',
            'type' => 'text',
            'label' => 'Số điện thoại'
        ]);

        $crudBag->addColumn([
            'name' => 'facebook',
            'type' => 'link',
            'label' => 'Facebook'
        ]);

        $crudBag->addColumn([
            'name' => 'email',
            'type' => 'text',
            'label' => 'Email'
        ]);

        $crudBag->addColumn([
            'name' => 'user_ref',
            'type' => 'text',
            'label' => 'Người giới thiệu',
        ]);

        $crudBag->addColumn([
            'name' => 'sibling',
            'type' => 'hasMany',
            'label' => 'Anh chị em',
            'attributes' => [
                'relation.id' => 'id',
                'relation.label' => 'uuid',
                'relation.entity' => 'student'
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'last_login',
            'label' => 'Truy cập lần cuối',
            'type' => 'last_login',
        ]);

        return $crudBag;
    }

    /**
     * @param CrudBag $crudBag
     * @param Student|null $student
     * @return CrudBag
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 03/01/2024 5:35 pm
     */
    private function handleFields(CrudBag $crudBag, Student $student = null): CrudBag
    {
        $crudBag->addFields([
            'name' => 'avatar',
            'type' => 'avatar-select',
            'required' => false,
            'label' => 'Chọn avatar',
            'options' => User::avatarStudentSelect(),
            'value' => $student ? $student['avatar'] : 0,
            'class' => 'col-10 mb-3'
        ]);

        $crudBag->addFields([
            'name' => 'name',
            'type' => 'text',
            'required' => true,
            'label' => 'Tên học sinh',
            'value' => $student ? $student['name'] : null,
        ]);

        $crudBag->addFields([
            'name' => 'english_name',
            'type' => 'text',
            'required' => false,
            'label' => 'Tên tiếng anh',
            'value' => $student ? $student['english_name'] : null
        ]);

        $crudBag->addFields([
            'name' => 'gender',
            'nullable' => 1,
            'value' => $student ? $student['gender'] : "",
            'type' => 'select',
            'required' => true,
            'label' => 'Giới tính',
            'options' => [
                1 => 'Nam',
                2 => 'Nữ',
            ]
        ]);
        $crudBag->addFields([
            'name' => 'student_status',
            'nullable' => 0,
            'value' => $student ? $student['student_status'] : 1,
            'type' => 'select',
            'required' => true,
            'label' => 'Trạng thái học sinh',
            'options' => Student::StudentStatusLabel()
        ]);

        $crudBag->addFields([
            'name' => 'birthday',
            'value' => isset($student['birthday']) && $student['birthday'] != null ? Carbon::parse($student['birthday'])->toDateString() : null,
            'type' => 'date',
            'required' => false,
            'label' => 'Ngày sinh',
        ]);

        $crudBag->addFields([
            'name' => 'phone',
            'value' => $student ? $student['phone'] : null,
            'type' => 'phone',
            'required' => false,
            'label' => 'Số điện thoại',
        ]);

        $crudBag->addFields([
            'value' => $student ? $student['facebook'] : null,
            'name' => 'facebook',
            'type' => 'text',
            'required' => false,
            'label' => 'Link facebook của học sinh',
        ]);

        $crudBag->addFields([
            'value' => $student ? $student['email'] : null,
            'name' => 'email',
            'type' => 'email',
            'required' => false,
            'label' => 'Email của học sinh',
        ]);

        $crudBag->addFields([
            'value' => $student ? $student['address'] : null,
            'name' => 'address',
            'type' => 'address',
            'required' => false,
            'label' => 'Địa chỉ sinh sống',
        ]);

        $crudBag->addFields([
            'value' => $student ? $student['school'] : null,
            'name' => 'school',
            'type' => 'text',
            'required' => false,
            'label' => 'Trường đang theo học',
        ]);

        $this->crudBag->addFields([
            'value' => $student ? $student['user_ref'] : null,
            'name' => 'user_ref',
            'type' => 'select',
            'required' => false,
            'label' => 'Người giới thiệu',
            'nullable' => true
        ]);

        if (!$student?->{'home_id'}) {
            $this->crudBag->addFields([
                'label' => 'Học sinh liên kết',
                'name' => 'parallel',
                'type' => 'select',
                'required' => false,
                'options' => Student::query()->get()->mapWithKeys(function (Student $item) {
                    return [$item['id'] => "{$item['uuid']}-{$item['name']}"];
                })->toArray(),
                'nullable' => true,
            ]);
        }

        /**
         * @var CustomFields[] $customFields
         */
        $customFields = CustomFields::query()->where('entity_type', CustomFields::ENTITY_STUDENT)->where('branch', Auth::user()->{'branch'})->get();

        foreach ($customFields as $customField) {
            $fieldData = [
                'name' => 'custom_field[' . $customField->name . ']',
                'type' => CustomFields::convertedType()[$customField->type],
                'label' => $customField->label,
                'required' => $customField->required,
                'value' => $student?->getCustomField($customField->name) ?? null
            ];

            if ($customField->type == CustomFields::TEXTAREA_TYPE) {
                $fieldData['class'] = 'col-md-10 mb-3';
            }

            if ($customField->type == CustomFields::DATE_TYPE) {
                if ($fieldData['value']) {
                    $fieldData['value'] = Carbon::parse($fieldData['value'])->toDateString();
                }
                $fieldData['class'] = 'col-md-10 mb-3';
            }

            if ($customField->type == CustomFields::SELECT_TYPE) {
                $fieldData['options'] = $customField->convertInitValue();
                if ($customField->required == 0) {
                    $fieldData['nullable'] = 1;
                }
            }

            $crudBag->addFields($fieldData);
        }

        return $crudBag;
    }

    private function saveCustomFields(array $customFields, int $studentId): void
    {
        $customFieldsData = [];

        foreach ($customFields as $name => $customField) {
            if (!$customField) {
                continue;
            }

            $customFieldRecord = CustomFields::query()->where('name', $name)->where('branch', Auth::user()->{'branch'})->first();
            if (!$customFieldRecord) {
                continue;
            }

            if ($customFieldRecord['type'] == CustomFields::DATE_TYPE) {
                $customField = Carbon::parse($customField)->toDateString();
            }

            $customFieldsData[$name] = $customField;
        }

        StudentProfile::query()->where('user_id', $studentId)->update([
            'extra_information' => json_encode($customFieldsData)
        ]);
    }

    /**
     * @param int $id
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(int $id): View
    {
        /**
         * @var Student $student
         */
        $student = Student::query()->where('id', $id)->where('branch', Auth::user()->{'branch'})->firstOrFail();

        $profileBag = new ProfileBag();
        $profileBag->setEntity('student');
        $profileBag->setUserProfileObject(new UserProfileObject([
            'uuid' => $student['uuid'],
            'id' => $student['id'],
            'name' => $student['name'],
            'avatar' => $student['avatar'] ?? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQS0Oi53Y0SYUnNZ6FDFALWjbzr2siFFZqRAI_ygcnbVunsa0Ywsn1u1xGx7FisdgzGdcQ&usqp=CAU",
            'branch' => $student['branch'],
            'email' => $student['email'] ?? " ",
            'phone' => $student['phone'] ?? " ",
            'role' => $student['role'] ?? "-",
        ]));

        $customFields = CustomFields::query()->where('entity_type', CustomFields::ENTITY_STUDENT)->where('branch', Auth::user()->{'branch'})->get();

        foreach ($customFields as $customField) {
            $extraData = $student->getExtraInformationAttribute($customField['name']);
            if ($extraData) {
                $profileBag->addCustomField(new CustomFieldShow([
                    'label' => $customField['label'],
                    'value' => $extraData
                ]));
            }
        }

        return \view('profile', [
            'profileBag' => $profileBag
        ]);
    }

    public function getProfilesView()
    {
        $authId = Auth::id();
        $user = User::query()->where('id', $authId)->firstOrFail();

        /**
         * @var Home $home
         */
        $home = Home::query()->where('id', $user->{'home_id'})->first();

        if (!$home) {
            $profiles = [];
        } else {
            $profiles = $home->Students()->get();
        }

        return \view('switch-profile', [
            'profiles' => $profiles
        ]);
    }

    public function switchProfilesView(int $userId)
    {
        $authId = Auth::id();

        if ($authId == $userId) {
            return redirect('/');
        }

        $profile = Student::query()->whereExists(function ($query) use ($authId) {
            $query->select(DB::raw(1))
                ->from('parallels')
                ->where(function ($subQuery) use ($authId) {
                    $subQuery->where('parallels.original_student', $authId)
                        ->orWhere('parallels.parallel_student', $authId);
                })
                ->whereRaw('users.id = parallels.original_student OR users.id = parallels.parallel_student');
        })->where('users.id', $userId)->first();

        if ($profile) {
            Auth::loginUsingId($profile['id']);
        }

        return redirect('/');
    }
}
