<?php

namespace App\Http\Controllers;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class FileController extends Controller
{
    public function uploadTemp(Request $request): JsonResponse
    {
        $notification = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jpg,jpeg,png,gif,doc,docx,pdf|max:2048',
        ]);

        if ($notification->fails()) {
            return response()->json($notification->errors(), 422);
        }

        // Store the file temporarily
        $file = $request->file('file');

        $fileUrl = uploads($file, 'studylog', Str::random(8));

        return response()->json(['url' => $fileUrl]);
    }
}
