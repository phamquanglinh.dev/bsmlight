<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\Account;
use App\Models\User;
use App\Models\UserConfig;
use Database\Factories\AccountFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Laravel\Socialite\Facades\Socialite;

class AccountController extends Controller
{
    public function getAllAccountAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $builder = Account::query();

        Account::handleQuery($request, $builder);

        $accounts = $builder->get()->map(function (Account $account) use ($request) {
            $account->handleQueryProcess($request);
            return $account;
        })->toArray();

        return $httpResponse->responseCollection($accounts);
    }

    public function getOneAccountAction(Request $request, HttpResponse $httpResponse, int $accountId): JsonResponse
    {
        $builder = Account::query();

        Account::handleQuery($request, $builder);

        /**
         * @var  Account $accounts
         */
        $accounts = $builder->where('id', $accountId)->first();

        if (!$accounts) {
            return $httpResponse->notFoundResponse();
        }

        $accounts->handleQueryProcess($request);

        return $httpResponse->responseData($accounts->toArray());
    }

    /**
     */
    public function createAccountAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, Account::validationRulesForCreate());

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->errors());
        }

        $account = AccountFactory::makeAccount($input);

        $account->save();

        return $httpResponse->responseCreated($account->{'id'});
    }

    public function updateAccountAction(HttpResponse $httpResponse, Request $request, int $accountId): JsonResponse
    {
        /**
         * @var Account $account
         */
        $account = Account::query()->where('id', $accountId)->first();

        if (!$account) {
            return $httpResponse->notFoundResponse();
        }

        $input = $request->input();

        $notification = Validator::make($input, Account::validationRulesForUpdate());

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->errors());
        }

        $account = AccountFactory::modifyAccount($account, $input);

        $account->save();

        return $httpResponse->responseMessage('Cập nhật thành công');
    }

    public function deleteAccountAction(HttpResponse $httpResponse, int $accountId): JsonResponse
    {
        /**
         * @var Account $account
         */
        $account = Account::query()->where('id', $accountId)->first();

        if (!$account) {
            return $httpResponse->notFoundResponse();
        }

        $account->delete();

        return $httpResponse->responseMessage('Xoá thành công');
    }

    public function restoreAccountAction(int $accountId)
    {

    }

    public function listView(): View
    {
        return view('accounts.list');
    }

    public function createView(): \Illuminate\Contracts\View\View|Factory|Application
    {
        return \view("account.create");
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * @return Application|RedirectResponse|Redirector
     */
    public function handleFacebookCallback()
    {

        try {
            $facebookUser = Socialite::driver('facebook')->stateless()->user();
            if (Auth::check()) {
                $user = Auth::user();
                $config = UserConfig::where('user_id', $user->id)->where('type', 'facebook')->first();

                $facebookData = [
                    'facebook_id' => $facebookUser->id,
                    'access_token' => $facebookUser->token,
                    'refresh_token' => $facebookUser->refreshToken,
                    'expired' => now()->addSeconds($facebookUser->expiresIn)->toDateTimeString(),
                ];

                if ($config) {

                    $settings = json_decode($config->settings, true);
                    $settings['facebook'] = $facebookData;
                    $config->settings = json_encode($settings);
                    $config->branch = $user->branch;
                    $config->save();

                } else {

                    UserConfig::create([
                        'user_id' => $user->id,
                        'type' => 'facebook',
                        'settings' => json_encode($facebookData),
                        'branch' => $user->branch,
                    ]);
                }
                Auth::login($user);
                return redirect("/")->with("success", "Liên kết thành công với FaceBook");
            } else {
                $userConfig = UserConfig::where('settings->facebook_id', $facebookUser->id)->first();
                if ($userConfig) {
                    $userC = User::where('id', $userConfig->user_id)->first();

                    Auth::login($userC);
                    return redirect('/')->with(['success' => 'Đăng nhập với FaceBook thành công']);
                }
                return redirect('/')->withErrors(['success' => '']);
            }
        } catch (\Exception $e) {
            return redirect('login')->withErrors(['success' => 'Đăng nhập bằng FaceBook thất bại']);
        }
    }


    public function redirectToGoogle(): \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback(): Redirector|Application|RedirectResponse
    {

        try {
            $googleUser = Socialite::driver('google')->stateless()->user();

            if (Auth::check()) {
                $user = Auth::user();
                $config = UserConfig::where('user_id', $user->id)->where('type', 'google')->first();
                $googleData = [
                    'google_id' => $googleUser->id,
                    'access_token' => $googleUser->token,
                    'refresh_token' => $googleUser->refreshToken,
                    'expired' => now()->addSeconds($googleUser->expiresIn)->toDateTimeString(),

                ];
                if ($config) {
                    $settings = json_decode($config->settings, true);
                    $settings['google'] = $googleData;
                    $config->settings = json_encode($settings);
                    $config->branch = $user->branch;
                    $config->save();
                } else {
                    UserConfig::create([
                        'user_id' => $user->id,
                        'type' => 'google',
                        'settings' => json_encode($googleData),
                        'branch' => $user->branch,
                    ]);
                }
                Auth::login($user);
                return redirect('/')->with("success", "Liên kết với Google thành công");
            }

            $userConfig = UserConfig::query()->where("type","google")->where('settings->google_id', $googleUser->id)->first();
            if ($userConfig) {
                Auth::loginUsingId($userConfig["user_id"]);
                return redirect('/')->with(['success' => 'Đăng nhập bằng Google thành công']);
            }

            return redirect('/login')->withErrors(['success' => 'Đăng nhập bằng Google thất bại']);

        } catch (\Exception $e) {
            return redirect('/login')->withErrors(['error' => 'Không thể kết nối tới tài khoản google']);
        }
    }

    public function edit(int $id): Factory|\Illuminate\Contracts\View\View|Application
    {
        $user = Auth::user();
        return view("dashboard-setting-profile");
    }


    public function update(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'nullable|string|max:20',
            'avatar' => 'nullable|string',
            'password' => 'nullable|confirmed',
            'old_password' => 'required_with:password',
        ]);

        $input = $request->input();

        $user = User::query()->where('id', $id)->first();

        if (!empty($input['password'])) {
            if (Hash::check($input['old_password'], $user['password'])) {
                $user->setAttribute('password', Hash::make($input['password']));
                unset($input['password']);
            } else {
                return redirect()->back()->withErrors([
                    'old_password' => 'Mật khẩu cũ không chính xác'
                ]);
            }
        }
        $input = array_filter($input, fn($value) => $value !== null);

        $user->fill($input);

        $user->save();

        return redirect('/')->with('success', 'Chỉnh sửa tài khoản thành công');
    }

    public function deleteAuthFaceBook(Request $request): Redirector|Application|RedirectResponse
    {
        $user = Auth::user();
        $userConfig = UserConfig::query()->where("user_id", $user->id)->where("type", 'facebook')->first();
        $userConfig->delete();
        return redirect("/")->with("success", "Hủy liên kết FaceBook thành công");
    }

    public function deleteAuthGoogle(Request $request): Redirector|Application|RedirectResponse
    {
        $user = Auth::user();
        $userConfig = UserConfig::query()->where("user_id", $user->id)->where("type", 'google')->first();
        $userConfig->delete();
        return redirect("/")->with("success", "Hủy liên kết Google thành công");
    }
}
