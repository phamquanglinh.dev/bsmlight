<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use App\Models\CoverShift;
use App\Models\Supporter;
use App\Models\Teacher;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Inertia\Inertia;
use Inertia\Response;

class CalendarController extends Controller
{
    public function index(Request $request): View
    {
        return view('calendar.calendar');
    }

    public function getCalendar(Request $request): JsonResponse
    {
        $currentMonth = $request->get('month') ?? Carbon::now()->month;
        $currentYear = $request->get('year') ?? Carbon::now()->year;

        $loopEventCollection = [];
        /**
         * @var Classroom[] $classrooms
         */
        $classrooms = Classroom::query()->get();
        /**
         * classroom: "Lớp C001",
         * start: "19:00",
         * end: "21:00",
         * teacherName: "Sophia",
         * supporterName: "Lan Anh",
         */

        $filters = array_filter([
            'classroom_id' => $request->get('classroom_id') ?? null,
            'teacher_id' => $request->get('teacher_id') ?? null,
            'supporter_id' => $request->get('supporter_id') ?? null
        ], function ($item) {
            return $item !== null;
        });

        foreach ($classrooms as $classroom) {
            $shifts = $classroom->Shifts();

            if (!empty($filters)) {
                $shifts->where($filters);
            }

            foreach ($shifts->get() as $shift) {
                $loopEventCollection[$shift['Schedule']['week_day']][] = [
                    'classroom_id' => $shift['classroom_id'],
                    'classroom' => $shift['classroom']['name'],
                    'start' => $shift['start_time'],
                    'end' => $shift['end_time'],
                    'teacher_id' => $shift['teacher_id'],
                    'teacherName' => $shift->teacher?->name,
                    'supporter_id' => $shift['supporter_id'],
                    'supporterName' => $shift->supporter?->name,
                    'type' => 'loop',
                ];
            }
        }

        $months = $this->getMonthCollection($currentYear, $currentMonth);

        /**
         * [
         * {
         * day: "29/04/2024",
         * weekday: 2,
         * active: false
         * },
         */
        $monthCollection = [];
        $week = 1;
        foreach ($months as $key => $month) {
            $weekDay = $this->handleWeekday($month->dayOfWeek);
            $active = true;

            if ($month->month != $currentMonth) {
                $active = false;
            }

            $monthCollection[$week][] = [
                'day' => $month->isoFormat('DD/MM/YYYY'),
                'original_format_day' => $month->isoFormat('YYYY-MM-DD'),
                'active' => $active,
                'weekday' => $weekDay,
                'events' => $active ? $loopEventCollection[$weekDay] ?? [] : [],
            ];

            if (($key + 1) % 7 == 0) {
                $week++;
            }
        }

        $covers = $this->getCovers($currentMonth, $currentYear, $filters);

        $coverDataCollection = [];

        foreach ($months as $month) {
            if ($month->month != $currentMonth) {
                continue;
            }

            foreach ($covers as $cover) {
                if (!in_array($this->handleWeekday($month->dayOfWeek), $cover['week_days'])) {
                    continue;
                }

                if ($month < $cover['start_cover_date']) {
                    continue;
                }

                if ($month > $cover['end_cover_date']) {
                    continue;
                }

                $coverDataCollection[$month->isoFormat('DD/MM/YYYY')][] = [
                    'id' => $cover['id'],
                    'classroom_id' => $cover['classroom_id'],
                    'classroom' => $cover->classroom?->name,
                    'start' => $cover['start'],
                    'end' => $cover['end'],
                    'teacher_id' => $cover['teacher_id'],
                    'teacherName' => $cover->teacher?->name,
                    'supporter_id' => $cover['supporter_id'],
                    'supporterName' => $cover->supporter?->name,
                    'type' => 'cover',
                ];
            }
        }

        foreach ($monthCollection as $weekKey => $week) {
            foreach ($week as $dayKey => $day) {
                foreach ($coverDataCollection as $coverDay => $cover) {
                    if ($day['day'] == $coverDay) {

                        $monthCollection[$weekKey][$dayKey]['events'] = array_merge($monthCollection[$weekKey][$dayKey]['events'], $cover);
                    }
                }
            }
        }

        return response()->json(array_values($monthCollection));
    }

    /**
     * @param int $year
     * @param int $month
     * @return Carbon[]
     */
    private function getMonthCollection(int $year, int $month): array
    {
        $firstDayOfMonth = Carbon::create($year, $month)->startOfMonth();
        $firstDayOfWeek = $firstDayOfMonth->dayOfWeek;
        $lastDayOfMonth = Carbon::create($year, $month)->endOfMonth();

        if ($this->handleWeekday($firstDayOfWeek) != CarbonInterface::MONDAY) {

            $daysToAdd = $this->handleWeekday($firstDayOfWeek) - $this->handleWeekday(CarbonInterface::MONDAY);

            $firstDayOfMonth->subDays($daysToAdd);
        }


        if ($this->handleWeekday($lastDayOfMonth->dayOfWeek) != $this->handleWeekday(CarbonInterface::SUNDAY)) {

            while ($this->handleWeekday($lastDayOfMonth->dayOfWeek) != $this->handleWeekday(CarbonInterface::SUNDAY)) {
                $lastDayOfMonth->addDay();
            }
        }

        $totalDays = $firstDayOfMonth->diffInDays($lastDayOfMonth) + 1;

        $monthDays = [];

        for ($i = 0; $i < $totalDays; $i++) {
            $monthDays[] = $firstDayOfMonth->copy()->addDays($i);
        }

        return $monthDays;
    }

    private function handleWeekday($day)
    {
        if ($day == CarbonInterface::SUNDAY) {
            return 8;
        }

        return $day + 1;
    }

    public function getClassroom(): JsonResponse
    {
        $collection = Classroom::query()->get()->map(function (Classroom $classroom) {
            return [
                'value' => $classroom['id'],
                'label' => $classroom['uuid'] . "-" . $classroom['name']
            ];
        });

        return response()->json($collection);
    }

    public function getSupporters(): JsonResponse
    {
        $collection = Supporter::query()->get()->map(function (Supporter $supporter) {
            return [
                'value' => $supporter['id'],
                'label' => $supporter['name'] . "-" . $supporter['uuid'],
                'avatar' => $supporter['avatar'],
            ];
        });

        return response()->json($collection);
    }

    public function getTeachers(): JsonResponse
    {
        $collection = Teacher::query()->get()->map(function (Teacher $teacher) {
            return [
                'value' => $teacher['id'],
                'label' => $teacher['name'] . "-" . $teacher['uuid'],
                'avatar' => $teacher['avatar'],
            ];
        });

        return response()->json($collection);
    }


    public function storeCover(Request $request): JsonResponse
    {
        $input = $request->input();

        Validator::validate($input, [
            'classroom_id' => 'required|integer',
            'teacher_id' => 'required|integer',
            'supporter_id' => 'integer|nullable',
            'start_cover_date' => 'required|date',
            'end_cover_date' => 'required|date',
            'start' => 'required|date_format:H:i',
            'end' => 'required|date_format:H:i',
            'week_days' => 'required|array|in:1,2,3,4,5,6,7,8',
        ]);

        $data = [
            'classroom_id' => $input['classroom_id'],
            'teacher_id' => $input['teacher_id'],
            'supporter_id' => $input['supporter_id'],
            'start_cover_date' => $input['start_cover_date'],
            'end_cover_date' => $input['end_cover_date'],
            'start' => $input['start'],
            'end' => $input['end'],
            'week_days' => $input['week_days'],
        ];

        if (!empty($input['id'])) {
            $cover = CoverShift::query()->where('id', $input['id'])->update($data);
            return response()->json(['success' => $cover]);
        }

        $cover = CoverShift::query()->create($input);

        return response()->json(['success' => $cover]);
    }

    public function deleteCover(int $id): JsonResponse
    {
        CoverShift::query()->find($id)->delete();

        return response()->json(['success' => 'oke']);
    }

    /**
     * @param int $currentMonth
     * @param int $currentYear
     * @param array $filters
     * @param string $value
     * @return Collection
     */
    private function getCovers(int $currentMonth, int $currentYear, array $filters = [], string $value = ''): Collection
    {
        $startOfMonth = Carbon::create($currentYear, $currentMonth)->startOfMonth();

        $builder = CoverShift::query()->where(
            'end_cover_date', '>=', $startOfMonth
        )->whereHas('Classroom', function (Builder $classroom) {
            $classroom->where('branch', Auth::user()->{'branch'});
        });

        if (!empty($filters)) {
            $builder->where($filters);
        }

        return $builder->get();
    }

    public function getOneCoverAction(int $id): JsonResponse
    {
        $cover = CoverShift::query()->find($id);
        return response()->json([
            'id' => $cover['id'],
            'classroom_id' => $cover['classroom_id'],
            'classroom_label' => $cover->classroom?->name . "-" . $cover->classroom?->uuid,
            'teacher_id' => $cover['teacher_id'],
            'teacher_label' => $cover->teacher?->name . "-" . $cover->teacher?->uuid,
            'supporter_label' => $cover->supporter?->name . "-" . $cover->supporter?->uuid,
            'start_cover_date' => Carbon::parse($cover['start_cover_date'])->format('Y-m-d'),
            'end_cover_date' => Carbon::parse($cover['end_cover_date'])->format('Y-m-d'),
            'week_days' => $cover['week_days'],
            'supporter_id' => $cover['supporter_id'],
            'start' => Carbon::parse($cover['start'])->isoFormat('HH:mm'),
            'end' => Carbon::parse($cover['end'])->isoFormat('HH:mm'),
        ]);
    }
}
