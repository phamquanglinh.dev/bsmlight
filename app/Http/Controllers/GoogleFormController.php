<?php

namespace App\Http\Controllers;

use App\Helper\GoogleClientHelper;
use App\Helper\HttpResponse;
use App\Models\Branch;
use App\Models\UserConfig;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class GoogleFormController extends Controller
{
    private GoogleClientHelper $googleClientHelper;

    public function __construct(
        GoogleClientHelper $googleClientHelper
    )
    {
        $this->googleClientHelper = $googleClientHelper;
    }

    public function getView(Request $request): View|RedirectResponse
    {
        $params = $request->input();

        if (!empty($params['code'])) {
            $callback = url('/') . '/integration/google_form';

            $tokens = $this->googleClientHelper->authWithCode($params['code'], $callback);

            if (!$tokens) {
                return view('integration.google_form')->with('error', 'Kết nối thất bại');
            }

            $dataSetting = [
                'access_token' => $tokens['access_token'],
                'refresh_token' => $tokens['refresh_token'],
                'expired' => Carbon::now()->addSeconds($tokens['expires_in']),
                'is_connected' => 1
            ];

            UserConfig::query()->updateOrInsert([
                'user_id' => Auth::id(),
                'branch' => Auth::user()->{'branch'}
            ], [
                'user_id' => Auth::id(),
                'type' => UserConfig::GOOGLE_FORM,
                'settings' => json_encode($dataSetting),
                'branch' => Auth::user()->{'branch'}
            ]);

            return redirect()->to("integration/google_form")->with('success', 'Kết nối thành công');
        }

        return view('integration.google_form');
    }

    public function getConnectUrlAction(HttpResponse $httpResponse): JsonResponse
    {
        $scopes = [
            'https://www.googleapis.com/auth/spreadsheets'
        ];

        $callback = url('/') . '/integration/google_form';

        return $httpResponse->responseData([
            'url' => $this->googleClientHelper->makeLoginUrl($scopes, $callback)
        ]);
    }

    public function getGoogleFormConfigAction(HttpResponse $httpResponse): JsonResponse
    {
        $record = UserConfig::query()->where([
            'user_id' => Auth::id(),
            'branch' => Auth::user()->{'branch'}
        ])->first();

        if (!$record) {
            return $httpResponse->responseData([
                'is_connected' => 0
            ]);
        }

        $setting = silence(fn() => json_decode($record->{'settings'}, 1), [
            'is_connected' => 0
        ]);

        unset($setting['access_token']);
        unset($setting['refresh_token']);
        unset($setting['expired']);

        return $httpResponse->responseData($setting);
    }

    public function getGoogleFormTabsAction(HttpResponse $httpResponse, string $sheetId): JsonResponse
    {
        $client = new Client();

        $config = UserConfig::query()->where([
            'user_id' => Auth::id(),
            'branch' => Auth::user()->{'branch'}
        ])->first();

        if (!$config) {
            throw new BadRequestException('Kết nối lỗi, ngắt kết nối và thử lại');
        }

        $settings = $this->getGoogleSetting();

        $baseUrl = 'https://sheets.googleapis.com/v4/spreadsheets/' . $sheetId;

        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $settings['access_token'],
                'Content-Type' => 'application/json',
            ]
        ];

        $response = $this->tryApiException(
            fn() => $client->get($baseUrl, $options)
        );

        $responseData = json_decode($response->getBody(), 1);

        return $httpResponse->responseData($responseData['sheets']);
    }

    public function getGoogleFormSheetDetailAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $client = new Client();

        $settings = $this->getGoogleSetting();

        $params = $request->input();

        $spreadsheetId = $params['spreadsheetId'];

        $tabId = $params['tabId'];

        $range = "{$tabId}!1:1";

        $url = "https://sheets.googleapis.com/v4/spreadsheets/{$spreadsheetId}/values/{$range}";

        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $settings['access_token'],
                'Content-Type' => 'application/json',
            ]
        ];

        $response = $this->tryApiException(
            fn() => $client->get($url, $options)
        );

        $responseData = json_decode($response->getBody(), 1);

        $dataValues = [];

        foreach ($responseData['values'][0] as $key => $value) {
            $dataValues[] = [
                'key' => $key,
                'value' => $value
            ];
        }

        return $httpResponse->responseData($dataValues);
    }

    public function createGoogleScriptCode(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $branch = Branch::query()->where('uuid', Auth::user()->{'branch'})->first();
        $token = $branch->{'api-x-token'};
        $form = $request->input()['form'];

        $dataPayload = [];

        foreach ($form as $formData) {

            if ($formData['bsm'] == "Chọn thuộc tính ....") {
                continue;
            }

            $dataPayload[$formData['bsm']] = "formData[{$formData['google']['key']}]";
        }

        $dataPayload['X-API-KEY'] = $token;

        $url = url('/') . "/api/webhook/account/google_sheet";

        $payload = json_encode($dataPayload);
        $payload = str_replace("\"formData", "formData", $payload);
        $payload = str_replace("]\",", "],\n", $payload);
        $script = <<<EOD
function sendFormData(e) {
  const url = '$url';
  const formData = e.values;

  const payload = $payload;

  const options = {
    'method': 'post',
    'contentType': 'application/json',
    'payload': JSON.stringify(payload),
  };

  UrlFetchApp.fetch(url, options);
}

function deleteAllTriggers() {
    var allTriggers = ScriptApp.getProjectTriggers();
    for (var i = 0; i < allTriggers.length; i++) {
        ScriptApp.deleteTrigger(allTriggers[i]);
    }
    Logger.log('Deleted ' + allTriggers.length + ' triggers.');
}

function initialize() {
  deleteAllTriggers()
  const sheet = SpreadsheetApp.getActiveSpreadsheet();
  ScriptApp.newTrigger('sendFormData')
    .forSpreadsheet(sheet)
    .onFormSubmit()
    .create();
}
EOD;
        return $httpResponse->responseData([
            'script' => $script
        ]);
    }

    private function tryApiException(callable $callback): ResponseInterface
    {
        try {
            return $callback();
        } catch (ClientException $exception) {
            throw new BadRequestException($exception->getMessage());
        }
    }

    /**
     * @throws GuzzleException
     */
    private function getGoogleSetting(): array
    {
        $config = UserConfig::query()->where([
            'user_id' => Auth::id(),
            'branch' => Auth::user()->{'branch'}
        ])->first();

        if (!$config) {
            throw new BadRequestException('Kết nối lỗi, ngắt kết nối và thử lại');
        }

        $settings = json_decode($config->{'settings'}, 1);

        if (Carbon::parse($settings['expired']) < now()) {
            $tokens = $this->googleClientHelper->authWithRefreshToken($settings['refresh_token']);

            $dataSetting = [
                'access_token' => $tokens['access_token'],
                'refresh_token' => $settings['refresh_token'],
                'expired' => Carbon::now()->addSeconds($tokens['expires_in']),
                'is_connected' => 1
            ];

            UserConfig::query()->updateOrInsert([
                'user_id' => Auth::id(),
                'branch' => Auth::user()->{'branch'}
            ], [
                'user_id' => Auth::id(),
                'type' => UserConfig::GOOGLE_FORM,
                'settings' => json_encode($dataSetting),
                'branch' => Auth::user()->{'branch'}
            ]);
        }

        return $settings;
    }

}
