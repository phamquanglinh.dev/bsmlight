<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\HttpResponse;
use App\Models\Cashier;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class CashierController extends Controller
{
    public function getAllCashierAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $builder = Cashier::query();

        Cashier::handleQuery($request, $builder);
        Cashier::customQuery($request, $builder);

        $calculateCashierAmount = Cashier::calculateCashierAmount();

        $collection = $builder->get()->map(function (Cashier $cashier) use ($calculateCashierAmount, $request) {
            $cashier->handleQueryProcess($request);
            $cashier->handleBalance($calculateCashierAmount);
            return $cashier;
        })->toArray();

        return $httpResponse->responseCollection($collection);
    }

    public function listView(): View
    {
        return view('finance.cashier.list');
    }

    public function getCashierFields(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $fields = Cashier::primaryFields();

        return $httpResponse->responseCollection($fields);
    }

    public function createTransactionView(Request $request): Factory|\Illuminate\Contracts\View\View|Application
    {
        $crudBag = new CrudBag();

        $crudBag->setLabel('Giao dịch');
        $crudBag->setAction('cashiers.store');
        $crudBag->setHasFile(true);

        $this->handleField($crudBag);

        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    private function handleField(CrudBag &$crudBag, Cashier $cashier = null): void
    {
        $crudBag->setParam(CrudBag::BEFORE_CONTENT, 'fields.quick_add_transaction_type');
        $crudBag->addFields([
            'name' => 'transaction_type',
            'type' => 'select',
            'options' => TransactionType::query()->where('branch', Auth::user()->{'branch'})->orWhere('branch', null)->get()->mapWithKeys(function ($item) {
                return [$item->id => $item->name];
            })->toArray(),
            'label' => 'Loại giao dịch',
            'required' => true,
            'value' => $cashier ? $cashier['transaction_type'] : "",
        ]);

        $crudBag->addFields([
            'name' => 'amount',
            'type' => 'number',
            'label' => 'Số tiền',
            'required' => true,
            'value' => $cashier ? $cashier['amount'] : ""
        ]);

        $crudBag->addFields([
            'name' => 'transaction_day',
            'type' => 'datetime',
            'label' => 'Ngày giao dịch',
            'required' => true,
            'value' => $cashier ? $cashier['transaction_day'] : ""
        ]);
        $crudBag->addFields([
            'name' => 'object_image',
            'type' => 'upload',
            'label' => 'Biên lai',
        ]);

        $crudBag->addFields([
            'name' => 'notes',
            'type' => 'textarea',
            'label' => 'Ghi chú',
            'class' => 'col-md-10',
            'value' => $cashier ? $cashier['notes'] : ""
        ]);
    }


    /**
     * @throws ValidationException
     */
    public function storeTransactionType(Request $request): RedirectResponse
    {
        $input = $request->input();

        $this->validate($request, [
            'group' => 'string|required|in:' . implode(',', array_keys(TransactionType::groupOptions())),
            'name' => [
                'required',
                Rule::unique('transaction_types')->where(function ($query) {
                    return $query->where('branch', Auth::user()->{'branch'});
                }),
            ],
        ]);

        TransactionType::query()->create([
            'group' => $input['group'],
            'name' => $input['name'],
            'branch' => Auth::user()->{'branch'},
            'status' => 1
        ]);

        return redirect()->back()->with('success', 'Thêm thành công');
    }

    /**
     * @throws ValidationException
     */
    public function storeOtherTransaction(Request $request): RedirectResponse
    {
        $input = $request->input();

        $this->validate($request, [
            'transaction_type' => 'integer|required',
            'amount' => 'string|required',
            'transaction_day' => 'string|required',
            'object_image' => 'file|mimes:jpeg,png,jpg,gif,svg|nullable',
            'notes' => 'string|nullable',
        ]);

        Transaction::query()->create([
            'uuid' => Carbon::now()->timestamp,
            'transaction_type' => $input['transaction_type'],
            'amount' => n($input['amount']),
            'transaction_day' => $input['transaction_day'],
            'object_image' => isset($input['object_image']) ? uploads($input['object_image']) : null,
            'notes' => $input['notes'] ?? "Không có ghi chú",
            'branch' => Auth::user()->{'branch'},
            'created_by' => Auth::id(),
            'object_id' => 0,
            'object_type' => 'other',
            'approve' => 0,
            'status' => 1,
        ]);

        return redirect()->to('/finance/cashiers/list')->with('success', 'Thành công');
    }

    public function editCashierView(int $id): View|RedirectResponse
    {
        /**
         * @var Cashier $cashier
         */
        $cashier = Cashier::query()->where('id', $id)->firstOrFail();

        if ($cashier->{'object_type'} == Transaction::CARD_TRANSACTION_TYPE) {
            return redirect("/transaction/edit/{$id}/card/{$cashier->{'object_id'}}");
        }

        if ($cashier->{'object_type'} == Transaction::SALARY_PAID_OBJECT_TYPE) {
            return redirect("/salary/transaction/{$id}");
        }

        $crudBag = new CrudBag();

        $crudBag->setId($id);

        $crudBag->setAction('cashiers.update');

        $this->handleField($crudBag, $cashier);

        return \view("create", [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function updateCashierView(Request $request, int $id)
    {
        $cashier = Cashier::query()->where('id', $id)->firstOrFail();

        $this->validate($request, [
            'transaction_type' => 'required',
            'amount' => 'string|required',
            'transaction_day' => 'string|required',
            'object_image' => 'file|mimes:jpeg,png,jpg,gif,svg|nullable',
            'notes' => 'string|nullable',
        ]);

        $input = $request->input();

        $cashier->update([
            'transaction_type' => $input['transaction_type'],
            'amount' => n($input['amount']),
            'transaction_day' => $input['transaction_day'],
            'object_image' => isset($input['object_image']) ? uploads($input['object_image']) : $cashier['object_image'],
            'notes' => $input['notes'] ?? $cashier['notes'],
        ]);

        return redirect()->to('/finance/cashiers/list')->with('success', 'Thành công');
    }

    public function deleteCashierView(int $id)
    {
        $cashier = Cashier::query()->where('id', $id)->firstOrFail();

        if ($cashier->{'object_type'} == Transaction::CARD_TRANSACTION_TYPE) {
            return redirect("/transaction/delete/{$id}");
        }

        $cashier->delete();

        return redirect()->to('/finance/cashiers/list')->with('success', 'Thành công');
    }
}
