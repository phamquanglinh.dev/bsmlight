<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\Staff;
use App\Models\Student;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function create()
    {
        $modules = Permission::groupByModule();

        return view('permission.permission-create', [
            'modules' => $modules
        ]);
    }

    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $users = $this->handleGetUsers($request->get('users') ?? []);

            $permissionsMapped = Permission::query()->get()->mapWithKeys(function (Permission $permission) {
                return [$permission['key'] => $permission['id']];
            });

            $rolePermissions = [];

            $name = $request->get('name');

            $role = Role::query()->create([
                'name' => $name,
                'branch' => Auth::user()->{'branch'}
            ]);

            $permissions = array_keys($request->get('permissions'));

            foreach ($permissions as $permission) {
                $permissionId = $permissionsMapped[$permission];

                $rolePermissions[] = [
                    'role_id' => $role['id'],
                    'permission_id' => $permissionId
                ];
            }

            DB::table('role_permission')->insert($rolePermissions);

            $userRole = [];
            foreach ($users as $user) {
                $userRole[] = [
                    'role_id' => $role['id'],
                    'user_id' => $user
                ];
            }

            DB::table('role_user')->insert($userRole);
        });

        return redirect()->to('permission/setting')->with('success', 'Thêm vai trò thành công');
    }

    public function getOneRole(Request $request)
    {
        /**
         * @var Role $roleRecord
         */

        $role = $request->get('role');

        switch ($role) {
            case 'default_staff':
                return $this->getDefaultRolePermissions(User::STAFF_ROLE);

            case 'default_teacher':
                return $this->getDefaultRolePermissions(User::TEACHER_ROLE);

            case 'default_supporter':
                return $this->getDefaultRolePermissions(User::SUPPORTER_ROLE);

            case 'default_student':
                return $this->getDefaultRolePermissions(User::STUDENT_ROLE);

            default:

                $roleRecord = Role::query()->where('id', $role)->first();

                if (!$roleRecord) {
                    $permissions = [];

                    return response()->json([
                        'permissions' => $permissions
                    ]);
                }

                $permissions = $roleRecord->Permissions()?->get()->pluck('key')->toArray();

                return response()->json([
                    'permissions' => $permissions
                ]);

        }
    }

    private function getDefaultRolePermissions(int $role)
    {
        $permissions = [];
        $entries = defineScope()[$role];

        foreach ($entries as $entry) {
            foreach (Permission::defineAction() as $actionKey => $action) {
                $permissions[] = "$actionKey $entry";
            }
        }


        foreach (definePermission()[$role] as $singlePermission) {
            $permissions[] = $singlePermission;
        }

        return response()->json([
            'permissions' => $permissions
        ]);
    }

    private function handleGetUsers(array $users)
    {
        $userIds = [];

        foreach ($users as $user) {
            switch ($user) {
                case 'teacher':
                    $teachers = Teacher::query()->where('branch', Auth::user()->{'branch'})->get();

                    foreach ($teachers as $teacher) {
                        $userIds[] = $teacher['id'];
                    }
                    break;
                case 'student':
                    $students = Student::query()->where('branch', Auth::user()->{'branch'})->get();

                    foreach ($students as $student) {
                        $userIds[] = $student['id'];
                    }
                    break;
                case 'staff':
                    $staffs = Staff::query()->where('branch', Auth::user()->{'branch'})->get();

                    foreach ($staffs as $staff) {
                        $userIds[] = $staff['id'];
                    }
                    break;
                case 'supporter':
                    $supporters = Supporter::query()->where('branch', Auth::user()->{'branch'})->get();

                    foreach ($supporters as $supporter) {
                        $userIds[] = $supporter['id'];
                    }
                    break;
                default:
                    $userIds[] = $user;
                    break;
            }
        }

        return $userIds;
    }
}
