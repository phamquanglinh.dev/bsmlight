<?php

namespace App\Http\Controllers;

use App\Models\SystemLogTest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SystemLogApiController extends Controller
{
    public function create(Request $request): JsonResponse
    {
        $input = $request->input();
        $input['description'] = json_encode($input['description']);
        $systemLog = SystemLogTest::query()->create($input);

        return response()->json($systemLog->toArray());
    }
}
