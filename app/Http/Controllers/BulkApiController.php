<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Helper\UUID;
use App\Models\Card;
use App\Models\Classroom;
use App\Models\Student;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BulkApiController extends Controller
{
    public function bulkCloneCardAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'card_id' => 'required|integer',
            'student_ids' => 'required|array',
        ]);

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->errors());
        }

        $card = Card::query()->where('id', $input['card_id'])->first();

        if (!$card) {
            return $httpResponse->notFoundResponse();
        }

        $studentIds = $input['student_ids'];

        foreach ($studentIds as $studentId) {
            $newCard = $card->replicate();

            $newCard->setAttribute('student_id', $studentId);

            $newCard->setAttribute('uuid', UUID::make(Auth::user()->{'branch'}, new Card(), 'StudyCard'));

            $newCard->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Bulk clone card success',
        ]);
    }

    public function getAllStudent(HttpResponse $httpResponse): JsonResponse
    {
        $students = Student::query()->where('branch', Auth::user()->{'branch'})->get()->map(function (Student $student) {
            return [
                'id' => $student['id'],
                'name' => $student['name'],
                'avatar' => $student['avatar'],
                'uuid' => $student['uuid'],
                'linked' => $student->Cards()?->count() > 0 ? 1 : 0
            ];
        });

        return $httpResponse->responseCollection($students->toArray());
    }

    public function getAllCard(HttpResponse $httpResponse): JsonResponse
    {
        $cards = Card::query()->where('branch', Auth::user()->{'branch'})->get()->map(function (Card $card) {
            return [
                'id' => $card['id'],
                'uuid' => $card['uuid'],
                'student' => $card->student?->only(['id', 'name', 'uuid', 'avatar']),
                'value' => $card['uuid'] . "-" . $card->student?->name . "-" . $card->student?->uuid,
                'linked' => $card->classroom_id !== null ? 1 : 0
            ];
        });

        return $httpResponse->responseCollection($cards->toArray());
    }

    public function addCardToClassroom(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'card_ids' => 'required|array',
            'classroom_id' => 'required|integer',
        ]);

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->errors());
        }

        $classroom = Classroom::query()->where('id', $input['classroom_id'])->first();

        if (!$classroom) {
            return $httpResponse->notFoundResponse();
        }

        $cardIds = $input['card_ids'];

        foreach ($cardIds as $cardId) {
            $card = Card::query()->where('id', $cardId)->first();

            if (!$card) {
                continue;
            }

            $card->setAttribute('classroom_id', $input['classroom_id']);

            $card->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Add card to classroom success',
        ]);
    }

    public function getAllClassroom(HttpResponse $httpResponse): JsonResponse
    {
        $cards = Classroom::query()->where('branch', Auth::user()->{'branch'})->get()->map(function (Classroom $classroom) {
            return [
                'id' => $classroom['id'],
                'uuid' => $classroom['uuid'],
                'name' => $classroom['name'],
                'avatar' => $classroom['avatar'],
                'value' => $classroom['uuid'] . "-" . $classroom['name']
            ];
        });

        return $httpResponse->responseCollection($cards->toArray());
    }
}
