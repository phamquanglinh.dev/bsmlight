<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\CardLog;
use App\Models\Cashier;
use App\Models\Classroom;
use App\Models\Salary;
use App\Models\StudyLog;
use App\Models\Teacher;
use App\Models\TeacherProfile;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\WorkingShift;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;


class ReportController extends Controller
{
    public function getCashFlowView(): View
    {
        return \view('report.reportReact');
    }

    public function getEducationGrossView(): View
    {
        return \view('report.reportReact');
    }

    public function getCashFlowPieChart(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();
        $start = isset($input['time']) ? Carbon::createFromTimestamp($input['time'])->firstOfMonth() : Carbon::now()->firstOfMonth();
        $end = isset($input['time']) ? Carbon::createFromTimestamp($input['time'])->endOfMonth() : Carbon::now()->endOfMonth();
        $data = [];
        $total = 0;

        $revenueColor = [
            "Aqua", "Chartreuse", "Crimson", "Cyan", "DeepPink",
            "Fuchsia", "Gold", "GreenYellow", "HotPink", "Lime",
            "Magenta", "Orange", "OrangeRed", "Pink", "Red",
            "SpringGreen", "Turquoise", "Violet", "Yellow", "LightSkyBlue",
            "MediumSpringGreen", "Salmon", "LightCoral", "DarkOrange", "YellowGreen",
            "MediumAquamarine", "PaleGreen", "LightSeaGreen", "MediumTurquoise", "MediumPurple"
        ];
        $paymentColor = [
            "Red", "Orange", "Yellow", "Gold", "Coral",
            "Tomato", "DarkOrange", "Crimson", "IndianRed", "Salmon",
            "FireBrick", "OrangeRed", "DarkRed", "LightCoral", "Sienna",
            "Chocolate", "SandyBrown", "GoldenRod", "DarkGoldenRod", "Peru",
            "DarkOrange", "DarkSalmon", "RosyBrown", "Tan", "BurlyWood",
            "PeachPuff", "Moccasin", "Bisque", "BlanchedAlmond", "NavajoWhite"
        ];
        switch ($input["type"]) {
            case 'in':
                /**
                 * @var Cashier $inCashier
                 */
                $inCashiers = Cashier::query()->select([
                    "object_type",
                    "object_id",
                    "amount",
                    "status",
                    "transaction_type"

                ])->where(function (Builder $builder) {
                    $builder->whereIn('object_type', [
                        Transaction::CARD_TRANSACTION_TYPE,
                    ])->orWhereHas('TransactionType', function (Builder $builder) {
                        $builder->where('group', TransactionType::GROUP_A)->where('branch', Auth::user()->{'branch'});
                    });
                })
                    ->where("status", 1)
                    ->where('transaction_day', '>=', $start)
                    ->where('transaction_day', '<=', $end)
                    ->get();
                foreach ($inCashiers as $inCashier) {
                    $inCashier->handleQueryProcess($request);
                }
                foreach ($inCashiers as $inCashier) {
                    $total += $inCashier['amount'];

                    $key = $inCashier['transaction_type_detail'];
                    if (!isset($data[$key])) {
                        $data[$key]['value'] = $inCashier['amount'];

                    } else {
                        $data[$key]['value'] += $inCashier['amount'];
                    }
                }

                foreach ($data as $key => $item) {
                    $data[$key]['percent'] = round($item['value'] / $total * 100);
                    $data[$key]['label'] = $key == "A.2. Thu học phí Ngoại Ngữ (Tiếng Anh, Tiếng Trung,...)" ? "A.2. Thu học phí" : $key;
                    $data[$key]['color'] = array_pull($revenueColor, array_key_first($revenueColor));
                }
                $pieData = [
                    "data" => array_values($data),
                    'total' => $total,
                ];
                return $httpResponse->responseData($pieData);
            case 'out':
                $outCashiers = Cashier::query()->select([
                    "object_type",
                    "object_id",
                    "amount",
                    "status",
                    "transaction_type"
                ])->where("status", 1)
                    ->where(function (Builder $builder) {
                        $builder->whereIn('object_type', [
                            Transaction::SALARY_PAID_OBJECT_TYPE,
                        ])->orWhereHas('TransactionType', function (Builder $builder) {
                            $builder->where('group', TransactionType::GROUP_C)->where('branch', Auth::user()->{'branch'});
                        });
                    })
                    ->where('transaction_day', '>=', $start)
                    ->where('transaction_day', '<=', $end)->get();
                foreach ($outCashiers as $outCashier) {
                    $outCashier->handleQueryProcess($request);
                }
                foreach ($outCashiers as $outCashier) {
                    $total += $outCashier['amount'];

                    $key = $outCashier['transaction_type_detail'];
                    if (!isset($data[$key])) {
                        $data[$key]['value'] = $outCashier['amount'];

                    } else {
                        $data[$key]['value'] += $outCashier['amount'];
                    }
                }

                foreach ($data as $key => $item) {
                    $data[$key]['percent'] = round($item['value'] / $total * 100);
                    $data[$key]['label'] = $key;
                    $data[$key]['color'] = array_pull($paymentColor, array_key_first($paymentColor));
                }
                $pieData = [
                    "data" => array_values($data),
                    'total' => $total,
                ];
                return $httpResponse->responseData($pieData);
            default:
                return $httpResponse->notFoundResponse();
        }
    }

    public function getCashFlowData(Request $request, HttpResponse $httpResponse): JsonResponse
    {

    }

    public function getCashFlowStatistics(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();
        $start = isset($input['time']) ? Carbon::createFromTimestamp($input['time'])->firstOfMonth() : Carbon::now()->firstOfMonth();
        $end = isset($input['time']) ? Carbon::createFromTimestamp($input['time'])->endOfMonth() : Carbon::now()->endOfMonth();

        $inCashiers = Cashier::query()->select([
            "object_type",
            "object_id",
            "amount",
            "status",
            "transaction_type"

        ])->where(function (Builder $builder) {
            $builder->whereIn('object_type', [
                Transaction::CARD_TRANSACTION_TYPE,
            ])->orWhereHas('TransactionType', function (Builder $builder) {
                $builder
                    ->where('group', TransactionType::GROUP_A)
                    ->where('branch', Auth::user()->{'branch'});
            });
        })
            ->where("status", 1)
            ->where('transaction_day', '>=', $start)
            ->where('transaction_day', '<=', $end)
            ->sum('amount');

        $outCashiers = Cashier::query()->select([
            "object_type",
            "object_id",
            "amount",
            "status",
            "transaction_type"
        ])->where("status", 1)
            ->where(function (Builder $builder) {
                $builder->whereIn('object_type', [
                    Transaction::SALARY_PAID_OBJECT_TYPE,
                ])->orWhereHas('TransactionType', function (Builder $builder) {
                    $builder->where('group', TransactionType::GROUP_C)->where('branch', Auth::user()->{'branch'});
                });
            })
            ->whereHas("Creator", function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'});
            })
            ->where('transaction_day', '>=', $start)
            ->where('transaction_day', '<=', $end)
            ->sum('amount');

        return $httpResponse->responseData([
            'revenue' => (int)$inCashiers,
            'payment' => (int)$outCashiers,
            'total' => $inCashiers - $outCashiers,
            'color' => $inCashiers > $outCashiers ? "success" : "danger"
        ]);
    }

    public function getEducationGrossBarChart(Request $request, HttpResponse $httpResponse): JsonResponse
    {

    }

    public function getEducationGrossStatistic(Request $request, HttpResponse $httpResponse): JsonResponse
    {

    }

    public function getEducationGrossData(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $start = isset($input['time']) ? Carbon::createFromTimestamp($input['time'])->firstOfMonth() : Carbon::now()->firstOfMonth();
        $end = isset($input['time']) ? Carbon::createFromTimestamp($input['time'])->endOfMonth() : Carbon::now()->endOfMonth();

        $data = [];

        /**
         * @var Classroom $classroom
         */
        $classrooms = Classroom::query()->get();
        $labels = [];
        foreach ($classrooms as $classroom) {
            $data[] = [
                'id' => $classroom['id'],
                'uuid' => $classroom['uuid'],
                'name' => $classroom['name'],
                'attended_times' => $this->getAttendedTimes($classroom, $start, $end),
                'active_days' => implode($this->getActiveDays($classroom, $start, $end)),
                'student_alive' => $this->getStudentAlive($classroom, $start, $end),
                'avg_student_alive' => $this->avgStudentAlive($classroom, $start, $end),
                'revenue' => (int)$this->getRevenue($classroom, $start, $end),
                'teacher_v_salary' => $this->getSalaries($classroom, $start, $end)['teacher_v'],
                'supporter_salary' => $this->getSalaries($classroom, $start, $end)['supporter'],
                'teacher_f_salary' => $this->getSalaries($classroom, $start, $end)['teacher_f'],
                'gross' => $this->getGross($classroom, $start, $end),
                'gross_percent' => round($this->getGrossPercent($classroom, $start, $end))
            ];
            $labels[] = $classroom['name'];
        }
        $series = [];
        $labelData = [
            'revenue' => 'Doanh thu thực',
            'teacher_v_salary' => 'Chi lương GVVN',
            'teacher_f_salary' => 'Chi lương GVNN',
            'supporter_salary' => 'Chi lương TG',
            'gross' => 'Gross'
        ];
        $colorData = [
            'revenue' => 'green',
            'teacher_v_salary' => 'pink',
            'teacher_f_salary' => 'pink',
            'supporter_salary' => 'pink',
            'gross' => 'yellow'
        ];

        foreach ($labelData as $key => $label) {
            $series[] = [
                'dataKey' => $key,
                'label' => $label,
                'color' => $colorData[$key] ?? ""
            ];
        }

        $dataBarChart = [
            'dataset' => $data,
            'series' => $series,
            'labels' => $labels,
            'tableTitles' => [
                ['key' => 'uuid', 'label' => 'Mã'],
                ['key' => 'name', 'label' => 'Tên lớp'],
                ['key' => 'attended_times', 'label' => 'Số buổi'],
                ['key' => 'active_days', 'label' => 'Danh sách ngày trong tháng hoạt động'],
                ['key' => 'student_alive', 'label' => 'Số lượt đi học + VCT'],
                ['key' => 'avg_student_alive', 'label' => 'Sĩ số TB/Buổi'],
                ['key' => 'revenue', 'label' => 'Doanh thu thực'],
                ['key' => 'teacher_v_salary', 'label' => 'Chi lương GV Việt'],
                ['key' => 'supporter_salary', 'label' => 'Chi lương GVNN'],
                ['key' => 'teacher_f_salary', 'label' => 'Chi lương TG'],
                ['key' => 'gross', 'label' => 'Gross'],
                ['key' => 'gross_percent', 'label' => '% Gross'],
            ],
            'dataTable' => [[
                'uuid' => 'Tổng',
                'name' => '-',
                'attended_times' => $this->array_sum_key($data, 'attended_times'),
                'active_days' => '',
                'student_alive' => $this->array_sum_key($data, 'active_days'),
                'avg_student_alive' => $this->array_sum_key($data, 'attended_times') != 0 ? $this->array_sum_key($data, 'active_days') / $this->array_sum_key($data, 'attended_times') : 0,
                'revenue' => $this->array_sum_key($data, 'revenue'),
                'teacher_v_salary' => $this->array_sum_key($data, 'teacher_v_salary'),
                'supporter_salary' => $this->array_sum_key($data, 'supporter_salary'),
                'teacher_f_salary' => $this->array_sum_key($data, 'teacher_f_salary'),
                'gross' => $this->array_sum_key($data, 'gross'),
                'gross_percent' => $this->array_sum_key($data, 'revenue') != 0 ? $this->array_sum_key($data, 'gross') / $this->array_sum_key($data, 'revenue') * 100 : 0,
            ], ...$data]
        ];

        return $httpResponse->responseData($dataBarChart);
    }

    private function studyLogBuilder($classroom, $start, $end): Builder|HasMany
    {
        return $classroom->StudyLogs()
            ->where("studylog_day", ">=", $start)
            ->where('studylog_day', "<=", $end)
            ->where("status", StudyLog::ACCEPTED);
    }

    private function getAttendedTimes(Builder|Classroom $classroom, $start, $end)
    {
        return $this->studyLogBuilder($classroom, $start, $end)->count();
    }

    private function getActiveDays(Builder|Classroom $classroom, Carbon $start, Carbon $end): array
    {
        return $this->studyLogBuilder($classroom, $start, $end)->get(["studylog_day", "id"])->mapWithKeys(function (StudyLog $studyLog) {
            return [$studyLog['id'] => $studyLog["studylog_day"]->day];
        })->toArray();
    }

    private function getStudentAlive(Builder|Classroom $classroom, Carbon $start, Carbon $end): int
    {
        return CardLog::query()->whereHas('StudyLog', function (Builder $studylog) use ($end, $start, $classroom) {
            $studylog->withoutGlobalScopes()
                ->where("studylog_day", ">=", $start)
                ->where('studylog_day', "<=", $end)
                ->where('classroom_id', $classroom['id'])->where('status', StudyLog::ACCEPTED);
        })
            ->where("status", "<", 3)
            ->where('can_deg', 1)->count('id');
    }

    private function getRevenue(Builder|Classroom $classroom, Carbon $start, Carbon $end)
    {
        return CardLog::query()->whereHas('StudyLog', function (Builder $studylog) use ($end, $start, $classroom) {
            $studylog->withoutGlobalScopes()
                ->where("studylog_day", ">=", $start)
                ->where('studylog_day', "<=", $end)
                ->where('classroom_id', $classroom['id'])
                ->where('status', StudyLog::ACCEPTED);
        })
            ->where('can_deg', 1)->sum('fee');
    }

    private function avgStudentAlive(Builder|Classroom $classroom, Carbon $start, Carbon $end): float|int
    {
        return $this->getAttendedTimes($classroom, $start, $end) > 0 ? $this->getStudentAlive($classroom, $start, $end) / $this->getAttendedTimes($classroom, $start, $end) : 0;
    }

    private function validWorkingShifts(Builder|Classroom $classroom, Carbon $start, Carbon $end)
    {
        return WorkingShift::query()->whereHas('StudyLog', function (Builder $studylog) use ($end, $start, $classroom) {
            $studylog->withoutGlobalScopes()
                ->where("studylog_day", ">=", $start)
                ->where('studylog_day', "<=", $end)
                ->where('classroom_id', $classroom['id'])
                ->where('status', StudyLog::ACCEPTED);
        })->get();
    }

    private function getSalaries(Builder|Classroom $classroom, Carbon $start, Carbon $end): array
    {
        $salaries = [
            'teacher_v' => 0,
            'teacher_f' => 0,
            'supporter' => 0
        ];
        $workingShifts = $this->validWorkingShifts($classroom, $start, $end);

        foreach ($workingShifts as $workingShift) {
            $teacherSource = TeacherProfile::query()->where('user_id', $workingShift['teacher_id'])->first()?->teacher_source;
            $teacherSalary = Salary::query()
                ->where('user_id', $workingShift['teacher_id'])
                ->where('classroom_id', $classroom['id'])
                ->where('period', $start->toDateString())
                ->first()?->total_value ?? 0;

            $supporterSalary = Salary::query()
                ->where('user_id', $workingShift['teacher_id'])
                ->where('classroom_id', $classroom['id'])
                ->where('period', $start->toDateString())
                ->first()?->total_value ?? 0;
            $salaries['teacher_v'] += $teacherSource == Teacher::INTERNAL_SOURCE ? $teacherSalary : 0;
            $salaries['teacher_f'] += $teacherSource == Teacher::EXTERNAL_SOURCE ? $teacherSalary : 0;
            $salaries['supporter'] += $supporterSalary;
        }

        return $salaries;
    }

    private function getGross(Builder|Classroom $classroom, Carbon $start, Carbon $end)
    {
        $salaries = $this->getSalaries($classroom, $start, $end);

        return $this->getRevenue($classroom, $start, $end) - $salaries['teacher_v'] - $salaries['teacher_f'] - $salaries['supporter'];
    }

    private function getGrossPercent(Builder|Classroom $classroom, Carbon $start, Carbon $end): float|int
    {
        return $this->getRevenue($classroom, $start, $end) != 0 ? ($this->getGross($classroom, $start, $end) / $this->getRevenue($classroom, $start, $end)) * 100 : 0;
    }

    private function array_sum_key(array $data, string $key, $method = 'sum')
    {
        $value = 0;
        $count = 0;
        foreach ($data as $item) {
            $value += (int)$item[$key];
            $count++;
        }

        if ($method === 'avg') {
            return $count !== 0 ? $value / ($count) : 0;
        }

        return $value;
    }
}
