<?php

namespace App\Http\Controllers;

use App\Models\Receipt;
use App\Models\ReceiptTemplate;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use PhpOffice\PhpWord\IOFactory;
use ZipArchive;

class DocumentController extends Controller
{
    public function uploadReceiptView(): View
    {
        return view('receipt.upload');
    }

    public function receiptPrint(int $id): Factory|\Illuminate\Contracts\View\View|Application
    {
        $template = Receipt::query()->find($id);

        $htmlContent = str_replace("images", url("/") . "/images", $template['html_content']);

        return view('print.receipt', ['receiptData' => $htmlContent]);
    }

    public function handleUploadReceipt(Request $request)
    {
        // Validate the uploaded file
        $request->validate([
            'file' => 'required|mimes:zip',
        ]);

        // Store the uploaded ZIP file temporarily
        $path = $request->file('file')->store('temp');

        // Create a new instance of ZipArchive
        $zip = new ZipArchive;
        $res = $zip->open(storage_path('app/' . $path));

        if ($res === TRUE) {
            // Create a directory to extract the contents
            $extractPath = storage_path('app/extracted/' . pathinfo($path, PATHINFO_FILENAME));
            if (!file_exists($extractPath)) {
                mkdir($extractPath, 0777, true);
            }

            // Extract the ZIP file
            $zip->extractTo($extractPath);
            $zip->close();

            // Initialize variables
            $htmlContent = null;
            $htmlFilePath = null;
            $imageMap = [];

            // Scan the extracted directory
            $files = scandir($extractPath);
            $images = scandir($extractPath . "/images");
            $randomFolder = Str::random(5);
            foreach ($images as $image) {
                if ($image !== "." && $image != "..") {
                    $imagePath = $extractPath . "/images/" . $image;
                    $imageContent = file_get_contents($imagePath);

                    $path = public_path("images/{$randomFolder}/$image");

                    $directory = public_path()."/images/{$randomFolder}";

                    mkdir($directory, 0777, true);

                    file_put_contents($path, $imageContent);
                }
            }

            foreach ($files as $file) {
                $filePath = $extractPath . '/' . $file;

                // Check for HTML file
                if (pathinfo($file, PATHINFO_EXTENSION) == 'html') {
                    $htmlFilePath = $filePath;
                }
            }

            // Read HTML content
            if ($htmlFilePath) {
                $htmlContent = file_get_contents($htmlFilePath);

                $htmlContent = str_replace("images", "images/" . $randomFolder, $htmlContent);

                // Save HTML content to database
                ReceiptTemplate::query()->create([
                    'html_content' => $htmlContent,
                    'name' => $request->get('name') ?? Carbon::now()->toDateTimeString(),
                    'created_by' => Auth::id(),
                ]);
            }

            // Delete temporary files
            Storage::delete($path);
            $this->deleteDirectory($extractPath);

            return redirect()->back()->with('success', 'Thành công');
        } else {
            return redirect()->back()->with('error', 'Thất bại');
        }
    }

    private function deleteDirectory($dir): bool
    {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}
