<?php

namespace App\Http\Controllers;

use App\Events\ClassroomWasCreated;
use App\Events\ClassroomWasUpdated;
use App\Helper\CalendarViewModel;
use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Models\Card;
use App\Models\Classroom;
use App\Models\ClassroomSchedule;
use App\Models\ClassroomShift;
use App\Models\Staff;
use App\Models\Student;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\User;
use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ClassroomController extends Controller
{
    public function list(Request $request)
    {

        Cache::put('search_classroom' . Auth::id(), $request->getQueryString());

        $crudBag = new CrudBag();

        $crudBag->setLabel('Lớp học');
        $crudBag->setEntity('classroom');
        $crudBag->setSearchValue($request->get('search') ?? '');
        $crudBag = $this->handleFiltering($crudBag, $request);
        $crudBag = $this->handleStatistic($crudBag, $request);
        $crudBag = $this->handleColumn($crudBag, $request);
        $builder = Classroom::query();

        $this->handleBuilder($builder, $crudBag);
        if ($request->get('gross:handle') == null) {
            $listModelView = new ListViewModel($builder->paginate($request->get('perPage') ?? 10));

        } else {
            $gross = $request->get('gross:handle');

            $collection = $builder->get()->filter(function (Classroom $classroom) use ($gross) {
                return $classroom->getGrossStatusAttribute() == $gross;
            });

            $perPage = $request->get('perPage') ?? 10;

            $currentPage = $request->get('page');

            $pagedData = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();

            $paginatedObjects = new Collection($pagedData);

            $paginator = new LengthAwarePaginator(
                $paginatedObjects,
                count($collection),
                $perPage,
                $currentPage
            );

            $listModelView = new ListViewModel($paginator);
        }

        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => $listModelView
        ]);
    }

    private function handleColumn($crudBag, $request)
    {
        $crudBag->addColumn([
            'name' => 'name',
            'label' => 'Lớp học',
            'type' => 'profile',
            'attributes' => [
                'identity' => 'id',
                'entity' => 'classroom',
                'address' => 'uuid',
                'avatar' => 'avatar',
                'init'  => true
            ],
            'fixed' => 'first'
        ]);
        /**
         * @property string $uuid # Mã lớp
         * @property string $name # Tên lớp
         * @property string $avatar #Ảnh lớp
         * @property string $broadcast_chat # Nhóm chat về nội dung dạy cho nội bộ
         * @property string|null $broadcast_teacher_chat # Nhóm chat thông báo cho PH
         * @property string|null $broadcast_student_chat # Nhóm chat thông báo cho HS
         * @property ClassroomSchedule[] $classroom_schedule # Lịch học
         * @property string $schedule_last_update # Ngày cập nhật gần nhất
         * #Lấy từ ca học
         * @property Teacher[] $teacher # Giáo viên phụ trách
         * @property Supporter[] $supporter # Trợ giảng phụ trách
         * @property Staff $staff # Nhân viên phụ trách
         * Phân tích lãi lỗ (popup ra bảng lịch sử theo từng tháng)
         * @property int $total_meetings # Số buổi lớp đã chạy trong tháng
         * @property int $days # Danh sách các ngày trong tháng
         * @property int $attended_days # Số buổi bị trừ khi điểm danh
         * @property int $student_attended # Tổng số buổi đi học của học sinh
         * @property int $student_left # Tổng số buổi vắng của học sinh
         * @property int $attendance # Sĩ số hs đi học trung bình/buổi
         * @property int $total_earned # Doanh thu thực tế
         * @property int $external_salary #lương giáo viên nước ngoài
         * @property int $internal_salary #lương giáo viên Việt Nam
         * @property int $supporter_salary #lương trợ giảng
         * @property int $gross # Lãi gộp
         * @property int $gross_percent # %lãi gộp
         */
        $crudBag->addColumn([
            'name' => 'status',
            'label' => 'Trạng thái',
            'type' => 'select',
            'attributes' => [
                'options' => Classroom::classroomStatusLabel(),
                'bg' => Classroom::classroomStatusBg()
            ]
        ]);
        $crudBag->addColumn([
            'name' => 'members',
            'label' => 'Sĩ số'
        ]);
        $crudBag->addColumn([
            'name' => 'broadcast_chat',
            'label' => 'Nhóm chat về nội dung dạy cho nội bộ',
            'type' => 'link',
        ]);
        $crudBag->addColumn([
            'name' => 'broadcast_teacher_chat',
            'label' => 'Nhóm chat thông báo cho PH',
            'type' => 'link',
        ]);
        $crudBag->addColumn([
            'name' => 'broadcast_student_chat',
            'label' => 'Nhóm chat thông báo cho HS',
            'type' => 'link',
        ]);
        $crudBag->addColumn([
            'name' => 'classroom_schedule',
            'label' => 'Lịch học',
            'type' => 'array',
            'attributes' => [
                'limit' => 5
            ]
        ]);
        $crudBag->addColumn([
            'name' => 'schedule_last_update',
            'label' => 'Ngày cập nhật gần nhất',
            'type' => 'text',
        ]);
        $crudBag->addColumn([
            'name' => 'total_meetings',
            'label' => 'Số buổi chạy của lớp',
            'type' => 'text',
        ]);

        $crudBag->addColumn([
            'name' => 'student_attended',
            'label' => 'Tổng số buổi đi học của học sinh',
            'type' => 'text',
        ]);

        $crudBag->addColumn([
            'name' => 'student_left',
            'label' => 'Tổng số buổi vắng của học sinh',
            'type' => 'text',
        ]);

        $crudBag->addColumn([
            'name' => 'avg_attendance',
            'label' => 'Sĩ số hs đi học trung bình/buổi',
            'type' => 'text',
        ]);

        $crudBag->addColumn([
            'name' => 'total_earned',
            'label' => 'Doanh thu thực tế',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'name' => 'internal_salary',
            'label' => 'Chi lương giáo viên VN',
            'type' => 'number',
        ]);
        $crudBag->addColumn([
            'name' => 'external_salary',
            'label' => 'Chi lương giáo viên nước ngoài',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'name' => 'supporter_salary',
            'label' => 'Chi lương trợ giảng',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'name' => 'gross',
            'label' => 'Lãi gộp',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'name' => 'gross_percent',
            'label' => '%lãi gộp',
            'attributes' => [
                'suffix' => '%',
            ],
            'type' => 'number',

        ]);

        $crudBag->addColumn([
            'name' => 'gross_status',
            'label' => 'Tình trạng lớp',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    'SOS' => 'SOS',
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                ],
                'bg' => [
                    'SOS' => 'bg-danger-dark',
                    'A' => 'bg-danger',
                    'B' => 'bg-danger-light',
                    'C' => 'bg-green',
                    'D' => 'bg-success',
                    'E' => 'bg-purple',
                ]
            ]
        ]);

        return $crudBag;
    }
//    private function handleBuilder(Builder $builder, CrudBag $crudBag)
//    {
//        if ($crudBag->getSearchValue() !== '') {
//            $builder->where(function (Builder $subBuilder) use ($crudBag) {
//                $subBuilder->where('name', 'like', "%" . $crudBag->getSearchValue() . "%");
//            });
//        }
//
//        $builder->orderBy('created_at', 'desc');
//    }
    private function handleFiltering(CrudBag $crudBag, Request $request): CrudBag
    {
        $crudBag->addFilter([
            'name' => 'teacher:handle',
            'label' => 'Giáo viên',
            'type' => 'select2',
            'value' => $request->get('teacher:handle'),
            'attributes' => [
                'options' => Teacher::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function (Teacher $teacher) {
                    return [$teacher['id'] => $teacher['uuid'] . '-' . $teacher['name']];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'supporter:handle',
            'label' => 'Trợ giảng',
            'type' => 'select2',
            'value' => $request->get('supporter:handle'),
            'attributes' => [
                'options' => Supporter::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function (Supporter $supporter) {
                    return [$supporter['id'] => $supporter['uuid'] . '-' . $supporter['name']];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'student:handle',
            'label' => 'Học sinh',
            'type' => 'select2',
            'value' => $request->get('student:handle'),
            'attributes' => [
                'options' => Student::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function (Student $student) {
                    return [$student['id'] => $student['uuid'] . '-' . $student['name']];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'staff:handle',
            'label' => 'Nhân viên',
            'type' => 'select2',
            'value' => $request->get('staff:handle'),
            'attributes' => [
                'options' => Staff::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function (Staff $staff) {
                    return [$staff['id'] => $staff['uuid'] . '-' . $staff['name']];
                })
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'status:eq',
            'label' => 'Trạng thái lớp',
            'type' => 'select2',
            'value' => $request->get('status:eq'),
            'attributes' => [
                'options' => [
                    Classroom::STATUS_ACTIVE => 'Đang chạy',
                    Classroom::STATUS_PAUSE => 'Tạm dừng',
                    Classroom::STATUS_STOP => 'Đã dừng'
                ],
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'gross:handle',
            'label' => 'Trạng thái xử lý',
            'type' => 'select2',
            'value' => $request->get('gross:handle'),
            'attributes' => [
                'options' => [
                    'SOS' => 'SOS',
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                ],
            ]
        ]);

        $crudBag->addFilter([
            'name' => 'week:handle',
            'label' => 'Ngày trong tuần',
            'type' => 'select2',
            'value' => $request->get('week:handle'),
            'attributes' => [
                'options' => [
                    '2' => 'Thứ 2',
                    '3' => 'Thứ 3',
                    '4' => 'Thứ 4',
                    '5' => 'Thứ 5',
                    '6' => 'Thứ 6',
                    '7' => 'Thứ 7',
                    '8' => 'Chủ nhật',
                ]
            ]
        ]);

        return $crudBag;
    }

    private function handleStatistic($crudBag, Request $request)
    {
        return $crudBag;
    }

    public function create()
    {
        $crudBag = $this->handleCreateOrEdit();

        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    public function edit(int $id)
    {
        $crudBag = $this->handleCreateOrEdit($id);

        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        request()->session()->flash('schedules', $request->get('schedules'));

        $this->validate($request, [
            'name' => 'required|string|max:40',
            'avatar' => 'required',
            'schedules' => 'array',
            'schedules.*.week_day' => 'required',
            'schedules.*.start_time' => 'required',
            'schedules.*.end_time' => 'required',
            'schedules.*.shifts' => 'array|nullable',
            'schedules.*.shifts.*.teacher_id' => 'required',
            'schedules.*.shifts.*.supporter_id' => 'integer|nullable',
            'schedules.*.shifts.*.room' => 'string|nullable',
            'card_ids' => 'array',
            'status' => 'integer|required|in:1,2,3'
        ]);

        $factory = $this->classroomFactory($request->input());

        if (!empty($factory['errors'])) {
            return redirect()->back()->withErrors($factory['errors'])->with('schedules', $request->get('schedules'));
        }

        $request->merge(['schedules' => $factory['data']]);

        $dataToCreateClassroom = [
            'name' => $request->get('name'),
            'avatar' => $request->get('avatar'),
            'uuid' => Classroom::generateUUID(),
            'book' => 'template',
            'status' => $request->get('status'),
            'branch' => Auth::user()->{'branch'},
        ];

        if (Auth::user()->{'role'} == User::HOST_ROLE) {
            if ($request->has('staff_id')) {
                $dataToCreateClassroom['staff_id'] = $request->get('staff_id');
            } else {
                $dataToCreateClassroom['staff_id'] = null;
            }
        } else {
            $dataToCreateClassroom['staff_id'] = Auth::id();
        }

        $schedules = $request->get('schedules');
        $cardIds = $request->get('card_ids');

        DB::transaction(function () use ($dataToCreateClassroom, $schedules, $cardIds) {
            /**
             * @var Classroom $classroom
             */
            $classroom = Classroom::query()->create($dataToCreateClassroom);

            foreach ($schedules ?? [] as $schedule) {
                $classroomSchedule = new ClassroomSchedule();
                $classroomSchedule->classroom_id = $classroom->id;
                $classroomSchedule->week_day = $schedule['week_day'];
                $classroomSchedule->start_time = $schedule['start_time'];
                $classroomSchedule->end_time = $schedule['end_time'];

                $classroomSchedule->save();

                foreach ($schedule['shifts'] as $shift) {
                    $classroomShift = new ClassroomShift();
                    $classroomShift->classroom_schedule_id = $classroomSchedule->id;
                    $classroomShift->classroom_id = $classroom->id;
                    $classroomShift->start_time = $shift['start_time'];
                    $classroomShift->end_time = $shift['end_time'];
                    $classroomShift->teacher_id = $shift['teacher_id'];
                    $classroomShift->supporter_id = $shift['supporter_id'] ?? 0;
                    $classroomShift->room = $shift['room'] ?? '--';

                    $classroomShift->save();
                }
            }

            foreach ($cardIds ?? [] as $cardId) {
                $card = Card::query()->find($cardId);

                $card->classroom_id = $classroom->id;

                $card->save();
            }

            event(new ClassroomWasCreated($classroom));
        });
        $card_href_classroom = Cache::get('search_classroom' . Auth::id()) ?? "";
        return redirect()->to('/classroom/list?=&' . $card_href_classroom)->with('success', 'Tạo lớp học thành công');
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        /**
         * @var Classroom $classroom
         */
        $classroom = Classroom::query()->where('id', $id)->firstOrFail();

        request()->session()->flash('schedules', $request->get('schedules'));

        $this->validate($request, [
            'name' => 'required|max:40',
            'avatar' => 'required',
            'schedules' => 'array',
            'schedules.*.week_day' => 'required',
            'schedules.*.start_time' => 'required',
            'schedules.*.end_time' => 'required',
            'schedules.*.shifts' => 'array|required',
            'schedules.*.shifts.*.teacher_id' => 'required',
            'schedules.*.shifts.*.supporter_id' => 'integer|nullable',
            'schedules.*.shifts.*.room' => 'string|nullable',
            'card_ids' => 'array',
            'status' => 'integer|in:1,2,3'
        ]);

        $factory = $this->classroomFactory($request->input());

        if (!empty($factory['errors'])) {
            return redirect()->back()->withErrors($factory['errors'])->with('schedules', $request->get('schedules'));
        }

        $request->merge(['schedules' => $factory['data']]);

        $dataToCreateClassroom = [
            'name' => $request->get('name') ?? $classroom['name'],
            'avatar' => $request->get('avatar') ?? $classroom['avatar'],
            'book' => 'template',
            'staff_id' => $request->get('staff_id') ?? $classroom['staff_id'],
            'status' => $request->get('status') ?? $classroom['status']
        ];

        $schedules = $request->get('schedules');
        $cardIds = $request->get('card_ids');

        DB::transaction(function () use ($dataToCreateClassroom, $schedules, $cardIds, $classroom) {
            $classroom->update(($dataToCreateClassroom));
            $classroom->Schedules()->delete();
            $classroom->Shifts()->delete();

            foreach ($schedules ?? [] as $schedule) {
                $classroomSchedule = new ClassroomSchedule();
                $classroomSchedule->classroom_id = $classroom->id;
                $classroomSchedule->week_day = $schedule['week_day'];
                $classroomSchedule->start_time = $schedule['start_time'];
                $classroomSchedule->end_time = $schedule['end_time'];

                $classroomSchedule->save();

                foreach ($schedule['shifts'] ?? [] as $shift) {
                    $classroomShift = new ClassroomShift();
                    $classroomShift->classroom_schedule_id = $classroomSchedule->id;
                    $classroomShift->classroom_id = $classroom->id;
                    $classroomShift->start_time = $shift['start_time'];
                    $classroomShift->end_time = $shift['end_time'];
                    $classroomShift->teacher_id = $shift['teacher_id'];
                    $classroomShift->supporter_id = $shift['supporter_id'] ?? 0;
                    $classroomShift->room = $shift['room'] ?? '--';

                    $classroomShift->save();
                }
            }

            $classroom->Cards()->update([
                'classroom_id' => null
            ]);

            foreach ($cardIds ?? [] as $cardId) {
                $card = Card::query()->find($cardId);

                $card->classroom_id = $classroom->id;

                $card->save();
            }

            event(new ClassroomWasUpdated($classroom));
        });

        $card_href_classroom = Cache::get('search_classroom' . Auth::id()) ?? "";
        return redirect()->to('/classroom/list?=&' . $card_href_classroom)->with('success', 'Sửa lớp học thanh cong');
    }

    public function delete(int $id): RedirectResponse
    {
        $classroom = Classroom::query()->where('id', $id)->firstOrFail();

        $classroom->delete();

        return redirect()->to('/classroom/list')->with('success', 'Xoá thành công');
    }

    private function handleCreateOrEdit($id = null): CrudBag
    {
        $crudBag = new CrudBag();
        if ($id) {
            /**
             * @var Classroom $classroom
             */
            $classroom = Classroom::query()->where('id', $id)->firstOrFail();
            $crudBag->setId($id);
        }

        $crudBag->addFields([
            'name' => 'avatar',
            'type' => 'avatar-select',
            'required' => false,
            'label' => 'Chọn ảnh lớp học',
            'options' => [
                'https://www.iconbunny.com/icons/media/catalog/product/4/0/4082.9-class-icon-iconbunny.jpg',
                'https://cdn-icons-png.flaticon.com/512/3352/3352938.png',
                'https://www.iconbunny.com/icons/media/catalog/product/4/0/4082.9-class-icon-iconbunny.jpg',
                'https://cdn-icons-png.flaticon.com/512/3352/3352938.png',
                'https://www.iconbunny.com/icons/media/catalog/product/4/0/4082.9-class-icon-iconbunny.jpg',
                'https://cdn-icons-png.flaticon.com/512/3352/3352938.png',
            ],
            'class' => 'col-10 mb-3',
            'value' => $classroom->avatar ?? 'https://www.iconbunny.com/icons/media/catalog/product/4/0/4082.9-class-icon-iconbunny.jpg',
        ]);

        $crudBag->addFields([
            'name' => 'name',
            'type' => 'text',
            'required' => true,
            'label' => 'Tên lớp học',
            'value' => $classroom->name ?? ''
        ]);

        if (Auth::user()->{'role'} == User::HOST_ROLE) {
            $crudBag->addFields([
                'name' => 'staff_id',
                'type' => 'select',
                'label' => 'Nhân viên phụ trách',
                'nullable' => true,
                'options' => Staff::query()->get()->mapwithkeys(function ($staff) {
                    return [$staff->id => $staff->uuid . " - " . $staff->name];
                })->all(),
                'value' => $classroom->staff_id ?? null,
            ]);
        }

        $crudBag->addFields([
            'name' => 'status',
            'type' => 'select',
            'label' => 'Trạng thái lớp',
            'options' => Classroom::classroomStatusLabel(),
            'value' => $classroom->status ?? 0,
        ]);

        if ($id) {
            $cardList = Card::query()->where(function (Builder $builder) use ($classroom) {
                $builder->where('classroom_id', $classroom->id)->orWhere('classroom_id', null);
            })->where('student_id', '!=', null)->where('card_status', Card::STATUS_ACTIVE)
                ->get()->mapwithkeys(function (Card $card) {
                    $englishName = $card->student?->english_name ?? '';
                    $englishNamePrefix = $englishName ? ' - ' : '';

                    return [$card->id => $card->uuid . " - " . $card->student?->name . $englishNamePrefix . $englishName ?? 'Chưa gắn học sinh'];
                })->all();
        } else {
            $cardList = Card::query()
                ->where('student_id', '!=', null)
                ->where('classroom_id', null)
                ->where('card_status', Card::STATUS_ACTIVE)
                ->get()
                ->mapWithKeys(function ($card) {
                    $englishName = $card->student?->english_name ?? '';
                    $englishNamePrefix = $englishName ? ' - ' : '';

                    return [$card->id => $card?->uuid . " - " . $card->student?->name . $englishNamePrefix . $englishName];
                })->all();
        }

        $crudBag->addFields([
            'name' => 'card_ids',
            'type' => 'select-multiple',
            'label' => 'Gắn thẻ học',
            'options' => $cardList,
            'class' => 'col-10 mb-3',
            'value' => isset($classroom) ? json_encode($classroom?->Cards()?->get()?->pluck('id')->toArray()) : []
        ]);

        $crudBag->addFields([
            'name' => 'card_schedules',
            'type' => 'custom_fields',
            'label' => 'Lịch học',
            'attributes' => [
                'view' => 'classroom_schedule',
                'value' => [
                    'teacher_list' => Teacher::query()->get([
                        'id',
                        'name',
                        'uuid'
                    ])->mapwithkeys(function (Teacher $teacher) {
                        return [$teacher->id => $teacher->uuid . " - " . $teacher->name];
                    })->all(),
                    'supporter_list' => Supporter::query()->get([
                        'id',
                        'name',
                        'uuid'
                    ])->mapwithkeys(function (Supporter $supporter) {
                        return [$supporter->id => $supporter->uuid . " - " . $supporter->name];
                    })->all(),
                    'schedules' => isset($classroom) ? $classroom->SchedulesFormat() : [ClassroomSchedule::TEMPLATE]
                ],
            ],
            'class' => 'col-10'
        ]);

        $crudBag->setAction($id ? 'classroom.update' : 'classroom.store');
        $crudBag->setEntity('classroom');
        $crudBag->setLabel('Lớp học');

        return $crudBag;
    }

    private function classroomFactory(array $dataValidate): array
    {
        $errors = [];
        $schedules = $dataValidate['schedules'];

        foreach ($schedules as $scheduleKey => $schedule) {
            $startTimeBound = Carbon::parse($schedule['start_time']);
            $endTimeBound = Carbon::parse($schedule['end_time']);

            if ($startTimeBound > $endTimeBound) {
                $errors["schedules.$scheduleKey.start_time"] = 'Thời gian bắt đầu không được lớn hơn thời gian kết thúc';
                break;
            }

            $shifts = $schedule['shifts'];

            if (count($shifts) == 1) {
                if ($shifts[1]['start_time'] == null) {
                    $schedules[$scheduleKey]['shifts'][1]['start_time'] = $schedule['start_time'];
                } else {
                    if ($schedule['start_time'] != $shifts[1]['start_time']) {
                        $errors["schedules.$scheduleKey.shifts.1.start_time"] = 'Thời gian ca học phải nằm trong thời gian của buổi học';
                        break;
                    }
                }
                if ($shifts[1]['end_time'] == null) {
                    $schedules[$scheduleKey]['shifts'][1]['end_time'] = $schedule['end_time'];
                } else {
                    if ($schedule['end_time'] != $shifts[1]['end_time']) {
                        $errors["schedules.$scheduleKey.shifts.1.end_time"] = 'Thời gian ca học phải nằm trong thời gian của buổi học';
                    }
                }
            } else {
                $scheduleTimeBound = Carbon::parse($schedule['start_time'])->diffInMinutes(Carbon::parse($schedule['end_time']));
                $shiftTimeBound = 0;
                foreach ($shifts as $shiftKey => $shift) {
                    if ($shift['start_time'] == null) {
                        $errors["schedules.$scheduleKey.shifts.$shiftKey.start_time"] = 'Trường thời gian bắt đầu ca không được để trống';
                        break;
                    }

                    if ($shift['end_time'] == null) {
                        $errors["schedules.$scheduleKey.shifts.$shiftKey.end_time"] = 'Trường thời gian kết thúc ca không được để trống';
                        break;
                    }

                    $shiftStartTime = Carbon::parse($shift['start_time']);
                    $shiftEndTime = Carbon::parse($shift['end_time']);

                    if ($shiftStartTime > $shiftEndTime) {
                        $errors["schedules.$scheduleKey.shifts.$shiftKey.start_time"] =
                            "Thời gian bắt đầu không lớn hơn thời gian kết thúc";
                        break;
                    }

                    if ($shiftStartTime < $startTimeBound) {
                        $errors["schedules.$scheduleKey.shifts.$shiftKey.start_time"] =
                            "Vượt quá phạm vi ({$startTimeBound->isoFormat('HH:mm')} - {$endTimeBound->isoFormat('HH:mm')}) ";
                        break;
                    }

                    if ($shiftEndTime > $endTimeBound) {
                        $errors["schedules.$scheduleKey.shifts.$shiftKey.end_time"] = "Vượt quá phạm vi ({$startTimeBound->isoFormat('HH:mm')} - {$endTimeBound->isoFormat('HH:mm')}) ";
                        break;
                    }

                    $shiftTimeBound += $shiftStartTime->diffInMinutes($shiftEndTime);

                    if ($shiftTimeBound > $scheduleTimeBound) {
                        $errors["schedules.$scheduleKey.end_time"] = "Thời gian ca học vượt quá thời gian buổi học ({$shiftTimeBound} phút / {$scheduleTimeBound} phút)";
                    }

                    $startTimeBound = $shiftEndTime;
                }

//                if (empty($errors)) {
//                    if ($shiftTimeBound < $scheduleTimeBound) {
//                        $errors["schedules.$scheduleKey.end_time"] = "Tổng thời gian ca học chưa đủ ({$shiftTimeBound} phút / {$scheduleTimeBound} phút)";
//                    }
//                }
            }
        }

        return [
            'errors' => $errors,
            'data' => $schedules
        ];
    }

    public function getUserCalendar(Request $request)
    {
        $month = $request->get('month') ?? Carbon::now()->month;
        $year = $request->get('month') ?? Carbon::now()->year;

        $userId = Auth::id();
        $events = [];
        /**
         * @var Classroom[] $classrooms
         */
        $classrooms = Classroom::query()->get();

        foreach ($classrooms as $classroom) {
            /**
             * @var ClassroomSchedule[] $schedules
             */
            $schedules = $classroom->Schedules()->get();

            foreach ($schedules as $schedule) {
                $carbonDayOfWeek = $schedule['week_day'] == 8 ? 0 : ($schedule['week_day'] - 1);

                $days = getDaysOfWeekInMonth($year, $month, $carbonDayOfWeek);

                if (empty($days)) {
                    continue;
                }
                /**
                 * @var Date[] $days
                 */
                foreach ($days as $day) {
                    $events[] = [
                        'id' => $schedule['id'],
                        'classroom_id' => $schedule['id'],
                        'week_day' => $schedule['week_day'],
                        'title' => $classroom['name'],
                        'start' => $day->toDateString() . ' ' . $schedule['start_time'],
                        'end' => $day->toDateString() . ' ' . $schedule['end_time'],
                        'url' => url('/studylog/quick/') . "?classroom_id=" . $classroom['id'] . "&studylog_day=" . $day->toDateString() . "&classroom_schedule_id=" . $schedule['id']
                    ];
                }
            }
        }

        return response()->json($events);
    }

    public function calendarView()
    {
        return view('calendar', [
            'calendarViewModel' => new CalendarViewModel()
        ]);
    }

    public function getCoverView(int $classroomId)
    {
        /**
         * @var Classroom $classroom
         */
        $classroom = Classroom::query()->where('id', $classroomId)->firstOrFail();

        return view('classroom.cover', [
            'classroom' => $classroom,
            'covers' => DB::table('cover_classroom')->where('classroom_id', $classroomId)->get()->map(function ($record) {
                return [
                    'user_id' => $record->{'user_id'},
                    'due' => $record->{'due'}
                ];
            })->toArray(),
            'listUsers' => User::query()->where('branch', Auth::user()->{'branch'})
                ->where('role', '!=', User::STUDENT_ROLE)->get()->mapWithKeys(function (User $user) {
                    return [$user['id'] => $user['uuid'] . '-' . $user['name']];
                })->toArray()
        ]);
    }

    public function updateCoverClassroom(Request $request, int $classroomId)
    {
        $users = $request->get('user_id');
        $due = $request->get('due');

        DB::table('cover_classroom')->where('classroom_id', $classroomId)->delete();

        foreach ($users as $key => $user) {
            if (!$user || !$due[$key]) {
                continue;
            }

            DB::table('cover_classroom')->insert([
                'user_id' => $user,
                'classroom_id' => $classroomId,
                'due' => $due[$key]
            ]);
        }

        return redirect()->back()->with('success', 'Cập nhật thành công');
    }

    private function handleBuilder(Builder &$builder, $crudBag): void
    {

        if ($crudBag->getSearchValue() != '') {
            $searchValue = '%' . $crudBag->getSearchValue() . '%';

            $builder->where(function ($subQuery) use ($searchValue) {
                $subQuery->where('name', 'like', $searchValue)
                    ->orWhere('uuid', 'like', $searchValue);
            });
        }

        foreach ($crudBag->getFilters() as $filter) {
            if (!$filter->getValue()) {
                continue;
            }
            switch ($filter->getName()) {
                case 'teacher:handle':
                    $builder->whereHas('Shifts', function (Builder $shift) use ($filter) {
                        $shift->where('teacher_id', $filter->getValue());
                    });
                    break;
                case 'supporter:handle':
                    $builder->whereHas('Shifts', function (Builder $shift) use ($filter) {
                        $shift->where('supporter_id', $filter->getValue());
                    });
                    break;
                case 'student:handle':
                    $builder->whereHas('Cards', function (Builder $card) use ($filter) {
                        $card->where('student_id', $filter->getValue());
                    });
                    break;
                case 'staff:handle':
                    $builder->where('staff_id', $filter->getValue());
                    break;
                case "status:eq":
                    $builder->where('status', $filter->getValue());
                    break;
                case "week:handle":
                    $builder->whereHas('Schedules', function (Builder $builder) use ($filter) {
                        $builder->where('week_day', $filter->getValue());
                    });
                    break;
                default:
                    break;
            }
        }

        $builder->orderBy('created_at', 'desc');
    }

    /**
     * @throws ValidationException
     */
    public function filterClassroomAction(Request $request): JsonResponse
    {
        $input = $request->input();

        Validator::validate($input, [
            'search' => 'string',
            'value' => 'string'
        ]);

        $searches = explode(',', $input['search']);

        $builder = Classroom::query()->select(['name', 'uuid', 'id']);

        foreach ($searches as $index => $search) {
            if ($index == 0) {
                $builder->where($search, 'like', "%{$input['value']}%");
            } else {
                $builder->orWhere($search, 'like', "%{$input['value']}%");
            }
        }

        $classrooms = $builder->orderBy('created_at', 'DESC')->get()->makeHidden(Classroom::getAppends());

        return response()->json([
            'collection' => $classrooms,
            'title' => 'name',
            'description' => 'uuid'
        ]);
    }
}
