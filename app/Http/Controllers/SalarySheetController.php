<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Models\Classroom;
use App\Models\SalaryCriteria;
use App\Models\SalarySheet;
use App\Models\SalarySheetDetail;
use App\Models\SalarySheetUser;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class SalarySheetController extends Controller
{
    public function show(int $id): Redirector|Application|RedirectResponse
    {
        return redirect("salary-snap/list?salary_sheet_id=$id");
    }

    public function list(Request $request): View
    {
        $paginate = $request->get('perPage') ?? 10;

        $crudBag = new CrudBag();

        $crudBag->setLabel('Cài đặt mức lương');

        $crudBag->setEntity('salary-sheet');

        $crudBag->setParam('disable_all_top_btn', true);

        $this->handleColumns($crudBag);

        $this->handleFilter($crudBag, $request);

        $query = SalarySheet::query();

        $this->handleQuery($query, $crudBag);

        $salarySheets = $query->paginate($paginate);

        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => new ListViewModel($salarySheets)
        ]);
    }

    public function create(): View
    {
        abort(404);
        $crudBag = new CrudBag();
        $crudBag->setLabel('Cấu hình phiếu lương');
        $crudBag->setAction('salary-sheet.store');
        $crudBag->setEntity('salary-sheet');

        $this->handleFields($crudBag);

        return \view('create', [
            'crudBag' => $crudBag
        ]);
    }

    public function edit(int $id)
    {
        $crudBag = new CrudBag();
        $crudBag->setLabel('Cấu hình phiếu lương');
        $crudBag->setAction('salary-sheet.update');
        $crudBag->setId($id);
        $crudBag->setEntity('salary-sheet');

        /**
         * @var SalarySheet $salarySheet
         */
        $salarySheet = SalarySheet::query()->where('id', $id)->firstOrFail();

        $this->handleFields($crudBag, $salarySheet);

        return \view('create', [
            'crudBag' => $crudBag
        ]);
    }

    public function store(Request $request): RedirectResponse
    {
        $validation = Validator::make($request->input(), [
            'sheet_name' => 'string|required',
            'loop_type' => 'integer|nullable',
            'period_year' => 'integer|required',
            'period_months' => 'string|nullable',
            'description' => 'string|nullable',
            'salary_rate' => 'string|required',
            'sheet_user' => 'array|required',
            'fund_percent' => 'numeric|required',
            'sheet_detail' => 'array|nullable',
            'sheet_detail.*.criteria_rate' => 'numeric|required',
            'sheet_detail.*.id' => 'integer|required',
            'classroom_id' => 'integer|required'
        ]);

        if ($validation->fails()) {
            $this->handleConfigCriteria($request);
            request()->flash();
            return redirect()->back()->withErrors($validation->errors());
        }

        $dataCreateSalarySheet = [
            'sheet_name' => $request->get('sheet_name'),
            'loop_type' => $request->get('period_months') ? SalarySheet::LOOP_TYPE_MONTH_SETTING : SalarySheet::LOOP_TYPE_MONTH,
            'period_year' => $request->get('period_year'),
            'attributes' => json_encode([
                'period_months' => explode(',', $request->get('period_months')) ?? ""
            ]),
            'description' => $request->get('description') ?? "",
            'salary_rate' => n($request->get('salary_rate')) ?? "",
            'fund_percent' => n($request->get('fund_percent')) ?? "",
            'branch' => Auth::user()->{'branch'},
            'classroom_id' => $request['classroom_id'],
        ];

        $sheetUsers = $this->calculateRelationUser($request->get('sheet_user'));

        $sheetDetails = $request->get('sheet_detail') ?? [];


        DB::transaction(function () use ($sheetUsers, $dataCreateSalarySheet, $sheetDetails) {
            $salarySheet = SalarySheet::query()->create($dataCreateSalarySheet);

            $dataCreateSheetDetails = [];

            foreach ($sheetDetails as $key => $sheetDetail) {
                $dataCreateSheetDetails[] = [
                    'id' => $key,
                    'salary_sheet_id' => $salarySheet['id'],
                    'criteria_name' => $sheetDetail['criteria_name'],
                    'criteria_code' => $sheetDetail['criteria_code'],
                    'criteria_type' => $sheetDetail['criteria_type'],
                    'criteria_rate' => $sheetDetail['criteria_rate'],
                    'unit_type' => $sheetDetail['unit_type'],
                    'criteria_description' => $sheetDetail['criteria_description'],
                    'branch' => Auth::user()->{'branch'}
                ];
            }

            foreach ($dataCreateSheetDetails as $createSheetDetail) {
                SalarySheetDetail::query()->insert($createSheetDetail);
            }

            foreach ($sheetUsers as $sheetUser) {
                SalarySheetUser::query()->create([
                    'salary_sheet_id' => $salarySheet['id'],
                    'user_id' => $sheetUser,
                    'branch' => Auth::user()->{'branch'}
                ]);
            }

            try {
                Artisan::call('salary:calculate ' . $salarySheet['id']);
            } catch (\Exception $exception) {
                dd($exception);
            }
        });

        return redirect()->to('/salary-sheet/list')->with('success', 'Thêm mới thành công');
    }

    public function update(Request $request, int $id)
    {
        $validation = Validator::make($request->input(), [
            'sheet_name' => 'string|required',
            'description' => 'string|nullable',
            'salary_rate' => 'string|required',
            'fund_percent' => 'numeric|required',
            'sheet_detail' => 'array|nullable',
            'sheet_detail.*.criteria_rate' => 'numeric|required',
            'sheet_detail.*.id' => 'integer|required',
        ]);

        if ($validation->fails()) {
            $this->handleConfigCriteria($request);
            request()->flash();
            return redirect()->back()->withErrors($validation->errors());
        }
        /**
         * @var  SalarySheet $salarySheet
         */
        $salarySheet = SalarySheet::query()->where('id', $id)->firstOrFail();

        $dataUpdateSalarySheet = [
            'sheet_name' => $request->get('sheet_name') ?? null,
            'description' => $request->get('description') ?? null,
            'salary_rate' => n($request->get('salary_rate')) ?? null,
            'fund_percent' => n($request->get('fund_percent')) ?? null,
            'classroom_id' => $request['classroom_id'] ?? null,
            'period_year' => $request->get('period_year'),
            'attributes' => json_encode([
                'period_months' => explode(',', $request->get('period_months')) ?? ""
            ]),
        ];

        $dataUpdateSalarySheet = array_filter($dataUpdateSalarySheet, function ($value) {
            return $value !== null;
        });

        $sheetDetails = $request->get('sheet_detail') ?? [];

        DB::transaction(function () use ($salarySheet, $dataUpdateSalarySheet, $sheetDetails) {
            $salarySheet->update($dataUpdateSalarySheet);

            $updateSheetDetails = [];

            foreach ($sheetDetails as $key => $sheetDetail) {
                $updateSheetDetails[] = [
                    'id' => $key,
                    'salary_sheet_id' => $salarySheet['id'],
                    'criteria_name' => $sheetDetail['criteria_name'],
                    'criteria_code' => $sheetDetail['criteria_code'],
                    'criteria_type' => $sheetDetail['criteria_type'],
                    'criteria_rate' => $sheetDetail['criteria_rate'],
                    'unit_type' => $sheetDetail['unit_type'],
                    'criteria_description' => $sheetDetail['criteria_description'],
                    'branch' => Auth::user()->{'branch'},
                    'classroom_id' => $salarySheet['classroom_id']
                ];
            }

            $newDetailsIds = collect($updateSheetDetails)->pluck('id')->all();

            foreach ($salarySheet->getSalarySheetDetails()->get() as $record_sheetDetail) {
                if (!in_array($record_sheetDetail['id'], $newDetailsIds)) {
                    $record_sheetDetail->delete();
                }
            }

            foreach ($updateSheetDetails as $updateSheetDetail) {
                if (!isset($updateSheetDetail['branch'])) {
                    continue;
                }
                SalarySheetDetail::query()->updateOrCreate(
                    [
                        'id' => $updateSheetDetail['id']
                    ],
                    $updateSheetDetail
                );
            }
        });

        Artisan::call('salary:calculate ' . $salarySheet['id']);

        return redirect()->to('/salary-sheet/list')->with('success', 'Cập nhật thành công');
    }

    private function handleColumns(CrudBag &$crudBag)
    {
        $crudBag->addColumn([
            'name' => 'sheet_name',
            'label' => 'Tên phiếu',
            'type' => 'text',
            'attributes' => [
                'edit' => true,
                'entity' => 'salary-sheet',
                'show' => true
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'period_year',
            'label' => 'Năm thiết lập',
        ]);

        $crudBag->addColumn([
            'name' => 'user_id',
            'type' => 'entity',
            'label' => 'Nhân sự',
            'attributes' => [
                'avatar' => 'avatar',
                'name' => 'name',
                'uuid' => 'uuid',
                'id' => 'id',
                'entity' => 'user',
                'model' => 'User',
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'classroom_id',
            'type' => 'entity',
            'label' => 'Lớp',
            'attributes' => [
                'avatar' => 'avatar',
                'name' => 'name',
                'uuid' => 'uuid',
                'id' => 'id',
                'entity' => 'classroom',
                'model' => 'Classroom',
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'salary_rate',
            'label' => 'Lương theo giờ',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'name' => 'fund_percent',
            'label' => '% Quỹ gắn bó'
        ]);
    }

    private function handleQuery(Builder &$query, CrudBag $crudBag)
    {
        $filters = $crudBag->getFilters();

        foreach ($filters as $filter) {
            if (!$filter->getValue()) {
                continue;
            }
            switch ($filter->getName()) {
                case 'user_id':
                    $query->where('user_id', $filter->getValue());
                    break;
                case 'classroom_id':
                    $query->where('classroom_id', $filter->getValue());
                    break;
                default:
                    break;
            }
        }

        $query->orderBy('created_at', 'DESC');
    }

    private function handleFields(CrudBag &$crudBag, SalarySheet $salarySheet = null): void
    {
        $crudBag->addFields([
            'name' => 'sheet_name',
            'label' => 'Tên phiếu lương',
            'class' => 'col-md-5',
            'value' => $salarySheet ? $salarySheet['sheet_name'] : 'Phiếu lương #' . Carbon::now()->timestamp
        ]);

        $crudBag->addFields([
            'label' => 'Lương theo giờ',
            'name' => 'salary_rate',
            'type' => 'number',
            'suffix' => 'đ',
            'class' => 'col-md-5',
            'value' => $salarySheet ? $salarySheet['salary_rate'] : null,
        ]);

        $crudBag->addFields([
            'label' => 'Qũy gắn bó (%)',
            'name' => 'fund_percent',
            'suffix' => '%',
            'class' => 'col-md-5',
            'value' => $salarySheet ? $salarySheet['fund_percent'] : null,
        ]);

        $crudBag->addFields([
            'name' => 'description',
            'label' => 'Mô tả',
            'type' => 'textarea',
            'class' => 'col-md-10',
            'value' => $salarySheet ? $salarySheet['description'] : null
        ]);


        $crudBag->addFields([
            'label' => 'Cấu hình chỉ tiêu',
            'name' => 'sheet_details',
            'type' => 'custom_fields',
            'attributes' => [
                'view' => 'salary_sheet_detail',
                'value' => [
                    'sheetDetails' => SalarySheet::getOrDefaultSheetDetail($salarySheet),
                    'criterias' => SalaryCriteria::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function ($criteria) {
                        return [$criteria['id'] => [
                            'id' => $criteria['id'],
                            'criteria_name' => $criteria['criteria_name'],
                            'criteria_code' => $criteria['criteria_code'],
                            'criteria_type' => $criteria['criteria_type'],
                            'is_bsm_criteria' => $criteria['is_bsm_criteria'],
                            'unit_type' => $criteria['unit_type'],
                            'criteria_rate' => $criteria['criteria_rate'],
                            'criteria_description' => $criteria['criteria_description']
                        ]];
                    })
                ]
            ],
            'class' => 'col-10'
        ]);
    }

    private function handleConfigCriteria(Request &$request)
    {
        $input = $request->input();

        $sheetDetail = $request->input('sheet_detail') ?? [];

        $sheetDetailIds = array_column($sheetDetail, 'id');

        $input['criterias'] = SalaryCriteria::query()->where('branch', Auth::user()->{'branch'})
            ->whereNotIn('id', $sheetDetailIds)
            ->get()->mapWithKeys(function ($criteria) {
                return [$criteria['id'] => [
                    'id' => $criteria['id'],
                    'criteria_name' => $criteria['criteria_name'],
                    'criteria_code' => $criteria['criteria_code'],
                    'criteria_type' => $criteria['criteria_type'],
                    'is_bsm_criteria' => $criteria['is_bsm_criteria'],
                    'unit_type' => $criteria['unit_type'],
                    'criteria_rate' => $criteria['criteria_rate'],
                    'criteria_description' => $criteria['criteria_description']
                ]];
            });

        $request->merge($input);
    }

    private function calculateRelationUser($sheetUsers): array
    {
        $sheetUsersData = [];

        foreach ($sheetUsers as $sheetUser) {
            switch ($sheetUser) {
                case 'teacher':
                    $teachers = Teacher::query()->get()->pluck('id')->toArray();
                    $sheetUsersData = array_merge($sheetUsersData, $teachers);
                    break;
                case 'supporter':
                    $supporters = Supporter::query()->get()->pluck('id')->toArray();
                    $sheetUsersData = array_merge($sheetUsersData, $supporters);
                    break;
                case 'foreign':
                    $foreign = Teacher::query()->whereHas('profile', function (Builder $profile) {
                        $profile->where('teacher_source', Teacher::EXTERNAL_SOURCE);
                    })->get()->pluck('id')->toArray();
                    $sheetUsersData = array_merge($sheetUsersData, $foreign);
                    break;
                default:
                    $sheetUsersData = array_merge($sheetUsersData, [$sheetUser]);
            }
        }

        return array_unique($sheetUsersData);
    }

    public function delete(int $id)
    {
        SalarySheet::query()->find($id)->delete();
        SalarySheetDetail::query()->where('salary_sheet_id', $id)->delete();
        SalarySheetUser::query()->where('salary_sheet_id', $id)->delete();

        return redirect()->back()->with('success', 'Thành công');
    }

    private function handleFilter(CrudBag &$crudBag, Request $request)
    {
        if ($request->hasAny(['user_id', 'classroom_id'])) {
            $crudBag->setParam('show_filter', true);
        }

        $crudBag->addFilter([
            'name' => 'user_id',
            'label' => 'Nhân sự',
            'type' => 'select2',
            'attributes' => [
                'options' => User::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function ($user) {
                    return [$user->id => "{$user->uuid} ({$user->name})"];
                }),
                'class' => 'col-md-4'
            ],
            'value' => $request->get('user_id'),

        ]);

        $crudBag->addFilter([
            'name' => 'classroom_id',
            'label' => 'Lớp',
            'type' => 'select2',
            'attributes' => [
                'options' => Classroom::query()->where('branch', Auth::user()->{'branch'})->get()->mapWithKeys(function ($classroom) {
                    return [$classroom->id => "{$classroom->uuid} ({$classroom->name})"];
                }),
                'class' => 'col-md-4'
            ],
            'value' => $request->get('classroom_id'),

        ]);
    }
}
