<?php

namespace App\Http\Controllers;

use App\Models\SystemLog;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class SystemLogController extends Controller
{
    public function list()
    {
        if (Auth::user()->{'role'} != User::HOST_ROLE) {
            abort(403);
        }
        /**
         * @var SystemLog[] $systemLogs
         */
        $systemLogs = SystemLog::query()->where('branch', Auth::user()->{'branch'})->orderBy('created_at', 'DESC')->paginate(10);

        return view('system_log.list', [
            'systemLogs' => $systemLogs
        ]);
    }

    public function handleAccessToBsm(Request $request): JsonResponse
    {
        $systemLog = SystemLog::query()->where([
            'object_id' => Auth::id(),
            'user_id' => Auth::id(),
            'object_type' => SystemLog::OBJECT_TYPE_USER,
            'active_type' => SystemLog::ACTION_TYPE_LOGIN,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'branch' => Auth::user()->{'branch'}
        ])->first();

        $systemLog?->update([
            'created_at' => now()
        ]);

        return response()->json([
            'message' => 'Xin chào ! Chúc ngày mới tốt lành ! Bạn sẽ không phiền khi được ghi nhận lịch sử truy cập nhỉ'
        ]);
    }
}
