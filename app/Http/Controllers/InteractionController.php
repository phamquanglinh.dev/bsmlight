<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\CardLog;
use App\Models\Student;
use App\Models\StudyLog;
use App\Models\User;
use App\Models\WorkingShift;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class InteractionController extends Controller
{
    public function getStudentCommentLogData(int $studentId): JsonResponse
    {
        /**
         * @var CardLog[] $cardLogs
         */
        $cardLogs = CardLog::query()->whereHas('Card', function (Builder $card) use ($studentId) {
            $card->where('student_id', $studentId);
        })->orderBy('created_at')->paginate(20);

        foreach ($cardLogs as $cardLog) {
            $workingShifts = WorkingShift::query()->where('studylog_id', $cardLog->{'studylog_id'})->with([
                'Teacher',
                'Supporter',
                'Staff'])
                ->get()->map(function (WorkingShift $workingShift) {
                    return [
                        'teacher_name' => silence(fn() => $workingShift['Teacher']['name']),
                        'teacher_avatar' => silence(fn() => $workingShift['Teacher']['avatar']),
                        'teacher_uuid' => silence(fn() => $workingShift['Teacher']['uuid']),
                        'supporter_name' => silence(fn() => $workingShift['Supporter']['name']),
                        'supporter_avatar' => silence(fn() => $workingShift['Supporter']['avatar']),
                        'supporter_uuid' => silence(fn() => $workingShift['Supporter']['uuid']),
                        'staff_name' => silence(fn() => $workingShift['Staff']['name']),
                        'staff_avatar' => silence(fn() => $workingShift['Staff']['avatar']),
                        'staff_uuid' => silence(fn() => $workingShift['Staff']['uuid']),
                    ];
                })->toArray();

            $cardLog->setAttribute('shifts', $workingShifts);
        }

        $collection = $cardLogs->map(function (CardLog $cardLog) {
            return [
                "title" => $cardLog->StudyLog()->first()?->title,
                'content' => str_replace('\r\n', PHP_EOL, $cardLog->StudyLog()->first()?->content),
                'teacher_note' => str_replace('\r\n', PHP_EOL, $cardLog->teacher_note),
                'time' => Carbon::parse($cardLog->{'created_at'})->isoFormat('DD/MM/YYYY hh:ss:mm'),
                'shifts' => $cardLog->{'shifts'}
            ];
        })->toArray();

        return response()->json($collection);
    }

    public function getUserInfoAction(HttpResponse $httpResponse): JsonResponse
    {
        $user = User::query()->where('id', Auth::id())->first();

        $origin = $_SERVER['HTTP_HOST'];
        
        return $httpResponse->responseData([
            'name' => $user['name'],
            'uuid' => $user['uuid'],
            'origin' => $origin,
            'role' => $user['role'],
            'chat_uuid' => "{$origin}_{$user['uuid']}"
        ]);
    }
}
