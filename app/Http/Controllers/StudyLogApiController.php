<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\Card;
use App\Models\CardLog;
use App\Models\Classroom;
use App\Models\ClassroomSchedule;
use App\Models\ClassroomShift;
use App\Models\LessonPlant;
use App\Models\StudyLog;
use App\Models\User;
use App\Models\WorkingShift;
use Carbon\Carbon;
use FontLib\Table\Type\glyf;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class StudyLogApiController extends Controller
{
    public function createStudyLogView(): View
    {
        return view('studylog.react.reactStudyLog');
    }

    public function getAllClassroomAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $search = $request->get('search');

        $builder = Classroom::query();

        if ($search) {
            $builder->where('name', 'like', '%' . $search . '%')->orWhere('uuid', 'like', '%' . $search . '%');
        }

        $classroom = $builder->get()->map(function (Classroom $classroom) {
            return [
                'id' => $classroom['id'],
                'name' => $classroom['name'],
                'avatar' => $classroom['avatar'],
                'uuid' => $classroom['uuid'],
                'weekdays' => $classroom->Schedules()->get()->map(function (ClassroomSchedule $schedule) {
                    return [
                        'week_day' => $schedule['week_day'],
                    ];
                }),
                'members' => $classroom->getMembersAttribute()
            ];
        });

        return $httpResponse->responseCollection($classroom->toArray());
    }

    public function getAllSchedulesAction(Request $request, HttpResponse $httpResponse, int $classroomId): JsonResponse
    {
        $day = $request->get('day');


        $builder = ClassroomSchedule::query();

        if ($day) {
            $weekRawDay = Carbon::parse($day)->dayOfWeek;

            if ($weekRawDay == Carbon::SUNDAY) {
                $weekDay = 8;
            } else {
                $weekDay = $weekRawDay + 1;
            }

            $builder->where('week_day', $weekDay);
        }

        $schedules = $builder->where('classroom_id', $classroomId)->get()->map(function (ClassroomSchedule $schedule) {
            return [
                'id' => $schedule['id'],
                'week_day_label' => $schedule->getWeekStringAttribute(),
                'week_day' => $schedule['week_day'],
                'start_time' => $schedule['start_time'],
                'end_time' => $schedule['end_time'],
                'time_range' => $schedule['start_time'] . ' - ' . $schedule['end_time'],
                'image' => 'https://cdn-icons-png.freepik.com/512/3652/3652191.png',
            ];
        });

        return $httpResponse->responseCollection($schedules->toArray());
    }

    public function getAllShiftsAction(HttpResponse $httpResponse, int $classroomScheduleId): JsonResponse
    {
        $shifts = ClassroomShift::query()->orderBy('start_time')->where('classroom_schedule_id', $classroomScheduleId)->get()->map(function (ClassroomShift $shift) {
            return [
                'id' => $shift['id'],
                'classroom_id' => $shift['classroom_id'],
                'start_time' => $shift['start_time'],
                'end_time' => $shift['end_time'],
                'room' => $shift['room'],
                'supporter' => [
                    'id' => $shift['supporter_id'],
                    'name' => $shift->Supporter?->name,
                    'avatar' => $shift->Supporter?->avatar,
                ],
                'teacher' => [
                    'id' => $shift['teacher_id'],
                    'name' => $shift->Teacher?->name,
                    'avatar' => $shift->Teacher?->avatar,
                ],
                'supporter_id' => (int)$shift['supporter_id'],
                'teacher_id' => (int)$shift['teacher_id'],
                'duration' => Carbon::parse($shift['end_time'])->diffInMinutes(Carbon::parse($shift['start_time'])),
            ];
        });

        return $httpResponse->responseCollection($shifts->toArray());
    }

    public function getAllLessonPlantAction(HttpResponse $httpResponse, int $classroomId): JsonResponse
    {
        $lessonPlants = LessonPlant::query()->where('status', 1)->get()->map(function (LessonPlant $lessonPlant) {
            return [
                'id' => $lessonPlant['id'],
                'name' => $lessonPlant['name'],
                'title' => $lessonPlant['title'],
                'audio' => $lessonPlant['audio'],
                'video' => $lessonPlant['video'],
                'content' => $lessonPlant['content'],
            ];
        });

        return $httpResponse->responseCollection($lessonPlants->toArray());
    }

    public function getAllCardsAction(int $classroomId, HttpResponse $httpResponse): JsonResponse
    {
        $cards = Card::query()->where('classroom_id', $classroomId)->get()->map(function (Card $card) {
            return [
                'card_id' => $card['id'],
                'uuid' => $card['uuid'],
                'attended' => round($card->getVanAndAttendedDaysAttribute()),
                'can_use_by' => round($card->getCanUseDayAttribute()),
                'can_deg' => $card->getCanDegAttribute(),
                'limit_deg' => $card->limit_deg,
                'status' => n(0),
                'day' => 1,
                'student' => [
                    'id' => $card['student_id'],
                    'name' => $card->Student?->name,
                    'uuid' => $card->Student?->uuid,
                    'avatar' => $card->Student?->avatar
                ],
                'user_ability' => [
                    'can_change_deg' => Auth::user()->{'role'} == User::HOST_ROLE,
                    'can_remove' => Auth::user()->{'role'} == User::HOST_ROLE,
                    'access' => in_array(Auth::user()->{'role'}, [
                        User::HOST_ROLE,
                        User::STAFF_ROLE
                    ])
                ]
            ];
        });

        return $httpResponse->responseCollection($cards->toArray());
    }

    public function extendDegCardAction(Request $request, int $cardId, HttpResponse $httpResponse): JsonResponse
    {
        /**
         * @var Card $card
         */
        $card = Card::query()->where('id', $cardId)->firstOrFail();

        $card->update([
            'allow_deg' => 1,
            'limit_deg' => $request->get('limitDeg') ?? 9999,
        ]);

        return $httpResponse->responseData([
            'card_id' => $card['id'],
            'uuid' => $card['uuid'],
            'status' => 0,
            'attended' => round($card->getVanAndAttendedDaysAttribute()),
            'can_use_by' => round($card->getCanUseDayAttribute()),
            'can_deg' => $card->getCanDegAttribute(),
            'limit_deg' => (int)$card->limit_deg,
            'day' => 1,
            'student' => [
                'id' => $card['student_id'],
                'name' => $card->Student?->name,
                'uuid' => $card->Student?->uuid,
                'avatar' => $card->Student?->avatar
            ],
            'user_ability' => [
                'can_change_deg' => Auth::user()->{'role'} == User::HOST_ROLE,
                'can_remove' => Auth::user()->{'role'} == User::HOST_ROLE,
                'access' => in_array(Auth::user()->{'role'}, [
                    User::HOST_ROLE,
                    User::STAFF_ROLE
                ])
            ]
        ]);
    }

    public function createStudyLogAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'title' => 'string|max:100',
            'content' => 'string',
            'audio' => 'string',
            'video' => 'string',
            'file' => 'string',
            'image' => 'string',
            'classroom_id' => 'integer|required|exists:classrooms,id',
            'schedule_id' => 'integer|required',
            'studylog_day' => 'date|required',
            'shifts' => 'array',
            'shifts.*.start_time' => 'required',
            'shifts.*.end_time' => 'required',
            'shifts.*.teacher_id' => 'required|integer|exists:users,id',
            'shifts.*.supporter_id' => 'integer|nullable',
            'cardLogs' => 'array',
            'cardLogs.*.card_id' => 'required|exists:cards,id',
            'cardLogs.*.day' => 'required|integer|in:0,1',
            'cardLogs.*.status' => 'required|integer|in:0,1,2,3,4,5',
            'cardLogs.*.student.id' => 'required',
        ]);

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->getMessageBag());
        }

        $collectStudyLogData = [
            'created_by' => Auth::id(),
            'classroom_id' => $input['classroom_id'],
            'classroom_schedule_id' => $input['schedule_id'],
            'title' => $input['title'] ?? "Buổi học ngày {$input['studylog_day']}",
            'content' => $input['content'] ?? '',
            'image' => $input['image'] ?? '',
            'video' => $input['video'] ?? '',
            'audio' => $input['audio'] ?? '',
            'file' => $input['file'] ?? '',
            'studylog_day' => $input['studylog_day'],
        ];

        $collectShiftsData = array_map(function ($shift) {
            return [
                'staff_id' => Auth::id(),
                'teacher_id' => $shift['teacher_id'],
                'supporter_id' => $shift['supporter_id'] ?? '',
                'room' => $shift['room'] ?? '',
                'start_time' => $shift['start_time'],
                'end_time' => $shift['end_time'],
                'studylog_id' => 0,
                'teacher_timestamp' => $shift['teacher_timestamp'] ?? '',
                'supporter_timestamp' => $shift['supporter_timestamp'] ?? '',
                'teacher_comment' => $shift['teacher_comment'] ?? '',
                'supporter_comment' => $shift['supporter_comment'] ?? '',
            ];

        }, $input['shifts']);

        $collectCardLogsData = array_map(function ($cardLog) {
            /**
             * @var Card $card
             */
            $card = Card::query()->where('id', $cardLog['card_id'])->first();

            return [
                'card_id' => $cardLog['card_id'],
                'studylog_id' => 0,
                'student_id' => $cardLog['student']['id'],
                'day' => $cardLog['day'],
                'fee' => $card->getDailyFeeAttribute(),
                'status' => $cardLog['status'],
                'teacher_note' => $cardLog['teacher_note'] ?? '',
                'can_deg' => $card->getCanDegAttribute(),
            ];
        }, $input['cardLogs']);

        DB::beginTransaction();

        $identity = DB::transaction(function () use ($collectCardLogsData, $collectStudyLogData, $collectShiftsData) {
            $studyLog = StudyLog::query()->create($collectStudyLogData);

            if ($studyLog) {
                foreach ($collectShiftsData as $workingShift) {
                    $workingShift['studylog_id'] = $studyLog['id'];
                    WorkingShift::query()->create($workingShift);
                }

                foreach ($collectCardLogsData as $cardLog) {
                    $cardLog['studylog_id'] = $studyLog['id'];
                    CardLog::query()->create($cardLog);
                }
            }

            return $studyLog['id'];
        });

        return $httpResponse->responseCreated($identity);
    }

    public function updateStudyLogAction(Request $request, HttpResponse $httpResponse, int $id): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'title' => 'string|max:100',
            'content' => 'string|nullable',
            'audio' => 'string|nullable',
            'video' => 'string|nullable',
            'file' => 'string|nullable',
            'image' => 'string|nullable',
            'classroom_id' => 'integer|filled|exists:classrooms,id',
            'schedule_id' => 'integer|filled',
            'studylog_day' => 'date|filled',
            'shifts' => 'array|filled',
            'shifts.*.start_time' => 'required',
            'shifts.*.end_time' => 'required',
            'shifts.*.teacher_id' => 'required|integer|exists:users,id',
            'shifts.*.supporter_id' => 'nullable|integer',
            'cardLogs' => 'array',
            'cardLogs.*.card_id' => 'required|exists:cards,id',
            'cardLogs.*.day' => 'required|integer|in:0,1',
            'cardLogs.*.status' => 'required|integer|in:0,1,2,3,4,5',
            'cardLogs.*.student.id' => 'required',
        ]);
        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->getMessageBag());
        }

        $collectStudyLogData = [
            'created_by' => Auth::id(),
            'classroom_id' => $input['classroom_id'],
            'classroom_schedule_id' => $input['schedule_id'],
            'title' => $input['title'] ?? "Buổi học ngày {$input['studylog_day']}",
            'content' => $input['content'] ?? '',
            'image' => $input['image'] ?? '',
            'video' => $input['video'] ?? '',
            'audio' => $input['audio'] ?? '',
            'file' => $input['file'] ?? '',
            'studylog_day' => $input['studylog_day'],
        ];

        $collectShiftsData = array_map(function ($shift) {
            return [
                'id' => $shift['id'],
                'staff_id' => Auth::id(),
                'teacher_id' => $shift['teacher_id'],
                'supporter_id' => $shift['supporter_id'] ?? '',
                'room' => $shift['room'] ?? '',
                'start_time' => $shift['start_time'],
                'end_time' => $shift['end_time'],
                'teacher_timestamp' => $shift['teacher_timestamp'] ?? '',
                'supporter_timestamp' => $shift['supporter_timestamp'] ?? '',
                'teacher_comment' => $shift['teacher_comment'] ?? '',
                'supporter_comment' => $shift['supporter_comment'] ?? '',
            ];

        }, $input['shifts']);

        $collectCardLogsData = array_map(function ($cardLog) {
            /**
             * @var Card $card
             */
            $card = Card::query()->where('id', $cardLog['card_id'])->first();

            return [
                'card_id' => $cardLog['card_id'],
                'student_id' => $cardLog['student']['id'],
                'day' => $cardLog['day'],
                'fee' => $card->getDailyFeeAttribute(),
                'status' => $cardLog['status'],
                'teacher_note' => $cardLog['teacher_note'] ?? '',
            ];
        }, $input['cardLogs']);

        #cập nhật studylog
        StudyLog::query()->where('id', $id)->update($collectStudyLogData);


        #Cập nhật cardLogs

        foreach ($collectCardLogsData as $cardLogData) {
            $cardLog = CardLog::query()->where('card_id', $cardLogData['card_id'])->where('studylog_id', $id)->first();

            /**
             * @var Card $card
             */
            $card = Card::query()->where('id', $cardLogData['card_id'])->first();

            $cardLogData['can_deg'] = $card->getCanDegAttribute();

            if (!$cardLog) {
                $cardLog = new CardLog([
                    'studylog_id' => $id,
                ]);
            }

            $cardLog->fill($cardLogData);

            $cardLog->save();
        }

        $workingShiftIds = WorkingShift::query()->where('studylog_id', $id)->get()->pluck('id')->toArray();

        $updateWorkingShiftIds = [];

        foreach ($collectShiftsData as $shiftData) {
            $updateWorkingShiftIds[] = $shiftData['id'];
        }

        $canUpdatedIs = array_intersect($updateWorkingShiftIds, $workingShiftIds);
        $canRemoveIds = array_diff($workingShiftIds, $updateWorkingShiftIds);
        $canAddIds = array_diff($updateWorkingShiftIds, $workingShiftIds);


        #Cập nhật workingShifts
        foreach ($collectShiftsData as $shiftData) {
            if (in_array($shiftData['id'], $canUpdatedIs)) {
                WorkingShift::query()->where('id', $shiftData['id'])->update($shiftData);
            }

            if (in_array($shiftData['id'], $canAddIds)) {
                $shiftData['studylog_id'] = $id;
                WorkingShift::query()->create($shiftData);
            }

            if (in_array($shiftData['id'], $canRemoveIds)) {
                WorkingShift::query()->where('id', $shiftData['id'])->delete($shiftData);
            }
        }

        return $httpResponse->responseMessage('Cập nhật thành công');
    }

    public function getOneStudyLogAction(int $id, HttpResponse $httpResponse): JsonResponse
    {
        $studyLog = StudyLog::query()->where('id', $id)->firstOrFail();

        $cardLogs = CardLog::query()->where('studylog_id', $id)->get();

        $workingShifts = WorkingShift::query()->where('studylog_id', $id)->get();

        $cardLogData = $cardLogs->map(function (CardLog $cardLog) {
            /**
             * @var Card $card
             */
            $card = Card::query()->where('id', $cardLog['card_id'])->first();

            if (! $card) {
                return [];
            }

            return [
                'card_id' => (int)$cardLog['card_id'],
                'uuid' => $card['uuid'],
                'student_id' => (int)$cardLog['student_id'],
                'day' => (int)$cardLog['day'],
                'status' => (int)$cardLog['status'],
                'teacher_note' => $cardLog['teacher_note'],
                'attended' => round($card->getVanAndAttendedDaysAttribute()),
                'can_use_by' => round($card->getCanUseDayAttribute()),
                'can_deg' => $card->getCanDegAttribute(),
                'limit_deg' => $card->limit_deg,
                'student' => [
                    'id' => $card['student_id'],
                    'name' => $card->Student?->name,
                    'uuid' => $card->Student?->uuid,
                    'avatar' => $card->Student?->avatar
                ],
                'user_ability' => [
                    'can_change_deg' => Auth::user()->{'role'} == User::HOST_ROLE,
                    'can_remove' => Auth::user()->{'role'} == User::HOST_ROLE,
                    'access' => in_array(Auth::user()->{'role'}, [
                        User::HOST_ROLE,
                        User::STAFF_ROLE
                    ])
                ]
            ];
        });

        $shiftsData = $workingShifts->map(function (WorkingShift $workingShift) {
            return [
                'id' => (int)$workingShift['id'],
                'teacher_id' => (int)$workingShift['teacher_id'],
                'supporter_id' => (int)$workingShift['supporter_id'],
                'room' => $workingShift['room'],
                'start_time' => $workingShift['start_time'],
                'end_time' => $workingShift['end_time'],
                'teacher_timestamp' => $workingShift['teacher_timestamp'],
                'supporter_timestamp' => $workingShift['supporter_timestamp'],
                'teacher_comment' => $workingShift['teacher_comment'],
                'supporter_comment' => $workingShift['supporter_comment']
            ];
        });

        $studyLogData = [
            'title' => $studyLog['title'],
            'content' => $studyLog['content'],
            'audio' => $studyLog['audio'],
            'video' => $studyLog['video'],
            'file' => $studyLog['file'],
            'image' => $studyLog['image'],
            'schedule_id' => (int)$studyLog['classroom_schedule_id'],
            'classroom_id' => $studyLog['classroom_id'],
            'studylog_day' => $studyLog['studylog_day'],
            'shifts' => $shiftsData,
            'cardLogs' => $cardLogData,
        ];

        return $httpResponse->responseData($studyLogData);
    }

    public function getAllDayValidDays(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $days = [];
        $year = $request->get('year') ?? Carbon::now()->year;
        $month = $request->get('month') ?? Carbon::now()->month;
        $classroomSchedules = ClassroomSchedule::query()->where('classroom_id', $request->get('classroom_id'))->get();

        foreach ($classroomSchedules as $classroomSchedule) {
            $day = $classroomSchedule['week_day'] === 8 ? 0 : $classroomSchedule['week_day'] - 1;

            $days = array_merge($days, $this->getAllWeekDayInMonth($year, $month, $day));
        }

        return $httpResponse->responseData($days);
    }

    public function getAllWeekDayInMonth($year, $month, $weekDay): array
    {
        $firstDay = Carbon::createFromDate($year, $month, 1);

        $lastDay = $firstDay->copy()->endOfMonth();

        $days = [];
        $currentDay = $firstDay->copy()->next($weekDay);

        while ($currentDay->lte($lastDay)) {
            $days[] = $currentDay->day;
            $currentDay->addWeek();
        }

        return $days;
    }

    public function getUnAttendedCardAction(HttpResponse $httpResponse, $studyLogId): JsonResponse
    {
        /**
         * @var StudyLog $studyLog
         * @var Card $card
         */
        $studyLog = StudyLog::query()->where('id', $studyLogId)->first();

        $attendedCardLogId = $studyLog->CardLogs()->get()->pluck('card_id')->toArray();

        $cards = Card::query()->where('classroom_id', $studyLog->classroom_id)->get();

        $unAttendedCards = [];

        foreach ($cards as $card) {
            if (in_array($card['id'], $attendedCardLogId)) {
                continue;
            }

            $unAttendedCards[] = [
                'card_id' => $card['id'],
                'uuid' => $card['uuid'],
                'attended' => round($card->getVanAndAttendedDaysAttribute()),
                'can_use_by' => round($card->getCanUseDayAttribute()),
                'can_deg' => $card->getCanDegAttribute(),
                'limit_deg' => $card->limit_deg,
                'status' => n(0),
                'day' => 1,
                'student' => [
                    'id' => $card['student_id'],
                    'name' => $card->Student?->name,
                    'uuid' => $card->Student?->uuid,
                    'avatar' => $card->Student?->avatar
                ],
                'user_ability' => [
                    'can_change_deg' => Auth::user()->{'role'} == User::HOST_ROLE,
                    'can_remove' => Auth::user()->{'role'} == User::HOST_ROLE,
                    'access' => in_array(Auth::user()->{'role'}, [
                        User::HOST_ROLE,
                        User::STAFF_ROLE
                    ])
                ]
            ];
        }

        return $httpResponse->responseCollection($unAttendedCards);
    }
}
