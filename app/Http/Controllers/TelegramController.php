<?php

namespace App\Http\Controllers;

use App\Models\CreateRegister;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class TelegramController extends Controller
{
    /**
     * @throws GuzzleException
     */
    public function handleWebhook(Request $request): void
    {
        Log::info($request->input());

        $update = $request->input();

        $command = silence(fn() => $update['message']['text']);

        $chatId = silence(fn() => $update['message']['chat']['id']);

        Log::info(json_encode([
            'command' => $command,
            'chat_id' => $chatId
        ]));

        switch ($command) {
            case 'HELLO';
                $this->sendMessage('Chào !', $chatId);
                break;
            case "PULL_ICDT":
                $output = shell_exec('git pull origin master');
                $this->sendMessage($output, $chatId);
                break;
            case "migrate":
                Artisan::call('migrate');
                $this->sendMessage('Đã chạy migrate', $chatId);
                break;
            default:
                break;
        }

        if (str_contains($command, 'Mã khởi tạo cho')) {
            $host = trim(str_replace("Mã khởi tạo cho", "", $command));
            $code = Str::random(6);

            CreateRegister::query()->updateOrCreate([
                'username' => $host
            ], [
                'username' => $host,
                'register' => $code
            ]);

            $this->sendMessage("Mã khởi tạo cho {$host}: {$code}, tuyệt đối không sử dụng để đăng ký host khác !", $chatId, 2);
        }

    }

    /**
     * @param $message
     * @param $chatId
     * @return void
     * @throws GuzzleException
     */
    public function sendMessage($message, $chatId, $chanel = 1)
    {
        $token = '7008787612:AAFqu3MvC9YKZT1sxJKNJlt2fstrheyRzhs';
        $token2 = '7155009952:AAH-shv4aEaarl7KIixeDdDody7if8Tde6E';

        $client = new Client([
            'base_uri' => 'https://api.telegram.org/bot' . $token . '/',
        ]);

        $client2 = new Client([
            'base_uri' => 'https://api.telegram.org/bot' . $token2 . '/',
        ]);

        if ($chanel == 1) {
            $a = silence(fn() => $client->post('sendMessage', [
                'json' => [
                    'chat_id' => $chatId,
                    'text' => $message,
                ],
            ]));
        }

        if ($chanel == 2) {
            $b = silence(fn() => $client2->post('sendMessage', [
                'json' => [
                    'chat_id' => $chatId,
                    'text' => $message,
                ],
            ]));
        }
    }

    public function sendImage($photoUrl, $chatId, $caption = '')
    {
        $token = '7008787612:AAFqu3MvC9YKZT1sxJKNJlt2fstrheyRzhs';

        $url = 'https://api.telegram.org/bot' . $token . '/sendPhoto';

        $photoContent = file_get_contents($photoUrl);

        $response = Http::attach(
            'photo',
            $photoContent,
            'photo.jpg'
        )->post($url, [
            'chat_id' => $chatId,
            'caption' => $caption
        ]);

        return $response->json();
    }

    /**
     * @throws GuzzleException
     */
    private function handleSearch(string $command, string $chatId): void
    {
        $apiKey = 'AIzaSyAfKNUuDB61qCLLlN-y68uTPKA9tNtbTkw';
        $searchEngineId = '1107899cac04f4978';

        $client = new Client();

        $response = $client->get("https://www.googleapis.com/customsearch/v1?key=$apiKey&cx=$searchEngineId&q=$command");

        $results = json_decode($response->getBody()->getContents());

        $this->sendMessage($response->getBody()->getContents(), $chatId);
    }
}
