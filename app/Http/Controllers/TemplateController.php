<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\HttpResponse;
use App\Helper\ListViewModel;
use App\Models\Branch;
use App\Models\Card;
use App\Models\Receipt;
use App\Models\ReceiptTemplate;
use App\Models\Template;
use App\Models\UserConfig;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class TemplateController extends Controller
{
    public function list(Request $request)
    {
        $crudBag = new CrudBag();

        $crudBag->setLabel('Bản in');
        $crudBag->setEntity('templates');

        $this->handleColumn($crudBag);

        $builder = Template::query();

        $this->handleQuery($crudBag, $builder);

        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => new ListViewModel($builder->paginate($request->get('perPage') ?? 10))
        ]);
    }

    public function create(Request $request): View
    {
        $defaultTemplate = UserConfig::query()->where('type', 'payment_template')->first();
        return view("templates.create", [
            'init' => $defaultTemplate['settings']
        ]);
    }

    private function handleColumn(CrudBag &$crudBag): void
    {
        $crudBag->addColumn([
            'name' => 'name',
            'label' => 'Tên bản in',
            'type' => 'text'
        ]);
    }

    private function handleQuery(CrudBag $crudBag, Builder &$builder): void
    {
        $builder->orderBy("created_at", 'DESC');
    }

    public function store(Request $request)
    {
        $template = new Template();

        $template->fill($request->input());

        $template->setAttribute('module', 'card_transaction');

        $template->setAttribute('created_by', Auth::id());

        $template->setAttribute('host_id', Auth::id());

        $template->save();

        return redirect('templates/list')->with('success', 'Thêm bản in thành công');
    }

    public function printCardTransaction(Request $request, HttpResponse $httpResponse): ?JsonResponse
    {
        $input = $request->input();
        $validate = Validator::make($input, [
            'amount' => 'string|required',
            'notes' => 'string',
            'transaction_day' => 'string|nullable',
            'card_id' => 'integer|required',
            'template_id' => 'required|integer'
        ]);

        if ($validate->fails()) {
            return $httpResponse->validateResponse($validate->errors());
        }

        $day = $input['transaction_day'] ? Carbon::parse($input['transaction_day']) : Carbon::now();
        /**
         * @var Card $card
         */
        $card = Card::query()->where('id', $input['card_id'])->first();

        if (!$card) {
            return $httpResponse->notFoundResponse();
        }
        /**
         * {{day}}/ {{month}} / {{year}} v
         * {{student_name}} v
         * {{student_uuid}} v
         * {{note}} v
         * {{amount}} v
         * {{w_amount}} v
         */
        $mappedData = [];
        $mappedData['day'] = $day->day;
        $mappedData['month'] = $day->month;
        $mappedData['year'] = $day->year;
        $mappedData['amount'] = $input['amount'] . " đ";
        $mappedData['w_amount'] = $this->numberToVietnameseMoney(n($input['amount']));
        $mappedData['student_name'] = $card->Student()?->first()->name;
        $mappedData['student_uuid'] = $card->Student()?->first()->uuid;
        $mappedData['note'] = $input['notes'];

        $template = ReceiptTemplate::query()->where('id', $request->get('template_id'))->first();

        $templateData = $template['html_content'];

        foreach ($mappedData as $key => $data) {
            $keyName = "{{{$key}}}";

            $templateData = str_replace($keyName, $data, $templateData);
        }

        $branch = Auth::user()->{'branch'};

        $receipt = Receipt::query()->create([
            'name' => "Phiếu thu học phí {$mappedData['student_name']} chi nhánh $branch",
            'html_content' => $templateData,
            'created_by' => Auth::id()
        ]);

        return $httpResponse->responseData([
            'url' => url("document/receipt/print/{$receipt['id']}")
        ]);
    }

    private function numberToVietnameseMoney($number): string
    {
        $vietnameseNumbers = [
            0 => "không",
            1 => "một",
            2 => "hai",
            3 => "ba",
            4 => "bốn",
            5 => "năm",
            6 => "sáu",
            7 => "bảy",
            8 => "tám",
            9 => "chín"
        ];

        $units = ["", "nghìn", "triệu", "tỷ", "nghìn tỷ", "triệu tỷ", "tỷ tỷ"];

        $result = '';

        // Lấy số nguyên dương của số nhập vào
        $num = abs((int)$number);

        // Xử lý nếu số nhập vào là 0
        if ($num == 0) {
            return "không đồng";
        }

        // Lặp qua từng nhóm 3 số
        $i = 0;
        while ($num > 0) {
            $group = $num % 1000; // Lấy 3 chữ số cuối cùng
            $num = (int)($num / 1000); // Chia cho 1000 để lấy nhóm tiếp theo

            if ($group > 0) {
                $groupResult = '';
                $hundreds = (int)($group / 100);
                $tens = (int)(($group % 100) / 10);
                $ones = (int)($group % 10);

                if ($hundreds > 0) {
                    $groupResult .= $vietnameseNumbers[$hundreds] . ' trăm ';
                }

                if ($tens > 1) {
                    $groupResult .= $vietnameseNumbers[$tens] . ' mươi ';
                    if ($ones > 0) {
                        $groupResult .= $vietnameseNumbers[$ones] . ' ';
                    }
                } elseif ($tens == 1) {
                    $groupResult .= 'mười ';
                    if ($ones > 0) {
                        $groupResult .= $vietnameseNumbers[$ones] . ' ';
                    }
                } else {
                    if ($ones > 0) {
                        $groupResult .= $vietnameseNumbers[$ones] . ' ';
                    }
                }

                $result = trim($groupResult . $units[$i] . ' ' . $result);
            }

            $i++;
        }

        // Xử lý số âm
        if ($number < 0) {
            $result = 'Âm ' . $result;
        }

        return trim($result . ' đồng');
    }

    private function handleTemplateData(array|string &$templateData)
    {
        $css = "<style>
table {
   zoom: 85%;
}
</style>";

        $templateData = $css . "\n" . $templateData;
    }
}
