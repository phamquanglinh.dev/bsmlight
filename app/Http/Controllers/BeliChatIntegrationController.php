<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\Card;
use App\Models\Classroom;
use App\Models\ClassroomShift;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class BeliChatIntegrationController extends Controller
{
    const BASE_URL = "https://api.bsmlight.com/beli_chat";

    public function getAllChannelsAction(HttpResponse $httpResponse): JsonResponse
    {
        $uuid = $_SERVER['HTTP_HOST'] . "_" . Auth::user()->{'uuid'};

        $url = self::BASE_URL . "/channels/$uuid";

        $client = new Client();

        try {
            $response = $client->get($url);

            $data = json_decode($response->getBody(), 1);

            return $httpResponse->responseData($data);

        } catch (\Exception $exception) {
            throw new BadRequestException($exception->getMessage());
        }
    }

    public function getAllUsers(HttpResponse $httpResponse): JsonResponse
    {

        if (Auth::user()->{'role'} == User::HOST_ROLE) {
            $users = User::query()->whereHas('branch', function (Builder $branch) {
                $branch->where("host_id", Auth::id());
            })->get();
        } else {
            $users = User::query()->where('branch', Auth::user()->{'branch'})->get();
        }

        $users = $users->map(function (User $user) {
            return [
                'chat_uuid' => $_SERVER['HTTP_HOST'] . "_" . $user['uuid'],
                'uuid' => $user['uuid'],
                'name' => $user['name'],
                'avatar' => $user['avatar']
            ];
        })->toArray();

        return $httpResponse->responseData($users);
    }

    public function getCurrentUsers(HttpResponse $httpResponse): JsonResponse
    {
        $user = User::query()->where('id', Auth::id())->first();

        return $httpResponse->responseData([
            'name' => $user['name'],
            'uuid' => $user['uuid'],
            'avatar' => $user['avatar'],
            'chat_uuid' => $_SERVER['HTTP_HOST'] . "_" . $user['uuid']
        ]);
    }

    public function getAllMessages(string $channel, HttpResponse $httpResponse): JsonResponse
    {
        $url = self::BASE_URL . "/messages/$channel";

        $client = new Client();

        try {
            $response = $client->get($url);

            $data = json_decode($response->getBody(), 1);

            return $httpResponse->responseData($data);

        } catch (\Exception $exception) {
            throw new BadRequestException($exception->getMessage());
        }
    }

    public function createMessage(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $user = User::query()->where('id', Auth::id())->first();

        $url = self::BASE_URL . "/messages";

        $client = new Client();

        $input = $request->input();

        $notification = Validator::make($input, [
            'channel' => 'required|string',
            'message' => 'required|string',
            'type' => 'integer|nullable|in:1,2'
        ]);

        if ($notification->fails()) {
            return $httpResponse->badResponse($notification->errors()->first());
        }

        $form = [
            'channel_name' => $input['channel'],
            'user_uuid' => $user['uuid'],
            'message' => $input['message'],
            'origin' => $_SERVER['HTTP_HOST'],
            'type' => $input['type'] ?? 0
        ];

        try {
            $response = $client->post($url, [
                'json' => $form
            ]);

            $data = json_decode($response->getBody(), 1);

            return $httpResponse->responseData([
                'id' => $data['id'],
                'uuid' => $data['user_uuid'],
                'message' => $data['message'],
                'type' => $data['type'],
                'attachment' => $data['attachment'],
                'created_at' => Carbon::parse($data['created_at'])->isoFormat('DD/MM/YYYY hh:mm:ss')
            ]);

        } catch (\Exception $exception) {
            throw new BadRequestException($exception->getMessage());
        } catch (GuzzleException $exception) {
            throw new BadRequestException($exception->getMessage());
        }
    }

    public function createChatByClassroom(int $classroomId): RedirectResponse
    {
        if (Auth::user()->{'role'} != User::HOST_ROLE) {
            abort(403);
        }

        /**
         * @var Classroom $classroom
         */
        $classroom = Classroom::query()->where('id', $classroomId)->whereNull('chat_name')->first();

        if (!$classroom) {
            return redirect()->back()->with('error', 'Không tìm thấy lớp');
        }

        $users = [];

        /**
         * @var Card $card
         */
        $cards = $classroom->Cards()->get();

        foreach ($cards as $card) {
            $uuid = $card->Student()->first()?->uuid;

            if ($uuid) {
                $users[] = $uuid;
            }
        }

        /**
         * @var ClassroomShift $shift
         */
        $shifts = $classroom->Shifts()->get();

        foreach ($shifts as $shift) {
            $teacherUuid = $shift->Teacher()->first()?->uuid;

            if ($teacherUuid) {
                $users[] = $teacherUuid;
            }

            $supporterUuid = $shift->Supporter()->first()?->uuid;

            if ($teacherUuid) {
                $users[] = $supporterUuid;
            }
        }

        $users[] = Auth::user()->{'uuid'};

        $staffUuid = $classroom->Staff()->first()?->uuid;

        if ($staffUuid) {
            $users[] = $staffUuid;
        }

        $users = array_unique($users);

        $origin = $_SERVER['HTTP_HOST'];

        $formData = [
            'add_user_ids' => $users,
            'origin' => $origin,
            'label' => "[" . $classroom->getAttribute('branch') . "] " . $classroom->getAttribute('name'),
            'avatar' => $classroom->getAttribute('avatar'),
        ];

        $url = self::BASE_URL . "/channels";

        $client = new Client();

        try {
            $response = $client->post($url, [
                'json' => $formData
            ]);

            $responseData = json_decode($response->getBody(), 1);

            $classroom->setAttribute('chat_name', $responseData['name']);

            $classroom->save();
        } catch (ClientException $clientException) {
            return redirect()->back()->with('error', $clientException->getMessage());
        } catch (GuzzleException $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

        return redirect()->to('/')->with('message', 'Init thành công');
    }
}
