<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Models\Salary;
use App\Models\SalaryGroup;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use function Symfony\Component\String\u;

class SalaryGroupController extends Controller
{
    public function list(
        Request $request
    )
    {
        $crudBag = new CrudBag();

        $crudBag->setLabel('Phiếu lương');

        $crudBag->setParam('disable_all_top_btn', true);

        $this->handleColumn($crudBag);

        $this->handleFilter($crudBag, $request);

        $query = SalaryGroup::query();

        $this->handleQuery($query, $crudBag);

        $listViewModel = new ListViewModel($query->paginate($request->get('perPage') ?? 10));

//        $listViewModel->getOriginCollection()->map(function (SalaryGroup $salaryGroup) {
//            return $salaryGroup->handleStatusProcess();
//        });

        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => $listViewModel,
        ]);
    }

    public function show(int $id)
    {
        /**
         * @var SalaryGroup $salaryGroup
         */
        $salaryGroup = SalaryGroup::query()->findOrFail($id);

        $salaryGroup->handleStatusProcess();

        return view('salary-group.show', [
            'salaryGroup' => $salaryGroup,
        ]);
    }

    private function handleColumn(CrudBag &$crudBag)
    {
        $crudBag->addColumn([
            'label' => 'Nhân sự',
            'name' => 'user_entity',
            'type' => 'entity',
            'attributes' => [
                'uuid' => 'uuid',
                'id' => 'id',
                'avatar' => 'avatar',
                'name' => 'name',
                'entity' => 'user_entity',
                'model' => 'User'
            ],
        ]);

        $crudBag->addColumn([
            'label' => 'Tổng số thẻ lương',
            'name' => 'salary_count',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'label' => 'Trạng thái',
            'name' => 'status',
            'type' => 'salary-status',
        ]);

        $crudBag->addColumn([
            'label' => 'Tổng lương',
            'name' => 'total_with_fund',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'label' => 'Tổng thực lĩnh',
            'name' => 'total_amount',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'label' => 'Đã chi',
            'name' => 'total_paid',
            'type' => 'number',
        ]);

        $crudBag->addColumn([
            'label' => 'Còn lại',
            'name' => 'total_unpaid',
            'type' => 'number',
        ]);

        $crudBag->setParam(CrudBag::DISABLE_ACTION, true);

        $crudBag->addColumn([
            'label' => 'Hành động',
            'name' => 'action',
            'type' => 'salary_group_action',
            'attributes' => [
                'id' => 'id',
            ]
        ]);
    }

    private function handleFilter(CrudBag &$crudBag, Request $request)
    {
        $crudBag->addFilter([
            'type' => 'select2',
            'label' => 'Trạng thái thanh toán',
            'name' => 'status',
            'attributes' => [
                'options' => SalaryGroup::statusLabel()
            ],
            'nullable' => true,
            'value' => $request->get('status')
        ]);
    }

    private function handleQuery(Builder &$query, CrudBag &$crudBag)
    {
        $filters = $crudBag->getFilters();

        if (!empty($filters)) {
            $crudBag->setParam(CrudBag::SHOW_FILTER, true);
        }

        foreach ($filters as $filter) {
            if (!$filter->getValue()) {
                continue;
            }
            switch ($filter->getName()) {
                case 'status':
                    $query->where('status', $filter->getValue());
                    break;
                default:
                    break;
            }
        }

        $query->orderBy('created_at', 'desc');
    }

    public function requestPaid(Request $request, int $id)
    {
        /**
         * @var SalaryGroup $salaryGroup
         */
        $salaryGroup = SalaryGroup::query()->where('id', $id)->firstOrFail();

        $crudBag = new CrudBag();

        $crudBag->setLabel('Phiếu lương');

        $crudBag->setAction('salary-group.paid');

        $crudBag->setId($id);

        $this->handleFieldsForPaid($crudBag, $salaryGroup, $request);

        return view('create', [
            'crudBag' => $crudBag,
        ]);
    }

    private function handleFieldsForPaid(CrudBag $crudBag, SalaryGroup $salaryGroup, Request $request)
    {

        $crudBag->setHasFile(true);
        $crudBag->addFields([
            'name' => 'salary_id',
            'label' => 'Chi lương thẻ',
            'type' => 'select',
            'options' => $salaryGroup->Salaries()->get()->mapWithKeys(function (Salary $salary) {
                return [
                    $salary->getAttribute('id') => "{$salary->getAttribute('uuid')} (Còn " . number_format($salary->getUnpaidAttribute()) . " đ)"
                ];
            })->toArray(),
            'value' => $request->get('salary_id'),
        ]);

        $crudBag->addFields([
            'type' => 'number',
            'label' => 'Số tiền chi',
            'name' => 'amount'
        ]);

        $crudBag->addFields([
            'name' => 'note',
            'type' => 'textarea',
            'label' => 'Ghi chú',
            'class' => 'col-md-10'
        ]);
        $crudBag->addFields([
            'type' => 'upload',
            'name' => 'object_image',
            'label' => 'Ảnh đối soát',
            'class' => 'col-md-10'
        ]);
    }

    public function paid(Request $request)
    {
        $ref = $request->get('ref') ?? 'salary-group/list';

        $input = $request->input();

        $file = $request->file('object_image');

        $input['object_image'] = uploads($file);

        $notification = Validator::make($input, [
            'salary_id' => 'required|integer',
            'amount' => 'required|string',
            'note' => 'nullable|string',
            'object_image' => 'required|string',
        ]);

        if ($notification->fails()) {
            return redirect()->back()->withErrors($notification->errors())->withInput();
        }

        $data = [
            'created_by' => Auth::id(),
            'uuid' => now()->timestamp,
            'transaction_type' => 'paid',
            'object_image' => $input['object_image'],
            'amount' => n($input['amount']),
            'notes' => $input['note'] ?? '',
            'object_id' => $input['salary_id'],
            'object_type' => Transaction::SALARY_PAID_OBJECT_TYPE,
            'status' => 1,
            'approve' => Auth::id(),
            'transaction_day' => now()
        ];

        DB::beginTransaction();

        Transaction::query()->create($data);

        DB::commit();

        return redirect($ref);
    }

    public function cancel(Request $request, int $id): RedirectResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'reason' => 'string|max:200|required',
        ]);

        if ($notification->fails()) {
            return redirect()->back()->withErrors($notification->errors());
        }

        Salary::query()->where('id', $id)->update([
            'refund_reason' => $input['reason'],
            'status' => -1
        ]);

        return redirect('salary-group/list')->with('success', 'OK');
    }

    public function requestCancel(int $id): View
    {
        /**
         * @var Salary $salary
         */
        $salary = Salary::query()->where('id', $id)->firstOrFail();

        $crudBag = new CrudBag();

        $crudBag->setAction('salary-group.cancel');
        $crudBag->setParam(CrudBag::CUSTOM_LABEL_ACTION, '');
        $crudBag->setParam(CrudBag::CUSTOM_LABEL_ACTION_BTN, 'Hủy chi lương');
        $crudBag->setId($id);
        $crudBag->setLabel("Hủy chi lương cho : {$salary->getAttribute('uuid')}");
        $crudBag->addFields([
            'name' => 'reason',
            'label' => 'Lý do hủy',
            'type' => 'textarea'
        ]);

        return view('create', [
            'crudBag' => $crudBag,
        ]);
    }

    public function updateJson(int $id, Request $request): JsonResponse
    {
        $input = $request->input();

        $salaryGroup = SalaryGroup::query()->where('id', $id)->firstOrFail();

        $salaryGroup->update([
            'status' => $input['status'],
        ]);

        return response()->json([
            'success' => true,
            'background' => SalaryGroup::bgStatusLabel()[$input['status']],
        ]);
    }
}
