<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\HttpResponse;
use App\Helper\ListViewModel;
use App\Models\Account;
use App\Models\CustomFieldList;
use App\Models\CustomFields;
use App\Models\CustomFieldStruct;
use Database\Factories\CustomFieldStructFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class CustomFieldController extends Controller
{
    public function create()
    {
        $crudBag = new CrudBag();

        $crudBag->setLabel('Trường tự định nghĩa');
        $crudBag->setEntity('custom_field');
        $crudBag->setAction('custom_field.store');


        $crudBag = $this->handleFields($crudBag);

        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    private function handleFields(CrudBag $crudBag, CustomFields $customFields = null): CrudBag
    {
        $crudBag->addFields([
            'name' => 'label',
            'required' => true,
            'label' => 'Tên trường',
            'value' => $customFields?->label,
        ]);

        $crudBag->addFields([
            'name' => 'entity_type',
            'value' => $customFields?->entity_type,
            'required' => true,
            'label' => 'Sử dụng cho',
            'options' => CustomFields::listEntityType(),
            'type' => 'select',
        ]);

        $crudBag->addFields([
            'name' => 'type',
            'required' => true,
            'label' => 'Kiểu trường',
            'type' => 'select',
            'options' => CustomFields::listType(),
            'value' => $customFields?->type
        ]);

        $crudBag->addFields([
            'name' => 'initValue',
            'label' => 'Dữ liệu khởi tạo',
            'type' => 'select',
            'nullable' => 1,
            'options' => CustomFields::listSource(),
            'value' => isset($customFields?->initValue) ? json_decode($customFields?->initValue, true)['type'] : null
        ]);

        $crudBag->addFields([
            'name' => 'required',
            'label' => 'Bắt buộc',
            'type' => 'checkbox',
            'value' => $customFields?->required
        ]);

        return $crudBag;
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'label' => 'string|required',
            'entity_type' => 'string|required',
            'type' => 'integer|required',
            'initValue' => 'string|nullable',
        ]);

        $name = Str::slug($request['label']);

        if (CustomFields::query()->where('name', $name)->where('branch', Auth::user()->{'branch'})->exists()) {
            return redirect()->back()->withErrors([
                'label' => 'Trường tự định nghĩa đã tồn tại, vui lòng đặt tên khác',
            ]);
        }
        $dataToCreate = [
            'label' => $request['label'],
            'name' => $name,
            'entity_type' => $request['entity_type'],
            'type' => $request['type'],
            'initValue' => $request['initValue'] ? json_encode([
                'type' => $request['initValue'],
            ]) : null,
            'required' => $request['required'] ?? '0',
            'branch' => Auth::user()->{'branch'},
        ];

        CustomFields::query()->create($dataToCreate);

        return redirect()->to('custom_field/list')->with('success', 'Thêm mới thành công');
    }

    public function list(Request $request)
    {
        $crudBag = new CrudBag();
        $crudBag->setLabel('Trường tự định nghĩa');
        $crudBag->setEntity('custom_field');

        $crudBag = $this->handleColumns($crudBag);

        $query = CustomFields::where('branch', Auth::user()->{'branch'})->orderBy('id', 'desc');

        $listViewModel = new ListViewModel($query->paginate($request->get('perPage') ?? 10));
        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => $listViewModel
        ]);
    }

    private function handleColumns(CrudBag $crudBag)
    {
        $crudBag->addColumn([
            'name' => 'label',
            'label' => 'Tên trường',
            'attributes' => [
                'edit' => true,
                'entity' => 'custom_field'
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'entity_type',
            'label' => 'Sử dụng cho',
            'type' => 'select',
            'attributes' => [
                'options' => CustomFields::listEntityType(),
                'bg' => CustomFields::backgroundEntityType()
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'type',
            'label' => 'Kiểu trường',
            'type' => 'select',
            'attributes' => [
                'options' => CustomFields::listType(),
                'bg' => CustomFields::backgroundType()
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'required',
            'label' => 'Bắt buộc',
            'type' => 'select',
            'attributes' => [
                'options' => [
                    '0' => 'Không',
                    '1' => 'Bắt buộc'
                ],
                'bg' => [
                    '0' => 'bg-label-github',
                    '1' => 'bg-success'
                ]
            ]
        ]);

        return $crudBag;
    }

    public function edit(int $id)
    {
        /**
         * @var CustomFields $customFields
         */
        $customFields = CustomFields::query()->where('id', $id)->where('branch', Auth::user()->{'branch'})->firstOrFail();
        $crudBag = new CrudBag();
        $crudBag->setLabel('Trường tự định nghĩa');
        $crudBag->setEntity('custom_field');
        $crudBag->setId($id);
        $crudBag->setAction('custom_field.update');
        $crudBag = $this->handleFields($crudBag, $customFields);

        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $this->validate($request, [
            'label' => 'string|required',
            'entity_type' => 'string|required',
            'type' => 'integer|required',
            'initValue' => 'string|nullable',
        ]);

        $customFields = CustomFields::query()->where('id', $id)->where('branch', Auth::user()->{'branch'})->firstOrFail();

        $dataToUpdate = [
            'label' => $request['label'],
            'entity_type' => $request['entity_type'],
            'type' => $request['type'],
            'initValue' => $request['initValue'] ? json_encode([
                'type' => $request['initValue'],
            ]) : null,
            'required' => $request['required'] ?? '0',
        ];

        $customFields->update($dataToUpdate);

        return redirect()->to('custom_field/list')->with('success', 'Cập nhật thành công');
    }

    public function delete(int $id): RedirectResponse
    {
        $customFields = CustomFields::query()->where('id', $id)->where('branch', Auth::user()->{'branch'})->firstOrFail();
        $customFields->delete();

        return redirect()->to('custom_field/list')->with('success', 'Xóa thành công');
    }

    public function getCrmModuleFields(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $fields = [];
        $module = $request->get('module') ?? "";

        switch ($module) {
            case "account":
                $fields = $this->getAccountModuleFields();
                break;
            default:
                break;
        }

        return $httpResponse->responseData($fields);
    }

    private function getAccountModuleFields(): array
    {
        $primaryFields = Account::primaryFields();

        $customFields = CustomFieldStruct::query()
            ->where('module', CustomFieldStruct::MODULE_ACCOUNT)
            ->where(function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'})
                    ->orWhere('default', 1);
            })
            ->get(['field_name', 'field_html_type', 'field_label', 'is_required', 'default', 'is_multiple', 'is_duplicate'])->toArray();
        return array_merge($primaryFields, $customFields);
    }

    public function getCustomFieldView(): View
    {
        return \view('integration.custom_field');
    }

    public function createAccountCustomField(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $form = $input['form'] ?? [];

        $notification = Validator::make($form, [
            'field_name' => 'required|string',
            'field_html_type' => 'string|nullable',
            'field_label' => 'required|string',
            'is_required' => 'integer|nullable',
            'is_duplicate' => 'integer|nullable',
            'is_multiple' => 'integer|nullable',
            'lists' => 'array|nullable',
        ]);

        if (in_array($form['field_name'], array_keys(Account::defineFillableField()))) {
            $notification->errors()->add('field_name', 'Đã tồn tại trong hệ thống');
        }

        $existCustomField = CustomFieldStruct::query()
            ->where(function (Builder $builder) use ($form) {
                $builder->where('field_name', $form['field_name'])
                    ->orWhere('field_label', $form['field_label']);
            })->where(function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'})
                    ->orWhere('branch', null);
            })->exists();

        if ($existCustomField) {
            $notification->errors()->add('field_name', 'Đã tồn tại trong hệ thống');
        }

        if (!empty($notification->errors()->messages())) {
            return $httpResponse->validateResponse($notification->errors());
        }

        unset($form['has_list']);

        $customFieldLists = $form['lists'] ?? [];

        unset($form['lists']);

        $customFieldStruct = CustomFieldStructFactory::makeCustomFieldStruct($form, CustomFieldStruct::MODULE_ACCOUNT);

        $customFieldStruct->save();

        $customFieldStructId = $customFieldStruct->{'id'};

        foreach ($customFieldLists as $customFieldOption) {
            if (empty($customFieldOption['option'])) {
                continue;
            }
            CustomFieldList::query()->create([
                'custom_field_id' => $customFieldStructId,
                'field_option' => $customFieldOption['option']
            ]);
        }

        return $httpResponse->responseMessage('OK');
    }

    public function getOneAccountCustomField(HttpResponse $httpResponse, string $name): JsonResponse
    {
        $customField = CustomFieldStruct::query()->where('field_name', $name)->where(function (Builder $builder) {
            $builder->where('branch', Auth::user()->{'branch'})->orWhere('branch', null);
        })->first(array_keys(CustomFieldStruct::defineRetrievableField()));

        if (!$customField) {
            return $httpResponse->notFoundResponse();
        }

        if (in_array($customField->{'field_html_type'}, [CustomFieldStruct::HTML_SELECT_TYPE, CustomFieldStruct::HTML_CHECKBOX_TYPE])) {
            $customFieldList = CustomFieldList::query()
                ->where('custom_field_id', $customField->{'id'})
                ->get()
                ->map(function (CustomFieldList $customFieldList) {
                    return [
                        'id' => $customFieldList['id'],
                        'option' => $customFieldList['field_option']
                    ];
                })->toArray();

            $customField->setAttribute('lists', $customFieldList);

            $customField->setAttribute('has_list', 1);
        }

        return $httpResponse->responseData($customField->toArray());
    }

    public function updateAccountCustomField(HttpResponse $httpResponse, Request $request)
    {
        $form = $request->input()['form'] ?? [];

        $id = $form['id'] ?? null;

        if (!$id) {
            return $httpResponse->notFoundResponse();
        }

        unset($form['id']);

        /**
         * @var CustomFieldStruct $customField
         */
        $customField = CustomFieldStruct::query()->where('id', $id)->first();

        if (!$customField) {
            return $httpResponse->notFoundResponse();
        }

        unset($form['has_list']);
        unset($form['module']);
        unset($form['default']);
        unset($form['branch']);

        $updateFieldList = $form['lists'] ?? [];

        unset($form['lists']);

        $notification = Validator::make($form, [
            'field_name' => 'string|filled',
            'filed_label' => 'string|filled',
        ]);

        if (in_array($form['field_name'], array_keys(Account::defineFillableField()))) {
            $notification->errors()->add('field_name', 'Đã tồn tại trong hệ thống');
        }

        $existCustomField = CustomFieldStruct::query()
            ->where(function (Builder $builder) use ($form) {
                $builder->where('field_name', $form['field_name'])
                    ->orWhere('field_label', $form['field_label']);
            })->where(function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'})
                    ->orWhere('branch', null);
            })->where('id', "!=", $customField->{'id'})->exists();

        if ($existCustomField) {
            $notification->errors()->add('field_name', 'Đã tồn tại trong hệ thống');
        }

        $customField = CustomFieldStructFactory::modifyCustomFieldStruct($customField, $form);

        $customField->save();

        $currentCustomFieldListOption = CustomFieldList::query()->where('custom_field_id', $id)->pluck('id', 'id')->toArray();

        foreach ($updateFieldList as $fieldList) {
            if (!empty($currentCustomFieldListOption[$fieldList['id'] ?? -1])) {
                CustomFieldList::query()->where('id', $fieldList['id'])->update([
                    'field_option' => $fieldList['option']
                ]);

                unset($currentCustomFieldListOption[$fieldList['id']]);

                continue;
            }

            $newCustomFieldList = new CustomFieldList([
                'custom_field_id' => $id,
                'field_option' => $fieldList['option']
            ]);

            $newCustomFieldList->save();
        }

        $deletedIds = array_keys($currentCustomFieldListOption);

        CustomFieldList::query()->whereIn('id', $deletedIds)->delete();

        return $httpResponse->responseMessage("Cập nhật thành công");
    }

    public function deleteAccountCustomField(string $name, HttpResponse $httpResponse): JsonResponse
    {
        $customField = CustomFieldStruct::query()
            ->where('field_name', $name)->where('module', CustomFieldStruct::MODULE_ACCOUNT)
            ->where(function (Builder $builder) {
                $builder->where('branch', Auth::user()->{'branch'})->orWhere('branch', null);
            })->first(array_keys(CustomFieldStruct::defineRetrievableField()));

        if (!$customField) {
            return $httpResponse->notFoundResponse();
        }

        $id = $customField->{'id'};

        $customField->delete();

        CustomFieldList::query()->where('custom_field_id', $id)->delete();

        return $httpResponse->responseMessage("Xoá thành công");
    }
}
