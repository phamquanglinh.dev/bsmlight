<?php

namespace App\Http\Controllers\Salary;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Http\Controllers\Controller;
use App\Models\ClassroomCover;
use App\Models\SalaryCriteria;
use App\Models\SalarySheet;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SalaryCriteriaController extends Controller
{
    public function getSheetUserByClassroom(int $classroomId): JsonResponse
    {
        $results = [];

        if ($classroomId == 0) {
            foreach (SalarySheet::sheetUserOptions() as $key => $label) {
                $results[] = [
                    'id' => $key,
                    'text' => $label
                ];
            }
        }

        $teachers = Teacher::query()->whereHas('ClassroomShift', function (Builder $shift) use ($classroomId) {
            $shift->whereHas("Classroom", function (Builder $classroom) use ($classroomId) {
                $classroom->where('id', $classroomId);
            });
        })->get()->mapWithKeys(function (Teacher $teacher) {
            return [$teacher['id'] => $teacher['uuid'] . "-" . $teacher['name']];
        })->toArray();

        $supporters = Supporter::query()->whereHas('ClassroomShift', function (Builder $shift) use ($classroomId) {
            $shift->whereHas("Classroom", function (Builder $classroom) use ($classroomId) {
                $classroom->where('id', $classroomId);
            });
        })->get()->mapWithKeys(function (Supporter $supporter) {
            return [$supporter['id'] => $supporter['uuid'] . "-" . $supporter['name']];
        })->toArray();

        $coverClassroom = ClassroomCover::query()->where('classroom_id', $classroomId)->get()->mapWithKeys(function ($classroomCover) {
            $user = User::query()->where('id', $classroomCover['user_id'])->first(['uuid', 'name']);
            if (! $user) {
                return [];
            }

            return [$classroomCover['user_id'] => $user['uuid'] . "-" . $user['name'] . " [Cover:" . $classroomCover['due'] . "]"];
        })->toArray();

        foreach ($teachers as $key => $label) {
            $results[] = [
                'id' => $key,
                'text' => $label
            ];
        }

        foreach ($supporters as $key => $label) {
            $results[] = [
                'id' => $key,
                'text' => $label
            ];
        }

        foreach ($coverClassroom as $key => $label) {
            $results[] = [
                'id' => $key,
                'text' => $label
            ];
        }

        return response()->json([
            'results' => $results,
        ]);
    }

    public function list(Request $request): View
    {
        $crudBag = new CrudBag();
        $crudBag->setLabel('Cài đặt thưởng phạt');
        $crudBag->setEntity('salary-criteria');
        $perPage = $request->get('perPage') ?? 10;
        $query = SalaryCriteria::query();

        $this->handleQuery($crudBag, $query);

        $this->handleColumn($crudBag);

        $this->handleFilter($crudBag);

        $salaryCriterias = $query->paginate($perPage);

        return view('list', [
            'listViewModel' => new ListViewModel($salaryCriterias),
            'crudBag' => $crudBag,
        ]);
    }

    public function create()
    {
        $crudBag = new CrudBag();
        $crudBag->setLabel('Cài đặt thưởng phạt');
        $crudBag->setEntity('salary-criteria');
        $crudBag->setAction('salary-criteria.store');
        $this->handleFields($crudBag);

        return \view('create', [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'criteria_name' => 'required|string',
            'criteria_type' => 'required|integer|in:' . implode(',', [
                    SalaryCriteria::TYPE_PUNISH,
                    SalaryCriteria::TYPE_REWARD
                ]),
            'unit_type' => 'required|numeric|in:' . implode(',', [
                    SalaryCriteria::UNIT_PERCENT,
                    SalaryCriteria::UNIT_INTEGER
                ]),
            'criteria_rate' => 'required|numeric',
            'criteria_description' => 'nullable|string',
        ]);

        $dataToCreate = [
            'criteria_name' => $request->get('criteria_name'),
            'criteria_type' => $request->get('criteria_type'),
            'unit_type' => $request->get('unit_type'),
            'criteria_rate' => $request->get('criteria_rate'),
            'criteria_code' => SalaryCriteria::CRITERIA_MANUAL,
            'is_bsm_criteria' => 0,
            'criteria_description' => $request->get('criteria_description'),
            'branch' => Auth::user()->{'branch'}
        ];

        SalaryCriteria::query()->create($dataToCreate);

        return redirect()->back()->with('success', 'Thành công');
    }

    public function edit(int $id) {}

    public function update(Request $request, int $id) {}

    public function delete(int $id)
    {
        /**
         * @var SalaryCriteria $salaryCriteria
         */
        $salaryCriteria = SalaryCriteria::query()->where('id', $id)->firstOrFail();

        if ($salaryCriteria->is_bsm_criteria) {
            return redirect()->back()->with('error', 'Không thể xoá chỉ tiêu mặc định');
        }

        $salaryCriteria->delete();

        return redirect()->back()->with('success', 'Xoá thành công');
    }

    public function show(int $id)
    {
    }

    private function handleQuery(CrudBag $crudBag, &$query)
    {
    }

    private function handleColumn(CrudBag &$crudBag)
    {
        $crudBag->addColumn([
            'name' => 'criteria_name',
            'label' => 'Tên chỉ tiêu',
        ]);

        $crudBag->addColumn([
            'name' => 'criteria_type',
            'label' => 'Loại chỉ tiêu',
            'type' => 'select',
            'attributes' => [
                'options' => SalaryCriteria::getCriteriaTypeOptions(),
                'bg' => [
                    SalaryCriteria::TYPE_REWARD => 'bg-success',
                    SalaryCriteria::TYPE_PUNISH => 'bg-danger'
                ]
            ],

        ]);

        $crudBag->addColumn([
            'name' => 'unit_type',
            'label' => 'Đơn vị tính',
            'type' => 'select',
            'attributes' => [
                'options' => SalaryCriteria::getUnitOptions(),
                'bg' => [
                    SalaryCriteria::UNIT_INTEGER => '',
                    SalaryCriteria::UNIT_PERCENT => '',
                ]
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'criteria_rate',
            'label' => 'Giá trị mặc đinh',
            'type' => 'text'
        ]);
    }

    private function handleFilter(CrudBag &$crudBag) {}

    public function handleFields(CrudBag &$crudBag): void
    {
        $crudBag->addFields([
            'name' => 'criteria_name',
            'label' => 'Tên chỉ tiêu',
        ]);

        $crudBag->addFields([
            'name' => 'criteria_type',
            'label' => 'Loại chỉ tiêu',
            'type' => 'select',
            'options' => SalaryCriteria::getCriteriaTypeOptions()
        ]);

        $crudBag->addFields([
            'name' => 'unit_type',
            'label' => 'Đơn vị tính',
            'type' => 'select',
            'options' => SalaryCriteria::getUnitOptions()
        ]);

        $crudBag->addFields([
            'name' => 'criteria_rate',
            'label' => 'Giá trị mặc đinh',
            'type' => 'text'
        ]);

        $crudBag->addFields([
            'name' => 'criteria_description',
            'label' => 'Mô tả chỉ tiêu',
            'type' => 'textarea'
        ]);
    }

    public function one(Request $request): View
    {
        $id = $request->get('id');

        if (! $id) {
            throw new NotFoundHttpException();
        }

        $salaryCriteria = SalaryCriteria::query()->findOrFail($id);

        $sheetDetail = [
            'id' => $salaryCriteria['id'],
            'criteria_name' => $salaryCriteria['criteria_name'],
            'criteria_code' => $salaryCriteria['criteria_code'],
            'criteria_type' => $salaryCriteria['criteria_type'],
            'criteria_rate' => $salaryCriteria['criteria_rate'],
            'unit_type' => $salaryCriteria['unit_type'],
            'criteria_description' => $salaryCriteria['criteria_description'],
        ];

        return view('salary.sheet_detail_field', [
            'sheetDetail' => $sheetDetail,
            'sheetDetailKey' => Carbon::now()->timestamp
        ]);
    }
}
