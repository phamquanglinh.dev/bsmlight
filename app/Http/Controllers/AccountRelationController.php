<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\AccountRelation;
use Database\Factories\AccountRelationFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class AccountRelationController extends Controller
{
    public function getAllAccountRelationAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $builder = AccountRelation::query();

        AccountRelation::handleQuery($request, $builder);

        return $httpResponse->responseCollection($builder->get()->toArray());
    }

    public function getOneAccountRelationAction(Request $request, HttpResponse $httpResponse, int $accountRelationId)
    {
        $builder = AccountRelation::query();

        AccountRelation::handleQuery($request, $builder);

        $accountRelation = $builder->where('id', $accountRelationId)->first();

        if (!$accountRelation) {
            return $httpResponse->notFoundResponse();
        }

        return $httpResponse->responseData($accountRelation->toArray());
    }

    public function createAccountRelationAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'relation_name' => 'string|required',
            'color' => 'string|nullable',
        ]);

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->errors());
        }

        $account = AccountRelationFactory::makeAccountRelation($input);

        $account->save();

        return $httpResponse->responseCreated($account->{'id'});
    }

    public function updateAccountRelationAction(Request $request, HttpResponse $httpResponse, int $accountRelationId): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'relation_name' => 'string|filled',
            'color' => 'string|nullable',
        ]);

        if ($notification->fails()) {
            return $httpResponse->validateResponse($notification->errors());
        }

        /**
         * @var AccountRelation $accountRelation
         */
        $accountRelation = AccountRelation::query()->where('id', $accountRelationId)->first();

        if (!$accountRelation) {
            return $httpResponse->notFoundResponse();
        }

        $account = AccountRelationFactory::modifyAccountRelation($accountRelation, $input);

        $account->save();

        return $httpResponse->responseMessage('Cập nhật thành công');
    }

    public function deleteAccountRelationAction(HttpResponse $httpResponse, int $accountRelationId)
    {
        $accountRelation = AccountRelation::query()->where('id', $accountRelationId)->first();

        if (!$accountRelation) {
            return $httpResponse->notFoundResponse();
        }

        if (! $accountRelation->{'branch'}) {
            throw new BadRequestException('Không thể xoá mối quan hệ mặc định');
        }

        $accountRelation->delete();

        return $httpResponse->responseMessage('Xoá thành công');
    }
}
