<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Models\Affiliate;
use App\Models\Card;
use App\Models\Salary;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class TransactionController extends Controller
{
    public function editCardTransaction(int $transactionId, int $cardId): View
    {
        $transaction = Transaction::query()->where('id', $transactionId)->firstOrFail();

        if (Auth::user()->{'id'} != $transaction->{'created_by'} && Auth::user()->{'role'} != User::HOST_ROLE) {
            abort(403);
        }

        $card = Card::query()->where('id', $cardId)->firstOrFail();

        if (Auth::user()->{'role'} != User::HOST_ROLE) {
            if ($transaction->{"status"} !== 0) {
                abort(403);
            }
        }

        $crudBag = new CrudBag();

        $crudBag->setAction('transaction.update.card');

        $crudBag->setLabel('Giao dịch thẻ học');

        $crudBag->setId($transactionId);

        $crudBag->setEntity('transaction');

        $crudBag->setHasFile(true);

        $crudBag->addFields([
            'name' => 'card_id',
            'label' => 'Thẻ học',
            'type' => 'select',
            'options' => [
                $card['id'] => $card['uuid']
            ],
            'required' => true
        ]);

        $crudBag->addFields([
            'name' => 'amount',
            'type' => 'number',
            'label' => 'Số tiền',
            'attributes' => [
                'suffix' => 'đ'
            ],
            'required' => true,
            'value' => $transaction->{'amount'} ?? "",
        ]);

        $crudBag->addFields([
            'name' => 'transaction_day',
            'type' => 'datetime',
            'label' => 'Thời điểm giao dịch',
        ]);

        $crudBag->addFields([
            'name' => 'payment_method',
            'type' => 'select',
            'label' => 'Phương thức thanh toán',
            'options' => [
                'cash' => 'Tiền mặt',
                'atm' => 'Chuyển khoản/ Quẹt thẻ',
                'credit' => 'Tín dụng trả góp'
            ],
            'value' => $transaction->{'payment_method'}
        ]);

        $crudBag->addFields([
            'name' => 'notes',
            'type' => 'textarea',
            'label' => 'Ghi chú',
            'class' => 'col-10 mb-3',
            'value' => $transaction->{'notes'} ?? "",
        ]);

        $crudBag->addFields([
            'name' => 'affiliate_users',
            'type' => 'select-multiple',
            'label' => 'Nhân sự hưởng doanh số',
            'options' => User::query()->where('branch', Auth::user()->{'branch'})->get(['id', 'name', 'uuid'])->mapWithKeys(
                fn($user) => [
                    $user->id => $user->name . ' - ' . $user->uuid
                ]
            )->toArray(),
        ]);

        $crudBag->addFields([
            'name' => 'transaction_type',
            'type' => 'select',
            'label' => 'Loại giao dịch',
            'options' => [
                Transaction::RENEW_TYPE => 'Gia hạn - Renew',
                Transaction::NEW_TYPE => 'Mới - New',
            ],
            'value' => $transaction->{'transaction_type'} ?? ""
        ]);

        $crudBag->addFields([
            'name' => 'object_image',
            'type' => 'upload',
            'label' => 'Bằng chứng giao dịch',
            'class' => 'col-10',
        ]);


        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    public function editSalaryTransaction(int $id, Request $request): View
    {
        $transaction = Transaction::query()->where('id', $id)->firstOrFail();

        $crudBag = new CrudBag();
        $crudBag->setLabel('Giao dịch');

        $crudBag->setAction('transaction.update.salary');
        $crudBag->setEntity('transaction');
        $crudBag->setId($id);

        $crudBag->setHasFile(true);

        $crudBag->addFields([
            'type' => 'text',
            'label' => 'Mã giao dịch',
            'name' => '_',
            'value' => $transaction->{'uuid'},
            'attributes' => [
                'readonly' => true,
            ]
        ]);

        $crudBag->addFields([
            'type' => 'number',
            'label' => 'Số tiền chi',
            'name' => 'amount',
            'value' => $transaction->{'amount'} ?? 0,
        ]);

        $crudBag->addFields([
            'name' => 'notes',
            'type' => 'textarea',
            'label' => 'Ghi chú',
            'class' => 'col-md-10',
            'value' => $transaction->{'notes'} ?? "",
        ]);
        $crudBag->addFields([
            'type' => 'upload',
            'name' => 'object_image',
            'label' => 'Ảnh đối soát',
            'class' => 'col-md-10',
            'value' => $transaction->{'object_image'} ?? "",
        ]);

        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    public function updateSalaryTransaction(int $id, Request $request): RedirectResponse
    {
        $dataToUpdate = [
            'amount' => n($request->get('amount')),
            'notes' => $request->get('notes') ?? '',
        ];

        $transaction = Transaction::query()->where('id', $id)->update($dataToUpdate);

        return redirect()->to('/salary-group/list');
    }

    /**
     * @throws ValidationException
     */
    public function updateCardTransaction(Request $request, int $transactionId): Redirector|Application|RedirectResponse
    {
        $input = $request->input();

        $this->validate($request, [
            'card_id' => 'required|exists:cards,id',
            'amount' => 'required',
            'transaction_type' => 'required',
            'object_image' => 'file|nullable',
            'affiliate_users' => 'array|nullable',
            'affiliate_users.*' => 'integer|exists:users,id',
            'payment_method' => 'string|nullable'
        ]);

        $input['amount'] = n($input['amount']);

        /**
         * @var Transaction $transaction
         */

        $transaction = Transaction::query()->where('id', $transactionId)->firstOrFail();

        if (Auth::user()->{'id'} != $transaction->{'created_by'} && Auth::user()->{'role'} != User::HOST_ROLE) {
            abort(403);
        }

        $transaction->update([
            'amount' => $input['amount'],
            'transaction_type' => $input['transaction_type'],
            'object_image' => $request->file('object_image') ? uploads($request->file('object_image')) : $transaction->{'object_image'},
            'notes' => $input['notes'],
            'transaction_day' => $request->get('transaction_day') ?? null,
            'payment_method' => $request->get('payment_method')
        ]);

        return redirect('/card/show/' . $request->get('card_id'));
    }

    public function createCardTransaction(Request $request): View
    {
        $cardId = $request->get('card_id');

        $card = Card::query()->where('id', $cardId)->firstOrFail(['id', 'uuid']);

        $crudBag = new CrudBag();

        $crudBag->setAction('transaction.store.card');
        $crudBag->setLabel('Giao dịch thẻ học');
        $crudBag->setHasFile(true);
        $crudBag->setParam(CrudBag::CARD_TRANSACTION_PRINT, true);
        $crudBag->addFields([
            'name' => 'card_id',
            'label' => 'Thẻ học',
            'type' => 'select',
            'options' => [
                $card['id'] => $card['uuid']
            ],
            'required' => true
        ]);

        $crudBag->addFields([
            'name' => 'amount',
            'type' => 'number',
            'label' => 'Số tiền',
            'attributes' => [
                'suffix' => 'đ'
            ],
            'required' => true
        ]);

        $crudBag->addFields([
            'name' => 'transaction_day',
            'type' => 'datetime',
            'label' => 'Thời điểm giao dịch',
        ]);

        $crudBag->addFields([
            'name' => 'payment_method',
            'type' => 'select',
            'label' => 'Phương thức thanh toán',
            'options' => [
                'cash' => 'Tiền mặt',
                'atm' => 'Chuyển khoản/ Quẹt thẻ',
                'credit' => 'Tín dụng trả góp'
            ]
        ]);

        $crudBag->addFields([
            'name' => 'notes',
            'type' => 'textarea',
            'label' => 'Ghi chú',
            'class' => 'col-10 mb-3'
        ]);

        $crudBag->addFields([
            'name' => 'affiliate_users',
            'type' => 'select-multiple',
            'label' => 'Nhân sự hưởng doanh số',
            'options' => User::query()->where('branch', Auth::user()->{'branch'})->get(['id', 'name', 'uuid'])->mapWithKeys(
                fn($user) => [
                    $user->id => $user->name . ' - ' . $user->uuid
                ]
            )->toArray(),
        ]);

        $crudBag->addFields([
            'name' => 'transaction_type',
            'type' => 'select',
            'label' => 'Loại giao dịch',
            'options' => [
                Transaction::RENEW_TYPE => 'Gia hạn - Renew',
                Transaction::NEW_TYPE => 'Mới - New',
            ],
        ]);

        $crudBag->addFields([
            'name' => 'object_image',
            'type' => 'upload',
            'label' => 'Bằng chứng giao dịch',
            'class' => 'col-10',
            'attributes' => [
                'print_btn' => true,
                'formId' => $crudBag->getAction()
            ]
        ]);


        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function storeCardTransaction(Request $request): Redirector|Application|RedirectResponse
    {
        $this->validate($request, [
            'card_id' => 'required|exists:cards,id',
            'amount' => 'required',
            'transaction_type' => 'required',
            'object_image' => 'file|nullable',
            'affiliate_users' => 'array|nullable',
            'affiliate_users.*' => 'integer|exists:users,id',
            'payment_method' => 'string|nullable'
        ]);

        $dataToCreateTransaction = [
            'object_type' => Transaction::CARD_TRANSACTION_TYPE,
            'object_id' => $request->get('card_id'),
            'uuid' => Carbon::now()->timestamp,
            'amount' => str_replace(',', '', $request->get('amount')),
            'transaction_type' => $request->get('transaction_type'),
            'notes' => $request->get('notes') ?? 'Không có ghi chú',
            'status' => Transaction::PENDING_STATUS,
            'object_image' => $request->file('object_image') ? uploads($request->file('object_image')) : null,
            'created_by' => Auth::id(),
            'transaction_day' => $request->get('transaction_day') ?? Carbon::now(),
            'approve' => 0,
            'payment_method' => $request->get('payment_method')
        ];

        $affiliate_users = $request->get('affiliate_users');

        DB::transaction(function () use ($dataToCreateTransaction, $affiliate_users) {
            /**
             * @var Transaction $transaction
             */
            $transaction = Transaction::query()->create($dataToCreateTransaction);

            foreach ($affiliate_users ?? [] as $userId) {
                Affiliate::query()->updateOrCreate([
                    'transaction_id' => $transaction->id,
                    'user_id' => $userId,
                ], [
                    'transaction_id' => $transaction->id,
                    'user_id' => $userId,
                    'transaction_amount' => $transaction->amount
                ]);
            }
        });


        return redirect('/card/show/' . $request->get('card_id'));
    }

    public function accept(int $id): RedirectResponse
    {
        /**
         * @var Transaction $transaction
         */

        if (!force_permission('accept transaction')) {
            abort(403);
        }

        $transaction = Transaction::query()->findOrFail($id);

        DB::transaction(function () use ($transaction) {
            $transaction->update([
                'status' => Transaction::ACTIVE_STATUS,
                'approve' => Auth::user()->{'id'}
            ]);
        });


        return redirect()->back()->with('success', 'Giao dịch đã được phê duyệt');
    }

    public function deny(int $id): RedirectResponse
    {

        $transaction = Transaction::query()->findOrFail($id);

        $transaction->update([
            'status' => Transaction::CANCEL_STATUS
        ]);

        return redirect()->back()->with('success', 'Giao dịch đã bị từ chối');
    }

    private function handleEffect(Transaction $transaction)
    {

    }

    public function test()
    {
        $transaction = Transaction::query()->get();

        return $transaction;
    }

    public function deleteCardTransaction(int $id): RedirectResponse
    {
        $transaction = Transaction::query()->where('id', $id)->firstOrFail();
        if (Auth::user()->{'id'} != $transaction->{'created_by'} && Auth::user()->{'role'} != User::HOST_ROLE) {
            abort(403);
        }
        $transaction->delete();

        return redirect()->back()->with('success', 'Thành công');
    }

}
