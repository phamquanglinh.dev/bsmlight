<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use App\Models\Account;
use Database\Factories\AccountFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class WebhookController extends Controller
{
    public function webhookCreateAccount(Request $request)
    {
        $xApiToken = $request->get('token');

        if (!$xApiToken) {
            throw new BadRequestException('Token không hợp lệ');
        }

        $branch = DB::table('branches')->where('x-api-token', $xApiToken)->first();

        if (!$branch) {
            throw new BadRequestException('Token không hợp lệ');
        }

        $branchUuid = $branch->{'uuid'};
    }

    public function webhookCreateAccountFromGoogleSheet(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $apiKey = $input['X-API-KEY'];

        unset($input['X-API-KEY']);

        $branch = DB::table('branches')->where('api-x-token', $apiKey)->first();

        if (!$branch) {
            return $httpResponse->notFoundResponse();
        }

        $branchUuid = $branch->{'uuid'};

        Validator::validate($input, Account::validationRulesForCreate());

        $account = AccountFactory::makeAccount($input);

        $account->setAttribute('account_manager', null);

        $account->setAttribute('branch', $branchUuid);

        $account->setAttribute('uuid', Account::makeUUID($branchUuid));

        $account->save();

        return $httpResponse->responseCreated($account->{'id'});
    }
}
