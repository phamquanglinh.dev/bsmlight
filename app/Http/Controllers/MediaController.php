<?php

namespace App\Http\Controllers;

use App\Helper\HttpResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Utils;
use Kurt\Imgur\Imgur;

class MediaController extends Controller
{
    public function uploadImage(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $image = $request->file('file');
        $filePath = $image->getPathname();
        $fileName = $image->getClientOriginalName();

        $client = new Client();

        $clientId = 'bfae53af93c8c2d';

        $headers = [
            'Authorization' => "Client-ID $clientId",
        ];
        $options = [
            'multipart' => [
                [
                    'name' => 'image',
                    'contents' => Utils::tryFopen($filePath, 'r'),
                    'filename' => $fileName,
                    'headers' => [
                        'Content-Type' => mime_content_type($filePath)
                    ]
                ],
                [
                    'name' => 'type',
                    'contents' => 'file'
                ],
                [
                    'name' => 'title',
                    'contents' => 'Simple upload'
                ],
                [
                    'name' => 'description',
                    'contents' => 'This is a simple image upload in Imgur'
                ]
            ]];

        $request = new \GuzzleHttp\Psr7\Request('POST', 'https://api.imgur.com/3/image', $headers);

        $res = $client->sendAsync($request, $options)->wait();
        $data = silence(fn() => json_decode($res->getBody(), 1)['data']);

        if ($data) {
            $id = $data['id'];
            $ext = $image->extension();
            return $httpResponse->responseData([
                'url' => "https://i.imgur.com/$id.$ext"
            ]);
        }

        return $httpResponse->badResponse('lỗi tải ảnh lên');
    }
}
