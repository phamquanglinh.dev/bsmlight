<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\Classroom;
use App\Models\Staff;
use App\Models\Student;
use App\Models\Supporter;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchAllController extends Controller
{
    public function searchAll(Request $request)
    {

        $searchParam = $request->post('q') ?? '';

        $searchResultCollection = [];

        foreach ($this->getSelectFields() as $model => $selectFields) {
            $searchFields = $this->getSearchFields()[$model];

            $columns = array_keys($selectFields);

            $columns = array_merge($columns, ['id']);
            /**
             * @var Model $model
             */

            $query = $model::query()->select($columns);

            if ($model == Student::class) {
                // Với mô hình Student, ta sẽ nối thêm điều kiện tìm kiếm vào EnglishAccount
                $query->where(function (Builder $builder) use ($searchFields, $searchParam) {
                    foreach ($searchFields as $key => $searchField) {
                        if ($key == 0) {
                            $builder->where($searchField, 'like', "%$searchParam%");
                        } else {
                            $builder->orWhere($searchField, 'like', "%$searchParam%");
                        }
                    }
                    // Thêm điều kiện tìm kiếm vào EnglishAccount
                    $builder->orWhereHas('profile', function (Builder $query) use ($searchParam) {
                        $query->where('english_name', 'like', "%$searchParam%");
                    });
                });
            } else {
                $query->where(function (Builder $builder) use ($searchFields, $searchParam) {
                    foreach ($searchFields as $key => $searchField) {
                        if ($key == 0) {
                            $builder->where($searchField, 'like', "%$searchParam%");
                        } else {
                            $builder->orWhere($searchField, 'like', "%$searchParam%");
                        }
                    }
                });
            }

//            dd(rawSql($query->toSql(), $query->getBindings()));

            $records = $query->limit(3)->get();

            $searchResultItem = [];

            $searchResultItem['module'] = $this->getModuleLabel()[$model];

            foreach ($records as $record) {
                /**
                 * @var Model $record
                 */

                $attributes = $record->getAttributes();

                $id = array_pull($attributes, 'id');

                $searchResultItem['url'] = $this->getModuleEntry()[$model] . '/show/' . $id;

                foreach ($attributes as $key => $attribute) {
                    $searchResultItem['attributes'][$key]['label'] = $selectFields[$key];
                    $searchResultItem['attributes'][$key]['value'] = $attribute;
                }

                $searchResultCollection[] = $searchResultItem;
            }
        }

        return view('search-all', [
            'searchResultCollection' => $searchResultCollection
        ]);
    }

    private function getModuleLabel(): array
    {
        return [
            Classroom::class => 'Lớp học',
            Student::class => 'Học sinh',
            Teacher::class => 'Giáo viên',
            Supporter::class => 'Trợ giảng',
            Staff::class => 'Nhân viên',
            Card::class => 'Thẻ học'
        ];
    }

    private function getModuleEntry(): array
    {
        return [
            Classroom::class => 'classroom',
            Student::class => 'student',
            Teacher::class => 'teacher',
            Supporter::class => 'supporter',
            Staff::class => 'staff',
            Card::class => 'card'
        ];
    }

    private function getSearchFields(): array
    {

        return [
            Classroom::class => [
                'name',
                'uuid',
            ],
            Student::class => [
                'name',
                'uuid',
                'email',
                'phone',
            ],
            Teacher::class => [
                'name',
                'uuid',
            ],
            Supporter::class => [
                'name',
                'uuid',
                'phone'
            ],
            Staff::class => [
                'name',
                'uuid',
                'phone'
            ],
            Card::class => [
                'uuid',
            ]
        ];
    }

    private function getSelectFields(): array
    {

        return [
            Student::class => [
                'name' => 'Tên',
                'uuid' => 'Mã học sinh',
                'email' => 'Email học sinh',
                'phone' => 'SĐT học sinh',
                'avatar' => 'Avatar',

            ],
            Classroom::class => [
                'name' => 'Tên lớp học',
                'uuid' => 'Mã lớp',
                'avatar' => 'Avatar',
            ],

            Teacher::class => [
                'name' => 'Tên giáo viên',
                'uuid' => 'Mã giáo viên',
                'phone' => 'SĐT giáo viên',
                'avatar' => 'Avatar'
            ],
            Supporter::class => [
                'name' => 'Tên trợ giảng',
                'uuid' => 'Mã trợ giảng',
                'phone' => 'SĐT trợ giảng',
                'avatar' => 'Avatar'
            ],
            Staff::class => [
                'name' => 'Tên nhân viên',
                'uuid' => 'Mã nhân viên',
                'phone' => 'SĐT nhân viên',
                'avatar' => 'Avatar'
            ],
            Card::class => [
                'uuid' => 'Mã thẻ học',
            ]
        ];
    }
}
