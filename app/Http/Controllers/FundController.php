<?php

namespace App\Http\Controllers;

use App\Helper\CrudBag;
use App\Helper\ListViewModel;
use App\Models\Fund;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class FundController extends Controller
{
    public function list(Request $request): View
    {
        Cache::put('redirect_fund_' . Auth::id(), $request->getQueryString());

        $crudBag = new CrudBag();

        $crudBag->setLabel('Quỹ gắn bó');
        $crudBag->setParam('disable_all_top_btn', true);
        $crudBag->setParam(CrudBag::BEFORE_CONTENT, \view());
        $crudBag->setParam(CrudBag::DISABLE_ACTION, true);
        $this->handleColumns($crudBag, $request);
        $crudBag->setSearchValue($request->get('search'));
        $query = Fund::query();

        $this->handleQuery($query, $crudBag);

        return view('list', [
            'crudBag' => $crudBag,
            'listViewModel' => new ListViewModel($query->paginate($request->get('perPage') ?? 10))
        ]);
    }

    public function edit(int $id)
    {
        $fund = Fund::query()->where('id', $id)->firstOrFail();

        $crudBag = new CrudBag();

        $crudBag->setId($id);

        $crudBag->setLabel('Quỹ gắn bó');

        $crudBag->setAction('fund.update');

        $this->handleFields($crudBag, $fund);

        return \view('create', [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $fund = Fund::query()->where('id', $id)->firstOrFail();

        $this->validate($request, [
            'fund_value' => 'required'
        ]);

        $fund->update(['fund_value' => n($request->get('fund_value'))]);

        $history = Cache::get('redirect_fund_' . Auth::id());

        return redirect('/fund/list?' . $history);
    }

    private function handleColumns(CrudBag &$crudBag, Request $request): void
    {
        $crudBag->addColumn([
            'name' => 'uuid',
            'type' => 'text',
            'label' => 'Mã nhân sự',
            'fixed' => 'first',
        ]);

        $crudBag->addColumn([
            'name' => 'name',
            'type' => 'profile',
            'label' => 'Họ tên',
            'attributes' => [
                'avatar' => 'avatar',
                'address' => 'address',
                'identity' => 'id'
            ]
        ]);

        $crudBag->addColumn([
            'name' => 'fund_value',
            'type' => 'number',
            'label' => 'Số dư quỹ',
        ]);

        $crudBag->addColumn([
            'name' => 'paid_fund_value',
            'type' => 'number',
            'label' => 'Đã chi',
        ]);

        $crudBag->addColumn([
            'name' => 'fund_action',
            'type' => 'fund_action',
            'label' => 'Hạnh động',
        ]);
    }

    private function handleQuery(Builder &$builder, CrudBag $crudBag): void
    {
        $searchValue = $crudBag->getSearchValue();

        if ($searchValue != '') {
            $builder->where('name', 'like', "%{$searchValue}%");
            $builder->orWhere('uuid', 'like', "%{$searchValue}%");
        }

        $builder->orderBy('created_at', 'DESC');
    }

    public function handleFields(CrudBag &$crudBag, $fund = null): void
    {
        $crudBag->addFields([
            'name' => 'fund_value',
            'label' => 'Số dư quỹ',
            'type' => 'number',
            'value' => $fund ? $fund['fund_value'] : 0
        ]);
    }

    public function paidFundView(int $fundId): View
    {
        $fund = Fund::query()->where('id', $fundId)->firstOrFail();

        $crudBag = new CrudBag();

        $crudBag->setParam(CrudBag::BEFORE_TEXT, 'Số dư quỹ: ' . number_format($fund->{'fund_value'}) . " đ");

        $crudBag->setAction('fund.paid');
        $crudBag->setLabel('Chi quỹ gắn bó');
        $crudBag->setParam(CrudBag::CUSTOM_LABEL_ACTION, '');
        $crudBag->setId($fundId);

        $crudBag->addFields([
            'name' => 'amount',
            'type' => 'number',
            'label' => 'Số tiền'
        ]);

        $crudBag->addFields([
            'name' => 'object_image',
            'type' => 'upload',
            'label' => 'Bằng chứng chi tiền'
        ]);

        $crudBag->addFields([
            'name' => 'transaction_day',
            'label' => 'Ngày thanh toán',
            'type' => 'datetime'
        ]);

        $crudBag->addFields([
            'name' => 'notes',
            'label' => 'Ghi chú',
            'type' => 'textarea',
            'class' => 'col-10'
        ]);


        return view('create', [
            'crudBag' => $crudBag
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function paidFund(Request $request, int $id): RedirectResponse
    {
        $this->validate($request, [
            'amount' => 'required'
        ]);

        $fund = Fund::query()->where('id', $id)->firstOrFail();

        $transactionData = [
            'uuid' => Carbon::now()->timestamp,
            'object_type' => Fund::OBJECT_TYPE,
            'transaction_type' => 'renew',
            'object_id' => $id,
            'amount' => n($request->get('amount')),
            'object_image' => $request->hasFile('object_image') ? uploads($request->file('object_image')) : null,
            'created_by' => Auth::id(),
            'transaction_day' => $request->get('transaction_day') ?? Carbon::now(),
            'notes' => '',
            'status' => 1,
            'approve' => 0
        ];

        Transaction::query()->create($transactionData);

        $history = Cache::get('redirect_fund_' . Auth::id());

        return redirect('/fund/list?' . $history);
    }
}
