<?php

namespace App\Http\Controllers;

use App\Helper\GoogleClientHelper;
use App\Models\UserConfig;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class GoogleChatController extends Controller
{
    private GoogleClientHelper $googleClientHelper;

    public function __construct(
        GoogleClientHelper $googleClientHelper
    )
    {
        $this->googleClientHelper = $googleClientHelper;
    }

    public function connect(): RedirectResponse
    {
        $scopes = [
            'https://www.googleapis.com/auth/chat.spaces'
        ];

        return redirect($this->googleClientHelper->makeLoginUrl($scopes));
    }

    public function callback(Request $request)
    {
        $code = $request->get('code');

        if (! $code) {
            return redirect()->with('error', 'Kết nối thất bại');
        }

        $tokens = $this->googleClientHelper->authWithCode($code);

        if (! $tokens) {
            return redirect()->to('')->with('error', 'Kết nối thất bại');
        }

        $dataSetting = [
            'access_token' => $tokens['access_token'],
            'refresh_token' => $tokens['refresh_token'],
            'expired' => Carbon::now()->addSeconds($tokens['expires_in']),
            'user_id' => Auth::id()
        ];

        UserConfig::query()->updateOrInsert([
            'user_id' => Auth::id()
        ], [
            'user_id' => Auth::id(),
            'type' => UserConfig::GOOGLE_CHAT_TYPE,
            'settings' => json_encode($dataSetting)
        ]);

        return redirect()->to('')->with('success', 'Kết nối thành công');
    }
}
