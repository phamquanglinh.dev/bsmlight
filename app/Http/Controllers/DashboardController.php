<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Telegram\Bot\Laravel\Facades\Telegram;

class DashboardController extends Controller
{
    public function index()
    {
        return view("welcome", [
            'messages' => $this->getChannelMessages()
        ]);
    }

    public function getChannelMessages()
    {
        $channelUsername = '@bsmnewss'; // Replace with your Telegram channel username

        try {
            // Get updates from the channel
            $response = Telegram::getUpdates([
                'chat_id' => $channelUsername,
            ]);

            // Check if response is valid
            if (isset($response['ok']) && $response['ok']) {
                $updates = $response['result'];
                dd($updates); // Output updates for debugging
            } else {
                dd("Failed to fetch updates: " . $response['description']);
            }
        } catch (\Exception $e) {
            dd("Exception occurred: " . $e->getMessage());
        }
    }
}
