<?php

namespace App\Listeners;

use App\Events\ClassroomCovered;
use App\Events\ClassroomWasCreated;
use App\Events\ClassroomWasUpdated;
use App\Models\Classroom;
use App\Models\ClassroomShift;
use App\Models\SalarySheet;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\Table;

class CreateSalarySheet
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ClassroomWasCreated|ClassroomWasUpdated $event
     * @return void
     */
    public function handle(ClassroomWasCreated|ClassroomWasUpdated $event): void
    {
        $classroom = $event->getClassroom();
        /**
         * @var ClassroomShift[] $shifts
         */

        $shifts = $classroom->Shifts()->get();

        foreach ($shifts as $shift) {

            $dataToCreateSalaryTeacher =
                [
                    'classroom_id' => $classroom->id,
                    'sheet_name' => 'Cấu hình #' . now()->timestamp,
                    'loop_type' => 1,
                    'period_year' => now()->year,
                    'description' => 'Phiếu lương cho nhân sự : ' . $shift->teacher?->name . ' lớp' . $classroom->name,
                    'salary_rate' => 0,
                    'fund_percent' => 0,
                    'branch' => Auth::user()->{'branch'},
                    'user_id' => $shift->teacher?->id,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'attributes' => json_encode([])
                ];

            $dataToCreateSalarySupporter = [
                'classroom_id' => $classroom->id,
                'sheet_name' => 'Cấu hình #' . now()->timestamp,
                'loop_type' => 1,
                'period_year' => now()->year,
                'description' => 'Phiếu lương cho nhân sự : ' . $shift->supporter?->name . ' lớp' . $classroom->name,
                'salary_rate' => 0,
                'fund_percent' => 0,
                'branch' => Auth::user()->{'branch'},
                'user_id' => $shift->supporter?->id,
                'created_at' => now(),
                'updated_at' => now(),
                'attributes' => json_encode([])
            ];

            if (!$this->existSalaryShift($dataToCreateSalaryTeacher['user_id'], $classroom)) {
                SalarySheet::query()->create($dataToCreateSalaryTeacher);
            }

            if (! empty($shift['supporter_id'])) {
                if (! $this->existSalaryShift($dataToCreateSalarySupporter['user_id'], $classroom)) {
                    if ($shift->supporter?->id) {
                        SalarySheet::query()->create($dataToCreateSalarySupporter);
                    }
                }
            }
        }
    }

    private function existSalaryShift(int $userId, Classroom $classroom): bool
    {
        return SalarySheet::query()->where('classroom_id', $classroom->id)
            ->where('user_id', $userId)->where('deleted_at', null)->exists();
    }
}
